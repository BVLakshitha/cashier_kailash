//
//  Constants.h
//  Cashier
//
//  Created by Lakshitha on 26/11/2014.
//  Copyright (c) 2014 User. All rights reserved.
//  Last update 2105/8/19    -  showed TableName for TableID.

#ifndef Cashier_Constants_h
#define Cashier_Constants_h

#define CLIENT                                        @"PALMBEACH"

//Printer Settings
#define PRINTERMODE_C                                 @"PMC"
#define PRINTERMODE_K                                 @"PMK"
#define CASHERPRINTER                                 @"CP"
#define KITCHENPRINTER                                @"KP"
#define PRINTERON                                     @"ON"
#define PRINTEROFF                                    @"OFF"

#define WEBSERVICEKEY                                 @"BAPOS@2014#"

//payTypes
#define CARD                                          @"card"
#define GIFTVOUCHER                                   @"gift voucher"

//sentType
#define ACCOUNT                                       @"Account"
#define ONTIME                                        @"payandover"

//Key
#define KEYFORPAYMENTDETAILS                          @"PaymentDetails"

//Imagepath
#define IMAGEPATH                                     @"http://server217-174-240-51.live-servers.net/bapos/palmbeach/images/"

//Oorder types
#define COLLECTIONORDER                               @"0"
#define DELIVERYORDER                                 @"1"
#define TABLEORDER                                    @"2"


//Oorder Status
#define SAVE                                          @"13"
#define SENTTOKITCHEN                                 @"4"
#define PREPARED                                      @"5"
#define COMPLETED                                     @"6"
#define CANCEL                                        @"62"

//custom
#define NEWITEM                                       @"1"

//currency wings
#define CURRENCYSYMBOL                                @"£"

//Till

#define STARTTILLSESSION                               @"1"
#define ENDTILLSESSION                                 @"2"

#define KITCHEN                                        @"kitchen"
#define BAR                                            @"bar"
#define BARANDKITCHEN                                  @"kitchen and bar"

//New Server

//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/newadmin/mobile/"  //  WebService URL Ba-Pos NewAdmin.

//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/baposdemo/mobile/"  //  WebService URL Ba-Pos Demo.
//kailash
//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/kailash_momo/mobile/"  //  WebService URL for kailash.
//
#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/demo_kailash/mobile/"  //  WebService URL for kailash demo.
//kailash demo
//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/demo_kailash/mobile/"  //  WebService URL for kailash Demo.

//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/wings/mobile/"  //  WebService URL for Wings.

//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/me_luv_sushi/mobile/"  //  WebService URL for Me Love Sushi.

//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/palmbeach/mobile/"//  WebService URL for Palm Beach.

//#define SERVICEURL                                      @"http://server217-174-240-51.live-servers.net/bapos/demo_palmbeach/mobile/"  //  WebService URL for Palm Beach Demo.




#endif
