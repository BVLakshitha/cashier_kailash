//
//  CategoryDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "CategoryDAO.h"
#import "FoodCategory.h"
@implementation CategoryDAO


-(FoodCategory*)addOrUpdateCategoryDetails: (NSArray *)foodCategory
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    FoodCategory *objFoodCategory=nil;
    objFoodCategory= (FoodCategory  *)[NSEntityDescription
                                     insertNewObjectForEntityForName:@"FoodCategory"inManagedObjectContext:managedObjectContext];
   
   // objFoodCategory.name=[foodCategory valueForKey:@"catagory_name"];
//    NSLog(@"print value name==%@",objFoodCategory.name);
   
    return objFoodCategory;
}


-(FoodCategory *) getFoodCategory: (NSString*) categoryId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"FoodCategory" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(categoryId = %@)",categoryId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
@end
