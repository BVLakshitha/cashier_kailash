//
//  CheckOutOrderDao.h
//  ResbookCustoms
//
//  Created by orderfood on 28/06/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckOutOrder.h"
#import "AppDelegate.h"
@interface CheckOutOrderDao : NSObject
-(void)addOrUpdateCheckOutOrderDetails: (NSDictionary*) dicCheckOutData;
-(CheckOutOrder *) getCheckOutOrderList: (NSString*) stetus;
-(void)removeCheckOutOrderObject :(NSString *)itemId;
-(NSArray *) getCheckOutOrderAllList;
@end
