//
//  CheckOutOrderDao.m
//  ResbookCustoms
//
//  Created by orderfood on 28/06/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "CheckOutOrderDao.h"

@implementation CheckOutOrderDao
-(void)addOrUpdateCheckOutOrderDetails: (NSDictionary*) dicCheckOutData
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSString * getItermId = [dicCheckOutData valueForKey:@"order_id"];
    CheckOutOrder *ObjCheckOutOrder= [self getCheckOutOrderList:getItermId];
    // Check whether our local db has the mumbler user already or not,
    if (ObjCheckOutOrder == nil) {
        
//        NSLog(@"Order Iterm not found %@", getItermId);
        
        ObjCheckOutOrder= (CheckOutOrder  *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"CheckOutOrder"inManagedObjectContext:managedObjectContext];
        
        //[self addNewOrderIterm:foodCategory :orderFoodItermObj:foodCategoryId];
        
    } else {
        
//        NSLog(@"Order Iterm found %@", getItermId);
      //  [self updateOrderIterm:orderFoodItermObj];
        
        
    }
}

//-(void)addNewSelectOrderIterm  :(CheckOutOrder *)objCheckOutOrder : (OrderFoodData *)objOrderFoodData :(NSString*)selectFoodCategoryId
//{
//    
//
//    
//}

-(void)updateSelectOrderIterm : (CheckOutOrder *)objCheckOutOrder

{

}
-(CheckOutOrder *) getCheckOutOrderList: (NSString*) itemId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"CheckOutOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(itemId = %@)",itemId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
-(void)removeCheckOutOrderObject :(NSString *)itemId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    CheckOutOrder *ObjCheckOut= [self getCheckOutOrderList:itemId];
    [context deleteObject:ObjCheckOut];
}

-(NSArray *) getCheckOutOrderAllList
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"CheckOutOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(stetus = %@)",@"1"];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];    
    return objects;
}
@end
