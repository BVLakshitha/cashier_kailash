//
//  Companies.h
//  
//
//  Created by Lakshitha on 7/20/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Companies : NSManagedObject

@property (nonatomic, retain) NSString * cId;
@property (nonatomic, retain) NSString * cName;
@property (nonatomic, retain) NSString * cAddress;
@property (nonatomic, retain) NSString * cContact;

@end
