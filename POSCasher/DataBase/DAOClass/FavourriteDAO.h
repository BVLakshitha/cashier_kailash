//
//  FavourriteDAO.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/26/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Favourrite.h"
#import "AppDelegate.h"
#import "OrderFoodData.h"
@interface FavourriteDAO : NSObject
-(void)addOrUpdateFavourriteDetails: (OrderFoodData*) objOrderFoodData;
-(Favourrite *) getFavourriteList: (NSString*) stetus;
-(void)removeFavourriteObject :(NSString *)itemId;
-(NSArray *) getFavourriteAllList;
@end
