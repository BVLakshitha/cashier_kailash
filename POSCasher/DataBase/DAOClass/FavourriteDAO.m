//
//  FavourriteDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/26/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "FavourriteDAO.h"

@implementation FavourriteDAO
-(void)addOrUpdateFavourriteDetails: (OrderFoodData*) objOrderFoodData
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];



    Favourrite *objFavourriteFoodIterm= nil;


// Check whether our local db has the mumbler user already or not,


    objFavourriteFoodIterm= (Favourrite  *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"Favourrite"inManagedObjectContext:managedObjectContext];
    [self addNewFavourriteIterm :objOrderFoodData :objFavourriteFoodIterm];


}

-(void)addNewFavourriteIterm  :(OrderFoodData *)objOrderFoodData : (Favourrite *)objFavourriteFoodIterm
{

    objFavourriteFoodIterm.itemName=objOrderFoodData.oderName;
    objFavourriteFoodIterm.itemId=objOrderFoodData.orderId;
    objFavourriteFoodIterm.itemDescription=objOrderFoodData.orderDesdription;
    objFavourriteFoodIterm.itemPrice=objOrderFoodData.orderPrice;
    objFavourriteFoodIterm.stetus=@"1";
    objFavourriteFoodIterm.favourriteCount=@"1";
}


-(Favourrite *) getFavourriteList: (NSString*) itemId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;


    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Favourrite" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(itemId = %@)",itemId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];

    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
-(void)removeFavourriteObject :(NSString *)itemId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    Favourrite *favourriteFoodItermObj= [self getFavourriteList:itemId];
    [context deleteObject:favourriteFoodItermObj];
}
-(NSArray *) getFavourriteAllList
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;


    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Favourrite" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(stetus = %@)",@"1"];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];

    return objects;
}
@end
