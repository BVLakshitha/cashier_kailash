//
//  FoodCategoryDAO.h
//  resbookWaiter
//
//  Created by orderfood on 01/09/2014.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "FoodCategory.h"
@class AppDelegate;


@interface FoodCategoryDAO : NSObject{
    
    AppDelegate *appDelegate;
    
    NSManagedObjectContext          *managedObjectContext;
    NSManagedObjectModel            *managedObjectModel;
    NSPersistentStoreCoordinator    *persistentStoreCoordinator;
}
-(BOOL)insertCategories :(FoodCategory *)category;
-(NSArray *)getAllFoodCategory;
-(void)deleteAllFoodCategory;

@end
