//
//  FoodCategoryDAO.m
//  resbookWaiter
//
//  Created by orderfood on 01/09/2014.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "FoodCategoryDAO.h"
#import "FoodCategory.h"

@implementation FoodCategoryDAO

- (id)init
{
    self = [super init];
    if (self) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        managedObjectContext = [self managedObjectContext];
        managedObjectModel = appDelegate.managedObjectModel;
        persistentStoreCoordinator = appDelegate.persistentStoreCoordinator;
        
    }
    return self;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(BOOL)insertCategories :(NSArray *)categories{
    for (NSDictionary * category in categories) {
        
        FoodCategory * foodcategory = nil;
        NSString * catid = [category objectForKey:@"catagory_id"];
        NSString * catName = [category objectForKey:@"catagory_name"];
        NSString * imageurl = [category objectForKey:@"image_url"];
        
        if (catid != nil) {
            foodcategory = [self isRecordExistanceOrder:catid];
            if (!foodcategory) {
                foodcategory = [NSEntityDescription insertNewObjectForEntityForName:@"FoodCategory" inManagedObjectContext:managedObjectContext];
                [foodcategory setCategoryId:catid];
            }
            
            if (![imageurl isEqualToString:@""]) {
                [foodcategory setImagePath:imageurl];
            }else{
                [foodcategory setImagePath:@"no"];
            };
            
            [foodcategory setName:catName];
            
            NSError * error;
            if (![managedObjectContext save:&error]) {
                return false;
                NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            }
        }
    }
    return true;
}
-(id)isRecordExistanceOrder:(NSString *)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FoodCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryId LIKE %@",Record];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(NSArray *)getAllFoodCategory
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FoodCategory"];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults ;
        }
    }
    return nil;
}
-(void)deleteAllFoodCategory{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FoodCategory"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [managedObjectContext deleteObject:items];
    }
    
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
}
@end
