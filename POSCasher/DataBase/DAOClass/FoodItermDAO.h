//
//  FoodItermDAO.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "FoodIterm.h"
@interface FoodItermDAO : NSObject{
    
    AppDelegate *appDelegate;
    
    NSManagedObjectContext          *managedObjectContext;
    NSManagedObjectModel            *managedObjectModel;
    NSPersistentStoreCoordinator    *persistentStoreCoordinator;
}
-(BOOL)insertFooditems :(NSArray *)fooditems :(int)categoryid;
//-(FoodIterm *) getFoodIterm: (NSString*) categoryId ;
-(FoodIterm *)SelectFooditembyid:(int)itemid;
@end
