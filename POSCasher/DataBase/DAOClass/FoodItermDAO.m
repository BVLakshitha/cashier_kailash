//
//  FoodItermDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "FoodItermDAO.h"
#import "FoodIterm.h"

@implementation FoodItermDAO
- (id)init
{
    self = [super init];
    if (self) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        managedObjectContext = [self managedObjectContext];
        managedObjectModel = appDelegate.managedObjectModel;
        persistentStoreCoordinator = appDelegate.persistentStoreCoordinator;
        
    }
    return self;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(BOOL)insertFooditems :(NSArray *)fooditems :(int)categoryid{
    
    for (NSDictionary * fooditem in fooditems) {
        
        FoodIterm * fooditemmodel = nil;
        int itemid = [[fooditem objectForKey:@"item_id"] intValue];
        NSNumber * discout = [fooditem objectForKey:@"discount"];
        NSString * disc = [fooditem objectForKey:@"item_discription"];
        NSString * fromtime = [fooditem objectForKey:@"item_from_time"];
        NSString * image = [fooditem objectForKey:@"item_image"];
        NSString * name = [fooditem objectForKey:@"item_name"];
        NSString * totime = [fooditem objectForKey:@"item_to_time"];
        NSString * type = [fooditem objectForKey:@"type"];
        
        
        fooditemmodel = [self isRecordExistanceOrder:itemid];
        if (!fooditemmodel) {
            fooditemmodel = [NSEntityDescription insertNewObjectForEntityForName:@"FoodIterm" inManagedObjectContext:managedObjectContext];
            [fooditemmodel setItemid:[NSNumber numberWithInt:itemid]];
        }

        [fooditemmodel setDiscount:[discout stringValue]];
        [fooditemmodel setFoodItermDiscription:disc];
        [fooditemmodel setFromtime:fromtime];
        [fooditemmodel setImage:image];
        [fooditemmodel setName:name];
        [fooditemmodel setTotime:totime];
        [fooditemmodel setType:type];
        [fooditemmodel setCategoryid:[NSNumber numberWithInt:categoryid]];
        
        NSError * error;
        if (![managedObjectContext save:&error]) {
            return false;
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    return true;
}
-(id)isRecordExistanceOrder:(int)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FoodIterm"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemid == %d",Record];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}

-(FoodIterm *)SelectFooditembyid:(int)itemid
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FoodIterm"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemid == %d",itemid];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
@end
