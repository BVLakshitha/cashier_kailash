//
//  OrderDAO.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Order.h"
@interface OrderDAO : NSObject
-(Order *)addOrUpdateOrderDetails: (NSDictionary*)orderValue;
-(Order *) getOrder: (NSString*) orderId ;
@end
