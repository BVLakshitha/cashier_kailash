//
//  OrderDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderDAO.h"

@implementation OrderDAO
-(Order *)addOrUpdateOrderDetails: (NSDictionary*)orderValue
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    
    Order *objOrder=nil;
    objOrder= (Order  *)[NSEntityDescription
                                 insertNewObjectForEntityForName:@"Order"inManagedObjectContext:managedObjectContext];
    
    
    return objOrder;

}
-(Order *) getOrder: (NSString*) orderId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Order" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(objFoodCategory.categoryId = %@)",orderId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
//        NSLog(@"Size 1 ");
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
@end
