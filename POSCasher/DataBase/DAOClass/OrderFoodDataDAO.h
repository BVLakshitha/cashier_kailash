//
//  OrderFoodDataDAO.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/8/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderFoodData.h"
@interface OrderFoodDataDAO : NSObject
-(void)addOrUpdateCategoryDetails: (NSDictionary*)foodCategory : (NSString*)foodCategoryId;
-(OrderFoodData *) getFoodCategory: (NSString*) itermId;
-(void)removeSelectOrderObject :(NSString *)itemId;
-(NSArray *) getSelectFoodCategorys :(NSString *)category_Id;
@end
