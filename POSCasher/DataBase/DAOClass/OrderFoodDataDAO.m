//
//  OrderFoodDataDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/8/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderFoodDataDAO.h"
#import "AppDelegate.h"
@implementation OrderFoodDataDAO
-(void)addOrUpdateCategoryDetails: (NSDictionary*)foodCategory : (NSString*)foodCategoryId
{


    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSString * getItermId = [foodCategory valueForKey:@"item_id"];
    OrderFoodData *orderFoodItermObj= [self getFoodCategory:getItermId];
    // Check whether our local db has the mumbler user already or not,
    if (orderFoodItermObj == nil) {

//        NSLog(@"Order Iterm not found %@", getItermId);

        orderFoodItermObj= (OrderFoodData  *)[NSEntityDescription
                                         insertNewObjectForEntityForName:@"OrderFoodData"inManagedObjectContext:managedObjectContext];

        [self addNewOrderIterm:foodCategory :orderFoodItermObj:foodCategoryId];

    } else {

//        NSLog(@"Order Iterm found %@", getItermId);
        [self updateOrderIterm:orderFoodItermObj];
        
        
    }



}

-(void)addNewOrderIterm : (NSDictionary *)foodCategory :(OrderFoodData *)objFoodCategory :(NSString *)foodCategoryId
{
    NSString *foodItermName = [foodCategory valueForKey:@"item_name"];
    NSNumber *foodItermPrice = [foodCategory valueForKey:@"item_code"] ;
    NSNumber *foodItermQuatity =@0;
    NSNumber *foodItermId = [foodCategory valueForKey:@"item_id"];
    NSString *foodItermDescription = [foodCategory valueForKey:@"item_discription"];
    NSNumber *foodItermDiscount = [foodCategory valueForKey:@"discount"];

    NSString *price=[NSString stringWithFormat:@"%li",(long)[foodItermPrice integerValue]];
    NSString *discount=[NSString stringWithFormat:@"%li",(long)[foodItermDiscount integerValue]];
    NSString *itermId=[NSString stringWithFormat:@"%li",(long)[foodItermId integerValue]];

    objFoodCategory.oderName=foodItermName;
    objFoodCategory.orderPrice=price;
    objFoodCategory.orderQuatity=foodItermQuatity;
    objFoodCategory.orderDesdription=foodItermDescription;
    objFoodCategory.orderId=itermId;
    objFoodCategory.discount=discount;
    objFoodCategory.perOnePrice=price;
    objFoodCategory.categoryId=foodCategoryId;

    
}

-(void)updateOrderIterm : (OrderFoodData *)objFoodCategory

{
    NSNumber *getQuantityValue=objFoodCategory.orderQuatity;
    NSInteger oldQuantityValue = [getQuantityValue integerValue];
    NSInteger newQuantityValue=oldQuantityValue+1;
    NSString *getPriceValue=objFoodCategory.orderPrice;
    NSInteger oldPriceValue = [getPriceValue integerValue];
    if (newQuantityValue>1) {
        NSInteger newPriceValue=(oldPriceValue)/(newQuantityValue-1);
        NSInteger newaddPriceValue=newPriceValue*newQuantityValue;
        NSString *strPrice=[NSString stringWithFormat:@"%li",(long)newaddPriceValue];
        objFoodCategory.orderPrice=strPrice;
    }
else if(newQuantityValue==1)
{
   
    objFoodCategory.orderPrice=objFoodCategory.perOnePrice;
}
    NSNumber *numberQuantity = [NSNumber numberWithInteger: newQuantityValue];
    objFoodCategory.orderQuatity=numberQuantity;

   
}

-(OrderFoodData *) getFoodCategory: (NSString*) itermId
{
    // Core data logic to add or save to the db
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;


    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderFoodData" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(orderId = %@)",itermId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];

    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }

}
-(void)removeSelectOrderObject :(NSString *)itemId
{
    OrderFoodData *objFoodCategory= [self getFoodCategory:itemId];
    NSNumber *getQuantityValue=objFoodCategory.orderQuatity;
    NSInteger oldQuantityValue = [getQuantityValue integerValue];
    NSInteger newQuantityValue;
    if (oldQuantityValue>0) {
         newQuantityValue=oldQuantityValue-1;
        NSNumber *numberQuantity = [NSNumber numberWithInteger: newQuantityValue];
        objFoodCategory.orderQuatity=numberQuantity;
    }
    NSString *getPriceValue=objFoodCategory.orderPrice;
    NSInteger oldPriceValue = [getPriceValue integerValue];
    
    NSInteger newPriceValue=(oldPriceValue*newQuantityValue)/(oldQuantityValue);
    NSString *strPrice=[NSString stringWithFormat:@"%li",(long)newPriceValue];
    objFoodCategory.orderPrice=strPrice;
}



-(NSArray *) getSelectFoodCategorys :(NSString *)category_Id
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderFoodData" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(categoryId = %@)",category_Id];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    return objects;
}
@end
