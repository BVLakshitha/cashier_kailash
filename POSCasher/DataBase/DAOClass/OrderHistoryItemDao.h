//
//  OrderHistoryItemDao.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 7/4/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderHistoryItem.h"
#import "AppDelegate.h"
@interface OrderHistoryItemDao : NSObject

-(void)addOrUpdateCheckOutOrderDetails: (NSString*) orderId :(NSString*) item_id :(NSString*) item_name :(NSNumber *) item_qyt :(NSString*) item_price :(NSString *)chairid :(NSString *)itemDesID :(NSString *)ItemActiveST :(NSString *)note :(NSString *)itemLoaction :(NSString *)itemDiscount :(NSString *)defaultDiscountStatus;

-(OrderHistoryItem *) getCheckOutOrderList: (NSString*) orderId;
-(NSArray *) getAllCheckOutOrderList: (NSString*) orderId;
-(NSArray *) getAllCheckOutOrderList: (NSString*) orderId :(NSString *)chairid;
-(void)deleteAllHistory;



@end
