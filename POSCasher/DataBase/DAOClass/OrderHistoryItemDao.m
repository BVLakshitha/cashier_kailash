//
//  OrderHistoryItemDao.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 7/4/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderHistoryItemDao.h"

@implementation OrderHistoryItemDao
-(void)addOrUpdateCheckOutOrderDetails: (NSString*) orderId :(NSString*) item_id :(NSString*) item_name :(NSNumber *) item_qyt :(NSString*) item_price :(NSString *)chairid :(NSString *)itemDesID :(NSString *)ItemActiveST :(NSString *)note :(NSString *)itemLoaction :(NSString *)itemDiscount :(NSString *)defaultDiscountStatus
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];

    OrderHistoryItem *orderListmObj= [self getCheckOutOrderList:itemDesID];

    // Check whether our local db has the mumbler user already or not,
    if (orderListmObj == nil) {

        orderListmObj= (OrderHistoryItem  *)[NSEntityDescription
                                      insertNewObjectForEntityForName:@"OrderHistoryItem"inManagedObjectContext:managedObjectContext];

        orderListmObj.orderId=orderId;


//        NSLog(@"Adding_orderid %@",orderId);


//        if ([item_qyt isKindOfClass:[NSNull class]]) {
//            item_qyt=@"0";
//            orderListmObj.itemQuantity=[NSString stringWithFormat:@"%i",item_qyt.intValue];
//
//        }
//        else
//        {
//            
//
//        }
        
        orderListmObj.itemQuantity=[NSString stringWithFormat:@"%i",item_qyt.intValue];;

        if ([item_id isKindOfClass:[NSNull class]]) {
            item_id=@"";
            orderListmObj.iterm_id=item_id;
        }
        else
        {
            orderListmObj.iterm_id=item_id;
        }
        if ([item_name isKindOfClass:[NSNull class]]) {
            item_name=@"";
            orderListmObj.itemName=item_name;
        }
        else
        {
           orderListmObj.itemName=item_name;
        }

        if ([item_price isKindOfClass:[NSNull class]]) {
            item_name=@"";
            orderListmObj.price=item_price;
        }
        else
        {
            orderListmObj.price=item_price;
        }
        
        orderListmObj.chairid = chairid;
        orderListmObj.itemDesID = itemDesID;
        orderListmObj.activeST = ItemActiveST;
        orderListmObj.itemNote = note;
        orderListmObj.itemLocation = itemLoaction;
        orderListmObj.itemDiscount = itemDiscount;
        orderListmObj.defaultDiscountStatus = defaultDiscountStatus;

    } else {
        

//        if ([item_qyt isKindOfClass:[NSNull class]]) {
//            item_qyt=@"";
//            orderListmObj.itemQuantity=item_qyt;
//        }
//        else
//        {
//            orderListmObj.itemQuantity=item_qyt;
//
//        }
        orderListmObj.itemQuantity=[NSString stringWithFormat:@"%i",item_qyt.intValue];;

        if ([item_id isKindOfClass:[NSNull class]]) {
            item_id=@"";
            orderListmObj.iterm_id=item_id;
        }
        else
        {
            orderListmObj.iterm_id=item_id;
        }
        if ([item_price isKindOfClass:[NSNull class]]) {
            item_name=@"";
            orderListmObj.price=item_price;
        }
        else
        {
            orderListmObj.price=item_price;
        }
        if ([item_name isKindOfClass:[NSNull class]]) {
            item_name=@"";
            orderListmObj.itemName=item_name;
        }
        else
        {
            orderListmObj.itemName=item_name;
        }
        orderListmObj.chairid = chairid;
        orderListmObj.itemDesID = itemDesID;
        orderListmObj.activeST = ItemActiveST;
        orderListmObj.itemNote = note;
        orderListmObj.itemLocation = itemLoaction;
        orderListmObj.itemDiscount = itemDiscount;
        orderListmObj.defaultDiscountStatus = defaultDiscountStatus;
    }
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(OrderHistoryItem *) getCheckOutOrderList: (NSString*) orderId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;


    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderHistoryItem" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemDesID LIKE %@",orderId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];

    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }

}
-(NSArray *) getAllCheckOutOrderList: (NSString*) orderId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderHistoryItem" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(orderId = %@)",orderId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
//    NSLog(@"Count %i",objects.count);
    
    if ([objects count] != 0) {

        return objects;
        
    } else {
        return nil;
    }
    
}
-(NSArray *) getAllCheckOutOrderList: (NSString*) orderId :(NSString *)chairid
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderHistoryItem" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(orderId = %@) AND (chairid = %@)",orderId,chairid];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
//    NSLog(@"Count %i",objects.count);
    
    if ([objects count] != 0) {
        
        return objects;
        
    } else {
        return nil;
    }
    
}
-(void)deleteAllHistory{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"OrderHistoryItem"];
    NSError *error;
    NSArray * mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [managedObjectContext deleteObject:items];
    }
    
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
}
@end
