//
//  OrderItermDAO.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "OrderItem.h"
@interface OrderItermDAO : NSObject
-(OrderItem *)addOrUpdateOrderIterm: (NSDictionary*)orderIterm;
-(OrderItem *)getOrderDetails: (NSString*) orderItermId;
@end
