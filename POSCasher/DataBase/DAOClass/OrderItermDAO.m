//
//  OrderItermDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderItermDAO.h"

@implementation OrderItermDAO
-(OrderItem *)addOrUpdateOrderIterm: (NSDictionary*)orderIterm

{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    
    OrderItem *objOrderIterm=nil;
    objOrderIterm= (OrderItem  *)[NSEntityDescription
                                               insertNewObjectForEntityForName:@"OrderItem"inManagedObjectContext:managedObjectContext];
    
    
    return objOrderIterm;
}
-(OrderItem *)getOrderDetails: (NSString*) orderItermId
{

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderItem" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(objFoodCategory.categoryId = %@)",orderItermId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
//        NSLog(@"Size 1 ");
        
        OrderItem *objOrderIterm = [objects objectAtIndex:0];
        
        NSLog(@"getMumbler %@ ", objOrderIterm.quantity);
        
        return [objects objectAtIndex:0];
        
    } else {
        return nil;
    }
}
@end
