//
//  OrderListDao.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 7/4/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderList.h"
#import "AppDelegate.h"
@interface OrderListDao : NSObject


-(void)addOrUpdateCheckOutOrderDetails: (NSString*) orderId : (NSString*) totalValue : (NSString*) createDate : (NSString*) CreateTime :(NSString *)status :(NSString *)tableid :(NSString *)delivery_type :(NSString *)paymnetST :(NSString *)ActiveST :(NSData *)customer :(NSString *)deliverydate :(NSString *)deliveryAddress :(NSString *)deliveryName :(NSString *)deliveryPhone :(NSString *)deliveryCost :(NSString *)deliveryEmail;

-(OrderList *) getCheckOutOrderList: (NSString*) orderId;

-(NSArray *) getCheckOutOrderAllList:(NSString *)status;

-(void)deleteAllHistoryList;

@end
