//
//  OrderListDao.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 7/4/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderListDao.h"

@implementation OrderListDao
-(void)addOrUpdateCheckOutOrderDetails: (NSString*) orderId : (NSString*) totalValue : (NSString*) createDate : (NSString*) CreateTime :(NSString *)status :(NSString *)tableid :(NSString *)delivery_type :(NSString *)paymnetST :(NSString *)ActiveST :(NSData *)customer :(NSString *)deliverydate :(NSString *)deliveryAddress :(NSString *)deliveryName :(NSString *)deliveryPhone :(NSString *)deliveryCost :(NSString *)deliveryEmail
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];

    OrderList *orderListmObj= [self getCheckOutOrderList:orderId];

//    NSLog(@"Ordr iD %@",orderId);
    // Check whether our local db has the mumbler user already or not,
    if (orderListmObj == nil) {


//        NSLog(@"Ordr iD %@",orderId);

        orderListmObj= (OrderList  *)[NSEntityDescription
                                                  insertNewObjectForEntityForName:@"OrderList"inManagedObjectContext:managedObjectContext];

        orderListmObj.orderId=orderId;
        orderListmObj.totalValue=totalValue;
        orderListmObj.createDate=createDate;
        orderListmObj.createTime=CreateTime;
        orderListmObj.stetus=status;
        orderListmObj.tableid=tableid;
        orderListmObj.deliveryType = delivery_type;
        orderListmObj.paymentSt = paymnetST;
        orderListmObj.activeSt = ActiveST;
        orderListmObj.customer = customer;
        orderListmObj.deliveryDate = @"Not define";
        orderListmObj.deliveryAddress = deliveryAddress;
        orderListmObj.deliveryName = deliveryName;
        orderListmObj.deliveryPhone = deliveryPhone;
        orderListmObj.deliveryCost = deliveryCost;
        orderListmObj.deliveryEmail = deliveryEmail;
        
        
        if (![deliverydate isKindOfClass:[NSNull class]]) {
            orderListmObj.deliveryDate = deliverydate;
        }
        
        
        


    } else {

        orderListmObj.totalValue=totalValue;
        orderListmObj.createDate=createDate;
        orderListmObj.createTime=CreateTime;
        orderListmObj.stetus=status;
        orderListmObj.tableid=tableid;
        orderListmObj.deliveryType = delivery_type;
        orderListmObj.paymentSt = paymnetST;
        orderListmObj.activeSt = ActiveST;
        orderListmObj.customer = customer;
        orderListmObj.deliveryDate = @"Not define";
        orderListmObj.deliveryDate = deliverydate;
        orderListmObj.deliveryAddress = deliveryAddress;
        orderListmObj.deliveryName = deliveryName;
        orderListmObj.deliveryPhone = deliveryPhone;
        orderListmObj.deliveryCost = deliveryCost;
        orderListmObj.deliveryEmail = deliveryEmail;
        
        if (![deliverydate isKindOfClass:[NSNull class]]) {
            orderListmObj.deliveryDate = deliverydate;
        }
        
    }
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(OrderList *) getCheckOutOrderList: (NSString*) orderId

{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;


    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderList" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(orderId = %@)",orderId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];

    if ([objects count] == 1) {
//        NSLog(@"Size 1 ");
        return [objects objectAtIndex:0];

    } else {
        return nil;
    }

}

-(NSArray *) getCheckOutOrderAllList:(NSString *)status
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderList" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(stetus = %@)",status];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    return objects;

}
-(void)deleteAllHistoryList{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"OrderList"];
    NSError *error;
    NSArray * mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [managedObjectContext deleteObject:items];
    }
    
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
}
@end
