//
//  RestuarantDAO.m
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/2/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "RestuarantDAO.h"

@implementation RestuarantDAO
-(RestuarantDetail *)addOrUpdateRestuarantDetails: (NSDictionary*)RestuarantDetails
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    
    RestuarantDetail *objRestuarantDetail=nil;
    objRestuarantDetail= (RestuarantDetail  *)[NSEntityDescription
                         insertNewObjectForEntityForName:@"RestuarantDetail"inManagedObjectContext:managedObjectContext];
    
    
    return objRestuarantDetail;
}
-(RestuarantDetail *) getRestuarantDetails: (NSString*) restuarantId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"RestuarantDetail" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(objFoodCategory.categoryId = %@)",restuarantId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
        return [objects objectAtIndex:0];    
    } else {
        return nil;
    }
}
@end
