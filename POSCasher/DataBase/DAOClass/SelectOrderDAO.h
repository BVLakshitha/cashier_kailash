//
//  SelectOrderDAO.h
//  ResbookCustoms
//
//  Created by orderfood on 09/06/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SelectOrder.h"
#import "AppDelegate.h"
#import "DBManager.h"
@interface SelectOrderDAO : NSObject{
    
    DBManager * dbmanager;
    
}
-(void)addOrUpdateSelectCategoryDetails: (NSDictionary*)selectFoodCategory :(NSString*)selectFoodCategoryId;
-(void)addNewSelectOrderItermfromdic  : (NSDictionary*)selectFoodCategory :(NSString *)selectedtbl;
-(void)addNewSelectOrderItermNewQtyByRecordid  : (NSDictionary*)selectFoodCategory :(NSString *)selectedtbl;
-(SelectOrder *) getSelectFoodCategory: (NSString*) stetus;
-(void)removeSelectOrderObject :(NSString *)itemId;
-(BOOL)removeLocationObjectByRecorID :(NSString *)itemid;
-(NSArray *) getSelectFoodCategorys;
-(NSArray *) getFoodCategorys :(NSString *)category_Id;
-(void)addOrUpdateSelectCategoryDetails: (NSDictionary*)selectFoodCategory;
-(void)removeLocationObject;
-(BOOL)insertOrupdatenote:(NSString *)Note :(NSString *)cateId;
-(NSArray *) getSelectFoodCategorysbyChair :(NSString *)chairid;
-(BOOL)removeLocationObjectByID :(NSString *)itemid;
-(SelectOrder *) getSelectFoodCategoryByRecordID: (NSString*) itermId;
-(void)addNewSelectOrderItermNewQty  : (NSDictionary*)selectFoodCategory :(NSString *)selectedtbl;
-(BOOL)insertOrupdatenoteRID:(NSString *)Note :(NSString *)RID;
-(NSArray *) getFoodCategoryByDisID :(NSString *)ItemDisID;
-(NSArray *) getSelectValidFoodCategorys;
-(NSArray *) getSelectFoodCategorysforPrinter;
@end
