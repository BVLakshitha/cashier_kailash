//
//  SelectOrderDAO.m
//  ResbookCustoms
//
//  Created by orderfood on 09/06/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "SelectOrderDAO.h"
#import "OrderFoodData.h"
#import "OrderFoodDataDAO.h"
#import "FoodIterm.h"
#import "DBManager.h"
@implementation SelectOrderDAO
-(void)addOrUpdateSelectCategoryDetails: (NSDictionary*)selectFoodCategory :(NSString*)selectFoodCategoryId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSString * getItermId = [selectFoodCategory valueForKey:@"foodItermId"];
    NSLog(@"peint Iterm Id  %@",getItermId);
    OrderFoodDataDAO *objOrderFood=[[OrderFoodDataDAO alloc]init];
    OrderFoodData *getOrderFood=[objOrderFood getFoodCategory:getItermId];
    SelectOrder *selectOrderFoodItermObj= [self getSelectFoodCategory:getItermId];

    
    // Check whether our local db has the mumbler user already or not,
    if (selectOrderFoodItermObj == nil) {
        
//        NSLog(@"Order Iterm not found %@", getItermId);
        
        selectOrderFoodItermObj= (SelectOrder  *)[NSEntityDescription
                                              insertNewObjectForEntityForName:@"SelectOrder"inManagedObjectContext:managedObjectContext];
        [self addNewSelectOrderIterm :selectOrderFoodItermObj : getOrderFood :selectFoodCategory];
        
    } else {
        
//        NSLog(@"Order Iterm found %@", getItermId);
       [self updateSelectOrderIterm:selectOrderFoodItermObj];
        
        
    }
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

-(void)addOrUpdateSelectCategoryDetails: (NSDictionary*)selectFoodCategory
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSString * getItermId = [selectFoodCategory valueForKey:@"item_id"];
//    NSLog(@"peint Iterm Id  %@",getItermId);

    SelectOrder *selectOrderFoodItermObj= [self getSelectFoodCategory:getItermId];


    // Check whether our local db has the mumbler user already or not,
    if (selectOrderFoodItermObj == nil) {

//        NSLog(@"Order Iterm not found %@", getItermId);

        selectOrderFoodItermObj= (SelectOrder  *)[NSEntityDescription
                                                  insertNewObjectForEntityForName:@"SelectOrder"inManagedObjectContext:managedObjectContext];
        [self addNewSelectOrderIterm :selectOrderFoodItermObj :selectFoodCategory];

    } else {

//        NSLog(@"Order Iterm found %@", getItermId);
        [self updateSelectOrderIterm:selectOrderFoodItermObj];


    }
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }

}
-(void)addNewSelectOrderIterm  :(SelectOrder *)objSelectFoodCategory : (NSDictionary*)selectFoodCategory
{

    NSString * oderName=[selectFoodCategory valueForKey:@"item_name"];
    NSString * oderPrice=[selectFoodCategory valueForKey:@"new_amount"];
    NSString * orderId=[selectFoodCategory valueForKey:@"item_id"];
    NSString * ordertype=[selectFoodCategory valueForKey:@"delevart_type"];
    NSNumber * orderQuatity=@0;
    NSString * status=@"1";
    NSInteger oldQuantityValue = [orderQuatity integerValue];
    NSInteger newQuantityValue=oldQuantityValue+1;
    NSNumber *numberQuantity = [NSNumber numberWithInteger: newQuantityValue];

    objSelectFoodCategory.orderName=oderName;
    objSelectFoodCategory.orderPrice=oderPrice;
    objSelectFoodCategory.orderrQuantity=numberQuantity;
    objSelectFoodCategory.status=status;
    objSelectFoodCategory.orderId=orderId;
    objSelectFoodCategory.totalPrice=oderPrice;
    objSelectFoodCategory.ordertype = ordertype;


}

-(void)addNewSelectOrderItermfromdic  : (NSDictionary*)selectFoodCategory :(NSString *)selectedtbl
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    SelectOrder *selectOrderFoodItermObj = nil;
    int itemQty = 0;
//    selectOrderFoodItermObj= [self getSelectFoodCategory:[selectFoodCategory valueForKey:@"itemId"]];
    selectOrderFoodItermObj= [self getSelectFoodCategory:@"10000"];
    
    if (selectOrderFoodItermObj == nil) {
        selectOrderFoodItermObj= (SelectOrder  *)[NSEntityDescription
                                                  insertNewObjectForEntityForName:@"SelectOrder"inManagedObjectContext:managedObjectContext];
        
        selectOrderFoodItermObj.categoryId = [selectFoodCategory valueForKey:@"itemId"];
        itemQty = [[selectFoodCategory valueForKey:@"itemQty"] intValue];
    }else{
        itemQty = selectOrderFoodItermObj.orderrQuantity.intValue+1;
    }

    selectOrderFoodItermObj.recodId=[selectFoodCategory valueForKey:@"RecordId"];
    selectOrderFoodItermObj.orderName=[selectFoodCategory valueForKey:@"itemname"];
    selectOrderFoodItermObj.orderPrice=[selectFoodCategory valueForKey:@"itemPrice"];
    selectOrderFoodItermObj.orderrQuantity= [NSNumber numberWithInt:itemQty];
    selectOrderFoodItermObj.status=[selectFoodCategory valueForKey:@"status"];
    selectOrderFoodItermObj.orderId=[selectFoodCategory valueForKey:@"OrderID"];
    selectOrderFoodItermObj.descriptionID=[selectFoodCategory valueForKey:@"orderdetailsid"];
    selectOrderFoodItermObj.chairName=@"0";
    selectOrderFoodItermObj.orderdescription = [selectFoodCategory valueForKey:@"descit"];
    selectOrderFoodItermObj.img_URL = [selectFoodCategory valueForKey:@"img_url"];
    selectOrderFoodItermObj.ordernote =  [selectFoodCategory valueForKey:@"note"];
    selectOrderFoodItermObj.cancelStatus = [selectFoodCategory valueForKey:@"ActiveSt"];
    selectOrderFoodItermObj.itenLocation = [selectFoodCategory valueForKey:@"itemLocation"];
    selectOrderFoodItermObj.itemDiscount = [selectFoodCategory valueForKey:@"itemDiscount"];
    selectOrderFoodItermObj.defaultDiscountStatus = [selectFoodCategory valueForKey:@"defaultDiscountStatus"];
    
    //Genarate New Total Price
    float itemprice = [[selectFoodCategory valueForKey:@"itemPrice"] floatValue];
    
    float itemDiscout = [[selectFoodCategory valueForKey:@"itemDiscount"] floatValue];
    float itemDiscountPrice = itemprice - (itemDiscout / 100) * itemprice;

    float total = itemDiscountPrice * itemQty;
    NSString * totalprice = [NSString stringWithFormat:@"%0.2f",total ];
    
    selectOrderFoodItermObj.totalPrice = totalprice;
    selectOrderFoodItermObj.tableName = selectedtbl;

    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(void)addNewSelectOrderItermNewQty  : (NSDictionary*)selectFoodCategory :(NSString *)selectedtbl
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    SelectOrder *selectOrderFoodItermObj = nil;
    int itemQty = 0;
    selectOrderFoodItermObj= [self getSelectFoodCategory:[selectFoodCategory valueForKey:@"itemId"]];

    itemQty = [[selectFoodCategory valueForKey:@"itemQty"] intValue];
    selectOrderFoodItermObj.orderName=[selectFoodCategory valueForKey:@"itemname"];
    selectOrderFoodItermObj.orderPrice=[selectFoodCategory valueForKey:@"itemPrice"];
    selectOrderFoodItermObj.orderrQuantity= [NSNumber numberWithInt:itemQty];
    selectOrderFoodItermObj.status=[selectFoodCategory valueForKey:@"status"];;
    selectOrderFoodItermObj.orderId=[selectFoodCategory valueForKey:@"OrderID"];
    selectOrderFoodItermObj.descriptionID=[selectFoodCategory valueForKey:@"orderdetailsid"];
    selectOrderFoodItermObj.chairName=@"0";
    
    //Genarate New Total Price
    int itemprice = [[selectFoodCategory valueForKey:@"itemPrice"] intValue];
    
    float itemDiscout = [[selectFoodCategory valueForKey:@"itemDiscount"] floatValue];
    float itemDiscountPrice = itemprice - (itemDiscout / 100) * itemprice;
    
    float total = itemDiscountPrice * itemQty;
    NSString * totalprice = [NSString stringWithFormat:@"%0.2f",total ];
    
    selectOrderFoodItermObj.totalPrice = totalprice;
    selectOrderFoodItermObj.tableName = selectedtbl;
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(void)addNewSelectOrderItermNewQtyByRecordid  : (NSDictionary*)selectFoodCategory :(NSString *)selectedtbl
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    
    SelectOrder *selectOrderFoodItermObj = nil;
    int itemQty = 0;
    selectOrderFoodItermObj= [self getSelectFoodCategoryByRecordID:[selectFoodCategory valueForKey:@"RecordId"]];
    
    itemQty = [[selectFoodCategory valueForKey:@"itemQty"] intValue];
    selectOrderFoodItermObj.orderName=[selectFoodCategory valueForKey:@"itemname"];
    selectOrderFoodItermObj.orderPrice=[selectFoodCategory valueForKey:@"itemPrice"];
    selectOrderFoodItermObj.orderrQuantity= [NSNumber numberWithInt:itemQty];
    selectOrderFoodItermObj.status=@"1";
    selectOrderFoodItermObj.orderId=[selectFoodCategory valueForKey:@"OrderID"];
    selectOrderFoodItermObj.descriptionID = [selectFoodCategory valueForKey:@"orderdetailsid"];
    selectOrderFoodItermObj.chairName=@"0";
    selectOrderFoodItermObj.defaultDiscountStatus = [selectFoodCategory valueForKey:@"orderdetailsid"];
    
    //Genarate New Total Price
    float itemprice = [[selectFoodCategory valueForKey:@"itemPrice"] floatValue];
    
    float itemDiscout = [[selectFoodCategory valueForKey:@"itemDiscount"] floatValue];
    float itemDiscountPrice = itemprice - (itemDiscout / 100) * itemprice;
    
    float total = itemDiscountPrice * itemQty;
    NSString * totalprice = [NSString stringWithFormat:@"%0.2f",total ];
    
//    float total = itemprice * itemQty;
//    NSString * totalprice = [NSString stringWithFormat:@"%0.2f",total ];
    
    selectOrderFoodItermObj.totalPrice = totalprice;
    selectOrderFoodItermObj.tableName = selectedtbl;
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}


-(void)addNewSelectOrderIterm  :(SelectOrder *)objSelectFoodCategory : (OrderFoodData *)objOrderFoodData :(NSDictionary*)selectFoodCategory
{
    NSString * oderName=objOrderFoodData.oderName;
    NSString * orderDesdription=objOrderFoodData.orderDesdription;
    NSString * orderId=objOrderFoodData.orderId;
    NSNumber * orderQuatity=@0;
    NSString *status=@"1";
    NSInteger oldQuantityValue = [orderQuatity integerValue];
    NSInteger newQuantityValue=oldQuantityValue+1;
    NSNumber *numberQuantity = [NSNumber numberWithInteger: newQuantityValue];

    objSelectFoodCategory.orderName=oderName;
    objSelectFoodCategory.orderPrice=objOrderFoodData.perOnePrice;
    objSelectFoodCategory.orderrQuantity=numberQuantity;
    objSelectFoodCategory.orderdescription=orderDesdription;
    objSelectFoodCategory.status=status;
    objSelectFoodCategory.orderId=orderId;
    objSelectFoodCategory.totalPrice=objOrderFoodData.perOnePrice;
//     NSString * getItermId = [selectFoodCategory valueForKey:@"foodItermId"];
//    objSelectFoodCategory.categoryId=getItermId;
    
     NSString * tablename = [selectFoodCategory valueForKey:@"tablename"];
     NSString * chairname = [selectFoodCategory valueForKey:@"chairname"];
     NSString * ordertype=[selectFoodCategory valueForKey:@"delevart_type"];
    objSelectFoodCategory.ordertype = ordertype;
    if ([tablename isKindOfClass:[NSNull class]]) {
       
        objSelectFoodCategory.tableName = tablename;
    }
    else
    {
       objSelectFoodCategory.tableName = tablename;
    }
    if ([chairname isKindOfClass:[NSNull class]]) {
     
        objSelectFoodCategory.chairName = chairname;
    }
    else
    {
       objSelectFoodCategory.chairName = chairname;
    }
    
//    objSelectFoodCategory.
    
    
    
}

-(void)updateSelectOrderIterm : (SelectOrder *)objSelectFoodCategory

{
    
        NSNumber *getQuantityValue=objSelectFoodCategory.orderrQuantity;
        NSInteger oldQuantityValue = [getQuantityValue integerValue];
        NSInteger newQuantityValue=oldQuantityValue+1;
        NSString *getPriceValue=objSelectFoodCategory.totalPrice;
        NSInteger oldPriceValue = [getPriceValue integerValue];
        if (newQuantityValue>1) {
            NSInteger newPriceValue=(oldPriceValue)/(newQuantityValue-1);
            NSInteger newaddPriceValue=newPriceValue*newQuantityValue;
            NSString *strPrice=[NSString stringWithFormat:@"%li",(long)newaddPriceValue];
            objSelectFoodCategory.totalPrice=strPrice;
        }
        else if(newQuantityValue==1)
        {
            objSelectFoodCategory.status=@"1";
            objSelectFoodCategory.totalPrice=objSelectFoodCategory.orderPrice;
        }
        NSNumber *numberQuantity = [NSNumber numberWithInteger: newQuantityValue];
        objSelectFoodCategory.orderrQuantity=numberQuantity;
}

-(SelectOrder *) getSelectFoodCategory: (NSString*) itermId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(categoryId = %@)",itermId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
-(SelectOrder *) getSelectFoodCategoryByRecordID: (NSString*) itermId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(recodId = %@)",itermId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}

-(NSArray *) getSelectFoodCategorys
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cancelStatus = %@)",@"0"];
//    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    return objects;
}
-(NSArray *) getSelectFoodCategorysforPrinter
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"descriptionID LIKE %@",@"0"];
   [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    return objects;
}
-(NSArray *) getSelectValidFoodCategorys
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(cancelStatus = %@)",@"0"];
        [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    return objects;
}

-(NSArray *) getSelectFoodCategorysbyChair :(NSString *)chairid
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(chairName = %@)",chairid];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    return objects;
}
-(void)removeSelectOrderObject :(NSString *)itemId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    SelectOrder *objFoodCategory= [self getSelectFoodCategory:itemId];
    NSNumber *getQuantityValue=objFoodCategory.orderrQuantity;
    NSInteger oldQuantityValue = [getQuantityValue integerValue];
    NSInteger newQuantityValue;
    if (oldQuantityValue>0) {
        newQuantityValue=oldQuantityValue-1;
        
        if (newQuantityValue==0) {
            objFoodCategory.status=@"0";
            [context deleteObject:objFoodCategory];
        }
        NSNumber *numberQuantity = [NSNumber numberWithInteger: newQuantityValue];
        objFoodCategory.orderrQuantity=numberQuantity;
    }
    
    NSString *getPriceValue=objFoodCategory.totalPrice;
    NSInteger oldPriceValue = [getPriceValue integerValue];
    
    NSInteger newPriceValue=(oldPriceValue*newQuantityValue)/(oldQuantityValue);
    NSString *strPrice=[NSString stringWithFormat:@"%li",(long)newPriceValue];
    objFoodCategory.totalPrice=strPrice;
}


-(NSArray *) getFoodCategorys :(NSString *)category_Id
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(categoryId = %@)",category_Id];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    return objects;
}
-(NSArray *) getFoodCategoryByDisID :(NSString *)ItemDisID
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(descriptionID = %@)",ItemDisID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    return objects;
}
-(void)removeLocationObject
{

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = nil;
    context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:context];
     NSFetchRequest *request = [[NSFetchRequest alloc] init];
     [request setEntity:entityDesc];
    NSError * error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];

    for (SelectOrder *objSelectOrder in objects) {
        [context deleteObject:objSelectOrder];
    }
    NSError *saveError = nil;
    [context save:&saveError];
    
}
-(BOOL)removeLocationObjectByID :(NSString *)itemid
{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = nil;
    context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(categoryId = %@)",itemid];
    [request setPredicate:predicate];
    NSError * error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    for (SelectOrder *objSelectOrder in objects) {
        [context deleteObject:objSelectOrder];
    }
    NSError *saveError = nil;
    
    if ([context save:&saveError]) {
        return true;
    }else{
        return false;
    }
}
-(BOOL)removeLocationObjectByRecorID :(NSString *)itemid
{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = nil;
    context = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(recodId = %@)",itemid];
    [request setPredicate:predicate];
    NSError * error = nil;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    for (SelectOrder *objSelectOrder in objects) {
        [context deleteObject:objSelectOrder];
    }
    NSError *saveError = nil;
    
    if ([context save:&saveError]) {
        return true;
    }else{
        return false;
    }
}
-(BOOL)insertOrupdatenote:(NSString *)Note :(NSString *)cateId{
    
    SelectOrder *selectOrderFoodItermObj= [self getSelectFoodCategory:cateId];
    
    if (selectOrderFoodItermObj) {
        selectOrderFoodItermObj.ordernote = Note;
        return TRUE;
    }
   return FALSE;
}
-(BOOL)insertOrupdatenoteRID:(NSString *)Note :(NSString *)RID{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    SelectOrder *selectOrderFoodItermObj= [self getSelectFoodRID:RID];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    if (selectOrderFoodItermObj) {
        selectOrderFoodItermObj.ordernote = Note;
        
        NSError *saveError = nil;
        
        if ([context save:&saveError]) {
            return true;
        }else{
            return false;
        }
        
        
        return TRUE;
    }
    return FALSE;
}
-(SelectOrder *) getSelectFoodRID: (NSString*) itermId
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    
    
    managedObjectContext = [appDelegate managedObjectContext];
    //}
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"SelectOrder" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(recodId = %@)",itermId];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
@end
