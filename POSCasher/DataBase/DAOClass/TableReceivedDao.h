//
//  TableReceivedDao.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/8/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "TableReceived.h"
@interface TableReceivedDao : NSObject

-(TableReceived *) getTableDetail :(NSString *)table_Id;
-(void)addOrUpdateTableReceivedDetails: (NSDictionary*)addTableReceivedData;
-(NSArray *) getHoldOrders;
-(NSArray *) getAllTableinfomation;
-(NSArray *) getTableinfomationByArea :(NSString *)locationID;
@end
