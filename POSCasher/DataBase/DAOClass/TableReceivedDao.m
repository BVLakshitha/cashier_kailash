//
//  TableReceivedDao.m
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/8/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "TableReceivedDao.h"

@implementation TableReceivedDao


-(TableReceived *) getTableDetail :(NSString *)table_Id
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"TableReceived" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(tableId = %@)",table_Id];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];

    if ([objects count] == 1) {
        return [objects objectAtIndex:0];
    } else {
        return nil;
    }
}
-(NSArray *) getHoldOrders
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"TableReceived" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(tblstatus LIKE %@)",@"13"];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] >0) {

        return objects ;
        
    } else {
        return nil;
    }
}
-(NSArray *) getAllTableinfomation
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"TableReceived" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] >0) {
        
        return objects ;
        
    } else {
        return nil;
    }
}
-(NSArray *) getTableinfomationByArea :(NSString *)locationID
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = nil;
    managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"TableReceived" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(locationID LIKE %@)",locationID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if ([objects count] >0) {
        return objects ;
    } else {
        return nil;
    }
}
-(void)addOrUpdateTableReceivedDetails: (NSDictionary*)addTableReceivedData
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSString * getTableId = [addTableReceivedData valueForKey:@"id"];
    NSString * getTableName = [addTableReceivedData valueForKey:@"tabel_name"];
    NSString * getTablechairs = [addTableReceivedData valueForKey:@"chairs"];
    NSString * getTabletStetus = [addTableReceivedData valueForKey:@"table_st"];
    NSString * getTableArea = [addTableReceivedData valueForKey:@"place"];
    
//    NSLog(@"Selected area = %@",getTableArea);
    
    NSString * myordrid = @"";
    NSString * mywaiterid = @"";
    NSString * myavilabletype = @"";
    NSString * myorderstatus = @"";
    id getorderid = [addTableReceivedData valueForKey:@"orderid"];
    
    if ([getorderid isKindOfClass:[NSString class]]) {
        myordrid = [addTableReceivedData valueForKey:@"orderid"];
    }else{
        NSNumber * getorderid2 = [addTableReceivedData valueForKey:@"orderid"];
        myordrid = [getorderid2 stringValue];
    }
    
    id getwaiterid = [addTableReceivedData valueForKey:@"update_by"];
    
    if ([getwaiterid isKindOfClass:[NSString class]]) {
        mywaiterid = [addTableReceivedData valueForKey:@"update_by"];
    }else{
        NSNumber * getwaiterid2 = [addTableReceivedData valueForKey:@"update_by"];
        mywaiterid = [getwaiterid2 stringValue];
    }

    
    id avilablechairs = [addTableReceivedData valueForKey:@"available_chairs"];
    
    if ([avilablechairs isKindOfClass:[NSString class]]) {
        myavilabletype = [addTableReceivedData valueForKey:@"available_chairs"];
    }else{
        NSNumber * getwaiterid2 = [addTableReceivedData valueForKey:@"available_chairs"];
        myavilabletype = [getwaiterid2 stringValue];
    }
    
    id tblstatus = [addTableReceivedData valueForKey:@"order_status"];
    
    if ([tblstatus isKindOfClass:[NSString class]]) {
        myorderstatus = [addTableReceivedData valueForKey:@"order_status"];
    }else{
        NSNumber * getwaiterid2 = [addTableReceivedData valueForKey:@"order_status"];
        myorderstatus = [getwaiterid2 stringValue];
    }
    

    

    TableReceived *objTableReceived= [self getTableDetail:getTableId];
    
    if ([getTableArea isEqual:[NSNull null]])getTableArea = @"0";
    
    

    // Check whether our local db has the mumbler user already or not,
    if (objTableReceived == nil) {

        objTableReceived= (TableReceived  *)[NSEntityDescription
                                                  insertNewObjectForEntityForName:@"TableReceived"inManagedObjectContext:managedObjectContext];

        objTableReceived.tableId=getTableId;
        objTableReceived.tableName=getTableName;
        objTableReceived.tableStetus=getTabletStetus;
        objTableReceived.chairs=getTablechairs;
        objTableReceived.orderid = myordrid;
        objTableReceived.waiterid=mywaiterid;
        objTableReceived.emtChairs=myavilabletype;
        objTableReceived.tblstatus = myorderstatus;
        objTableReceived.locationID = getTableArea;

        
    } else {


        objTableReceived.tableId=getTableId;
        objTableReceived.tableName=getTableName;
        objTableReceived.tableStetus=getTabletStetus;
        objTableReceived.chairs=getTablechairs;
        objTableReceived.orderid = myordrid;
        objTableReceived.waiterid=mywaiterid;
        objTableReceived.emtChairs=myavilabletype;
        objTableReceived.tblstatus = myorderstatus;
        objTableReceived.locationID = getTableArea;

    }
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
@end
