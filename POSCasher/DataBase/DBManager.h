//
//  DBManager.h
//  PosApp
//
//  Created by Lakshitha on 8/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Location.h"
#import "SelectOrder.h"
#import "ExtraHistory.h"

@class AppDelegate;

@interface DBManager : NSObject{
    AppDelegate *appDelegate;
    
    NSManagedObjectContext          *managedObjectContext;
    NSManagedObjectModel            *managedObjectModel;
    NSPersistentStoreCoordinator    *persistentStoreCoordinator;
}
-(BOOL)insetorupdatemyorders:(NSArray *)items :(NSDictionary *)orderdetails :(NSString *)orderid :(NSString *)waiterId :(NSString *)ordertype :(NSString *)eatin;
-(NSArray* )selectorderdetails:(NSString *)orderid ;
-(NSArray* )selectitem:(int)itemid ;
-(NSArray *)selectAllOrders;

#pragma mark Split
-(void)insetSplitRecord:(NSString *)Dictriptionid :(NSNumber *)splitid :(NSString *)price :(NSString *)qty :(NSString *)name :(NSString *)isSplited :(NSString *)devQty :(NSString *)itemRecordID;
-(void)RemoveSplitRecrd :(NSString *)DisID;
-(NSArray* )selectSplitRecord:(NSNumber *)Spliid;
-(void)updateSplitRecord:(NSString *)Dictriptionid :(NSString *)price :(NSString *)qty :(NSNumber *)splitid ;
-(id)isRecordExistanceSplit:(NSString *)Record ;
-(NSArray *)selectAllSplitRecords;
-(void)updateSplitOrderID :(int)Splitid :(int)SplitOrderID;
-(void)updateSplitForPaied :(int)Splitid;

#pragma mark Location
-(BOOL)insetorupdateLocations:(NSString *)locationId :(NSString *)locationName :(NSString *)default_location;
-(NSArray *)getAllLocationinfo;
-(Location *)getLocationinfo :(NSString *)loationID;

#pragma mark OrderHistory
-(NSArray *) getOrderHistorybystatus :(NSString *)statusID;

#pragma mark CustomerDetauils
-(BOOL)insetorupdatCustomer:(NSDictionary *)CustomerDic;
-(id)isRecordExistanceCustomer:(NSString *)Record;
-(NSArray *)GetallCustomers;
-(NSArray *)SelectCustomerByName:(NSString *)firstname :(NSString *)lastname;

#pragma mark ExtraItems
-(void)insertExtraSingleSelection:(NSDictionary *)CategoryDetails :(NSDictionary *)itemDetails :(NSDictionary *)foodIDetails;
-(void)insertMultipleSelection:(NSDictionary *)CategoryDetails :(NSDictionary *)itemDetails :(NSDictionary *)foodIDetails;
-(void)insertExtraByHistory :(ExtraHistory *)extrasHistory :(int)RecordID;
-(void)updateExtraQty :(NSString *)RecordID :(NSString *)Qty;
-(id)isRecordExistanceExtraItemID:(int)Record :(int)RecordID;
-(NSArray *)GetAllExtrasByRID:(int)RecordID;
-(BOOL)UpdateQuantity:(SelectOrder *)orderDetails :(int)qty;
-(void)RemoveExtraItem :(int)Record :(int)RecordID;
-(void)deleteAllExtras;

#pragma mark ExtraHistory
-(void)insertExtaFmHistory:(NSDictionary *)CategoryDetails :(NSDictionary *)itemDetails;
-(NSArray *)GetExtraHistoryByDetailID:(int)itemdetailsID;

#pragma mark ItemDetails
-(BOOL)insetorupdatpaydetails:(NSDictionary *)paymentdetailDic;
-(void)deleteAllPaymentDetails;
-(NSArray *)GetAllCardDetails:(NSString *)RecordID;
-(void)RemovePaymentRecordByType :(NSString *)Record;

#pragma mark GiftVoucher
-(void)insertGiftVoucher :(NSString *)voucherID :(NSString *)amount :(NSString *)discription;
-(NSArray *)GetAllVouchers;
-(void)RemoveGiftvoucherById :(NSString *)Record;
-(NSArray *)GetAllVouchersByconstant :(NSString *)Record;
-(void)deleteAllGiftVouchers;
-(void)deleteSelectedGiftVouchers :(NSString *)Record;
-(void)deleteSelectedGiftVouchersByRid :(NSNumber *)Record;

#pragma mark PrinterFeeds
-(void)ActvePrinterFeeds :(BOOL)isHoldOrder;           
-(NSArray *)getAllPrinterFeeds;
-(NSArray *)GetAllKitchenPrinterFeeds;

#pragma mark GetAllItems
-(void)insertAllItemData:(NSDictionary *)ItemDetails;
-(NSArray *)GetAllItemData;
-(void)deleteAllItemDetails;

#pragma mark CompanyDetails
-(BOOL)insetorupdatCompanydetails:(NSDictionary *)CompanydetailDic;
-(id)isRecordExistanceCompany:(NSString *)Record;
-(NSArray *)GetAllCompanyDetails;
@end
