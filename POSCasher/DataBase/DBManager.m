//
//  DBManager.m
//  PosApp
//
//  Created by Lakshitha on 8/12/14.
//  Copyright (c) 2014 sajirathan sathyaseelan. All rights reserved.
//

#import "DBManager.h"
#import "SelectOrder.h"
#import "Myorders.h"
#import "SplitData.h"
#import "Location.h"
#import "Customer.h"
#import "ExtraItems.h"
#import "PaymentDetails.h"
#import "ExtraHistory.h"
#import "GiftVouchers.h"
#import "PrinterFeeds.h"
#import "AllItems.h"
#import "Companies.h"



@implementation DBManager

- (id)init
{
    self = [super init];
    if (self) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        managedObjectContext = [self managedObjectContext];
        managedObjectModel = appDelegate.managedObjectModel;
        persistentStoreCoordinator = appDelegate.persistentStoreCoordinator;
        
    }
    return self;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(BOOL) saveEntity
{
    NSError *error;
    if (![managedObjectContext save:&error])
    {
        return FALSE;
    }
    return TRUE;
}

//Selected order update notes

-(BOOL)insertOrupdatenote:(NSString *)Note :(NSString *)cateId{
    
    SelectOrder * itemid = [self isRecordExistanceOrder:cateId];
    if (itemid) {

            [itemid setOrdernote:Note];
    }else{
        return FALSE;
    }

    NSError * error;
    if (![managedObjectContext save:&error]) {
        return FALSE;
//        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    return TRUE;
}

#pragma mark - 
#pragma mark OrdersavedDetails
-(BOOL)insetorupdatemyorders:(NSArray *)items :(NSDictionary *)orderdetails :(NSString *)orderid :(NSString *)waiterId :(NSString *)ordertype :(NSString *)eatin{
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *stringFromDate = [dateFormatter1 stringFromDate:[NSDate date]];

   Myorders * order = [self isRecordExistanceOrder:orderid];
    if (!order) {
        order = [NSEntityDescription insertNewObjectForEntityForName:@"Myorders" inManagedObjectContext:managedObjectContext];
        [order setOrderID:[f numberFromString:orderid]];
    }
    [order setDate:stringFromDate];
    [order setOrderdetails:[self dictionarytoData:orderdetails]];
    [order setItems:[self dictionarytoData:items]];
    [order setWaiterid:waiterId];
    [order setOrdertype:ordertype];
    [order setEatinType:eatin];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        return false;
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    [self deleteOrderFromSelecttbl];
    return true;
}
-(id)isRecordExistanceOrder:(NSString *)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Myorders"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderID LIKE %@",Record];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(NSArray *)selectAllOrders
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Myorders"];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}
-(NSData *)dictionarytoData :(id)dic{
    
    NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:dic];
    return myData;
}
#pragma mark -
#pragma mark SelectedOrder
-(void)deleteOrderFromSelecttbl{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SelectOrder"];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];

    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}
-(NSArray* )selectorderdetails:(NSString *)orderid{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Myorders"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderID LIKE %@",orderid];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults ;
        }
    }
    return nil;
    
}

-(NSArray* )selectitem:(int)itemid{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FoodIterm"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemid == %i",itemid];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults ;
        }
    }
    return nil;
    
}
-(BOOL)UpdateQuantity:(SelectOrder *)orderDetails :(int)qty{

    if (qty > 0)[orderDetails setOrderrQuantity:[NSNumber numberWithInt:qty]];
    
    NSError * error;
    if (![managedObjectContext save:&error]) {
        return FALSE;
        //        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    return TRUE;
}
#pragma mark -
#pragma mark SPlit View
-(void)insetSplitRecord:(NSString *)Dictriptionid :(NSNumber *)splitid :(NSString *)price :(NSString *)qty :(NSString *)name :(NSString *)isSplited :(NSString *)devQty :(NSString *)itemRecordID{

    SplitData * order = [NSEntityDescription insertNewObjectForEntityForName:@"SplitData" inManagedObjectContext:managedObjectContext];
    
    [order setItemDisID:Dictriptionid];
    [order setSplitID:splitid];
    [order setQty:qty];
    [order setPrice:price];
    [order setItemname:name];
    [order setIsSplited:isSplited];
    [order setDivQty:devQty];
    [order setItemDisID:Dictriptionid];
    [order setItemRecordID:itemRecordID];
    [order setIsPaid:@"NO"];
    
    
    NSError * error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(void)updateSplitRecord:(NSString *)Dictriptionid :(NSString *)price :(NSString *)qty :(NSNumber *)splitid{
    
    SplitData * order = [self isRecordExistanceSplit:Dictriptionid];
    if (order) {
        [order setSplitID:splitid];
        [order setQty:qty];
        [order setPrice:price];
        [order setIsSplited:@"YES"];
        [order setIsPaid:@"NO"];
        NSError * error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}
-(id)isRecordExistanceSplit:(NSString *)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SplitData"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemDisID LIKE %@",Record];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(void)RemoveSplitRecrd :(NSString *)DisID{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SplitData"];
    if (DisID.intValue != 1000) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemDisID LIKE %@",DisID];
        [request setPredicate:predicate];
    }
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
}
-(NSArray* )selectSplitRecord:(NSNumber *)Spliid{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SplitData"];
    
    if (Spliid.intValue != 1000) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"splitID == %i",Spliid.intValue];
        [request setPredicate:predicate];
    }
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    NSLog(@"SpliCount in DB %i",[mutableFetchResults count]);
    
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults ;
        }
    }
    return nil;
    
}
-(NSArray *)selectAllSplitRecords{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SplitData"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    if (mutableFetchResults.count > 0) {
        return mutableFetchResults ;
    }else{
        return nil;
    }
}
-(void)updateSplitOrderID :(int)Splitid :(int)SplitOrderID{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SplitData"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"splitID == %i",Splitid];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (SplitData * splitdata in mutableFetchResults) {
        [splitdata setSplitOrderID:[NSNumber numberWithInt:SplitOrderID]];
        NSError * error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}
-(void)updateSplitForPaied :(int)Splitid{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SplitData"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"splitID == %i",Splitid];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (SplitData * splitdata in mutableFetchResults) {
        [splitdata setIsPaid:@"YES"];
        NSError * error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
}

#pragma mark -
#pragma mark Locations
-(BOOL)insetorupdateLocations:(NSString *)locationId :(NSString *)locationName :(NSString *)default_location{

    int Locationid = locationId.intValue;
    
    Location * location = [self isRecordExistanceLocation:Locationid];
    if (!location) {
        location = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:managedObjectContext];
        [location setLocationId:[NSNumber numberWithInt:Locationid]];
    }
    [location setLocation:locationName];
    [location setDefault_location:default_location];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        return false;
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    [self deleteOrderFromSelecttbl];
    return true;
}
-(id)isRecordExistanceLocation:(int)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"locationId == %i",Record];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(NSArray *)getAllLocationinfo{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}
-(Location *)getLocationinfo :(NSString *)loationID{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"locationId == %i",loationID.intValue];
    [request setPredicate:predicate];
	NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
	if (mutableFetchResults == nil) {
		return nil;
	}else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
#pragma mark -
#pragma mark OrderHistory

-(NSArray *) getOrderHistorybystatus :(NSString *)statusID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"OrderList" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(stetus LIKE %@)",statusID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *objects = [context executeFetchRequest:request error:&error];
    
    if ([objects count] >0) {
        
        return objects ;
        
    } else {
        return nil;
    }
}
#pragma mark -
#pragma mark Customer Details

-(BOOL)insetorupdatCustomer:(NSDictionary *)CustomerDic{
    
    NSLog(@"Try to fatch %@",CustomerDic);
    
    Customer * customer = [self isRecordExistanceCustomer:[CustomerDic objectForKey:@"user_id"]];
    if (!customer) {
        customer = [NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:managedObjectContext];
        [customer setUserID:[NSNumber numberWithInt:[[CustomerDic objectForKey:@"user_id"] intValue]]];
    }

    if ([CustomerDic objectForKey:@"user_id"]!= [NSNull null])[customer setUserName:[CustomerDic objectForKey:@"user_id"]];
    if ([CustomerDic objectForKey:@"firstname"]!= [NSNull null])[customer setFirstName:[CustomerDic objectForKey:@"firstname"]];
    if ([CustomerDic objectForKey:@"lastname"]!= [NSNull null])[customer setLastName:[CustomerDic objectForKey:@"lastname"]];
    if ([CustomerDic objectForKey:@"street_address"]!= [NSNull null])[customer setAddress:[CustomerDic objectForKey:@"street_address"]];
    if ([CustomerDic objectForKey:@"city"]!= [NSNull null])[customer setCity:[CustomerDic objectForKey:@"city"]];
    if ([CustomerDic objectForKey:@"contact_no"]!= [NSNull null])[customer setContactNo:[CustomerDic objectForKey:@"contact_no"]];
    if ([CustomerDic objectForKey:@"post_code"]!= [NSNull null])[customer setPostalcode:[CustomerDic objectForKey:@"post_code"]];
    if ([CustomerDic objectForKey:@"email"]!= [NSNull null])[customer setEmail:[CustomerDic objectForKey:@"email"]];
    if ([CustomerDic objectForKey:@"door_no"]!= [NSNull null])[customer setDono:[CustomerDic objectForKey:@"door_no"]];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        return false;
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    return true;
}
-(id)isRecordExistanceCustomer:(NSString *)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customer"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID == %i",Record.intValue];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(NSArray *)GetallCustomers
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customer"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}

-(NSArray *)SelectCustomerByName:(NSString *)firstname :(NSString *)lastname
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Customer"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName LIKE %@",firstname];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults ;
        }
    }
    return nil;
}

#pragma mark -
#pragma mark ExtraItems - singel Selection
-(void)insertExtraSingleSelection:(NSDictionary *)CategoryDetails :(NSDictionary *)itemDetails :(NSDictionary *)foodIDetails{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];

    int CID = [[CategoryDetails objectForKey:@"catID"] intValue];
    int RecordID = [[foodIDetails objectForKey:@"RecordID"] intValue];
    ExtraItems * extras = [self isRecordExistanceExtraCID:CID :RecordID];
    if (!extras) {
        extras = [NSEntityDescription insertNewObjectForEntityForName:@"ExtraItems" inManagedObjectContext:managedObjectContext];
        [extras setCID:[f numberFromString:[CategoryDetails objectForKey:@"catID"]]];
    }else{
        int Extraitemid = [[itemDetails objectForKey:@"cid"] intValue];
        int CurrectExtraID = extras.extraitemID.intValue;
        
        if (Extraitemid == CurrectExtraID) {
            [self RemoveExtraItem:CurrectExtraID :RecordID];
             return;
        }
    }
    
    [extras setCName:[CategoryDetails objectForKey:@"catName"]];
    [extras setCPrice:[CategoryDetails objectForKey:@"catPrice"]];
    [extras setCType:[CategoryDetails objectForKey:@"catType"]];
    [extras setExtraQty:[f numberFromString:[itemDetails objectForKey:@"ExtraQty"]]];
    [extras setExtraitemID:[f numberFromString:[itemDetails objectForKey:@"cid"]]];
    [extras setExtraitemName:[itemDetails objectForKey:@"cname"]];
    
    double qty = [[itemDetails objectForKey:@"ExtraQty"] doubleValue];
    double price = [[itemDetails objectForKey:@"cadditional_price"] doubleValue];
    double totItemprice = qty * price;
    
    [extras setExtraitemPrice:[NSString stringWithFormat:@"%0.2f",totItemprice]];
//    [extras setExtraitemRecordID:[f numberFromString:[CategoryDetails objectForKey:@"cid"]]];
    [extras setExtraitemType:[CategoryDetails objectForKey:@"ctype"]];
    
    [extras setItemID:[f numberFromString:[foodIDetails objectForKey:@"item_id"]]];
    [extras setItemRecordID:[f numberFromString:[foodIDetails objectForKey:@"RecordID"]]];
    [extras setIsEditable:@"YES"];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(id)isRecordExistanceExtraCID:(int)Record :(int)RecordID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cID == %i AND itemRecordID == %i",Record,RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}

#pragma mark ExtraItems - Multiple Selection
-(void)insertMultipleSelection:(NSDictionary *)CategoryDetails :(NSDictionary *)itemDetails :(NSDictionary *)foodIDetails{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    int CID = [[itemDetails objectForKey:@"cid"] intValue];
    int RecordID = [[foodIDetails objectForKey:@"RecordID"] intValue];
    
    ExtraItems * extras = [self isRecordExistanceExtraItemID:CID :RecordID];
    if (!extras) {
        extras = [NSEntityDescription insertNewObjectForEntityForName:@"ExtraItems" inManagedObjectContext:managedObjectContext];
        [extras setExtraitemID:[f numberFromString:[itemDetails objectForKey:@"cid"]]];
    }else{
        int Extraitemid = [[itemDetails objectForKey:@"cid"] intValue];
        int CurrectExtraID = extras.extraitemID.intValue;
        
        if (Extraitemid == CurrectExtraID) {
            [self RemoveExtraItem:CurrectExtraID :RecordID];
        }
        return;
    }
    
    [extras setCID:[f numberFromString:[CategoryDetails objectForKey:@"catID"]]];
    [extras setCName:[CategoryDetails objectForKey:@"catName"]];
    [extras setCPrice:[CategoryDetails objectForKey:@"catPrice"]];
    [extras setCType:[CategoryDetails objectForKey:@"catType"]];
    [extras setItemDetailID:[foodIDetails objectForKey:@"ItemDisID"]];
    [extras setExtraQty:[f numberFromString:[itemDetails objectForKey:@"ExtraQty"]]];
    
    [extras setExtraitemName:[itemDetails objectForKey:@"cname"]];
    
    double qty = [[itemDetails objectForKey:@"ExtraQty"] doubleValue];
    double price = [[itemDetails objectForKey:@"cadditional_price"] doubleValue];
    double totItemprice = qty * price;
    
    [extras setExtraitemPrice:[NSString stringWithFormat:@"%0.2f",totItemprice]];
//    [extras setExtraitemRecordID:[f numberFromString:[CategoryDetails objectForKey:@"cid"]]];
    [extras setExtraitemType:[itemDetails objectForKey:@"ctype"]];
    [extras setItemDetailID:[foodIDetails objectForKey:@"ItemDisID"]];
    
    [extras setItemID:[f numberFromString:[foodIDetails objectForKey:@"item_id"]]];
    [extras setItemRecordID:[f numberFromString:[foodIDetails objectForKey:@"RecordID"]]];
    [extras setIsEditable:@"YES"];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(void)updateExtraQty :(NSString *)RecordID :(NSString *)Qty{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemRecordID LIKE %@",RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults != nil) {
        
        for (ExtraItems * extraItem in mutableFetchResults) {
            
            extraItem.extraQty = [f numberFromString:Qty];
            double qty = Qty.intValue;
            double price = extraItem.extraitemPrice.doubleValue;
            double totItemprice = qty * price;
            extraItem.extraitemPrice = [NSString stringWithFormat:@"%0.2f",totItemprice];
            
            NSError *error;
            if (![managedObjectContext save:&error]) {
                NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            }
        }
    }
}
-(id)isRecordExistanceExtraItemID:(int)Record :(int)RecordID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"extraitemID == %i AND itemRecordID == %i",Record,RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(void)insertExtraByHistory :(ExtraHistory *)extrasHistory :(int)RecordID{
    
  ExtraItems * extras = [NSEntityDescription insertNewObjectForEntityForName:@"ExtraItems" inManagedObjectContext:managedObjectContext];
    
    [extras setExtraitemID:extrasHistory.itemID];
    [extras setItemRecordID:[NSNumber numberWithInt:RecordID]];
    [extras setExtraitemName:extrasHistory.extraName];
    [extras setExtraitemPrice:extrasHistory.extraPrices];
    [extras setItemID:extrasHistory.itemID];
    [extras setIsEditable:@"NO"];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
}
-(id)isRecordExistanceExtra:(int)Record :(int)RecordID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"extraitemID == %i AND itemRecordID == %i",Record,RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(void)RemoveExtraItem :(int)Record :(int)RecordID{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"extraitemID == %i AND itemRecordID == %i",Record,RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(NSArray *)GetAllExtrasByRID:(int)RecordID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemRecordID == %i",RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}
-(void)deleteAllExtras{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraItems"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}
#pragma mark -
#pragma mark ExtrahistryTable

-(void)insertExtaFmHistory:(NSDictionary *)CategoryDetails :(NSDictionary *)itemDetails{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    int itemDetailID = [[itemDetails objectForKey:@"details_id"] intValue];
    int ExtraID = [[CategoryDetails objectForKey:@"id"] intValue];
    
    ExtraHistory * extras = [self isRecordExistanceExtaFmHistory:itemDetailID :ExtraID];
    if (!extras) {
        extras = [NSEntityDescription insertNewObjectForEntityForName:@"ExtraHistory" inManagedObjectContext:managedObjectContext];
        [extras setExtraitemID:[NSNumber numberWithInt:ExtraID]];
        [extras setItemDetailID:[NSNumber numberWithInt:itemDetailID]];
    }
    [extras setItemID:[NSNumber numberWithInt:[[CategoryDetails objectForKey:@"item_id"] intValue]]];
    [extras setExtraName:[CategoryDetails objectForKey:@"name"]];
    [extras setExtraPrices:[CategoryDetails objectForKey:@"additional"]];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(id)isRecordExistanceExtaFmHistory:(int)itemdetailsID :(int)extraID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraHistory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemDetailID == %i AND extraitemID == %i",itemdetailsID,extraID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(NSArray *)GetExtraHistoryByDetailID:(int)itemdetailsID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"ExtraHistory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemDetailID == %i",itemdetailsID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}
#pragma mark -
#pragma mark Card and Vouchers details
-(id)isRecordPaymentdetails:(NSString *)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PaymentDetails"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"typrdetails LIKE %@",Record];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}
-(BOOL)insetorupdatpaydetails:(NSDictionary *)paymentdetailDic{

    PaymentDetails * payment = [self isRecordPaymentdetails:[paymentdetailDic objectForKey:@"typrdetails"]];
    if (!payment) {
        payment = [NSEntityDescription insertNewObjectForEntityForName:@"PaymentDetails" inManagedObjectContext:managedObjectContext];
        [payment setTyprdetails:[paymentdetailDic objectForKey:@"typrdetails"]];
    }

    [payment setRecordID:[NSNumber numberWithInt:0]];
    [payment setPaytype:[paymentdetailDic objectForKey:@"paytype"]];
    [payment setAmount:[paymentdetailDic objectForKey:@"RecordId"]];
    [payment setInterest:[paymentdetailDic objectForKey:@"Interest"]];
    [payment setSubtype:[paymentdetailDic objectForKey:@"cardType"]];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        return false;
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    return true;
}
-(void)deleteAllPaymentDetails{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PaymentDetails"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}
-(NSArray *)GetAllCardDetails:(NSString *)RecordID
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PaymentDetails"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"paytype LIKE %@",RecordID];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}
-(void)RemovePaymentRecordByType :(NSString *)Record{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PaymentDetails"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"paytype LIKE %@",Record];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
#pragma mark -
#pragma mark GiftVoucher
-(void)insertGiftVoucher :(NSString *)voucherID :(NSString *)amount :(NSString *)discription{
    
    GiftVouchers * GV = [NSEntityDescription insertNewObjectForEntityForName:@"GiftVouchers" inManagedObjectContext:managedObjectContext];
    int newrecordID = [self GetlatestRecordID] +1;
    
    [GV setVoucherID:voucherID];
    [GV setAmount:amount];
    [GV setDiscription:discription];
    [GV setRecordID:[NSNumber numberWithInt:newrecordID]];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(int)GetlatestRecordID{
    
    NSArray * vouchers = [self GetAllVouchers];
    int Id = 0;
    
    for (GiftVouchers * voucher in vouchers) {
        if (Id < voucher.recordID.intValue) {
            Id = voucher.recordID.intValue;
        }
    }
    return Id;
}
-(NSArray *)GetAllVouchers{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"GiftVouchers"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    return mutableFetchResults;
}
-(void)RemoveGiftvoucherById :(NSString *)Record{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"GiftVouchers"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recordID LIKE %@",Record];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}
-(NSArray *)GetAllVouchersByconstant :(NSString *)Record{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"GiftVouchers"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"amount LIKE %@",Record];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    return mutableFetchResults;
}
-(void)deleteSelectedGiftVouchers :(NSString *)Record{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"GiftVouchers"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"amount LIKE %@",Record];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)deleteSelectedGiftVouchersByRid :(NSNumber *)Record{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"GiftVouchers"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recordID == %@",Record];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}
-(void)deleteAllGiftVouchers{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"GiftVouchers"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

#pragma mark -
#pragma mark PrinterFeeds

-(void)ActvePrinterFeeds :(BOOL)isHoldOrder{
    
    [self ResetPrinterFeeds];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"SelectOrder"];
    
    if (!isHoldOrder) {
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"descriptionID LIKE %@",@"0"];
        [request setPredicate:predicate];
    }
    
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
  
    for (SelectOrder * selectitem in mutableFetchResults) {
        [self insertPrinterFeeds:selectitem];
    }
    
}

-(void)insertPrinterFeeds:(SelectOrder *)SelectedItems {
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    
    PrinterFeeds * feeds = [NSEntityDescription insertNewObjectForEntityForName:@"PrinterFeeds" inManagedObjectContext:managedObjectContext];

    [feeds setItemID:[f numberFromString:SelectedItems.categoryId]];
    [feeds setItemName:SelectedItems.orderName];
    [feeds setItemNote:SelectedItems.ordernote];
    [feeds setItemQty:SelectedItems.orderrQuantity.stringValue];
    [feeds setItemRecordID:SelectedItems.recodId];
    [feeds setItemLocation:SelectedItems.itenLocation];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

-(NSArray *)getAllPrinterFeeds{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PrinterFeeds"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    return mutableFetchResults;
}
-(NSArray *)GetAllKitchenPrinterFeeds{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PrinterFeeds"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemLocation LIKE %@",@"kitchen"];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    return mutableFetchResults;
}

-(void)ResetPrinterFeeds {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"PrinterFeeds"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

#pragma mark -
#pragma mark PrinterFeeds

-(void)insertAllItemData:(NSDictionary *)ItemDetails {
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
//    NSLog(@"Item code%@",ItemDetails);
    
    
    AllItems * feeds = [NSEntityDescription insertNewObjectForEntityForName:@"AllItems" inManagedObjectContext:managedObjectContext];

    [feeds setCategory:[f numberFromString:[ItemDetails objectForKey:@"item_category"]]];
    [feeds setCode:[ItemDetails objectForKey:@"item_code"]];
    [feeds setDiscount:[f numberFromString:[ItemDetails objectForKey:@"discount"]]];
    [feeds setItemDiscription:[ItemDetails objectForKey:@"item_discription"]];
    [feeds setFrom:[ItemDetails objectForKey:@"item_from_time"]];
    [feeds setItem_id:[f numberFromString:[ItemDetails objectForKey:@"item_id"]]];
    [feeds setItem_imgURL:[ItemDetails objectForKey:@"item_image"]];
    [feeds setItem_name:[ItemDetails objectForKey:@"item_name"]];
    [feeds setPrice:[f numberFromString:[ItemDetails objectForKey:@"item_price"]]];
    [feeds setTo:[ItemDetails objectForKey:@"item_to_time"]];
    [feeds setType:[ItemDetails objectForKey:@"type"]];
    [feeds setItemLocation:[ItemDetails objectForKey:@"itemLocation"]];
    [feeds setAddDefaultDiscount:[ItemDetails objectForKey:@"addDefaultDiscount"]];
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

-(NSArray *)GetAllItemData{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"AllItems"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    return mutableFetchResults;
}
-(void)deleteAllItemDetails{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"AllItems"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

#pragma mark -
#pragma mark CompanyDetails

-(BOOL)insetorupdatCompanydetails:(NSDictionary *)CompanydetailDic{
    
    Companies * company = [self isRecordExistanceCompany:[CompanydetailDic objectForKey:@"user_id"]];
    if (!company) {
        company = [NSEntityDescription insertNewObjectForEntityForName:@"Companies" inManagedObjectContext:managedObjectContext];
        [company setCId:[CompanydetailDic objectForKey:@"user_id"]];
    }
    company.cAddress = [CompanydetailDic objectForKey:@"company_address"];
    company.cName = [CompanydetailDic objectForKey:@"company_name"];
    
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        return false;
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    return true;
}

-(id)isRecordExistanceCompany:(NSString *)Record
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Companies"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cId == %i",Record.intValue];
    [request setPredicate:predicate];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return [mutableFetchResults firstObject];
        }
    }
    return nil;
}

-(NSArray *)GetAllCompanyDetails{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Companies"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    return mutableFetchResults;
}
@end
