//
//  AllItems.h
//  Cashier
//
//  Created by User on 15/05/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AllItems : NSManagedObject

@property (nonatomic, retain) NSNumber * discount;
@property (nonatomic, retain) NSNumber * item_id;
@property (nonatomic, retain) NSString * item_name;
@property (nonatomic, retain) NSString * item_imgURL;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * itemDiscription;
@property (nonatomic, retain) NSString * from;
@property (nonatomic, retain) NSString * to;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * category;
@property (nonatomic, retain) NSString * itemLocation;
@property (nonatomic, retain) NSString * addDefaultDiscount;

@end
