//
//  AllItems.m
//  Cashier
//
//  Created by User on 15/05/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "AllItems.h"


@implementation AllItems

@dynamic discount;
@dynamic item_id;
@dynamic item_name;
@dynamic item_imgURL;
@dynamic price;
@dynamic code;
@dynamic itemDiscription;
@dynamic from;
@dynamic to;
@dynamic type;
@dynamic category;
@dynamic itemLocation;
@dynamic addDefaultDiscount;

@end
