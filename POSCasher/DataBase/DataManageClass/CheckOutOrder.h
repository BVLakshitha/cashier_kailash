//
//  CheckOutOrder.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CheckOutOrder : NSManagedObject

@property (nonatomic, retain) NSString * orderDate;
@property (nonatomic, retain) NSString * orderId;
@property (nonatomic, retain) NSString * orderName;
@property (nonatomic, retain) NSString * orderPerPrice;
@property (nonatomic, retain) NSString * orderQuantity;
@property (nonatomic, retain) NSString * orderTotalPrice;

@end
