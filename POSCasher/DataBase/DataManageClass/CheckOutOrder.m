//
//  CheckOutOrder.m
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "CheckOutOrder.h"


@implementation CheckOutOrder

@dynamic orderDate;
@dynamic orderId;
@dynamic orderName;
@dynamic orderPerPrice;
@dynamic orderQuantity;
@dynamic orderTotalPrice;

@end
