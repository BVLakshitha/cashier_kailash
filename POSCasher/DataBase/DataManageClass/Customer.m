//
//  Customer.m
//  Cashier
//
//  Created by Lakshitha on 15/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "Customer.h"


@implementation Customer

@dynamic address;
@dynamic city;
@dynamic contactNo;
@dynamic firstName;
@dynamic lastName;
@dynamic postalcode;
@dynamic telNo;
@dynamic userID;
@dynamic userName;
@dynamic email;
@dynamic dono;

@end
