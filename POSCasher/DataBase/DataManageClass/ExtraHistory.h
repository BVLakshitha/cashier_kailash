//
//  ExtraHistory.h
//  Cashier
//
//  Created by User on 26/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ExtraHistory : NSManagedObject

@property (nonatomic, retain) NSNumber * itemID;
@property (nonatomic, retain) NSNumber * extraitemID;
@property (nonatomic, retain) NSNumber * itemDetailID;
@property (nonatomic, retain) NSString * extraName;
@property (nonatomic, retain) NSString * extraPrices;

@end
