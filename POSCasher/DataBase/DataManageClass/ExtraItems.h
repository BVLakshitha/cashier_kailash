//
//  ExtraItems.h
//  Cashier
//
//  Created by Lakshitha on 8/14/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ExtraItems : NSManagedObject

@property (nonatomic, retain) NSNumber * cID;
@property (nonatomic, retain) NSString * cName;
@property (nonatomic, retain) NSString * cPrice;
@property (nonatomic, retain) NSString * cType;
@property (nonatomic, retain) NSNumber * extraitemID;
@property (nonatomic, retain) NSString * extraitemName;
@property (nonatomic, retain) NSString * extraitemPrice;
@property (nonatomic, retain) NSNumber * extraitemRecordID;
@property (nonatomic, retain) NSString * extraitemType;
@property (nonatomic, retain) NSString * isEditable;
@property (nonatomic, retain) NSString * itemDetailID;
@property (nonatomic, retain) NSNumber * itemID;
@property (nonatomic, retain) NSNumber * itemRecordID;
@property (nonatomic, retain) NSNumber * extraQty;

@end
