//
//  ExtraItems.m
//  Cashier
//
//  Created by Lakshitha on 8/14/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import "ExtraItems.h"


@implementation ExtraItems

@dynamic cID;
@dynamic cName;
@dynamic cPrice;
@dynamic cType;
@dynamic extraitemID;
@dynamic extraitemName;
@dynamic extraitemPrice;
@dynamic extraitemRecordID;
@dynamic extraitemType;
@dynamic isEditable;
@dynamic itemDetailID;
@dynamic itemID;
@dynamic itemRecordID;
@dynamic extraQty;

@end
