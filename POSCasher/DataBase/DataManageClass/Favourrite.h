//
//  Favourrite.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Favourrite : NSManagedObject

@property (nonatomic, retain) NSString * favourriteCount;
@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSString * itemId;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSString * itemPrice;
@property (nonatomic, retain) NSString * stetus;

@end
