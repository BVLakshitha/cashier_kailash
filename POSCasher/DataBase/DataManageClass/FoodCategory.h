//
//  FoodCategory.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FoodCategory : NSManagedObject

@property (nonatomic, retain) NSString * categoryId;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSString * name;

@end
