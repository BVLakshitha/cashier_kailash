//
//  FoodCategory.m
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "FoodCategory.h"


@implementation FoodCategory

@dynamic categoryId;
@dynamic imagePath;
@dynamic name;

@end
