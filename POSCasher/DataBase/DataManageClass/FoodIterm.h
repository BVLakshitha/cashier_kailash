//
//  FoodIterm.h
//  resbookWaiter
//
//  Created by Lakshitha on 9/1/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FoodCategory;

@interface FoodIterm : NSManagedObject

@property (nonatomic, retain) NSString * discount;
@property (nonatomic, retain) NSString * foodItermDiscription;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * fromtime;
@property (nonatomic, retain) NSString * totime;
@property (nonatomic, retain) NSNumber * itemid;
@property (nonatomic, retain) NSNumber * categoryid;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * itemLocation;
@property (nonatomic, retain) FoodCategory *objFoodCategory;

@end
