//
//  FoodIterm.m
//  resbookWaiter
//
//  Created by Lakshitha on 9/1/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "FoodIterm.h"
#import "FoodCategory.h"


@implementation FoodIterm

@dynamic discount;
@dynamic foodItermDiscription;
@dynamic name;
@dynamic price;
@dynamic fromtime;
@dynamic totime;
@dynamic itemid;
@dynamic categoryid;
@dynamic image;
@dynamic type;
@dynamic objFoodCategory;
@dynamic itemLocation;

@end
