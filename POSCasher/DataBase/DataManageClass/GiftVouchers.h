//
//  GiftVouchers.h
//  Cashier
//
//  Created by User on 21/01/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GiftVouchers : NSManagedObject

@property (nonatomic, retain) NSString * voucherID;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * discription;
@property (nonatomic, retain) NSNumber * recordID;

@end
