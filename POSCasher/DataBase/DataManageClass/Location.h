//
//  Location.h
//  POSCasher
//
//  Created by orderfood on 26/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSNumber * locationId;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * default_location;

@end
