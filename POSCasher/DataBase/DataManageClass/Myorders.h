//
//  Myorders.h
//  resbookWaiter
//
//  Created by Lakshitha on 9/9/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Myorders : NSManagedObject

@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSData * items;
@property (nonatomic, retain) NSData * orderdetails;
@property (nonatomic, retain) NSNumber * orderID;
@property (nonatomic, retain) NSString * ordertype;
@property (nonatomic, retain) NSString * waiterid;
@property (nonatomic, retain) NSString * eatinType;

@end
