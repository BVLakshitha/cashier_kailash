//
//  Myorders.m
//  resbookWaiter
//
//  Created by Lakshitha on 9/9/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "Myorders.h"


@implementation Myorders

@dynamic date;
@dynamic items;
@dynamic orderdetails;
@dynamic orderID;
@dynamic ordertype;
@dynamic waiterid;
@dynamic eatinType;

@end
