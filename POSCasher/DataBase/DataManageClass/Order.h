//
//  Order.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Order : NSManagedObject

@property (nonatomic, retain) NSDate * deliveryTime;
@property (nonatomic, retain) NSNumber * deliveryType;
@property (nonatomic, retain) NSDate * orderTime;

@end
