//
//  Order.m
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "Order.h"


@implementation Order

@dynamic deliveryTime;
@dynamic deliveryType;
@dynamic orderTime;

@end
