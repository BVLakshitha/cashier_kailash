//
//  OrderFoodData.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OrderFoodData : NSManagedObject

@property (nonatomic, retain) NSString * categoryId;
@property (nonatomic, retain) NSString * discount;
@property (nonatomic, retain) NSString * oderName;
@property (nonatomic, retain) NSString * orderDesdription;
@property (nonatomic, retain) NSString * orderId;
@property (nonatomic, retain) NSString * orderPrice;
@property (nonatomic, retain) NSNumber * orderQuatity;
@property (nonatomic, retain) NSString * perOnePrice;

@end
