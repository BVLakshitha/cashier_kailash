//
//  OrderFoodData.m
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderFoodData.h"


@implementation OrderFoodData

@dynamic categoryId;
@dynamic discount;
@dynamic oderName;
@dynamic orderDesdription;
@dynamic orderId;
@dynamic orderPrice;
@dynamic orderQuatity;
@dynamic perOnePrice;

@end
