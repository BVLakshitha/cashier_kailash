//
//  OrderHistoryItem.h
//  Cashier
//
//  Created by User on 12/03/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OrderHistoryItem : NSManagedObject

@property (nonatomic, retain) NSString * activeST;
@property (nonatomic, retain) NSString * chairid;
@property (nonatomic, retain) NSString * itemDesID;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSString * itemQuantity;
@property (nonatomic, retain) NSString * iterm_id;
@property (nonatomic, retain) NSString * orderId;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * itemNote;
@property (nonatomic, retain) NSString * itemLocation;
@property (nonatomic, retain) NSString * itemDiscount;
@property (nonatomic, retain) NSString * defaultDiscountStatus;

@end
