//
//  OrderHistoryItem.m
//  Cashier
//
//  Created by User on 12/03/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "OrderHistoryItem.h"


@implementation OrderHistoryItem

@dynamic activeST;
@dynamic chairid;
@dynamic itemDesID;
@dynamic itemName;
@dynamic itemQuantity;
@dynamic iterm_id;
@dynamic orderId;
@dynamic price;
@dynamic itemNote;
@dynamic itemLocation;
@dynamic itemDiscount;
@dynamic defaultDiscountStatus;
@end
