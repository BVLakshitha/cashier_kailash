//
//  OrderItem.h
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FoodIterm, Order;

@interface OrderItem : NSManagedObject

@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) FoodIterm *objFoodIterm;
@property (nonatomic, retain) Order *objOrder;

@end
