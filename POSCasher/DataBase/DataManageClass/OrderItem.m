//
//  OrderItem.m
//  resbookWaiter
//
//  Created by Tharaka Dushmantha on 7/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import "OrderItem.h"
#import "FoodIterm.h"
#import "Order.h"


@implementation OrderItem

@dynamic quantity;
@dynamic objFoodIterm;
@dynamic objOrder;

@end
