//
//  OrderList.h
//  Cashier
//
//  Created by Lakshitha on 7/7/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface OrderList : NSManagedObject

@property (nonatomic, retain) NSString * activeSt;
@property (nonatomic, retain) NSString * chaircount;
@property (nonatomic, retain) NSString * createDate;
@property (nonatomic, retain) NSString * createTime;
@property (nonatomic, retain) id customer;
@property (nonatomic, retain) NSString * deliveryDate;
@property (nonatomic, retain) NSString * deliveryType;
@property (nonatomic, retain) NSString * eatintype;
@property (nonatomic, retain) NSString * orderId;
@property (nonatomic, retain) NSString * paymentSt;
@property (nonatomic, retain) NSString * stetus;
@property (nonatomic, retain) NSString * tableid;
@property (nonatomic, retain) NSString * totalValue;
@property (nonatomic, retain) NSString * deliveryAddress;
@property (nonatomic, retain) NSString * deliveryName;
@property (nonatomic, retain) NSString * deliveryPhone;
@property (nonatomic, retain) NSString * deliveryCost;
@property (nonatomic, retain) NSString * deliveryEmail;

@end
