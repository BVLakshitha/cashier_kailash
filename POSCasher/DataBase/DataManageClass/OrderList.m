//
//  OrderList.m
//  Cashier
//
//  Created by Lakshitha on 7/7/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import "OrderList.h"


@implementation OrderList

@dynamic activeSt;
@dynamic chaircount;
@dynamic createDate;
@dynamic createTime;
@dynamic customer;
@dynamic deliveryDate;
@dynamic deliveryType;
@dynamic eatintype;
@dynamic orderId;
@dynamic paymentSt;
@dynamic stetus;
@dynamic tableid;
@dynamic totalValue;
@dynamic deliveryAddress;
@dynamic deliveryName;
@dynamic deliveryPhone;
@dynamic deliveryCost;
@dynamic deliveryEmail;

@end
