//
//  PaymentDetails.h
//  Cashier
//
//  Created by Lakshitha on 18/12/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PaymentDetails : NSManagedObject

@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * interest;
@property (nonatomic, retain) NSString * paytype;
@property (nonatomic, retain) NSNumber * recordID;
@property (nonatomic, retain) NSString * typrdetails;
@property (nonatomic, retain) NSString * subtype;

@end
