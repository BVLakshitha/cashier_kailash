//
//  PaymentDetails.m
//  Cashier
//
//  Created by Lakshitha on 18/12/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "PaymentDetails.h"


@implementation PaymentDetails

@dynamic amount;
@dynamic interest;
@dynamic paytype;
@dynamic recordID;
@dynamic typrdetails;
@dynamic subtype;

@end
