//
//  PrinterFeeds.h
//  
//
//  Created by Lakshi Ben on 4/29/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PrinterFeeds : NSManagedObject

@property (nonatomic, retain) NSNumber * itemID;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSString * itemNote;
@property (nonatomic, retain) NSString * itemQty;
@property (nonatomic, retain) NSString * itemRecordID;
@property (nonatomic, retain) NSString * itemLocation;

@end
