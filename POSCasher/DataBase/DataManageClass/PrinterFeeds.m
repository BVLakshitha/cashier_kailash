//
//  PrinterFeeds.m
//  
//
//  Created by Lakshi Ben on 4/29/15.
//
//

#import "PrinterFeeds.h"


@implementation PrinterFeeds

@dynamic itemID;
@dynamic itemName;
@dynamic itemNote;
@dynamic itemQty;
@dynamic itemRecordID;
@dynamic itemLocation;

@end
