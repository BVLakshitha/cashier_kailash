//
//  SelectOrder.h
//  Cashier
//
//  Created by User on 16/02/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SelectOrder : NSManagedObject

@property (nonatomic, retain) NSString * categoryId;
@property (nonatomic, retain) NSString * chairName;
@property (nonatomic, retain) NSString * descriptionID;
@property (nonatomic, retain) NSString * img_URL;
@property (nonatomic, retain) NSString * orderdescription;
@property (nonatomic, retain) NSString * orderId;
@property (nonatomic, retain) NSString * orderName;
@property (nonatomic, retain) NSString * ordernote;
@property (nonatomic, retain) NSString * orderPrice;
@property (nonatomic, retain) NSNumber * orderrQuantity;
@property (nonatomic, retain) NSString * ordertype;
@property (nonatomic, retain) NSString * recodId;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * tableName;
@property (nonatomic, retain) NSString * totalPrice;
@property (nonatomic, retain) NSString * cancelStatus;
@property (nonatomic, retain) NSString * itenLocation;
@property (nonatomic, retain) NSString * itemDiscount;
@property (nonatomic, retain) NSString * defaultDiscountStatus;

@end
