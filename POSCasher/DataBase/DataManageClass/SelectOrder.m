//
//  SelectOrder.m
//  Cashier
//
//  Created by User on 16/02/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "SelectOrder.h"


@implementation SelectOrder

@dynamic categoryId;
@dynamic chairName;
@dynamic descriptionID;
@dynamic img_URL;
@dynamic orderdescription;
@dynamic orderId;
@dynamic orderName;
@dynamic ordernote;
@dynamic orderPrice;
@dynamic orderrQuantity;
@dynamic ordertype;
@dynamic recodId;
@dynamic status;
@dynamic tableName;
@dynamic totalPrice;
@dynamic cancelStatus;
@dynamic itenLocation;
@dynamic itemDiscount;
@dynamic defaultDiscountStatus;

@end
