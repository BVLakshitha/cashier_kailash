//
//  SplitData.h
//  Cashier
//
//  Created by User on 20/02/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SplitData : NSManagedObject

@property (nonatomic, retain) NSString * divQty;
@property (nonatomic, retain) NSString * isPaid;
@property (nonatomic, retain) NSString * isSplited;
@property (nonatomic, retain) NSString * itemDisID;
@property (nonatomic, retain) NSString * itemname;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * qty;
@property (nonatomic, retain) NSNumber * splitID;
@property (nonatomic, retain) NSNumber * splitOrderID;
@property (nonatomic, retain) NSString * itemRecordID;

@end
