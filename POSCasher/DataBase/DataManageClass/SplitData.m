//
//  SplitData.m
//  Cashier
//
//  Created by User on 20/02/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "SplitData.h"


@implementation SplitData

@dynamic divQty;
@dynamic isPaid;
@dynamic isSplited;
@dynamic itemDisID;
@dynamic itemname;
@dynamic price;
@dynamic qty;
@dynamic splitID;
@dynamic splitOrderID;
@dynamic itemRecordID;

@end
