//
//  TableReceived.h
//  POSCasher
//
//  Created by orderfood on 26/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TableReceived : NSManagedObject

@property (nonatomic, retain) NSString * avilablechairs;
@property (nonatomic, retain) NSString * chairs;
@property (nonatomic, retain) NSString * emtChairs;
@property (nonatomic, retain) NSString * orderid;
@property (nonatomic, retain) NSString * tableId;
@property (nonatomic, retain) NSString * tableName;
@property (nonatomic, retain) NSString * tableStetus;
@property (nonatomic, retain) NSString * tblstatus;
@property (nonatomic, retain) NSString * waiterid;
@property (nonatomic, retain) NSString * locationID;

@end
