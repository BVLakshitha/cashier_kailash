//
//  DetailViewController.h
//  DetailViewSwitch
//
//  Created by Tim Harris on 1/17/14.
//  Copyright (c) 2014 Tim Harris. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *PrinterModeTL;
@property (weak, nonatomic) IBOutlet UILabel *PrinterMode_K;

@property (weak, nonatomic) IBOutlet UILabel *CashierPrinterTL;
@property (weak, nonatomic) IBOutlet UILabel *KitchenPrinterTL;


@property (weak, nonatomic) IBOutlet UIButton *btncashierPrinter;
@property (weak, nonatomic) IBOutlet UIButton *btnkitchenPrinter;
@property (weak, nonatomic) IBOutlet UIButton *btnmodeClicks;

- (IBAction)btnPrinterModeClicks:(id)sender;

- (IBAction)btnCashierPrinterClicks:(id)sender;
- (IBAction)btnCashierPrinterSRClicks:(id)sender;


- (IBAction)btnkitchenPrinterClicks:(id)sender;
- (IBAction)btnKitchenPrinterSRCliks:(id)sender;


@property (weak, nonatomic) IBOutlet UISwitch *CashierPrinterSW;
- (IBAction)CashierPrinterSWClicks:(id)sender;
- (IBAction)KitchenPrinterClicks:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *KitchenPrinterSW;

@property (weak, nonatomic) IBOutlet UILabel *CPStatusTL;
@property (weak, nonatomic) IBOutlet UILabel *KPStatusTL;

@property (weak, nonatomic) IBOutlet UILabel *CashDrowerPTL;
@property (weak, nonatomic) IBOutlet UISwitch *CashDrowerSW;

@property (weak, nonatomic) IBOutlet UISwitch *CKOTSW;
@property (weak, nonatomic) IBOutlet UILabel *CKOTPTL;
- (IBAction)CKOTStatusAction:(id)sender;


- (IBAction)CashDrowerSWClicks:(id)sender;


@end
