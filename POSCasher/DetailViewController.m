//
//  DetailViewController.m
//  DetailViewSwitch
//
//  Created by Tim Harris on 1/17/14.
//  Copyright (c) 2014 Tim Harris. All rights reserved.
//

#import "DetailViewController.h"
#import "PrinterDetailsVC.h"
#import "Constants.h"
#import "PrinterFunctions.h"
#import "AppDelegate.h"
#import "AppUserDefaults.h"


@interface DetailViewController()

@end


@implementation DetailViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self btnOrganizer];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)btnOrganizer{
    
    self.btnmodeClicks.layer.cornerRadius = 7.0f;
    self.btnmodeClicks.layer.masksToBounds = YES;

    self.btncashierPrinter.layer.cornerRadius = 7.0f;
    self.btncashierPrinter.layer.masksToBounds = YES;
    
    self.btnkitchenPrinter.layer.cornerRadius = 7.0f;
    self.btnkitchenPrinter.layer.masksToBounds = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    
    NSString * printermode_C = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_C];
    NSString * printermode_K = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_K];
    NSDictionary * printers = nil;
    NSString * C_printername = @"";
    NSString * K_printername = @"";
    
    [self setcasherSwitch];
    [self setkitchenSwitch];
    [self setKOTCasherSwitch];
    [self CashDrowerStatus];
    
    if (PRINTERMODE_C != nil)self.PrinterModeTL.text = printermode_C;
    if (PRINTERMODE_K != nil)self.PrinterMode_K.text = printermode_K;
    
    if (CASHERPRINTER != nil){
        printers = [[NSUserDefaults standardUserDefaults]objectForKey:CASHERPRINTER];
        C_printername = [printers objectForKey:@"modelName"];
    }
    
    if (KITCHENPRINTER != nil){
        printers = [[NSUserDefaults standardUserDefaults]objectForKey:KITCHENPRINTER];
        K_printername = [printers objectForKey:@"modelName"];
    }
    self.CashierPrinterTL.text = C_printername;
    self.KitchenPrinterTL.text = K_printername;
}
#pragma mark -
#pragma mark SamplePrint
- (IBAction)btnCashierPrinterSRClicks:(id)sender{
    [self setPortInfo:CASHERPRINTER];
}
- (IBAction)btnKitchenPrinterSRCliks:(id)sender{
     [self setPortInfo:KITCHENPRINTER];
}
#pragma mark -
#pragma mark SetUpPortSettings
+ (void)setPortName:(NSString *)m_portName
{
    [AppDelegate setPortName:m_portName];
}

+ (void)setPortSettings:(NSString *)m_portSettings
{
    [AppDelegate setPortSettings:m_portSettings];
}
- (void)setPortInfo :(NSString *)printertype
{
    NSDictionary * printer = nil;
    NSString * p_portName = @"";
    NSString * p_portSettings = @"";

    
    if ([printertype isEqualToString:CASHERPRINTER]) {
        printer = [[NSUserDefaults standardUserDefaults] objectForKey:CASHERPRINTER];
        
        NSString * printermode = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_C];
        
        p_portName = [printer objectForKey:@"portName"];
        p_portSettings = @"Standard";
        [DetailViewController setPortName:p_portName];
        [DetailViewController setPortSettings:p_portSettings];
        
        if ([printermode isEqualToString:@"Line Mode"]) {
            [self CashierPrinterSamplePrintLinePrinter];
        }else{
            [self CashierPrinterSamplePrintRasterPrinter];
        }
        
    }else if ([printertype isEqualToString:KITCHENPRINTER]){
        printer = [[NSUserDefaults standardUserDefaults] objectForKey:KITCHENPRINTER];
        
        NSString * printermode = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_K];
        
        p_portName = [printer objectForKey:@"portName"];
        p_portSettings = @"Standard";
        [DetailViewController setPortName:p_portName];
        [DetailViewController setPortSettings:p_portSettings];
        
        if ([printermode isEqualToString:@"Line Mode"]) {
            [self CashierPrinterSamplePrintLinePrinter];
        }else{
            [self CashierPrinterSamplePrintRasterPrinter];
        }
    }
}

-(void)CashierPrinterSamplePrintRasterPrinter{
    
    NSString *portName = [AppDelegate getPortName];
    NSString *portSettings = [AppDelegate getPortSettings];
    
    [PrinterFunctions PrintRasterSampleReceipt3InchWithPortname:portName portSettings:portSettings];
}
-(void)CashierPrinterSamplePrintLinePrinter{
    
    NSString *portName = [AppDelegate getPortName];
    NSString *portSettings = [AppDelegate getPortSettings];
    
    [PrinterFunctions PrintSampleReceipt3InchWithPortname:portName portSettings:portSettings];
}

- (IBAction)btnPrinterModeClicks:(id)sender {
    
//    PrinterDetailsVC * Psettings = [self.storyboard instantiateViewControllerWithIdentifier:@"PrinterDetailsVC"];
//    Psettings.model = @"1";
//    [self.navigationController pushViewController:Psettings animated:YES];
    
}

- (IBAction)btnCashierPrinterClicks:(id)sender {
    
//    PrinterDetailsVC * Psettings = [self.storyboard instantiateViewControllerWithIdentifier:@"PrinterDetailsVC"];
//    Psettings.model = @"2";
//    [self.navigationController pushViewController:Psettings animated:YES];
}



- (IBAction)btnkitchenPrinterClicks:(id)sender {
    
//    PrinterDetailsVC * Psettings = [self.storyboard instantiateViewControllerWithIdentifier:@"PrinterDetailsVC"];
//    Psettings.model = @"3";
//    [self.navigationController pushViewController:Psettings animated:YES];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"PrinterMode_C"]){
        PrinterDetailsVC * printerdetails = [segue destinationViewController];
        [printerdetails setModel:@"1"];
        
    }else if ([[segue identifier] isEqualToString:@"PrinterMode_K"]){
        PrinterDetailsVC * printerdetails = [segue destinationViewController];
        [printerdetails setModel:@"2"];
        
    }else if ([[segue identifier] isEqualToString:@"PrinterSearch_C"]){
        PrinterDetailsVC * printerdetails = [segue destinationViewController];
        [printerdetails setModel:@"3"];
        
    }else if ([[segue identifier] isEqualToString:@"PrinterSearch_K"]){
        PrinterDetailsVC * printerdetails = [segue destinationViewController];
        [printerdetails setModel:@"4"];
    }
    
}
- (IBAction)CashierPrinterSWClicks:(id)sender {
    UISwitch *FirstSwitch = (UISwitch *)sender;
    
    if (FirstSwitch.on == YES) {
        [AppUserDefaults setPrinterStatus:PRINTERON :CASHERPRINTER];
        self.CPStatusTL.text = @"ON";
    }else{
        [AppUserDefaults setPrinterStatus:PRINTEROFF :CASHERPRINTER];
        self.CPStatusTL.text = @"OFF";
    }
    
}

- (IBAction)KitchenPrinterClicks:(id)sender {
    
    UISwitch *FirstSwitch = (UISwitch *)sender;
    
    if (FirstSwitch.on == YES) {
        [AppUserDefaults setPrinterStatus:PRINTERON :KITCHENPRINTER];
        self.KPStatusTL.text = @"ON";
    }else{
        [AppUserDefaults setPrinterStatus:PRINTEROFF :KITCHENPRINTER];
        self.KPStatusTL.text = @"OFF";
    }
}
-(void)setcasherSwitch{
    
    NSString * status = [AppUserDefaults getPrinterStatus:CASHERPRINTER];
    
    if ([status isEqualToString:PRINTERON]) {
         [self.CashierPrinterSW setOn:YES animated:YES];
        self.CPStatusTL.text = @"ON";
    }else{
        [self.CashierPrinterSW setOn:NO animated:YES];
        self.CPStatusTL.text = @"OFF";
    }
}
-(void)setkitchenSwitch{
    
    NSString * status = [AppUserDefaults getPrinterStatus:KITCHENPRINTER];
    
    if ([status isEqualToString:PRINTERON]) {
       [self.KitchenPrinterSW setOn:YES animated:YES];
        self.KPStatusTL.text = @"ON";
    }else{
       [self.KitchenPrinterSW setOn:NO animated:YES];
        self.KPStatusTL.text = @"OFF";
    }
}
-(void)setKOTCasherSwitch{
    
    NSString * status = [AppUserDefaults getKOTPrintinCashier];
    
    if ([status isEqualToString:PRINTERON]) {
        [self.CKOTSW setOn:YES animated:YES];
        self.CKOTPTL.text = @"ON";
    }else{
        [self.CKOTSW setOn:NO animated:YES];
        self.CKOTPTL.text = @"OFF";
    }
}

#pragma mark -
#pragma mark KOT in Cashier Printer

- (IBAction)CKOTStatusAction:(id)sender {
    
    UISwitch *FirstSwitch = (UISwitch *)sender;
    if (FirstSwitch.on == YES) {
        [AppUserDefaults setKOTPrintinCashierStatus:@"ON"];
        self.CKOTPTL.text = @"ON";
    }else{
        [AppUserDefaults setKOTPrintinCashierStatus:@"OFF"];
        self.CKOTPTL.text = @"OFF";
    }
}

#pragma mark -
#pragma mark CashDrower

- (IBAction)CashDrowerSWClicks:(id)sender {
    UISwitch *FirstSwitch = (UISwitch *)sender;
    if (FirstSwitch.on == YES) {
        [AppUserDefaults setCashDrawerStatus:@"YES"];
        self.CashDrowerPTL.text = @"YES";
    }else{
        [AppUserDefaults setCashDrawerStatus:@"NO"];
        self.CashDrowerPTL.text = @"NO";
    }
}
-(void)CashDrowerStatus{
    if ([[AppUserDefaults getCashDrawerStatus]isEqualToString:@"YES"]) {
        [self.CashDrowerSW setOn:YES animated:YES];
        self.CashDrowerPTL.text = @"YES";
    }else{
        [self.CashDrowerSW setOn:NO animated:YES];
        self.CashDrowerPTL.text = @"NO";
    }
}
@end
