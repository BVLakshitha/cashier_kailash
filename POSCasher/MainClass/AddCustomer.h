//
//  AddCustomer.h
//  POSCasher
//
//  Created by orderfood on 10/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "WebserviceApi.h"
#import "MBProgressHUD.h"
#import "CustomerCell.h"

@protocol AddcustomerViewDelegate <NSObject>

-(void)didAnimationOpenAddcustomer :(BOOL)isOpenAddcustomer;
-(void)didSelectCustomer :(BOOL)isSelected;

@end

@interface AddCustomer : UIView<UITableViewDataSource,UITableViewDelegate,webservicedelegate,UITextFieldDelegate,MBProgressHUDDelegate,UIGestureRecognizerDelegate,CustomerCellDelegate>{
    
    DBManager * dbmanager;
    WebserviceApi * webserviceapi;
    NSMutableArray * allcustomers;
    NSMutableArray * filtedCustomers;
    MBProgressHUD * HUD;
    UITapGestureRecognizer *backgroungtap;
    NSDictionary * cusDetails;
    BOOL isEditCustomer;
    int selectedCustomerID;
    BOOL isme;
    
    
    NSMutableDictionary *customersectiontitles;
    NSArray * customersections;
}

-(void)AnimatetableviewOpen:(BOOL)isUp;
@property ( nonatomic, retain ) id <AddcustomerViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *FNameTF;
@property (weak, nonatomic) IBOutlet UITextField *LNameTF;
@property (weak, nonatomic) IBOutlet UITextField *AddressTF;
@property (weak, nonatomic) IBOutlet UITextField *DoorNoTF;
@property (weak, nonatomic) IBOutlet UITextField *CityTF;
@property (weak, nonatomic) IBOutlet UITextField *StateTF;
@property (weak, nonatomic) IBOutlet UITextField *Email;
@property (weak, nonatomic) IBOutlet UITextField *PhoneTF;


@property (weak, nonatomic) IBOutlet UIButton *btnCloseCustomer;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UISegmentedControl *CustomerSegmantController;
@property (weak, nonatomic) IBOutlet UIView *EnterView;
@property (weak, nonatomic) IBOutlet UIView *SearchView;

@property (weak, nonatomic) IBOutlet UITableView *CustomerTV;

@property (weak, nonatomic) IBOutlet UISearchBar *customSearchbar;
@property (nonatomic, assign) bool isFiltered;
//@property(nonatomic,retain) IBOutlet UISearchDisplayController * searchDisplayController;

- (IBAction)CSCClicks:(id)sender;
- (IBAction)btnCloseClicks:(id)sender;
- (IBAction)btnSaveClicks:(id)sender ;


-(void)removeSecectedCustomer;
@end
