//
//  AddCustomer.m
//  POSCasher
//
//  Created by orderfood on 10/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "AddCustomer.h"
#import "CustomerCell.h"
#import "Customer.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation AddCustomer

@synthesize isFiltered;

#pragma mark main view actions
#pragma mark -

-(void)awakeFromNib{
    
    NSLog(@"View Awark");
    
    [self managetextfiedBG];
//    [self AnimatetableviewOpen:NO];
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi GetCustomers:nil];
    
    self.CustomerTV.delegate = self;
    self.CustomerTV.dataSource = self;
    isEditCustomer = NO;
    

    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}
-(void)managetextfiedBG{
    
    self.FNameTF.layer.masksToBounds = YES;
    self.FNameTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.FNameTF.layer.borderWidth = 1.0f;
    
    self.LNameTF.layer.masksToBounds = YES;
    self.LNameTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.LNameTF.layer.borderWidth = 1.0f;
    
    self.AddressTF.layer.masksToBounds = YES;
    self.AddressTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.AddressTF.layer.borderWidth = 1.0f;
    
    self.DoorNoTF.layer.masksToBounds = YES;
    self.DoorNoTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.DoorNoTF.layer.borderWidth = 1.0f;
    
    self.CityTF.layer.masksToBounds = YES;
    self.CityTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.CityTF.layer.borderWidth = 1.0f;
    
    self.StateTF.layer.masksToBounds = YES;
    self.StateTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.StateTF.layer.borderWidth = 1.0f;
    
    self.Email.layer.masksToBounds = YES;
    self.Email.layer.borderColor = [[UIColor grayColor] CGColor];
    self.Email.layer.borderWidth = 1.0f;
    
    self.PhoneTF.layer.masksToBounds = YES;
    self.PhoneTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.PhoneTF.layer.borderWidth = 1.0f;
    

    self.btnCloseCustomer.layer.masksToBounds = YES;
    self.btnCloseCustomer.layer.cornerRadius = 4;
    
    self.btnSave.layer.masksToBounds = YES;
    self.btnSave.layer.cornerRadius = 4;
    
    
}
-(void)addTapGesture:(BOOL)isUP{
    
    if (isUP) {
        if (!backgroungtap)backgroungtap = [[UITapGestureRecognizer alloc] initWithTarget:self.viewForBaselineLayout action:@selector(DidClickBackGround)];
        
        [self addGestureRecognizer:backgroungtap];
    }else{
        [self removeGestureRecognizer:backgroungtap];
    }
    
}
-(void)AnimatetableviewOpen:(BOOL)isUp{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"SCD"]!= nil) {
        [self.btnSave setTitle:@"Remove" forState:UIControlStateNormal];
        [self showCustomerDetails];
        self.CustomerSegmantController.selectedSegmentIndex = 0;
        self.EnterView.hidden = NO;
        self.SearchView.hidden = YES;
        
    }else{
        [self.btnSave setTitle:@"Save" forState:UIControlStateNormal];
    }
    
    
    
    if (isUp) {
        
        self.hidden = NO;
        [self.delegate didAnimationOpenAddcustomer:YES];
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,102, 709, 627);
                         }
                         completion:^(BOOL finished){
                             isme =YES;
                         }
         ];
    }else{
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,768, 709, 627);
                             
                         }
                         completion:^(BOOL finished){
                             [self.delegate didAnimationOpenAddcustomer:NO];
                             isme =NO;
                         }
         ];
    }
}
- (IBAction)btnCloseClicks:(id)sender {
    [self AnimatetableviewOpen:NO];
    [self cleanText];
}
- (IBAction)CSCClicks:(id)sender {
    if (self.CustomerSegmantController.selectedSegmentIndex == 1) {
        self.EnterView.hidden = YES;
        self.SearchView.hidden = NO;
    }else{
        self.EnterView.hidden = NO;
        self.SearchView.hidden = YES;
    }
}

#pragma mark main view actions
#pragma mark -
-(void)DidReceiveAllCustomers:(BOOL)isFatched{
    
    if (isFatched) {
        [self loadCustomerDetails];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"System Offline Please try again later." delegate:self cancelButtonTitle:@"dismiss" otherButtonTitles:nil, nil];
        [alert show];
        [self removeHUD];
    }
    
}

-(void)validateCustomerDetails{
    BOOL isAllOk = YES;
    NSString * wrongfield = @"";
    
    
    NSString * firstname = self.FNameTF.text;
    NSString * lastname = self.LNameTF.text;
    NSString * address = self.AddressTF.text;
    NSString * DoorNo = self.DoorNoTF.text;
    NSString * city = self.CityTF.text;
    NSString * email = self.Email.text;
    NSString * phoneno = self.PhoneTF.text;
    NSString * postalcode = self.StateTF.text;
    
    
    if (![self NSStringIsValidEmail:email]){
        isAllOk = NO;
        wrongfield = @"Email";
        
        self.Email.layer.masksToBounds = YES;
        self.Email.layer.borderColor = ([[UIColor redColor] CGColor]);
        self.Email.layer.borderWidth = 1.0f;
        
    }
    
    if (![self validatePhone:phoneno]){
        isAllOk = NO;
        wrongfield = @"Contact number";
        
        self.PhoneTF.layer.masksToBounds = YES;
        self.PhoneTF.layer.borderColor = ([[UIColor redColor] CGColor]);
        self.PhoneTF.layer.borderWidth = 1.0f;
        
    }
    
    if (firstname.length < 2){
        isAllOk = NO;
        wrongfield = @"First name";
        
        self.FNameTF.layer.masksToBounds = YES;
        self.FNameTF.layer.borderColor = ([[UIColor redColor] CGColor]);
        self.FNameTF.layer.borderWidth = 1.0f;
    }
    
    
    if (isAllOk) {
        
        if (lastname == nil)lastname = @"";
        if (address == nil)address = @"";
        if (DoorNo == nil)DoorNo = @"";
        if (city == nil)city = @"";
        if (postalcode == nil)postalcode = @"";
        
        
        if (isEditCustomer) {
            
            if (!dbmanager)dbmanager = [[DBManager alloc]init];
            Customer * customer = [dbmanager isRecordExistanceCustomer:[NSString stringWithFormat:@"%i",selectedCustomerID]];
            
            cusDetails = @{@"fname"         : firstname,
                           @"lName"         : lastname,
                           @"userId"        : customer.userID,
                           @"email"         : email,
                           @"mobNo"         : phoneno,
                           @"floor"         : @"0",
                           @"doorNo"        : DoorNo,
                           @"street"        : address,
                           @"postal_code"   : postalcode,
                           @"key"           : WEBSERVICEKEY,
                           @"city"          : city,
                           @"isEdit"        : [NSNumber numberWithInt:1]};
            
        }else{
            
            cusDetails = @{@"fname"         : firstname,
                           @"lName"         : lastname,
                           @"email"         : email,
                           @"mobNo"         : phoneno,
                           @"floor"         : @"0",
                           @"doorNo"        : DoorNo,
                           @"street"        : address,
                           @"postal_code"   : postalcode,
                           @"key"           : WEBSERVICEKEY,
                           @"city"          : city,
                           @"isEdit"         : @"0"};
            
        }
        
        
        
        if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
        [webserviceapi RegisterCustomer:cusDetails];
        [self initHUDWithTitle:@"Register customer."];
    }else{
        
        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Oops!" message:[NSString stringWithFormat:@"%@ is incorrect.",wrongfield] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertview show];
    }
    
    
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    BOOL isvalidPhonenumber = YES;
    
    NSString *phoneRegex = @"^([+-]?)(?:|0|[1-9]\\d*)?$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    
    if ([phoneTest evaluateWithObject:phoneNumber]) {
        
        if (phoneNumber.length > 8) {
            isvalidPhonenumber = YES;
        }else{
            isvalidPhonenumber = NO;
        }
    }else{
        isvalidPhonenumber = NO;
    }
    
    return isvalidPhonenumber;
}
-(void)DidReceiveRegister:(id)respond{
    
    NSString * status = [respond objectForKey:@"result"];
    
    if ([status isEqualToString:@"Error"]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Customer Registration fail." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        [self removeHUD];
    }else{
        [webserviceapi GetCustomers:nil];
    }
    
}
#pragma mark textfieldDelegate
#pragma mark -

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [self textfieldChangeframe:@"all" :[UIColor grayColor]];
    [self ViewmoveWithKeyBord:textField.tag];
    return YES;
}

-(void)textfieldChangeframe :(NSString *)fields :(UIColor *)colour{
    
    if ([fields isEqualToString:@"email"]) {
        
        self.Email.layer.masksToBounds = YES;
        self.Email.layer.borderColor = [colour CGColor];
        self.Email.layer.borderWidth = 1.0f;
        
    }else{
        self.FNameTF.layer.masksToBounds = YES;
        self.FNameTF.layer.borderColor = [colour CGColor];
        self.FNameTF.layer.borderWidth = 1.0f;
        
        self.Email.layer.masksToBounds = YES;
        self.Email.layer.borderColor = [colour CGColor];
        self.Email.layer.borderWidth = 1.0f;
        
        self.PhoneTF.layer.masksToBounds = YES;
        self.PhoneTF.layer.borderColor = [colour CGColor];
        self.PhoneTF.layer.borderWidth = 1.0f;
    }
    
}
-(void)ViewmoveWithKeyBord :(int)tagvalue{
    
    if (tagvalue == 1) {
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,60, 709, 627);
                         }
                         completion:^(BOOL finished){
                             [self addTapGesture:YES];
                         }
         ];
        
    }else if (tagvalue == 2){
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,-15, 709, 627);
                         }
                         completion:^(BOOL finished){
                            [self addTapGesture:YES];
                         }
         ];
        
    }else if (tagvalue == 3){
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,-90, 709, 627);
                         }
                         completion:^(BOOL finished){
                             [self addTapGesture:YES];
                         }
         ];
        
    }else if (tagvalue == 4){
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,-165, 709, 627);
                         }
                         completion:^(BOOL finished){
                             [self addTapGesture:YES];
                         }
         ];
        
    }else if (tagvalue == 1000){
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(158,102, 709, 627);
                         }
                         completion:^(BOOL finished){
                            [self addTapGesture:NO];
                         }
         ];
    }
    
}
-(void)keyboardWillHide{
    
    if (isme)[self ViewmoveWithKeyBord:1000];
}
-(void)DidClickBackGround{
    [self ViewmoveWithKeyBord:1000];

    [self.FNameTF resignFirstResponder];
    [self.LNameTF resignFirstResponder];
    [self.AddressTF resignFirstResponder];
    [self.DoorNoTF resignFirstResponder];
    [self.CityTF resignFirstResponder];
    [self.Email resignFirstResponder];
    [self.PhoneTF resignFirstResponder];
    
}
-(void)cleanText{
    [self.FNameTF setText:@""];
    [self.LNameTF setText:@""];
    [self.AddressTF setText:@""];
    [self.DoorNoTF setText:@""];
    [self.CityTF setText:@""];
    [self.StateTF setText:@""];
    [self.Email setText:@""];
    [self.PhoneTF setText:@""];
}
- (IBAction)btnSaveClicks:(id)sender {
    
    if (![self.btnSave.titleLabel.text isEqualToString:@"Save"]) {
        [self removeSecectedCustomer];
        [self.btnSave setTitle:@"Save" forState:UIControlStateNormal];
    }else{
       [self validateCustomerDetails];
    }
}
-(void)iconChangeAddCustomer :(BOOL)isAdded{
    
    if (isAdded) {
        [self.delegate didSelectCustomer:YES];
    }else{
        [self.delegate didSelectCustomer:NO];
    }
    
}

#pragma mark Customer tableview
#pragma mark -

-(void)loadCustomerDetails{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSArray * Customers = [dbmanager GetallCustomers];
    if (!allcustomers)allcustomers = [[NSMutableArray alloc]init];
    
    [allcustomers removeAllObjects];
    
    [allcustomers addObjectsFromArray:[self dividetoSections:Customers]];
    [self.CustomerTV reloadData];
    [self removeHUD];
    [self cleanText];
    isEditCustomer = NO;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (self.isFiltered) {
        return 1;
    }else{
        return [customersections count];
    }
   
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return [customersections objectAtIndex:section];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.isFiltered) {
        return [filtedCustomers count];
        
    } else {
        
        NSString * sectiontitle = [customersections objectAtIndex:section];
        NSArray * sectioncustomer = [customersectiontitles objectForKey:sectiontitle];
        return [sectioncustomer count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

        static NSString *simpleTableIdentifier = @"CustomerCell";
        CustomerCell *cell = (CustomerCell *)[self.CustomerTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.delegate = self;
            
        }
    
    Customer * newCustomer;
    
    if (isFiltered) {
        newCustomer = [filtedCustomers objectAtIndex:indexPath.row];
    }else{
        
        NSString * sectiontitle = [customersections objectAtIndex:indexPath.section];
        NSArray * sectioncustomer = [customersectiontitles objectForKey:sectiontitle];
        newCustomer = [sectioncustomer objectAtIndex:indexPath.row];
    }
    
    cell.customerIDTL.text = newCustomer.userID.stringValue;
    cell.UserNameTL.text = [NSString stringWithFormat:@"%@ %@",newCustomer.firstName,newCustomer.lastName];
    cell.AddressTL.text = newCustomer.address;
    cell.DoorNOTL.text = newCustomer.dono;
    cell.CityTL.text = newCustomer.city;
    cell.PostalCodeTL.text = newCustomer.postalcode;
    cell.EmailTL.text = newCustomer.email;
    cell.ContactNoTL.text = newCustomer.contactNo;
    cell.customerid = newCustomer.userID.stringValue;
    
    if (newCustomer.contactNo == nil) {
        cell.ContactNoTL.text = @"No Contact.";
    }else{
        cell.ContactNoTL.text = newCustomer.contactNo;
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    return customersections;
}
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        filtedCustomers = [[NSMutableArray alloc] init];
        
        for (Customer* customer in allcustomers)
        {
            NSRange nameRange = [customer.firstName rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [customer.lastName rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [filtedCustomers addObject:customer];
            }
        }
    }
    
    [self.CustomerTV reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Customer * newCustomer;
    
    if (isFiltered) {
        newCustomer = [filtedCustomers objectAtIndex:indexPath.row];
    }else{
        NSString * sectiontitle = [customersections objectAtIndex:indexPath.section];
        NSArray * sectioncustomer = [customersectiontitles objectForKey:sectiontitle];
        newCustomer = [sectioncustomer objectAtIndex:indexPath.row];
    }
    
    [self SaveSelectedCustomer:newCustomer];
}

-(void)SaveSelectedCustomer :(Customer *)customer{
    
    NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
    
//    NSLog(@"Selected id  = %@",customer.userID.stringValue);

    [customerdefalut setObject:customer.userID forKey:@"SCD"];

    if ([customerdefalut synchronize]) {
        [self AnimatetableviewOpen:NO];
        [self iconChangeAddCustomer:YES];
    }
}
-(void)showCustomerDetails{
    
    NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
    NSString * CID = [customerdefalut objectForKey:@"SCD"];
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    Customer * customer = [dbmanager isRecordExistanceCustomer:CID];
    
    if (customer != nil) {
        [self.FNameTF setText:customer.firstName];
        [self.LNameTF setText:customer.lastName];
        [self.AddressTF setText:customer.address];
        [self.DoorNoTF setText:customer.dono];
        [self.CityTF setText:customer.city];
        [self.StateTF setText:customer.postalcode];
        [self.Email setText:customer.email];
        [self.PhoneTF setText:customer.contactNo];
    }else{
        
    }
}
-(void)removeSecectedCustomer{
    NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
    [customerdefalut removeObjectForKey:@"SCD"];
    [self cleanText];
    if ([customerdefalut synchronize])[self iconChangeAddCustomer:NO];
}
#pragma mark -
#pragma mark Filtere customers
-(NSArray *)dividetoSections :(NSArray *)customers{
    
    NSMutableArray  * filtedCustomerslist = [[NSMutableArray alloc]init];
    
    if (!customersectiontitles)customersectiontitles = [[NSMutableDictionary alloc]init];
    
    [customersectiontitles removeAllObjects];
    
    NSArray * Alphabet = [[NSArray alloc]initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",
                          @"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z", nil];
    

    for (NSString * letter in Alphabet) {
        
        NSMutableArray * objects = [[NSMutableArray alloc]init];
        [objects removeAllObjects];
        
        for (Customer * selectedCus in customers) {
        
            
            
            char firstchair = [[selectedCus.firstName uppercaseString] characterAtIndex:0];
            char letterC = [[letter uppercaseString] characterAtIndex:0];
            if (firstchair == letterC) {
                
                [objects addObject:selectedCus]; //Section Array
                [filtedCustomerslist addObject:selectedCus]; //Array for Seach and tap Gesture
                
//                NSLog(@"selectedCus firstname %@ == %@",selectedCus.firstName,letter);
//                NSLog(@"lettr %@ added",letter);
            }
        }
        if ([objects count] > 0) {
            [customersectiontitles setObject:objects forKey:letter];
        }
    }
    
    customersections = [[customersectiontitles allKeys]sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

//    NSLog(@"FINEL DIC %@",customersectiontitles);
    
    return filtedCustomerslist;
}
-(void)didSelectEditincustomer:(NSString *)CUSID{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    Customer * customer = [dbmanager isRecordExistanceCustomer:CUSID];
    
    if (customer != nil) {
        [self.FNameTF setText:customer.firstName];
        [self.LNameTF setText:customer.lastName];
        [self.AddressTF setText:customer.address];
        [self.DoorNoTF setText:customer.dono];
        [self.CityTF setText:customer.city];
        [self.StateTF setText:customer.postalcode];
        [self.Email setText:customer.email];
        [self.PhoneTF setText:customer.contactNo];
        selectedCustomerID = customer.userID.intValue;
    }
    
    self.CustomerSegmantController.selectedSegmentIndex = 0;
    self.EnterView.hidden = NO;
    self.SearchView.hidden = YES;
    isEditCustomer = YES;
    
    [self.btnSave setTitle:@"Save" forState:UIControlStateNormal];
    
}

#pragma mark -
#pragma mark HudMethods

-(void)initHUDWithTitle :(NSString *)title{
    
    HUD = [[MBProgressHUD alloc]initWithView:self];
    [self addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = title;
    [HUD show:YES];
    
}
-(void)removeHUD{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}
@end
