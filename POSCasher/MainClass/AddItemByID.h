//
//  AddItemByID.h
//  Cashier
//
//  Created by User on 14/05/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "AllItems.h"
#import "SelectOrderDAO.h"

@protocol AddItemDelegate <NSObject>

-(void)didAnimationclose;
-(void)didSelectItemByItemID;

@end

@interface AddItemByID : UIView<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>{
    
    NSMutableArray * ItemArray;
    NSMutableArray * filtedItemArray;
    
    DBManager * dbmanager;
    AllItems * allitem;
    SelectOrderDAO * addtocart;
    
    
    
}

@property (weak, nonatomic) IBOutlet UILabel *ItemcountTL;

@property (weak, nonatomic) IBOutlet UITableView *CustomerTV;
@property (weak, nonatomic) IBOutlet UISearchBar *customSearchbar;
@property ( nonatomic, retain ) id <AddItemDelegate> delegate;
@property (nonatomic, assign) bool isFiltered;


- (IBAction)btnDoneClicks:(id)sender;
-(void)AnimateviewOpen:(BOOL)isUp;
@end
