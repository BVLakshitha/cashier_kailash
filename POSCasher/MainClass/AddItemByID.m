//
//  AddItemByID.m
//  Cashier
//
//  Created by User on 14/05/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "AddItemByID.h"
#import "ItemDetailsCell.h"
#import "AFImageRequestOperation.h"
#import "Constants.h"
#import "SelectOrderDAO.h"

@implementation AddItemByID

@synthesize isFiltered;

-(void)awakeFromNib{
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
   
}

-(void)AnimateviewOpen:(BOOL)isUp{
    
    if (isUp) {
        
         [self dataLoader];
        
        self.hidden = NO;
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.frame;
                             frame.origin.y = 102;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 768;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                             [self.delegate didAnimationclose];
                         }
         ];
    }
}
- (IBAction)btnDoneClicks:(id)sender{
    
    [self AnimateviewOpen:NO];
    
}

-(void)dataLoader{
    
    self.CustomerTV.delegate = self;
    self.CustomerTV.dataSource = self;
    
    if (!ItemArray)ItemArray = [[NSMutableArray alloc]init];
    [ItemArray removeAllObjects];
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSArray * data = [dbmanager GetAllItemData];
    [ItemArray addObjectsFromArray:data];
    [self.CustomerTV reloadData];
    [self cartItemcount];
    
}



#define mark -
#define mark Table Loader

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (isFiltered) {
        
        return [filtedItemArray count];
    }else{
        return [ItemArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"ItemDetailsCell";
    ItemDetailsCell *cell = (ItemDetailsCell *)[self.CustomerTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemDetailsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    if (isFiltered) {
        allitem = [filtedItemArray objectAtIndex:indexPath.row];
    }else{
        
        allitem = [ItemArray objectAtIndex:indexPath.row];
    }
    
    cell.itemNoTL.text = allitem.code;
    cell.itemNameTL.text = allitem.item_name;
    cell.itemPriceTL.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,allitem.price.stringValue];
    cell.itemDescriptionTL.text = allitem.itemDiscription;
    
    [cell fatchfromurl:allitem.item_imgURL];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isFiltered) {
        allitem = [filtedItemArray objectAtIndex:indexPath.row];
    }else{
        
        allitem = [ItemArray objectAtIndex:indexPath.row];
    }
    
    
    if (!addtocart)addtocart = [[SelectOrderDAO alloc]init];
    
    //OrderID
    SelectOrder * item = [addtocart getSelectFoodCategory:allitem.item_id.stringValue];
    NSString * discID = @"0";
    NSString * Orderid = item.orderId;
    if (Orderid == nil)Orderid = @"0";
    
    
    NSDictionary* itemdicmy = @{@"itemId"         : allitem.item_id.stringValue,
                                @"itemQty"        : @"1",
                                @"RecordId"       : [self recordidGen],
                                @"itemname"       : allitem.item_name,
                                @"tableid"        : @"12",
                                @"noofchairs"     : @"0",
                                @"OrderID"        : Orderid,
                                @"status"         : @"1",
                                @"descit"         : allitem.itemDiscription,
                                @"itemPrice"      : allitem.price.stringValue,
                                @"itemDiscount"   : allitem.discount.stringValue,
                                @"img_url"        : allitem.item_imgURL,
                                @"isEdited"       : @NO,
                                @"chair"          : @"0",
                                @"orderdetailsid" : discID,
                                @"itemLocation"   : allitem.itemLocation,
                                @"ActiveSt"       : @"0",
                                @"hasNote"        : @NO,
                                @"itemUpdate"     : @NO,
                                @"defaultDiscountStatus" : allitem.addDefaultDiscount};
    
    
    
    [addtocart addNewSelectOrderItermfromdic:itemdicmy :@"Table one"];
    [self.delegate didSelectItemByItemID];
    [self cartItemcount];
}
-(NSString *)recordidGen{
    int Trecordid = 0;
    if (!addtocart)addtocart = [[SelectOrderDAO alloc]init];
    NSArray * items = [addtocart getSelectFoodCategorys];
    for (SelectOrder * order in items) {
        int CRecordid = order.recodId.intValue;
        if (CRecordid != 0) {
            
            if (CRecordid > Trecordid) {
                Trecordid = CRecordid;
            }
        }
    }
    
    int NewRecordid = Trecordid +1;
    
    return [NSString stringWithFormat:@"%i",NewRecordid];
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        
        if (!filtedItemArray)filtedItemArray = [[NSMutableArray alloc] init];
        [filtedItemArray removeAllObjects];
        
        for (AllItems * items in ItemArray)
        {
            NSRange nameRange = [items.code rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [items.item_name rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [filtedItemArray addObject:items];
            }
        }
    }
    
    [self.CustomerTV reloadData];
}
-(void)cartItemcount{
    
    if (!addtocart)addtocart = [[SelectOrderDAO alloc]init];
    NSArray * items = [addtocart getSelectFoodCategorys];
    
    NSString * itemcount = [NSString stringWithFormat:@"%i",items.count];
    self.ItemcountTL.text = itemcount;
    
}
@end
