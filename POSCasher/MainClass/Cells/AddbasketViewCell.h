//
//  AddbasketViewCell.h
//  ResbookCustoms
//
//  Created by Tharaka Dushmantha on 6/14/14.
//  Copyright (c) 2014 Tharaka Dushmantha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddbasketViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblItermName;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblQuantity;
@property (strong, nonatomic) IBOutlet UILabel *lblPerOnePrice;
@property (weak, nonatomic) IBOutlet UILabel *currencySymbol;

@end
