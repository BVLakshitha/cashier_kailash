//
//  AddbasketViewminiCell.h
//  POSCasher
//
//  Created by orderfood on 10/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddbasketViewminiCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblItermName;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblQuantity;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscount;
@property (strong, nonatomic) IBOutlet UILabel *lblPerOnePrice;
@property (weak, nonatomic) IBOutlet UILabel *RecordIDTL;
@property (weak, nonatomic) IBOutlet UIImageView *CancelIMG;



@end
