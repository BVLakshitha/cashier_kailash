//
//  CardCell.h
//  Cashier
//
//  Created by User on 17/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *CardImageIV;
@property (weak, nonatomic) IBOutlet UILabel *CardNameTL;
@property (weak, nonatomic) IBOutlet UILabel *separater;

-(void)loadImage:(NSString *)imagePath;
@end
