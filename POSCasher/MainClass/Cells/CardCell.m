//
//  CardCell.m
//  Cashier
//
//  Created by User on 17/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import "CardCell.h"
#import "AFImageRequestOperation.h"
#import "Constants.h"

@implementation CardCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.separater.backgroundColor = [UIColor colorWithRed:210/255.0 green:57/255.0 blue:47/255.0 alpha:1.0];

    // Configure the view for the selected state
}
-(void)loadImage:(NSString *)imagePath{
    
    NSString * image_url = [NSString stringWithFormat:@"%@%@",IMAGEPATH,imagePath];
    
    NSLog(@"Image links %@",image_url);
    
    if ([imagePath isEqualToString:@""]) {
        [self.CardImageIV setImage:[UIImage imageNamed:@"placeholder.png"]];
    }else{
        AFImageRequestOperation* operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:image_url]] imageProcessingBlock:nil
                                                                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                                              {
                                                  [self.CardImageIV setImage:image];
                                                  
                                              }
                                                                                               failure:nil];
        
        [operation start];
    }
    
}
@end
