//
//  CompanyCell.h
//  Cashier
//
//  Created by Lakshitha on 7/20/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *companyNameTL;
@property (weak, nonatomic) IBOutlet UILabel *comapanyAddressTL;
@property (weak, nonatomic) IBOutlet UILabel *companyIdTL;


@end
