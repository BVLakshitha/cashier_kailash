//
//  CustomerCell.h
//  POSCasher
//
//  Created by Lakshitha on 10/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomerCellDelegate <NSObject>

-(void)didSelectEditincustomer :(NSString *)CUSID;

@end

@interface CustomerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ContactNoTL;
@property (weak, nonatomic) IBOutlet UILabel *UserNameTL;
@property (weak, nonatomic) IBOutlet UILabel *customerIDTL;
@property (weak, nonatomic) IBOutlet UILabel *AddressTL;
@property (weak, nonatomic) IBOutlet UILabel *DoorNOTL;
@property (weak, nonatomic) IBOutlet UILabel *CityTL;
@property (weak, nonatomic) IBOutlet UILabel *PostalCodeTL;
@property (weak, nonatomic) IBOutlet UILabel *EmailTL;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@property(nonatomic,retain)NSString * customerid;

@property ( nonatomic, retain ) id <CustomerCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *BG_RedTL;

- (IBAction)btnEditClicks:(id)sender;


@end
