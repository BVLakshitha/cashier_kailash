//
//  CustomerCell.m
//  POSCasher
//
//  Created by Lakshitha on 10/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "CustomerCell.h"

@implementation CustomerCell
@synthesize customerid;
@synthesize delegate = _delegate;


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];

    [self.BG_RedTL setBackgroundColor:[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1]];
}

- (IBAction)btnEditClicks:(id)sender {
    
    [self.delegate didSelectEditincustomer:self.customerid];
}
@end
