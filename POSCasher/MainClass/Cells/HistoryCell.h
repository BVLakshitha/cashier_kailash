//
//  HistoryCell.h
//  POSCasher
//
//  Created by Lakshitha on 9/24/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *OrderIDTL;
@property (weak, nonatomic) IBOutlet UILabel *tabeidTL;
@property (weak, nonatomic) IBOutlet UILabel *timeTL;
@property (weak, nonatomic) IBOutlet UIImageView *paiedImageView;

@end
