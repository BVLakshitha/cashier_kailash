//
//  ItemDetailsCell.h
//  Cashier
//
//  Created by Lakshi Ben on 5/15/15.
//  Copyright (c) 2015 Lakshi Ben. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemNoTL;
@property (weak, nonatomic) IBOutlet UILabel *itemNameTL;
@property (weak, nonatomic) IBOutlet UILabel *itemPriceTL;
@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionTL;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageIV;

-(void)fatchfromurl :(NSString *)imgURL;

@end
