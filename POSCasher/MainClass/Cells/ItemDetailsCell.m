//
//  ItemDetailsCell.m
//  Cashier
//
//  Created by Lakshi Ben on 5/15/15.
//  Copyright (c) 2015 Lakshi Ben. All rights reserved.
//

#import "ItemDetailsCell.h"
#import "AFImageRequestOperation.h"

@implementation ItemDetailsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)fatchfromurl :(NSString *)imgURL{
    //    NSLog(@"Image %@",imgURL);
    
    if ([imgURL isEqualToString:@""]) {
        [self.itemImageIV setImage:[UIImage imageNamed:@"placeholder.png"]];
    }else{
        AFImageRequestOperation* operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imgURL]] imageProcessingBlock:nil
                                                                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                                              {
                                                  [self.itemImageIV setImage:image];
                                                  
                                              }
                                                                                               failure:nil];
        
        [operation start];
    }
}

@end
