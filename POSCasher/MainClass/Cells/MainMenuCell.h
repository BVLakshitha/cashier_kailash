//
//  MainMenuCell.h
//  POSCasher
//
//  Created by Lakshitha on 9/23/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol mainmenudelegates <NSObject>

-(void)didSelectMenu :(NSNumber *)tabvalue;

@end

#define loadView() \
NSBundle *mainBundle = [NSBundle mainBundle]; \
NSArray *views = [mainBundle loadNibNamed:NSStringFromClass([self class]) owner:self options:nil]; \
[self addSubview:views[0]];


@interface MainMenuCell : UIView
@property (weak, nonatomic) IBOutlet UIImageView *Menuimage;
@property (weak, nonatomic) IBOutlet UILabel *MenuName;
@property(nonatomic,retain)NSString * imageurl;

- (void)downloadImageWithCompletionBlock :(NSString *)mageurl;

@property ( nonatomic, retain ) id <mainmenudelegates> delegate;

@end
