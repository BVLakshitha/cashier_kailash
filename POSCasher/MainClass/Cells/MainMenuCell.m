//
//  MainMenuCell.m
//  POSCasher
//
//  Created by Lakshitha on 9/23/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "MainMenuCell.h"
#import "AFImageRequestOperation.h"


@implementation MainMenuCell
@synthesize imageurl;
@synthesize delegate = _delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    //loading menu
    loadView()
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    loadView()
    return self;
}
- (void)downloadImageWithCompletionBlock :(NSString *)mageurl{
    NSString* urlString = mageurl;

    [self.Menuimage setImage:[UIImage imageNamed:@"placehonder_category2~ipad.png"]];
        AFImageRequestOperation* operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]] imageProcessingBlock:nil
                                                                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                                              {
                                                  [self.Menuimage setImage:image];
                                                  
                                              }
                                                                                               failure:nil];
        
        [operation start];

}
- (IBAction)menuClicks:(id)sender {
    
    [self.delegate didSelectMenu:[NSNumber numberWithInt:self.tag]];
}

@end
