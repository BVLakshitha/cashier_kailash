//
//  MenuItemCell.h
//  POSCasher
//
//  Created by Lakshitha on 9/24/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ItemImage;
@property (weak, nonatomic) IBOutlet UILabel *itemname;
@property (weak, nonatomic) IBOutlet UILabel *itemprice;
@property (weak, nonatomic) IBOutlet UILabel *itemdiscription;
@property (weak, nonatomic) IBOutlet UIView *bgImage;

-(void)fatchfromurl :(NSString *)imgURL;
@end
