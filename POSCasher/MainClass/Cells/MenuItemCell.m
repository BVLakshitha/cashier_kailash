//
//  MenuItemCell.m
//  POSCasher
//
//  Created by Lakshitha on 9/24/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "MenuItemCell.h"
#import "AFImageRequestOperation.h"

@implementation MenuItemCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    [self.bgImage setBackgroundColor:[UIColor whiteColor]];

    // Configure the view for the selected state
}
-(void)fatchfromurl :(NSString *)imgURL{
//    NSLog(@"Image %@",imgURL);
    
    if ([imgURL isEqualToString:@""]) {
        [self.ItemImage setImage:[UIImage imageNamed:@"placehonder_category2~ipad.png"]];
    }else{
        AFImageRequestOperation* operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imgURL]] imageProcessingBlock:nil
                                                                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                                              {
                                                  [self.ItemImage setImage:image];
                                                  
                                              }
                                                                                               failure:nil];
        
        [operation start];
    }
}
@end
