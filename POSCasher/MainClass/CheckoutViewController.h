//
//  CheckoutViewController.h
//  Cashier
//
//  Created by Lakshitha on 11/1/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceApi.h"
#import "MBProgressHUD.h"
#import "SelectOrderDAO.h"
#import "OrderListDao.h"
#import "OrderList.h"
#import "DBManager.h"
#import "CompanyView.h"
#import "Companies.h"

@interface CheckoutViewController : UIViewController<MBProgressHUDDelegate,webservicedelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CompanyDetailsDelegate>{
    WebserviceApi * webserviceapi;
    NSString * TotalValue;
    MBProgressHUD * HUD;
    DBManager * dbmanager;
    SelectOrderDAO * carttbl;
    OrderListDao * OLDAO;
    OrderList * orderlist;
    
    //GENARATE TOTAL
    NSMutableArray * cardData;
    NSMutableArray * CardPaymentData;
    NSDictionary * selectedCard;
    double paiedAmout;
    BOOL isCashdrowerOpen;
    BOOL isValidateCurrentGV;
    
    //PAYMENT
    NSMutableArray * carddetails;
    NSMutableArray * voucherdetails;
    NSMutableDictionary * paydictionry;
    BOOL ispaymentSuccess;
    
    //SPLITDATA
    NSMutableArray * SplitRecords;
    
    //GIFTVOUCHER
    NSString * GiftVoucherTotal;
    NSString * VoucherCode;
    
    //DISCOUNT
    BOOL isDiscountHave;
    NSString * Distype;
    NSString * Disvalue;
    
    
    //Companies
    Companies * selectedComapny;
    
    //Payment type
    
    NSString * paymentStatus;
    
    
}


@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


@property (weak, nonatomic) IBOutlet UILabel *OrderIDTL;
@property (weak, nonatomic) IBOutlet UILabel *CtimeTL;
@property (weak, nonatomic) IBOutlet UILabel *TotalTL;


@property (weak, nonatomic) IBOutlet UILabel *subtotalTL;
@property (weak, nonatomic) IBOutlet UILabel *taxTL;
@property (weak, nonatomic) IBOutlet UILabel *SCTL;
@property (weak, nonatomic) IBOutlet UILabel *tipTL;
@property (weak, nonatomic) IBOutlet UILabel *DiscountTL;


@property(nonatomic,retain)NSString * orderid;
@property(nonatomic,retain)NSString * SplitID;
@property(nonatomic,retain)NSString * splitOrderID;
@property(nonatomic,retain)NSString * Subtotal;
@property(nonatomic,retain)NSString * Customer;
@property(nonatomic,assign)BOOL isFromSplit;
@property(nonatomic,assign)BOOL isPaied;

- (IBAction)btnCancelClicks:(id)sender;


//bill print


//Calculater
- (IBAction)NumberbtnClicks:(id)sender;
- (IBAction)btnClearCal:(id)sender;
- (IBAction)btnDotClicks:(id)sender;
- (IBAction)ControlbtnClicks:(id)sender;
- (IBAction)btnAccountClicks:(id)sender;

- (IBAction)btnCashCancelClicks:(id)sender;
- (IBAction)btnCardCancelClicks:(id)sender;
- (IBAction)btnGiftCancelClicks:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *CalCisplayTL;

@property (weak, nonatomic) IBOutlet UILabel *cashTL;
@property (weak, nonatomic) IBOutlet UILabel *cardTL;
@property (weak, nonatomic) IBOutlet UILabel *giftVTL;
@property (weak, nonatomic) IBOutlet UILabel *BalanceTF;
@property (weak, nonatomic) IBOutlet UILabel *needTF;

//CardDetails //18826065
@property (weak, nonatomic) IBOutlet UIView *blureView;
@property (weak, nonatomic) IBOutlet UIView *CardDetailsPopup;
@property (weak, nonatomic) IBOutlet UITextField *CardnumberTF;
@property (weak, nonatomic) IBOutlet UITextField *CardAmountTF;


- (IBAction)btnCardDviewCancelClicks:(id)sender;
- (IBAction)btnCardviewOkClicks:(id)sender;

//GiftVoucher
@property (weak, nonatomic) IBOutlet UIView *GifiVview;
@property (weak, nonatomic) IBOutlet UITextField *vouchernumberTF;
@property (weak, nonatomic) IBOutlet UILabel *vigtAmountTL;
@property (weak, nonatomic) IBOutlet UILabel *statusTL;
@property (weak, nonatomic) IBOutlet UILabel *createdDateTL;
@property (weak, nonatomic) IBOutlet UILabel *lastvaliddateTL;
@property (weak, nonatomic) IBOutlet UITextField *selectedAmountTF;

- (IBAction)btngiftVoucherClicks:(id)sender;

//numberbuttons
- (IBAction)NumberClicks:(id)sender;

//Card and giftvoucherdetails

- (IBAction)btnCardDetailsClicks:(id)sender;
- (IBAction)btngiftVClicks:(id)sender;
@property (nonatomic, strong) UIPopoverController *paytypedetails;

@property (weak, nonatomic) IBOutlet UITableView *cardsTableView;

//Discount view
- (IBAction)btnDisViewActiveClicks:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *discountView;
@property (weak, nonatomic) IBOutlet UITextField *DisValueTF;
@property (weak, nonatomic) IBOutlet UILabel *btncymbolTL;

@property (weak, nonatomic) IBOutlet UIButton *btnPrec;
@property (weak, nonatomic) IBOutlet UIButton *btnAmount;


- (IBAction)btnPrecClicks:(id)sender;
- (IBAction)btnAmountClicks:(id)sender;
- (IBAction)btnDisViewDoneClicks:(id)sender;
- (IBAction)btnDisClearClicks:(id)sender;


//Company
- (IBAction)btnComapnyClicks:(id)sender;
@property (weak, nonatomic) IBOutlet CompanyView *companyview;
@property (weak, nonatomic) IBOutlet UIButton *btnCompany;
@property (weak, nonatomic) IBOutlet UIButton *btnCompanyCancel;
- (IBAction)btnCompanyCancelClicks:(id)sender;


@end
