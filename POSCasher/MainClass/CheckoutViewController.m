//
//  CheckoutViewController.m
//  Cashier
//
//  Created by Lakshitha on 11/1/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "CheckoutViewController.h"
#import "WebserviceApi.h"
#import "MBProgressHUD.h"
#import "MiniPrinterFunctions.h"
#import "AppDelegate.h"
#import "SelectOrderDAO.h"
#import "Printer.h"
#import "Checkoutdetails.h"
#import "Constants.h"
#import "CardCell.h"
#import "AppUserDefaults.h"
#import "PaymentDetails.h"
#import "SplitData.h"
#import "ExtraItems.h"

@interface CheckoutViewController ()

@end

@implementation CheckoutViewController
@synthesize Subtotal,orderid;
@synthesize paytypedetails;
@synthesize isFromSplit = _isFromSplit;

- (void)viewDidLoad {
    [super viewDidLoad];

    if (!CardPaymentData)CardPaymentData = [[NSMutableArray alloc]init];
    [CardPaymentData removeAllObjects];
    [self callForPaymentDetails];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Delegates
-(void)viewWillAppear:(BOOL)animated{
    
    NSLog(@"order id %@ and subtotal %@",self.orderid,self.Subtotal);
    
    if (self.SplitID.intValue > 0) {
        self.OrderIDTL.text = [NSString stringWithFormat:@"Order ID : %@ - #%@",self.orderid,self.SplitID];
    }else{
        self.OrderIDTL.text = [NSString stringWithFormat:@"Order ID : %@",self.orderid];
    }

    self.btnCancel.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnCancel.layer.masksToBounds = YES;
    self.btnCancel.layer.cornerRadius = 4;
    self.btnCancel.layer.borderWidth = 1.0f;

    self.btnCompany.layer.masksToBounds = YES;
    self.btnCompany.layer.cornerRadius = 4;
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *stringFromDate = [dateFormatter1 stringFromDate:[NSDate date]];
    self.CtimeTL.text = [NSString stringWithFormat:@"Time : %@",stringFromDate];
    
    isDiscountHave = NO;
}

#pragma mark -
#pragma mark Card pay
- (IBAction)btnCardClicks:(id)sender {
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"This function is not available in this version." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)callForPaymentDetails{
    
    [self inithud:@"Loading.."];
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi GetRestuarentProfile];
 
}
-(void)DidReceiveRestuarentProfile:(id)respond{
    
    NSUserDefaults * mydefalt = [NSUserDefaults standardUserDefaults];
    BOOL isValueFound = NO;

    if ([[respond objectForKey:@"Status"]isEqualToString:@"Success"]) {
        
        NSDictionary * dataDic = [respond objectForKey:@"data"];
        
        NSLog(@"Payment Details %@",dataDic);
        
        [AppUserDefaults removeOrderDetails];
        
        [AppUserDefaults setResturantDetails:[dataDic objectForKey:@"restuarent"]];//RESTURANT DETAILS
        [AppUserDefaults setPaymentdetails:[dataDic objectForKey:@"constants"]];//PAYMENTDETAILS
        [AppUserDefaults setCardDetails:[dataDic objectForKey:@"cardTypes"]];//CARDDETAILS
        [AppUserDefaults setVoucherConstants:[dataDic objectForKey:@"voucherAmounts"]];//VOUCHERSDETAILS
        
        isValueFound = YES;
        
    }else{

        if ([mydefalt objectForKey:KEYFORPAYMENTDETAILS]!= nil) {
             isValueFound = YES;
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Update fail." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }
    
    if (isValueFound) {

        float maxDiscount = [[[AppUserDefaults getPaymentdetails] objectForKey:@"maxDiscount"] floatValue];
        float Ordertax = [[[AppUserDefaults getPaymentdetails] objectForKey:@"taxForOrder"] floatValue];
        float DiscoutforOrder = [[[AppUserDefaults getPaymentdetails] objectForKey:@"discountForOrder"] floatValue];
        float SC = [[[AppUserDefaults getPaymentdetails] objectForKey:@"serviceCharge"] floatValue];
        [self loadCardData];
//        [self Detailsgenarater:maxDiscount :Ordertax :DiscoutforOrder :SC];
        [self setRefreshTotal];
    }
    [self hudWasHidden];
}
//-(void)Detailsgenarater:(float)maxDisC :(float)tax :(float)discouttotal :(float)SC{
//    
//    float totalvalue = 00.00;
//    float TotalCasherDiscount = 00.00;
//    float adminDiscount = 00.00;
//    
//    if (self.orderid != nil && self.Subtotal != nil) {
//        
//        //servicecharge
////        float SC = 5.9;
//        
//        self.subtotalTL.text = self.Subtotal;
//        NSString * subtotalstring = self.Subtotal;
//        //Tax
//        NSString * taxcalue = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (tax/100)];
//        self.taxTL.text = taxcalue;
//        //S/C
//        NSString * servicvalue = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (SC/100)];
//        self.SCTL.text = servicvalue;
//        //TIP
//        NSString * tipcal = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (3/100)];
//        self.tipTL.text = tipcal;
//        //DisCount
//
//        //Discount from Admin
//
//        NSString * discountablePrice = [self defaultDiscountForOrder];
//        adminDiscount = [[NSString stringWithFormat:@"%.2f",discountablePrice.floatValue * (discouttotal/100)] floatValue];
//
//        self.DiscountTL.text = [NSString stringWithFormat:@"%.2f",adminDiscount];
//        
//        totalvalue = taxcalue.floatValue + subtotalstring.floatValue+servicvalue.floatValue - adminDiscount;
//        
//        self.TotalTL.text = [NSString stringWithFormat:@"%@ %.2f",CURRENCYSYMBOL,totalvalue];
//        TotalValue = [NSString stringWithFormat:@"%.2f",totalvalue];
//        
//        
//    }else{
//        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Details missing." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}
#pragma mark -
#pragma mark SplitData
-(NSDictionary *)GenarateSplitRecords :(int)SplitID{
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSArray * splitRecordData = [dbmanager selectSplitRecord:[NSNumber numberWithInt:SplitID+1]];
    
    if (!SplitRecords)SplitRecords = [[NSMutableArray alloc]init];
    [SplitRecords removeAllObjects];
    


    for (SplitData * splitrecord in splitRecordData) {
       NSDictionary * splitdata = @{@"id" : splitrecord.itemDisID,
                                    @"qty" : [self decimelconverter:splitrecord.qty]};
        
        [SplitRecords addObject:splitdata];
    }
    NSDictionary * Orderfineldata = @{@"order" :SplitRecords};
    NSArray * bounder = [[NSArray alloc]initWithObjects:Orderfineldata, nil];
    
    NSDictionary * Splitfineldata = @{@"splitOrders" :bounder};
    
    NSString * jsonStrting = [self jsonconverter:Splitfineldata];
    
    NSDictionary * data = @{@"key" :WEBSERVICEKEY,
                            @"order_id" :self.orderid,
                            @"data" :jsonStrting};
    return data;
}
-(NSString * )decimelconverter :(NSString *)valueString{
    NSArray *subStrings = [valueString componentsSeparatedByString:@"/"]; //or rather @" - "
    NSString * finelString = @"";
    
    if ([subStrings count] > 1) {
        NSString *firstString = [subStrings objectAtIndex:0];
        NSString *lastString = [subStrings objectAtIndex:1];
        float values = firstString.floatValue/lastString.floatValue;
        
//        NSLog(@"firstString %@ lastString %@ finel = %f",firstString,lastString,values);
        
        finelString = [NSString stringWithFormat:@"%0.2f",values];
    }else{
        finelString = valueString;
    }

    return finelString;
}
-(void)GetPaymentinfor :(int)SplitID{
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    NSDictionary * splitParams = [self GenarateSplitRecords:SplitID];
    [webserviceapi SendSplitOrder:splitParams];
    
}
-(void)DidReceiveSplitinfo:(id)respond{
    
    NSLog(@"respond 12 %@",respond);
}
-(void)SendCashPayments:(NSDictionary *)params{
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi CashPayProcess:params];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSLog(@"nutton title %i",buttonIndex);
    if (buttonIndex != [alertView cancelButtonIndex]){
//        [self setPosition:@"paied"];
    }
    
}
- (IBAction)btnCancelClicks:(id)sender {
    
    if (self.isFromSplit) {

        [self dismissViewControllerAnimated:YES completion:nil];
        
    }else{
        
        [self setPosition:@"canceled"];
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        [dbmanager deleteAllPaymentDetails];
    }

}


-(void)setPosition:(NSString *)position{
    
    NSUserDefaults * mydefaly = [NSUserDefaults standardUserDefaults];
    [mydefaly setObject:position forKey:@"position"];
    if ([mydefaly synchronize]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark -
#pragma mark printer

- (IBAction)btnPrintbilClicks:(id)sender {
    
    [self PrintBill:NO];
}
-(void)PrintBill :(BOOL)isDroverOpen{
    
    Printer * printer = [[Printer alloc]init];
    if ([[AppUserDefaults getPrinterStatus:KITCHENPRINTER] isEqualToString:PRINTERON]) {
        NSDictionary * casherPrinterDic = [[NSUserDefaults standardUserDefaults] objectForKey:KITCHENPRINTER];
        if (casherPrinterDic !=nil) {
            if (!printer)printer = [[Printer alloc]init];
            printer.Subtotal = self.subtotalTL.text;
            printer.Discount = self.DiscountTL.text;
            printer.Cash = self.cashTL.text;
            printer.Card = self.cardTL.text;
            printer.GiftVoucher = self.giftVTL.text;
            printer.Change = self.BalanceTF.text;
            printer.Tax = self.taxTL.text;
            printer.Servicecharge = self.SCTL.text;
            printer.Total = TotalValue;
            printer.SplitId = self.SplitID;
            printer.PaymentStatus = paymentStatus;
            printer.Customer = self.Customer;
            [printer initWithKBillPrint:self.orderid :[casherPrinterDic objectForKey:@"portName"] :@"Standard" :isDroverOpen];
            
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Check printer settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

#pragma mark -
#pragma mark Calculater Functions

- (IBAction)NumberbtnClicks:(id)sender {
    
    UIButton *resultButton = (UIButton *)sender;
    NSString * number = resultButton.currentTitle;
    [self feeder:number];

}

- (IBAction)btnClearCal:(id)sender {
    
    self.CalCisplayTL.text = [self reduceNumber:self.CalCisplayTL.text];
}

-(NSString *)reduceNumber :(NSString *)number{
    
    NSString * returnValue = @"";
    NSArray *NumberSections = [number componentsSeparatedByString:@"."];
    
    if ([self isWholeNumber:number.doubleValue] && [NumberSections count] != 2) {
        NSLog(@"int");
        int number1 = number.intValue;
        int newNumber = number1/10;
        returnValue = [NSString stringWithFormat:@"%i",newNumber];
    }else{
        NSLog(@"double");
        
        NSArray *strings = [number componentsSeparatedByString:@"."];
        int decimalpoint = [[strings objectAtIndex:1] intValue];
        int newdecimalpoint = decimalpoint/10;
        
        returnValue = [NSString stringWithFormat:@"%@.%i",[strings objectAtIndex:0],newdecimalpoint];
    }
    
    if ([self isWholeNumber:returnValue.doubleValue])returnValue = [NSString stringWithFormat:@"%i",returnValue.intValue];
        
    if ([returnValue isEqualToString:@"0"])returnValue = @"";
    
    return returnValue;
    
}
-(BOOL)isWholeNumber:(double)number
{
    double integral;
    double fractional = modf(number, &integral);
    
    return fractional == 0.00 ? YES : NO;
}
- (IBAction)btnDotClicks:(id)sender {
    
    NSString * currenttext = self.CalCisplayTL.text;
    NSRange range = [currenttext rangeOfString:@"."];
    if (range.location == NSNotFound)
    {
        [self feeder:@"."];
    }
}

- (IBAction)btnCashCancelClicks:(id)sender {
    self.cashTL.text = @"00.00";
    [self CalculateBalance];
}

- (IBAction)btnCardCancelClicks:(id)sender {
    self.cardTL.text = @"00.00";
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    [dbmanager RemovePaymentRecordByType:CARD];
    [CardPaymentData removeAllObjects];
    selectedCard = nil;
    [self CalculateBalance];
}

- (IBAction)btnGiftCancelClicks:(id)sender {
    self.giftVTL.text = @"00.00";
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    [dbmanager RemovePaymentRecordByType:GIFTVOUCHER];
    [self CalculateBalance];
}
-(void)feeder:(NSString *)number{

    NSString * currenttext = self.CalCisplayTL.text;
    self.CalCisplayTL.text = [currenttext stringByAppendingString:number];
}
- (IBAction)ControlbtnClicks:(id)sender {
    
    UIButton *resultButton = (UIButton *)sender;
    
    NSString * value = self.CalCisplayTL.text;
    
    if ([value isEqualToString:@""])value = @" ";
    
    double Dvalue = value.doubleValue;
    double cashdoublevalue = self.cashTL.text.doubleValue;
    
        
        switch (resultButton.tag) {
            case 1:
                self.cashTL.text = [NSString stringWithFormat:@"%0.2f",Dvalue+cashdoublevalue];
                [self CalculateBalance];
                break;
            case 2:
                [self cardView:value];
                break;
            case 3:
                
                break;
            case 4:
                [self giftvoucherview];
                break;
            case 5:
                
                break;
                
            default:
                break;
                
        }
    
    
    self.CalCisplayTL.text = @"";
}

- (IBAction)btnCardDetailsClicks:(id)sender {
    
    Checkoutdetails *cdetails = [[Checkoutdetails alloc]initWithNibName:@"Checkoutdetails" bundle:nil];
    cdetails.paytype = CARD;    
    UIPopoverController *pop = [[UIPopoverController alloc]initWithContentViewController:cdetails];
    pop.popoverContentSize = CGSizeMake(316, 200);
    [pop presentPopoverFromRect:[(UIButton *)sender frame] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (IBAction)btngiftVClicks:(id)sender {
    
    Checkoutdetails *cdetails = [[Checkoutdetails alloc]initWithNibName:@"Checkoutdetails" bundle:nil];
    cdetails.paytype = GIFTVOUCHER;
    UIPopoverController *pop = [[UIPopoverController alloc]initWithContentViewController:cdetails];
    pop.popoverContentSize = CGSizeMake(316, 200);
    [pop presentPopoverFromRect:[(UIButton *)sender frame] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
#pragma mark -
#pragma mark numberButtonClicks

- (IBAction)NumberClicks:(id)sender {

    UIButton *resultButton = (UIButton *)sender;
    NSString * value = self.cashTL.text;
    
    double Dvalue = [value doubleValue];
    
    switch (resultButton.tag) {
        case 1:
            self.cashTL.text = [NSString stringWithFormat:@"%0.2f",Dvalue+1];
            break;
        case 2:
            self.cashTL.text = [NSString stringWithFormat:@"%0.2f",Dvalue+5];
            break;
        case 3:
            self.cashTL.text = [NSString stringWithFormat:@"%0.2f",Dvalue+10];
            break;
        case 4:
            self.cashTL.text = [NSString stringWithFormat:@"%0.2f",Dvalue+20];
            break;
        case 5:
            self.cashTL.text = [NSString stringWithFormat:@"%0.2f",Dvalue+50];
            break;
        default:
            break;
    }
    [self CalculateBalance];
}
-(void)CalculateBalance{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    //Load Voucher Details
    double cashamount = self.cashTL.text.doubleValue;
    
    //Load Card Details
    NSArray * CardValuesDetails = [dbmanager GetAllCardDetails:CARD];
    float cardAmount = 00.00;
    for (PaymentDetails * detals in CardValuesDetails) {
        float rowvalue = detals.amount.floatValue;
//        float netvalue = rowvalue + (rowvalue * detals.interest.floatValue/100);
        cardAmount = cardAmount+rowvalue;
    }
    self.cardTL.text = [NSString stringWithFormat:@"%0.2f",cardAmount];
    
    //Load Voucher Details
    NSArray * VoucherDetails = [dbmanager GetAllCardDetails:GIFTVOUCHER];
    float voucherAmount = 00.00;
    for (PaymentDetails * detals in VoucherDetails) {
        float rowvalue = detals.interest.floatValue;   //Update Interest as SelectedAmount
        voucherAmount = voucherAmount +rowvalue;
    }
    self.giftVTL.text = [NSString stringWithFormat:@"%0.2f",voucherAmount];
    
    //TotalValues
     paiedAmout = cashamount + cardAmount+voucherAmount;
    double total = TotalValue.doubleValue;
    double balance = paiedAmout-total;
    
    if (balance >= 0){
        if (total > voucherAmount) {
            self.BalanceTF.text = [NSString stringWithFormat:@"%0.2f",balance];
        }else{
           self.BalanceTF.text = @"00.00";
        }
      
    }else{
        self.BalanceTF.text = @"00.00";
        //Need
        self.needTF.text = [NSString stringWithFormat:@"%0.2f",fabs(paiedAmout - total)];
    }
    

}

-(void)cardView :(NSString *)value{
    
    //
    float rowAmount = self.CalCisplayTL.text.floatValue;
    NSString * cardAmount = [NSString stringWithFormat:@"%.2f",self.CalCisplayTL.text.floatValue];
 
    if (rowAmount > 0) {
        
        NSString * cardNumber = @"0000-0000-0000-0000";
        NSString * interest = @"00";
        NSString * Cardtype = @"notSelected";
        
        [self insertPayment:cardNumber :CARD :cardAmount :interest :Cardtype];//INSERT RECORD
    }else{
        UIAlertView * massageAlert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Value empty." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [massageAlert show];
    }
    
    /*
    [self loadCardData];
    
    self.CardAmountTF.layer.masksToBounds = YES;
    self.CardAmountTF.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.CardAmountTF.layer.borderWidth = 1.0f;
    self.CardAmountTF.backgroundColor = [UIColor whiteColor];
    
    self.CardnumberTF.layer.masksToBounds = YES;
    self.CardnumberTF.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.CardnumberTF.layer.borderWidth = 1.0f;
    self.CardnumberTF.backgroundColor = [UIColor whiteColor];
    
    self.CardAmountTF.text = TotalValue;

    self.blureView.hidden = NO;
    self.CardDetailsPopup.hidden = NO;
     */
}
-(void)giftvoucherview{
    self.vouchernumberTF.layer.masksToBounds = YES;
    self.vouchernumberTF.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.vouchernumberTF.layer.borderWidth = 1.0f;
    self.vouchernumberTF.backgroundColor = [UIColor whiteColor];
    
    self.blureView.hidden = NO;
    self.GifiVview.hidden =NO;
    isValidateCurrentGV = NO;
    
}
- (IBAction)btnCardviewOkClicks:(id)sender {
    
    
    if (selectedCard != nil) {
//        double EntredValue = self.CardAmountTF.text.doubleValue;
//        double totalvale = TotalValue.doubleValue;
        
        float EntredValue = [[NSString stringWithFormat:@"%0.2f",self.CardAmountTF.text.floatValue] floatValue];
        float totalvale = TotalValue.floatValue;
        NSString * cardNumber = self.CardnumberTF.text;
        
        NSLog(@"EntredValue %f",EntredValue);
        NSLog(@"totalvale %f",totalvale);
        
        if ([self ValidateCardNumber:cardNumber]) {
            
             if (EntredValue <= totalvale) {
                 
                 if ([self CureectCardAmount] < totalvale) {

                     NSString * amount = [NSString stringWithFormat:@"%0.2f",EntredValue];
                     
                     NSDictionary * cardpaymentdetails = @{@"carddetails": selectedCard,
                                                           @"amount": amount};
                     
                     [CardPaymentData addObject:cardpaymentdetails];
                     self.cardTL.text = [NSString stringWithFormat:@"%0.2f",[self CureectCardAmount]];
                     self.CardAmountTF.text = @"";
                     self.CardnumberTF.text = @"";
                     self.blureView.hidden = YES;
                     self.CardDetailsPopup.hidden = YES;
                     
                     [self.CardAmountTF resignFirstResponder];
                     [self.CardnumberTF resignFirstResponder];
                     
                     NSString * interest = [selectedCard objectForKey:@"interest"];
                     NSString * Cardtype = [selectedCard objectForKey:@"type"];
                     
                     [self insertPayment:cardNumber :CARD :amount :interest :Cardtype];//INSERT RECORD
                     
                 }else{
                     UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Amount Exceeded" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                 }
                 
             }else{
                 UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Invalid Value." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [alert show];
             }
            
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Invalid card number." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Select Card Type." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(BOOL)ValidateCardNumber:(NSString *)cartnumber{
    
    BOOL isvalidPhonenumber = YES;
    
    NSString *phoneRegex = @"^([+-]?)(?:|0|[1-9]\\d*)?$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    if ([phoneTest evaluateWithObject:cartnumber]) {
        
        if (cartnumber.length > 15) {
            isvalidPhonenumber = YES;
        }else{
            isvalidPhonenumber = NO;
        }
    }else{
        isvalidPhonenumber = NO;
    }
    
    return isvalidPhonenumber;
    
}
-(float)CureectCardAmount{
    
    float amount = 00.00;
    for (NSDictionary * valueDic in CardPaymentData) {
        amount = amount + [[valueDic objectForKey:@"amount"] doubleValue];
    }
    
     NSLog(@"amount %f",amount);
    return amount;
}


-(void)DidReceivetaxdetails:(id)respond{
    
    //Putdata to DB;
    
}
#pragma mark -
#pragma mark Card Table
-(void)insertPayment :(NSString *)detailID :(NSString *)type :(NSString *)ammount :(NSString *)interest :(NSString *)cardType {
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSDictionary* itemdicmy4 = @{@"typrdetails"    : detailID,
                                 @"paytype"        : type,
                                 @"RecordId"       : ammount,
                                 @"Interest"       : interest,
                                 @"cardType"       : cardType};
    
    [dbmanager insetorupdatpaydetails:itemdicmy4];
    [self CalculateBalance];
}
-(void)loadCardData{
    
    if (!cardData)cardData = [[NSMutableArray alloc]init];
    [cardData removeAllObjects];
    [cardData addObjectsFromArray:[AppUserDefaults getCardDetails]];
    [self.cardsTableView reloadData];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [cardData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"CardCell";
    CardCell *cell = (CardCell *)[self.cardsTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CardCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary * carddetails = [cardData objectAtIndex:indexPath.row];
    [cell loadImage:[carddetails objectForKey:@"img"]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedCard = [cardData objectAtIndex:indexPath.row];
}
#pragma mark -
#pragma mark Payment Sending Process

- (IBAction)btnCashClicks:(id)sender {
    
    if ([self isNotPaied]) {
        if (TotalValue.floatValue <= paiedAmout) {
            [self SendingPaymentinfo:ONTIME];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Invalid Payment." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Order already paid or not active." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    

}
- (IBAction)btnAccountClicks:(id)sender {

    if (TotalValue.doubleValue > 0) {
        [self SendingPaymentinfo:ACCOUNT];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Invalid Payment." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
}


-(void)SendingPaymentinfo :(NSString *)payType{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSDictionary* paymentDic = nil;
    
    //CASH PAYMENT--------------------------------------------------------------------
    NSString * cash = self.cashTL.text;
    if (cash.doubleValue < 1)cash = @"0";
    
    
    if ([payType isEqualToString:ONTIME]) {
        
        //CARD PAYMENT--------------------------------------------------------------
        if (!carddetails)carddetails = [[NSMutableArray alloc]init];
        [carddetails removeAllObjects];
        NSArray * CardValuesDetails = [dbmanager GetAllCardDetails:CARD];
        
        if ([CardValuesDetails count] > 0) {
            for (PaymentDetails * detals in CardValuesDetails) {
                
                NSDictionary* card = @{@"creditCardAmount"    : detals.amount,
                                       @"creditCardNo"        : detals.typrdetails,
                                       @"creditCardType"      : detals.subtype};
                
                [carddetails addObject:card];
                
            }
        }
        NSDictionary* cards = @{@"creditcards": carddetails};
        NSString * cardJson = [self jsonconverter:cards];
        
        
        //VOUCHER PAYMENT--------------------------------------------------------------
        if (!voucherdetails)voucherdetails = [[NSMutableArray alloc]init];
        [voucherdetails removeAllObjects];
        NSArray * Details = [dbmanager GetAllCardDetails:GIFTVOUCHER];
        
        if ([Details count] > 0) {
            for (PaymentDetails * detals in Details) {
                NSDictionary* card = @{@"no"             : detals.typrdetails,
                                       @"selectedAmount" : detals.interest};
                [voucherdetails addObject:card];
                
            }
        }
        NSDictionary* Vouchers = @{@"vouchers": voucherdetails};
         NSString * VouchersJson = [self jsonconverter:Vouchers];
        
        NSString * mastertillID = [AppUserDefaults getMasterTillId];
        NSString * sessiontillId = [AppUserDefaults getSessionTillId];
        NSString * userid = [AppUserDefaults getUserID];

        //DIC ---------------------------------------------------------------------------
        
        paymentStatus = @"Paid";

        if (!paydictionry)paydictionry = [[NSMutableDictionary alloc]init];
        [paydictionry removeAllObjects];
        
        [paydictionry setObject:WEBSERVICEKEY forKey:@"key"];
        [paydictionry setObject:self.orderid forKey:@"order_id"];
        [paydictionry setObject:cash forKey:@"cashAmount" ];
        [paydictionry setObject:cardJson forKey:@"creditcards"];
        [paydictionry setObject:TotalValue forKey:@"clientTotal"];
        [paydictionry setObject:VouchersJson forKey:@"giftVoucherCodes"];
        [paydictionry setObject:@"1" forKey:@"payment_type"];
        [paydictionry setObject:userid forKey:@"uid"];
        
        if (mastertillID != nil || sessiontillId != nil) {
            [paydictionry setObject:mastertillID forKey:@"masterTill"];
            [paydictionry setObject:sessiontillId forKey:@"sessionTill"];
        }
        
        if (isDiscountHave) {
            [paydictionry setObject:Distype forKey:@"discountType"];
            [paydictionry setObject:Disvalue forKey:@"discountValue"];
        }
        
    }else{
        
        
        if (selectedComapny != nil) {
            
            if (!paydictionry)paydictionry = [[NSMutableDictionary alloc]init];
            [paydictionry removeAllObjects];
            
            NSString * mastertillID = [AppUserDefaults getMasterTillId];
            NSString * sessiontillId = [AppUserDefaults getSessionTillId];
            NSString * userid = [AppUserDefaults getUserID];
            
            [paydictionry setObject:WEBSERVICEKEY forKey:@"key"];
            [paydictionry setObject:self.orderid forKey:@"order_id"];
            [paydictionry setObject:TotalValue forKey:@"accountAmount"];
            [paydictionry setObject:TotalValue forKey:@"clientTotal"];
            [paydictionry setObject:@"1" forKey:@"payment_type"];
            [paydictionry setObject:userid forKey:@"uid"];
            [paydictionry setObject:selectedComapny.cId forKey:@"accountUser"];
            
            
            if (mastertillID != nil || sessiontillId != nil) {
                [paydictionry setObject:mastertillID forKey:@"masterTill"];
                [paydictionry setObject:sessiontillId forKey:@"sessionTill"];
            }
            
            paymentStatus = @"Account";
            
        }else{
            UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Please select company." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertview show];
            
            return;
        }
        
        
        
    }
    
        NSLog(@"FinelDic %@",paydictionry);
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi SendPaymentDetails:paydictionry];

}
-(NSString *)jsonconverter :(NSDictionary *)dictionary{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
-(BOOL)isNotPaied{
    
    if (!OLDAO)OLDAO = [[OrderListDao alloc]init];
    orderlist = [OLDAO getCheckOutOrderList:self.orderid];
    
    NSLog(@"Creect OrderID %@",self.orderid);
    
    if ([orderlist.paymentSt isEqualToString:@"Not paid"] || [orderlist.paymentSt isEqualToString:@"1"]) {
        return YES;
    }else if(self.isFromSplit){
        return YES;
    }
    return NO;
}

-(void)DidReceivePaymentinfo:(id)respond{
    
    [self hudWasHidden];
    NSLog(@"respond %@",respond);
    
    if ([[respond objectForKey:@"status"]isEqualToString:@"Success"]) {
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Done" message:@"Checkout Success." delegate:self
                                              cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        alert.delegate = self;
        [alert show];
        [self PrintBill:YES];
        
        NSLog(@"SplitID %@",self.SplitID);
        
        if (self.isFromSplit) {
            if (!dbmanager)dbmanager = [[DBManager alloc]init];
            [dbmanager updateSplitForPaied:self.SplitID.intValue];
        }
        
        [AppUserDefaults setCurrentpayment:@"SUCCESS"];
        
        ispaymentSuccess = YES;
        
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:[respond objectForKey:@"message"] delegate:self
                                              cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        alert.delegate = self;
        [alert show];
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (selectedCard != nil) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Select card type first." delegate:self
                                              cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}
#pragma mark -
#pragma mark GiftCoucher

- (IBAction)btngiftVoucherClicks:(id)sender {

    NSString * voucher = self.vouchernumberTF.text;
    BOOL isvoucherexsist = NO;//[self LocalValidateVoucherCode:voucher];
    
    if (!isvoucherexsist) {
        if ([voucher length]>0) {
            
            NSDictionary* params =  @{@"key"  : WEBSERVICEKEY,
                                      @"code" : voucher};
            
            if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
            webserviceapi.delegate = self;
            [webserviceapi GiftVoucherValidate:params];
            [self inithud:@"Validating.."];
            
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Invalid Voucher Number." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Voucher Code already added." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(BOOL)LocalValidateVoucherCode :(NSString *)vouchercode{
    BOOL isMatched = YES;
    
    /*
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * VoucherDetails = [dbmanager GetAllCardDetails:GIFTVOUCHER];
    for (PaymentDetails * detals in VoucherDetails) {
        if ([detals.typrdetails isEqualToString:vouchercode]) {
            isMatched = YES;
        }
    }
     */
    return isMatched;
}
-(void)DidReceiveVoucherDetails:(id)respond{
    
    NSLog(@"Printinput %@",respond);
    
    if ([[respond objectForKey:@"status"]isEqualToString:@"Success"]) {
        NSDictionary * dataDic = [respond objectForKey:@"data"];
        
        NSString * fromdate = [dataDic objectForKey:@"valid_from"];
        NSString * todate = [dataDic objectForKey:@"valid_to"];
        NSString * cardno = [dataDic objectForKey:@"code"];
        NSString * amount = [dataDic objectForKey:@"amount"];
        
        GiftVoucherTotal = amount;
        VoucherCode = cardno;
        isValidateCurrentGV = YES;
//        NSString * status = [dataDic objectForKey:@"status"];
        
        BOOL isFromdateValid = NO;
        BOOL isTodateValid = NO;
        
        NSDate * now = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDate *dateFromString = [[NSDate alloc] init];
        NSDate *datetoString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:fromdate]; //Date valid from
        datetoString = [dateFormatter dateFromString:todate];  //Date valid to
        
        NSComparisonResult result1 = [now compare:dateFromString];
        NSComparisonResult result2 = [now compare:datetoString];
        
        if(result1==NSOrderedDescending)isFromdateValid = YES;
        if(result2==NSOrderedAscending)isTodateValid = YES;
        
        NSString * expirestatus = @"Voucher Expired !";
        if (isFromdateValid && isTodateValid) {
            NSLog(@"Valid");
        
//        [self btnCardDviewCancelClicks:nil];
//        [self CalculateBalance];
         expirestatus = @"Voucher Valid";
        }
        self.vigtAmountTL.text = amount;
        self.statusTL.text = expirestatus;
        self.createdDateTL.text = fromdate;
        self.lastvaliddateTL.text = todate;
        self.selectedAmountTF.text = amount;
        
          [self CalculateBalance];
        
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    [self hudWasHidden];
}

- (IBAction)btnCardDviewCancelClicks:(id)sender {
    
    if (isValidateCurrentGV)[self ValidateSelectedAmount];
    
    self.blureView.hidden = YES;
    self.GifiVview.hidden = YES;
    self.CardDetailsPopup.hidden = YES;
    
    [self.CardAmountTF resignFirstResponder];
    
}
-(NSString *)ValidateSelectedAmount{
    
    NSString * selectedAmount = self.selectedAmountTF.text;
    NSString * voucherAmount = self.vigtAmountTL.text;
    
    if (selectedAmount.doubleValue > 0 && selectedAmount.doubleValue <= voucherAmount.doubleValue) {
        
        [self insertPayment:VoucherCode :GIFTVOUCHER :GiftVoucherTotal :selectedAmount :@"NO"];
        
         isValidateCurrentGV = NO;
        self.vouchernumberTF.text = @"";
        self.createdDateTL.text = @"";
        self.lastvaliddateTL.text = @"";
        self.vigtAmountTL.text = @"";
        self.selectedAmountTF.text = @"";
        
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Invalid voucher amount." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        selectedAmount = @"0";
    }
    return selectedAmount;
}

#pragma mark -
#pragma mark CustomDiscount
- (IBAction)btnDisViewActiveClicks:(id)sender {
    
    [self AnimateDictountView:YES];
}

-(void)AnimateDictountView :(BOOL)isUp{
    
    if (isUp) {
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.discountView.frame;
                             frame.origin.y = 117;
                             [self.discountView setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
        
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.discountView.frame;
                             frame.origin.y = 770;
                             [self.discountView setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }
    
}

- (IBAction)btnPrecClicks:(id)sender {
    self.btncymbolTL.text = @"%";
    Distype = @"percentage";
    
    self.btnPrec.backgroundColor = [UIColor colorWithRed:210.0/255.0 green:57.0/255.0 blue:47.0/255.0 alpha:1];
    [self.btnPrec setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.btnAmount.backgroundColor = [UIColor whiteColor];
    [self.btnAmount setTitleColor:[UIColor colorWithRed:210.0/255.0 green:57.0/255.0 blue:47.0/255.0 alpha:1] forState:UIControlStateNormal];
    
}

- (IBAction)btnAmountClicks:(id)sender {
    self.btncymbolTL.text = CURRENCYSYMBOL;
    Distype = @"amount";
    
    self.btnAmount.backgroundColor = [UIColor colorWithRed:210.0/255.0 green:57.0/255.0 blue:47.0/255.0 alpha:1];
    [self.btnAmount setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.btnPrec.backgroundColor = [UIColor whiteColor];
    [self.btnPrec setTitleColor:[UIColor colorWithRed:210.0/255.0 green:57.0/255.0 blue:47.0/255.0 alpha:1] forState:UIControlStateNormal];
}

- (IBAction)btnDisViewDoneClicks:(id)sender {
    Disvalue = self.DisValueTF.text;
    
    if (Distype != nil) {
        
        if ([Distype isEqualToString:@"percentage"]) {
            
            if (Disvalue.doubleValue > 0 && Disvalue.doubleValue < 100) {
                [self setRefreshTotal];
                isDiscountHave = YES;
                [self AnimateDictountView:NO];
            }else{
                [self AlertToDisInfoWrong];
            }
            
        }else{
            
            if (Disvalue.doubleValue <= TotalValue.doubleValue && Disvalue.doubleValue > 0) {
                 [self setRefreshTotal];
                isDiscountHave = YES;
                [self AnimateDictountView:NO];
            }else{
                [self AlertToDisInfoWrong];
            }
        }
    }else{
        [self AlertToDisInfoWrong];
    }
}

-(void)AlertToDisInfoWrong{
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Please insert discount type and discount value." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    
    [alert show];
    
}

#pragma mark -
#pragma mark Detail setup

-(void)setRefreshTotal{
    
    
     float totalvalue = 00.00;
     float TotalCasherDiscount = 00.00;
    float adminDiscount = 00.00;
    NSString * subtotalstring = self.Subtotal;
    
    //SUBTOTAL
    self.subtotalTL.text = self.Subtotal;

    //TAX
    float Ordertax = [[[AppUserDefaults getPaymentdetails] objectForKey:@"taxForOrder"] floatValue];
    NSString * taxcalue = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (Ordertax/100)];
    self.taxTL.text = taxcalue;
    
    //SERVICE CHARGE
    float SC = [[[AppUserDefaults getPaymentdetails] objectForKey:@"serviceCharge"] floatValue];
    NSString * servicvalue = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (SC/100)];
    self.SCTL.text = servicvalue;
    
    //DISCOUNT
    
    //Discount from Admin
    float DiscoutforOrder = [[[AppUserDefaults getPaymentdetails] objectForKey:@"discountForOrder"] floatValue];

    NSString * discountablePrice = [self defaultDiscountForOrder];
    adminDiscount = [[NSString stringWithFormat:@"%.2f",discountablePrice.floatValue * (DiscoutforOrder/100)] floatValue];
    
    
    if ([Distype isEqualToString:@"percentage"]) {
        TotalCasherDiscount = [[NSString stringWithFormat:@"%.2f",discountablePrice.floatValue * (Disvalue.floatValue/100)] floatValue];
    }else{
        TotalCasherDiscount = Disvalue.floatValue;
    }
    
    float discount = 0.00;
    if (Disvalue.floatValue > 0) {
        discount = TotalCasherDiscount;
    }else{
        discount = adminDiscount;
    }

    self.DiscountTL.text = [NSString stringWithFormat:@"%.2f",discount];
    
    //CALCULATE TOTALVALUE
    totalvalue = taxcalue.floatValue + subtotalstring.floatValue+servicvalue.floatValue - discount;
    
    self.TotalTL.text = [NSString stringWithFormat:@"%@ %.2f",CURRENCYSYMBOL,totalvalue];
    TotalValue = [NSString stringWithFormat:@"%.2f",totalvalue];
    
}
-(NSString *)defaultDiscountForOrder{ //Get Select only eligible items
    
    float countAmount = 0.00;
    float extraAmount = 0.00;
    if (self.SplitID.intValue > 0) {   //Split records
        
         if (!dbmanager)dbmanager = [[DBManager alloc]init];
        NSArray * SplitdataArray = [dbmanager selectSplitRecord:[NSNumber numberWithInt:self.SplitID.intValue]];
        
        for (SplitData * item in SplitdataArray) {
             if (!carttbl)carttbl = [[SelectOrderDAO alloc]init];
            SelectOrder * currentItem = [carttbl getSelectFoodCategoryByRecordID:item.itemRecordID];

            if ([currentItem.defaultDiscountStatus isEqualToString:@"1"]) {
                countAmount = countAmount + item.price.floatValue;
            }
        }
        
    }else{
        
        if (!carttbl)carttbl = [[SelectOrderDAO alloc]init];
        
        NSArray * itemList = [ carttbl getSelectValidFoodCategorys];
        
        for (SelectOrder * item in itemList) {
            
            if ([item.defaultDiscountStatus isEqualToString:@"1"]) {
                countAmount = countAmount + item.totalPrice.floatValue;
                
                //ExtraItemPrice
                if (!dbmanager)dbmanager = [[DBManager alloc]init];
                NSArray * ExtraArray = [dbmanager GetAllExtrasByRID:item.recodId.intValue];
                
                for (ExtraItems * extra in ExtraArray) {
                    extraAmount = extraAmount + extra.extraitemPrice.floatValue;
                }
            }
        }
    }
    
    return [NSString stringWithFormat:@"%0.2f",countAmount + extraAmount];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
        if (ispaymentSuccess) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        ispaymentSuccess = NO;
       [self AnimateDictountView:NO];
    }
    else if (buttonIndex == 1) {
        NSLog(@"OK Tapped. Hello World!");
    }
}
- (IBAction)btnDisClearClicks:(id)sender {
    Disvalue = @"00.00";
    [self setRefreshTotal];
}

- (IBAction)btnComapnyClicks:(id)sender{

    [self.companyview AnimateviewOpen:YES];
    self.companyview.delegate = self;
}
- (IBAction)btnCompanyCancelClicks:(id)sender {
    
    [self.companyview AnimateviewOpen:NO];
}
-(void)didSelectCompanyDetails:(Companies *)company{
    
    NSLog(@"Company %@",company.cName);
    
    selectedComapny = company;
    [self.btnCompany setTitle:selectedComapny.cName forState:UIControlStateNormal];
    [self.companyview AnimateviewOpen:NO];
    
}
- (IBAction)btnCompanydeselectClicks:(id)sender {
    selectedCard = nil;
    [self.btnCompany setTitle:@"Company" forState:UIControlStateNormal];
}

-(void)inithud :(NSString *)message{
    
    if (self.isFromSplit) {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
    }else{
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
    }

    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}

@end
