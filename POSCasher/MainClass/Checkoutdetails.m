//
//  Checkoutdetails.m
//  Cashier
//
//  Created by Lakshitha on 04/11/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "Checkoutdetails.h"
#import "paydetailCell.h"
#import "Constants.h"
#import "PaymentDetails.h"

@interface Checkoutdetails ()

@end

@implementation Checkoutdetails

#pragma mark - Table view data source
- (void)awakeFromNib
{

}
-(void)viewDidLoad{
    
    [self activate];
}
-(void)activate{
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!detailsArray)detailsArray = [[NSMutableArray alloc]init];
    [detailsArray removeAllObjects];
    if ([self.paytype isEqualToString:CARD]) {
        [detailsArray addObjectsFromArray:[dbmanager GetAllCardDetails:CARD]];
    }else{
        [detailsArray addObjectsFromArray:[dbmanager GetAllCardDetails:GIFTVOUCHER]];
    }

    [self.detailtable reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [detailsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"paydetailCell";
    paydetailCell *cell = (paydetailCell *)[self.detailtable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"paydetailCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PaymentDetails * paymnet = [detailsArray objectAtIndex:indexPath.row];
    
   if ([self.paytype isEqualToString:CARD]) {
       cell.CardAmountTL.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,paymnet.amount];
   }else{
       cell.CardAmountTL.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,paymnet.interest];
   }
    
    cell.CardName.text = [NSString stringWithFormat:@"NO : %@",paymnet.typrdetails];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
    }
}
@end
