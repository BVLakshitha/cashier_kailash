//
//  CompanyView.h
//  Cashier
//
//  Created by Lakshitha on 7/20/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Companies.h"
#import "DBManager.h"

@protocol CompanyDetailsDelegate <NSObject>

-(void)didSelectCompanyDetails :(Companies *)company;

@end

@interface CompanyView : UIView<UITableViewDataSource,UITableViewDelegate>{
    
    NSMutableArray * ItemArray;
    NSMutableArray * filtedItemArray;
    DBManager * dbmanager;
    Companies * company;
}

- (IBAction)btnDoneClicks:(id)sender;
-(void)AnimateviewOpen:(BOOL)isUp;

@property (weak, nonatomic) IBOutlet UIButton * btnDone;
@property (weak, nonatomic) IBOutlet UITableView * companyTV;
@property (weak, nonatomic) IBOutlet UISearchBar *companySearchbar;
@property (nonatomic, assign) bool isFiltered;
@property ( nonatomic, retain ) id <CompanyDetailsDelegate> delegate;
-(void)dataLoader;

@end
