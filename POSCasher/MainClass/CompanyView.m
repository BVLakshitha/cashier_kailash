//
//  CompanyView.m
//  Cashier
//
//  Created by Lakshitha on 7/20/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import "CompanyView.h"
#import "CompanyCell.h"

@implementation CompanyView
@synthesize isFiltered;

-(void)awakeFromNib{
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    self.btnDone.layer.cornerRadius = 4;
    self.btnDone.layer.masksToBounds = YES;
    
}

-(void)AnimateviewOpen:(BOOL)isUp{
    
    if (isUp) {
        
        [self dataLoader];
        
        self.hidden = NO;
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.frame;
                             frame.origin.y = 193;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 768;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                         }
         ];
    }
}


- (IBAction)btnDoneClicks:(id)sender{
    [self AnimateviewOpen:NO];
}
#pragma mark -
#pragma mark Data Loader

-(void)dataLoader{
    
    if (!ItemArray)ItemArray = [[NSMutableArray alloc]init];
    [ItemArray removeAllObjects];
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSArray * data = [dbmanager GetAllCompanyDetails];
    [ItemArray addObjectsFromArray:data];
    
    //
    [self.companyTV reloadData];
}

#pragma mark -
#pragma mark SearchBar delegate


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
        
        if (!filtedItemArray)filtedItemArray = [[NSMutableArray alloc] init];
        [filtedItemArray removeAllObjects];
        
        for (Companies * items in ItemArray)
        {
            NSRange nameRange = [items.cName rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [items.cId rangeOfString:text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            { 
                [filtedItemArray addObject:items];
            }
        }
    }
    
    [self.companyTV reloadData];
}

#pragma mark -
#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (isFiltered) {
        
        return [filtedItemArray count];
    }else{
        return [ItemArray count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"CompanyCell";
    CompanyCell *cell = (CompanyCell *)[self.companyTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CompanyCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    
    if (isFiltered) {
        company = [filtedItemArray objectAtIndex:indexPath.row];
    }else{
        
        company = [ItemArray objectAtIndex:indexPath.row];
    }
    
    cell.companyNameTL.text = company.cName;
    cell.comapanyAddressTL.text = company.cAddress;
    cell.companyIdTL.text = company.cId;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (isFiltered) {
        company = [filtedItemArray objectAtIndex:indexPath.row];
    }else{
        company = [ItemArray objectAtIndex:indexPath.row];
    }
    [self.delegate didSelectCompanyDetails:company];
    
}
@end
