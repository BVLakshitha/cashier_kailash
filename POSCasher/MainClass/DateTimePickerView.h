//
//  DateTimePickerView.h
//  Cashier
//
//  Created by Lakshitha on 17/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DateTimePickerDelegate <NSObject>

-(void)didSelectDateTime :(NSString *)datetime;

@end

@interface DateTimePickerView : UIView

@property ( nonatomic, retain ) id <DateTimePickerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

- (IBAction)btnCancelClicks:(id)sender;
- (IBAction)btnDoneClicks:(id)sender ;


@end
