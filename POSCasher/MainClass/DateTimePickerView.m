//
//  DateTimePickerView.m
//  Cashier
//
//  Created by Lakshitha on 17/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "DateTimePickerView.h"

@implementation DateTimePickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib{
    [self addpipicker];
    
    
    
    
    self.btnDone.layer.masksToBounds = YES;
    self.btnDone.layer.cornerRadius = 4;
}
- (IBAction)btnCancelClicks:(id)sender{
    self.hidden = YES;
}
- (IBAction)btnDoneClicks:(id)sender{
     self.hidden = YES;
}
-(void)addpipicker{
    
    CGRect pickerFrame = CGRectMake(0,39,477,2016);
    
    UIDatePicker *myPicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    [myPicker addTarget:self action:@selector(pickerChanged:)               forControlEvents:UIControlEventValueChanged];
    [self addSubview:myPicker];
    
}
- (void)pickerChanged:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSLog(@"value: %@",[sender date]);
    
    NSString * datetime = [dateFormat stringFromDate:[sender date]];
    NSString * currentdate = [dateFormat stringFromDate:[NSDate date]];
    
    if ([self validateDateTime:[sender date]]) {
        [self.delegate didSelectDateTime:datetime];
    }else{
        [self.delegate didSelectDateTime:currentdate];
    }
}
-(BOOL)validateDateTime :(NSDate *)selecteddate{
    
    
    NSComparisonResult result = [[NSDate date] compare:selecteddate];
    if(result == NSOrderedDescending)
    {
        NSLog(@"date1 is later than date2");
        
        return false;
    }
    else if(result == NSOrderedAscending)
    {
        NSLog(@"date2 is later than date1");
        return true;
    }
    else
    {
        NSLog(@"date1 is equal to date2");
        return true;
    }
}
@end
