//
//  DeliveryDetails.h
//  Cashier
//
//  Created by Lakshitha on 17/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateTimePickerView.h"
#import "DBManager.h"
#import "Customer.h"
#import "WebserviceApi.h"
#import "MBProgressHUD.h"
#import "OrderList.h"

@protocol DeliveryDetailsDelegate <NSObject>

-(void)didAnimationOpenDeliveryDetails:(BOOL)isOpenAddcustomer;
-(void)didSelectDeleveryDetails :(BOOL)isSelected;
-(void)didUpdateDeliverDetails :(BOOL)updated;


@end

@interface DeliveryDetails : UIView<UITextFieldDelegate,DateTimePickerDelegate,webservicedelegate>{

    DBManager * dbmanager;
    Customer * customer;
    NSString * selectedtimeString;
    NSString * Deviverytype;
    WebserviceApi * webapi;
    MBProgressHUD * HUD;
    BOOL isUpdate;
    
}


@property ( nonatomic, retain ) id <DeliveryDetailsDelegate> delegate;
-(void)AnimatetableviewOpen:(BOOL)isUp;
@property (weak, nonatomic) IBOutlet DateTimePickerView *datetimepickerview;
@property (weak, nonatomic) IBOutlet UISegmentedControl *CDSegmentcontroller;

@property (nonatomic,assign)BOOL isFromhistory;
@property (nonatomic,retain)NSString * orderId;

@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *contactTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *timeTF;
@property (weak, nonatomic) IBOutlet UITextView *addressTV;
@property (weak, nonatomic) IBOutlet UILabel *datetimeviewTL;

//buttom top set.
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *btn15min;
@property (weak, nonatomic) IBOutlet UIButton *btn30min;
@property (weak, nonatomic) IBOutlet UIButton *btn45min;

@property (nonatomic,retain)OrderList * OrderlistDetails;
@property (nonatomic,retain)NSString * DeliveryType;

- (IBAction)btnCloseClicks:(id)sender;
- (IBAction)btnSaveClicks:(id)sender ;
- (IBAction)btn15minClicks:(id)sender;
- (IBAction)btn30minClicks:(id)sender;
- (IBAction)btn45minClicks:(id)sender;

- (IBAction)CDSegControllerClicks:(id)sender;

-(void)removeDeliveryDetails;
-(void)setAsCollection;

-(void)setDeliveryDetails;

- (IBAction)btnDoneClicks:(id)sender ;

@end
