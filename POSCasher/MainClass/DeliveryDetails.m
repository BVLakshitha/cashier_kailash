//
//  DeliveryDetails.m
//  Cashier
//
//  Created by Lakshitha on 17/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "DeliveryDetails.h"
#import "Customer.h"
#import "Constants.h"
#import "AppUserDefaults.h"
#import <QuartzCore/QuartzCore.h>

@implementation DeliveryDetails
@synthesize isFromhistory;
@synthesize orderId;

-(void)awakeFromNib{
    [self managetextfiedBG];
    self.datetimepickerview.delegate =self;
    [self addDatetotime:0];

    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}

#pragma mark Manage Screen
#pragma mark -

-(void)AnimatetableviewOpen:(BOOL)isUp{
    
    if (isUp) {

        [self CleanTexts];
        [self setDeliveryDetails];//set delivery details
        
//        [self setAsCollection];
        self.hidden = NO;
        [self.delegate didAnimationOpenDeliveryDetails:YES];
        [AppUserDefaults setDeliveryType:COLLECTIONORDER];
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.frame;
                             frame.origin.y = 102;
                             [self setFrame:frame];

                         }
                         completion:^(BOOL finished){
                             [self addGuesture:YES];
                         }
         ];
    }else{
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 768;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             [self.delegate didAnimationOpenDeliveryDetails:NO];
                             [self addGuesture:NO];
                         }
         ];
    }
}
-(void)managebtn{
    
    self.btn15min.layer.masksToBounds = YES;
    self.btn15min.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.btn15min.layer.borderWidth = 1.0f;
    self.btn15min.backgroundColor = [UIColor whiteColor];
    [self.btn15min setTitleColor:[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] forState:UIControlStateNormal];
    
    self.btn30min.layer.masksToBounds = YES;
    self.btn30min.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.btn30min.layer.borderWidth = 1.0f;
    self.btn30min.backgroundColor = [UIColor whiteColor];
    [self.btn30min setTitleColor:[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] forState:UIControlStateNormal];
    
    self.btn45min.layer.masksToBounds = YES;
    self.btn45min.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.btn45min.layer.borderWidth = 1.0f;
    self.btn45min.backgroundColor = [UIColor whiteColor];
    [self.btn45min setTitleColor:[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] forState:UIControlStateNormal];
    
}
-(void)managetextfiedBG{
    
    self.nameTF.layer.masksToBounds = YES;
    self.nameTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.nameTF.layer.borderWidth = 1.0f;
    
    self.contactTF.layer.masksToBounds = YES;
    self.contactTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.contactTF.layer.borderWidth = 1.0f;
    
    self.emailTF.layer.masksToBounds = YES;
    self.emailTF.layer.borderColor = [[UIColor grayColor] CGColor];
    self.emailTF.layer.borderWidth = 1.0f;
    
    self.timeTF.layer.masksToBounds = YES;
    self.timeTF.layer.borderColor = [[UIColor redColor] CGColor];
    self.timeTF.layer.borderWidth = 1.0f;
    
    self.addressTV.layer.masksToBounds = YES;
    self.addressTV.layer.borderColor = [[UIColor grayColor] CGColor];
    self.addressTV.layer.borderWidth = 1.0f;

    self.btnCancel.layer.masksToBounds = YES;
    self.btnCancel.layer.cornerRadius = 4;
    
    self.btnSave.layer.masksToBounds = YES;
    self.btnSave.layer.cornerRadius = 4;
    
    self.btnDone.layer.masksToBounds = YES;
    self.btnDone.layer.cornerRadius = 4;
    
    self.btn15min.layer.masksToBounds = YES;
    self.btn15min.layer.cornerRadius = 4;
    
    self.btn30min.layer.masksToBounds = YES;
    self.btn30min.layer.cornerRadius = 4;
    
    self.btn45min.layer.masksToBounds = YES;
    self.btn45min.layer.cornerRadius = 4;
    
    self.timeTF.layer.masksToBounds = YES;
    self.timeTF.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
    self.timeTF.layer.borderWidth = 1.0f;
    self.timeTF.backgroundColor = [UIColor whiteColor];
    self.timeTF.layer.cornerRadius = 4;
    
    [self managebtn];
}

#pragma mark Button Actions
#pragma mark -

- (IBAction)btnCloseClicks:(id)sender {
    [self AnimatetableviewOpen:NO];
}
- (IBAction)btnSaveClicks:(id)sender {
    isUpdate = YES;
    [self saveDeliveryDetails];
}

- (IBAction)btnDoneClicks:(id)sender {
    isUpdate = NO;
    [self saveDeliveryDetails];
}

- (IBAction)btn15minClicks:(id)sender {
    [self managebtn:1];
    [self deSelectCustomerTime];
    [self addDatetotime:15];
}
- (IBAction)btn30minClicks:(id)sender {
    [self managebtn:2];
    [self deSelectCustomerTime];
    [self addDatetotime:30];
}
- (IBAction)btn45minClicks:(id)sender {
    [self managebtn:3];
    [self deSelectCustomerTime];
    [self addDatetotime:45];
}

#pragma mark Delivery Time
#pragma mark -

-(void)managebtn :(int)index{
    
    [self managebtn];
    
    switch (index) {
            
        case 1:
            
            self.btn15min.layer.masksToBounds = YES;
            self.btn15min.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
            self.btn15min.layer.borderWidth = 1.0f;
            self.btn15min.backgroundColor = [UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1];
            [self.btn15min setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        case 2:
            
            self.btn30min.layer.masksToBounds = YES;
            self.btn30min.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
            self.btn30min.layer.borderWidth = 1.0f;
            self.btn30min.tintColor = [UIColor whiteColor];
            self.btn30min.backgroundColor = [UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1];
            [self.btn30min setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        case 3:
            
            self.btn45min.layer.masksToBounds = YES;
            self.btn45min.layer.borderColor = [[UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1] CGColor];
            self.btn45min.layer.borderWidth = 1.0f;
            self.btn45min.tintColor = [UIColor whiteColor];
            self.btn45min.backgroundColor = [UIColor colorWithRed:(224/255.0) green:(24/255.0) blue:(32/255.0) alpha:1];
            [self.btn45min setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
    
}
/*
-(void)setDeliveryDetails :(OrderList *)orderlist{
    
    NSString * time = orderlist.deliveryDate;
    NSString * name = orderlist.deliveryName;
    NSString * email = orderlist.deliveryEmail;
    NSString * address = orderlist.deliveryAddress;
    NSString * contact = orderlist.deliveryPhone;
    
    
    self.timeTF.text = time;
    self.nameTF.text = name;
    self.emailTF.text = email;
    self.addressTV.text = address;
    self.contactTF.text = contact;
    
    [self.delegate didSelectDeleveryDetails:YES];
    
}
 */
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 1){
       [self managebtn];
        self.datetimepickerview.hidden = NO;
        return NO;
    }
    
    return true;
}

-(void)deSelectCustomerTime{
    
    [self.timeTF resignFirstResponder];
    [self.timeTF setText:@""];
    
}
-(void)didSelectDateTime:(NSString *)datetime{
    
    self.timeTF.text = datetime;
    self.datetimeviewTL.text = [NSString stringWithFormat:@"Delivery Time Set For : %@",datetime];
    selectedtimeString = datetime;
    
}
-(NSString *)addDatetotime :(int)time{
   
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate* newDate = [[NSDate date] dateByAddingTimeInterval:(time*60)];
    NSString * fineldate = [dateFormatter1 stringFromDate:newDate];
    
    self.datetimeviewTL.text = [NSString stringWithFormat:@"Delivery Time Set For : %@",fineldate];

    selectedtimeString = fineldate;
    return fineldate;
}
#pragma mark Delivery Details
#pragma mark -

-(void)setDeliveryDetails{
    
    
     if (isFromhistory){
         
         self.btnDone.enabled = NO;
         self.btnSave.enabled = YES;
         self.orderId = 0;
         [self setDeliveryTypeForOrder:self.DeliveryType];
         
         NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
         
         if ([customerdefalut objectForKey:@"SCD"]!=nil) {
             if (!dbmanager)dbmanager = [[DBManager alloc]init];
             
             customer = [dbmanager isRecordExistanceCustomer:[customerdefalut objectForKey:@"SCD"]];
             
             self.nameTF.text = [NSString stringWithFormat:@"%@ %@",customer.firstName,customer.lastName];
             self.contactTF.text = customer.contactNo;
             self.emailTF.text = customer.email;
             
             NSString * dono = customer.dono;
             NSString * city = customer.city;
             NSString * address = customer.address;
             
             if (customer.dono == nil || [customer.dono isEqualToString:@"null"])dono = @"";
             if (customer.city == nil || [customer.city isEqualToString:@"null"])city = @"";
             if (customer.address == nil || [customer.address isEqualToString:@"null"])address = @"";
             self.addressTV.text = [NSString stringWithFormat:@"%@  %@  %@",dono,city,address];
             
         }else{
             
             NSString * time = self.OrderlistDetails.deliveryDate;
             NSString * name = self.OrderlistDetails.deliveryName;
             NSString * email = self.OrderlistDetails.deliveryEmail;
             NSString * address = self.OrderlistDetails.deliveryAddress;
             NSString * contact = self.OrderlistDetails.deliveryPhone;
             
             
             self.timeTF.text = time;
             self.nameTF.text = name;
             self.emailTF.text = email;
             self.addressTV.text = address;
             self.contactTF.text = contact;
             
         }

         
         [self.delegate didSelectDeleveryDetails:YES];
         
     }else{
         self.btnSave.enabled = NO;
         self.btnDone.enabled = YES;
         
         NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
         
         if ([customerdefalut objectForKey:@"SCD"]!=nil) {
             if (!dbmanager)dbmanager = [[DBManager alloc]init];
             
             customer = [dbmanager isRecordExistanceCustomer:[customerdefalut objectForKey:@"SCD"]];
             
             self.nameTF.text = [NSString stringWithFormat:@"%@ %@",customer.firstName,customer.lastName];
             self.contactTF.text = customer.contactNo;
             self.emailTF.text = customer.email;
             
             NSString * dono = customer.dono;
             NSString * city = customer.city;
             NSString * address = customer.address;
             
             if (customer.dono == nil || [customer.dono isEqualToString:@"null"])dono = @"";
             if (customer.city == nil || [customer.city isEqualToString:@"null"])city = @"";
             if (customer.address == nil || [customer.address isEqualToString:@"null"])address = @"";
             self.addressTV.text = [NSString stringWithFormat:@"%@  %@  %@",dono,city,address];
             
         }else{
             
             UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"You didn't select customer details." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
             [alert show];
             
         }
     }
}

#pragma mark Delivery Details Save
#pragma mark -

-(void)saveDeliveryDetails{
    
    NSDictionary * cusDetails = nil;
    BOOL isAllOk = YES;
    
    NSString * deliveryName = self.nameTF.text;
    NSString * deliveryEmail = self.emailTF.text;
    NSString * deliveryAddress = self.addressTV.text;
    NSString * deliveryContact = self.contactTF.text;
    
    NSString * deliveryDateTime = selectedtimeString;
 //   NSArray * datetime = [deliveryDateTime componentsSeparatedByString:@" "];
 //   NSString * deliveryDate =[datetime firstObject];
 //   NSString * deliveryTime = [datetime lastObject];
    
    
    //SetOriginalContact if field empty
    if (deliveryName == nil || [deliveryName isEqualToString:@"null"])deliveryName =customer.firstName;
    if (deliveryEmail == nil || [deliveryEmail isEqualToString:@"null"])deliveryEmail =customer.email;
    if (deliveryAddress == nil || [deliveryAddress isEqualToString:@"null"])deliveryAddress =customer.address;
    if (deliveryContact == nil || [deliveryContact isEqualToString:@"null"])deliveryContact =customer.contactNo;
    
    if (deliveryAddress == nil || [deliveryAddress isEqualToString:@""])deliveryAddress = @" ";
    if (deliveryName == nil || [deliveryName isEqualToString:@""])deliveryName = @" ";
    if (deliveryEmail == nil || [deliveryEmail isEqualToString:@""])deliveryEmail = @" ";
    if (deliveryContact == nil || [deliveryContact isEqualToString:@""])deliveryContact = @" ";
    
    
    
    
    if (![self NSStringIsValidEmail:deliveryEmail])isAllOk = NO;
    
    if (![self validatePhoneNumber:deliveryContact])isAllOk = NO;
    
    if ([deliveryName length] <= 0)isAllOk = NO;
    
    
    
    
    
    if (isAllOk) {
        if ([[AppUserDefaults getDeliveryType] isEqualToString:DELIVERYORDER]) {
            if (self.addressTV.text.length > 0) {
                cusDetails = @{@"deliveryDate"         : deliveryDateTime,
                               @"deliveryAddress"      : deliveryAddress,
                               @"deliveryPerson"       : deliveryName,
                               @"deliveryPhone"        : deliveryContact,
                               @"deliveryEmail"        : deliveryEmail};
                [self updateDeliveryDetails:cusDetails];
                
            }else{
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Address field empty." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }else{
            cusDetails = @{@"deliveryDate"         : deliveryDateTime,
                           @"deliveryAddress"      : deliveryAddress,
                           @"deliveryPerson"       : deliveryName,
                           @"deliveryPhone"        : deliveryContact,
                           @"deliveryEmail"        : deliveryEmail};
            [self updateDeliveryDetails:cusDetails];
        }
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Email or Password incorrect." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(BOOL)validatePhoneNumber:(NSString *)Phonenumber{
    
    BOOL isvalidPhonenumber = YES;

    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([Phonenumber rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        isvalidPhonenumber = YES;
    }else{
        isvalidPhonenumber = NO;
    }
    
    return isvalidPhonenumber;
}
-(void)updateDeliveryDetails :(NSDictionary *)deliverinfomations{           //Save Details
    
    NSUserDefaults * deliverydetails = [NSUserDefaults standardUserDefaults];
    [deliverydetails setObject:deliverinfomations forKey:@"SDD"];
    [deliverydetails synchronize];
    
    NSLog(@"OrderID %@",self.orderId);
    NSLog(@"Dic %@",deliverinfomations);

    if (isUpdate) {

        
        
        
        NSDictionary * deliverywebcall = @{@"key"             : @"BAPOS@2014#",
                                         @"order_id"          : @"141",
                                         @"delievry_address"  : [deliverinfomations objectForKey:@"deliveryAddress"],
                                         @"delievry_email"    : [deliverinfomations objectForKey:@"deliveryEmail"],
                                         @"delievry_name"     : [deliverinfomations objectForKey:@"deliveryPerson"],
                                         @"delievry_phone"    : [deliverinfomations objectForKey:@"deliveryPhone"],
                                         @"delivery_datetime" : [deliverinfomations objectForKey:@"deliveryDate"]};
        
        
//        NSMutableDictionary * deliverywebcall = [[NSMutableDictionary alloc]initWithDictionary:deliverinfomations];
//        [deliverywebcall setObject:WEBSERVICEKEY forKey:@"key"];
//        [deliverywebcall setObject:self.orderId forKey:@"order_id"];
        
        NSLog(@"logedDetails %@",deliverywebcall);
        
        if (!webapi)webapi = [[WebserviceApi alloc]init];
        webapi.delegate = self;
        [webapi UpdateDeliveryDetails:deliverywebcall];
        [self inithud:@"Updating.."];
    }else{
        [self AnimatetableviewOpen:NO];
        [self.delegate didSelectDeleveryDetails:YES];
    }
    

}
-(void)DidReceiveDeliveryUpdateDetails:(id)respond{
    [self hudWasHidden];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:[respond objectForKey:@"Status"] message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
    if ([[respond objectForKey:@"Status"] isEqualToString:@"Success"]) {
        [self.delegate didSelectDeleveryDetails:YES];
        [self.delegate didUpdateDeliverDetails:YES];
    }
}
-(void)CleanTexts{
    
    self.nameTF.text = @"";
    self.emailTF.text = @"";
    self.addressTV.text = @"";
    self.contactTF.text = @"";
    
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)removeDeliveryDetails{
    NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
    [customerdefalut removeObjectForKey:@"SDD"];
    if ([customerdefalut synchronize]){
        [self.delegate didSelectDeleveryDetails:NO];
    }
}

- (IBAction)CDSegControllerClicks:(id)sende{

    if (self.CDSegmentcontroller.selectedSegmentIndex == 1) {
         [AppUserDefaults setDeliveryType:COLLECTIONORDER];
        
    }else if (self.CDSegmentcontroller.selectedSegmentIndex == 2){
        [AppUserDefaults setDeliveryType:TABLEORDER];
        
    }else{
        [AppUserDefaults setDeliveryType:DELIVERYORDER];
    }
}
-(void)setAsCollection{
    self.CDSegmentcontroller.selectedSegmentIndex = 1;
    NSUserDefaults * CDUserdefalt = [NSUserDefaults standardUserDefaults];
    Deviverytype = @"1";
    [CDUserdefalt setObject:Deviverytype forKey:@"DT"];
}
#pragma mark -
#pragma mark AnimateUp
-(void)textFieldDidBeginEditing:(UITextField *)textField{
//    [self AnimateUP:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self AnimateUP:NO];
    return NO;
}


-(void)AnimateUP:(BOOL)isUp{ //Open and Close View
    
    if (isUp) {
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = -110;
                             [self setFrame:frame];
//                             self.frame = CGRectMake(157,-120, 512, 487);
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 102;
                             [self setFrame:frame];
//                             self.frame = CGRectMake(157,109, 512, 487);
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }
}
-(void)addGuesture :(BOOL)iSadd{
    
    if (iSadd) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }else{
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillHideNotification
                                                      object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillShowNotification
                                                      object:nil];
    }
    
}

-(void)setDeliveryTypeForOrder :(NSString *)CType{
    
   
    int CtypeInt = CType.intValue;
    self.CDSegmentcontroller.selectedSegmentIndex = UISegmentedControlNoSegment;
    switch (CtypeInt) {
        case 0:
            self.CDSegmentcontroller.selectedSegmentIndex = 1;
            break;
            
        case 1:
            self.CDSegmentcontroller.selectedSegmentIndex = 0;
            break;
            
        case 2:
            self.CDSegmentcontroller.selectedSegmentIndex = 2;
            break;
            
        default:
            break;
    }
    [self.CDSegmentcontroller sendActionsForControlEvents:UIControlEventValueChanged];
}
-(void)keyboardWillShow {
    [self AnimateUP:YES];
}
-(void)keyboardWillHide {
     [self AnimateUP:NO];
}
-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self];
    [self addSubview:HUD];
    HUD.labelText = message;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}
@end
