//
//  GiftVoucherView.h
//  Cashier
//
//  Created by User on 08/01/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DateTimePickerView.h"
#import "DBManager.h"
#import "WebserviceApi.h"

@protocol GiftVoucherViewDelegate <NSObject>

-(void)didAnimationOpenGiftView:(BOOL)isOpen;

@end

@interface GiftVoucherView : UIView<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,webservicedelegate>{
    
    int selectedTFTag;
    DBManager * dbmanager;
    WebserviceApi * webservice;
    BOOL isVoucherCodeGenarated;
}

-(void)AnimateviewOpen:(BOOL)isUp;

@property(nonatomic,retain)NSMutableArray * Vaouchers;
@property(nonatomic,retain)NSMutableArray * VaoucherCart;
@property(nonatomic,retain)NSMutableArray * Voucherbucket;


@property (weak, nonatomic) IBOutlet UIButton *btnDone;
- (IBAction)btnDoneClicks:(id)sender;
- (IBAction)btnSaveClicks:(id)sender;
- (IBAction)btnClearClicks:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *FromTF;
@property (weak, nonatomic) IBOutlet UITextField *ToTF;
@property (weak, nonatomic) IBOutlet DateTimePickerView *datetimepickerview;

@property ( nonatomic, retain ) id <GiftVoucherViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *VouchersTV;
@property (weak, nonatomic) IBOutlet UITableView *VoucherCartTV;
@property (weak, nonatomic) IBOutlet UILabel *VouchernameTL;
@property (weak, nonatomic) IBOutlet UILabel *VoucherTotalAmoutTL;
@end
