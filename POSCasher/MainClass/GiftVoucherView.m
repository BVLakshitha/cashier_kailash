//
//  GiftVoucherView.m
//  Cashier
//
//  Created by User on 08/01/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "GiftVoucherView.h"
#import "VoucherListCell.h"
#import "VoucherCartCell.h"
#import "GiftVouchers.h"
#import "AppUserDefaults.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@interface GiftVoucherView ()

@end

@implementation GiftVoucherView
-(void)awakeFromNib{

    NSMutableArray * Varray = [[NSMutableArray alloc]initWithArray:[AppUserDefaults getVoucherConstants]];
    [self loadVouchers:Varray];
    [self loadVoucherCart];
    
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}
-(void)loadVouchers :(NSArray *)vouchers{
    self.Vaouchers = [[NSMutableArray alloc]initWithArray:vouchers];
    [self.VouchersTV reloadData];
    
}
-(void)loadVoucherCart{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * giftArray = [dbmanager GetAllVouchers];
    self.VaoucherCart = [[NSMutableArray alloc]initWithArray:giftArray];
    
    double vouchertatal = 0.00;
    for (GiftVouchers * vouchers in self.VaoucherCart) {
        
        vouchertatal = vouchertatal +vouchers.amount.doubleValue;
    }
    self.VoucherTotalAmoutTL.text = [NSString stringWithFormat:@"%0.2f",vouchertatal];
    
    [self.VoucherCartTV reloadData];
    [self.VouchersTV reloadData];
}
-(void)AnimateviewOpen:(BOOL)isUp{
    
    if (isUp) {
        self.hidden = NO;
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.frame;
                             frame.origin.y = 102;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             [self clearcart];
                             [self loadVoucherCart];
                         }
         ];
    }else{
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 768;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                            self.hidden = YES;
                             [self.delegate didAnimationOpenGiftView:NO];
                             [self clearcart];
                             [self loadVoucherCart];
                         }
         ];
    }
}
- (IBAction)btnDoneClicks:(id)sender{
    
    [self AnimateviewOpen:NO];
}


- (IBAction)btnSaveClicks:(id)sender{

    if (isVoucherCodeGenarated) {
        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Voucher code Genarated.Please clean the voucher cart." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertview show];
    }else{
        
           if (!dbmanager)dbmanager = [[DBManager alloc]init];
        NSArray * giftArray = [dbmanager GetAllVouchers];
        
        if ([giftArray count] > 0) {
            if (!webservice)webservice = [[WebserviceApi alloc]init];
            webservice.delegate = self;
            [webservice getGiftvoucherDetails:[self sendingGiftRecords]];
        }else{
            UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Vouchers not found !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertview show];
        }
    }
}
- (IBAction)btnClearClicks:(id)sender{
    
    [self clearcart];
    [self loadVoucherCart];
}
-(void)clearcart{
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    [dbmanager deleteAllGiftVouchers];
    isVoucherCodeGenarated = NO;
    
}
-(NSDictionary *)sendingGiftRecords{
    
    NSArray * giftArray = [AppUserDefaults getVoucherConstants];
    if (!self.Voucherbucket)self.Voucherbucket = [[NSMutableArray alloc]init];
    [self.Voucherbucket removeAllObjects];
    NSString * voucherJson = @"";
    
    
    for (NSString * voucher in giftArray) {
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        
        NSArray * voucherbundle =  [dbmanager GetAllVouchersByconstant:voucher];
        
        if (voucherbundle.count > 0) {
            
            NSString * Vcount = [NSString stringWithFormat:@"%i",voucherbundle.count];
            NSDictionary * Vdisctionary = @{@"amount" : voucher,
                                            @"qty" : Vcount};
            [self.Voucherbucket addObject:Vdisctionary];
        }
    }
    
    if (self.Voucherbucket > 0) {
        NSDictionary * dic = @{@"voucherData" : self.Voucherbucket};
       voucherJson = [self jsonconverter:dic];
    }
    
    NSDictionary * voucherbundle = @{@"values" :voucherJson,
                                     @"key" : WEBSERVICEKEY};
    NSLog(@"Voucher Details %@",voucherbundle);
    return voucherbundle;
}
-(NSString *)jsonconverter :(NSDictionary *)dictionary{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
-(void)DidReceiveGiftVoucherDetails:(id)respond{
    
    NSLog(@"GiftVoucher %@",respond);
    NSArray * giftArray = [AppUserDefaults getVoucherConstants];
    NSDictionary * dataDictionary = [respond objectForKey:@"data"];
     if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    for (NSString * voucher in giftArray) {
        NSArray * voucherbundle =  [dbmanager GetAllVouchersByconstant:voucher];
        NSArray * incommingRecordDetals = [dataDictionary objectForKey:voucher];
        
        for (int i = 0; i<voucherbundle.count; i++) {
            
            NSString * voucherID = [incommingRecordDetals objectAtIndex:i];
            GiftVouchers * voucher = [voucherbundle objectAtIndex:i];
            NSString * amount = voucher.amount;
            NSString * Discriotion = voucher.discription;
            
            [dbmanager deleteSelectedGiftVouchersByRid:voucher.recordID];
            [dbmanager insertGiftVoucher:voucherID :amount :Discriotion];
            isVoucherCodeGenarated = YES;
        }
    }
    [self loadVoucherCart];
}
-(void)didSelectDateTime:(NSString *)datetime{
    
    if (selectedTFTag == 10) {
        self.FromTF.text = datetime;
    }else if (selectedTFTag == 11){
        self.ToTF.text = datetime;
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.tag == 10){
        self.datetimepickerview.hidden = NO;
        selectedTFTag = 10;
        return NO;
    }else if (textField.tag == 11){
        self.datetimepickerview.hidden = NO;
        selectedTFTag = 11;
        return NO;
    }else{
        selectedTFTag = 0;
        self.datetimepickerview.hidden = YES;
        return true;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.VouchersTV) {
        return self.Vaouchers.count;
    }else{
        return self.VaoucherCart.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.VouchersTV) {
        
        static NSString *simpleTableIdentifier = @"VoucherListCell";
        VoucherListCell *cell = (VoucherListCell *)[self.VouchersTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        NSString * voucheramt = [self.Vaouchers objectAtIndex:indexPath.row];
        cell.VouchernameTL.text = [NSString stringWithFormat:@"%@ £",voucheramt];
        return cell;
    }else{
        static NSString *simpleTableIdentifier = @"VoucherCartCell";
        VoucherCartCell *cell = (VoucherCartCell *)[self.VoucherCartTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherCartCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        
        GiftVouchers * Voucher = [self.VaoucherCart objectAtIndex:indexPath.row];
        cell.VoucherIDTL.text = Voucher.voucherID;
        cell.VoucherPriceTL.text = [NSString stringWithFormat:@"%@ £",Voucher.amount];
        return cell;
        
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.VouchersTV) {
        
        if (isVoucherCodeGenarated) {
            UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Voucher code Genarated.Please clean the voucher cart." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertview show];
        }else{
            NSString * voucheramt = [self.Vaouchers objectAtIndex:indexPath.row];
            if (!dbmanager)dbmanager = [[DBManager alloc]init];
            [dbmanager insertGiftVoucher:@"0000" :voucheramt :@"XXXX"];
            [self loadVoucherCart];
        }
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
     if (tableView == self.VouchersTV) {
         
     }else{
         if (editingStyle == UITableViewCellEditingStyleDelete) {
             GiftVouchers * Voucher = [self.VaoucherCart objectAtIndex:indexPath.row];
              if (!dbmanager)dbmanager = [[DBManager alloc]init];
             [dbmanager RemoveGiftvoucherById:Voucher.recordID.stringValue];
             [self loadVoucherCart];
         }
     }
}
@end
