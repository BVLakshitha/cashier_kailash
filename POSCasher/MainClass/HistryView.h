//
//  HistryView.h
//  POSCasher
//
//  Created by Lakshitha on 9/24/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderHistoryDelegate <NSObject>

-(void)didSelectTable :(NSNumber *)flowid :(NSNumber *)tableid;

@end

@interface HistryView : UIView
-(void)AnimatetableviewOpen:(BOOL)isUp;
@property ( nonatomic, retain ) id <OrderHistoryDelegate> delegate;

@end
