//
//  HistryView.m
//  POSCasher
//
//  Created by Lakshitha on 9/24/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "HistryView.h"

@implementation HistryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)AnimatetableviewOpen:(BOOL)isUp{
    
    if (isUp) {
        self.hidden = NO;
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(642,64, 381, 720);
                         }
                         completion:^(BOOL finished){

                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(1024, 64, 381, 720);
                             
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                         }
         ];
    }
}

@end
