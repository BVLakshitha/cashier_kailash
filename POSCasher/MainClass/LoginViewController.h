//
//  LoginViewController.h
//  POSCasher
//
//  Created by orderfood on 23/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceApi.h"
#import "DBManager.h"
#import "OrderHistoryItemDao.h"
#import "OrderListDao.h"
#import "FoodCategoryDAO.h"

@interface LoginViewController : UIViewController<MBProgressHUDDelegate,webservicedelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    
    WebserviceApi * webserviceapi;
    MBProgressHUD *HUD;
    DBManager * dbmanager;
    OrderHistoryItemDao * orderhistory;
    OrderListDao * orderlist;
    FoodCategoryDAO * foodcategory;
    
    NSMutableArray * TillsArray;
    NSString * currectTill;
    
    NSDictionary * Loginparams;
    BOOL isTillEnable;
    NSString * myname;
}

@property (weak, nonatomic) IBOutlet UITextField *CasherNameTF;
@property (weak, nonatomic) IBOutlet UITextField *PasswordTF;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;



//Till
@property (weak, nonatomic) IBOutlet UIView *TillView;
@property (weak, nonatomic) IBOutlet UIPickerView *tillPickerView;

@property (weak, nonatomic) IBOutlet UIButton *btnTillDone;
- (IBAction)btnTillDoneClicks:(id)sender;

//clientretails
@property (weak, nonatomic) IBOutlet UILabel *clientNameTL;



- (IBAction)LoginbtnClicks:(id)sender;

@end
