//
//  LoginViewController.m
//  POSCasher
//
//  Created by orderfood on 23/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "LoginViewController.h"
#import "Reachability.h"
#import "WebserviceApi.h"
#import "OrderHistoryItemDao.h"
#import "OrderListDao.h"
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MiniPrinterFunctions.h"
#import "AppDelegate.h"
#import "AppUserDefaults.h"
#import "Constants.h"
#import "FoodCategoryDAO.h"
#import "FoodCategory.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    self.PasswordTF.secureTextEntry = YES;
    self.CasherNameTF.layer.masksToBounds = YES;
    [[self.CasherNameTF layer] setBorderColor:[[UIColor redColor] CGColor]];
    self.CasherNameTF.layer.borderWidth = 1.0f;
    
    self.PasswordTF.layer.masksToBounds = YES;
    [[self.PasswordTF layer] setBorderColor:[[UIColor redColor] CGColor]];
    self.PasswordTF.layer.borderWidth = 1.0f;
    
    self.btnLogin.layer.cornerRadius = 7.0f;
    self.btnLogin.layer.masksToBounds = YES;
    
    //TillView
    
    self.TillView.layer.cornerRadius = 7.0f;
    self.TillView.layer.masksToBounds = YES;
    
    self.btnTillDone.layer.cornerRadius = 7.0f;
    self.btnTillDone.layer.masksToBounds = YES;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
//    [[self.CasherNameTF layer] setBorderColor:[[UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0] CGColor]];
 
  //  [self CleanUserdata];
    
    [super viewWillAppear:YES];
}

#pragma mark -
#pragma mark printing Process

+ (void)setPortName:(NSString *)m_portName
{
    [AppDelegate setPortName:m_portName];
}

+ (void)setPortSettings:(NSString *)m_portSettings
{
    [AppDelegate setPortSettings:m_portSettings];
}

- (void)setPortInfo
{
    NSString *localPortName = @"BT:PRNT Star";
    [LoginViewController setPortName:localPortName];
    
    NSString *localPortSettings = @"mini";
    
    [LoginViewController setPortSettings:localPortSettings];
}


#pragma mark -
#pragma mark Login Process

- (IBAction)LoginbtnClicks:(id)sender {
//    [self loginProcess];
    
//    [self loadTillPickerdata];
//    self.CasherNameTF.text = @"admin";
//    self.PasswordTF.text = @"order123";
    
    [self callForResturenttDetails];
}

-(void)callForResturenttDetails{
    
    [self inithud:@"Loading.."];
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi GetRestuarentProfile];
    NSLog(@"Resturent Profile  Fatcheing..");
    
}
-(void)DidReceiveRestuarentProfile:(id)respond{
    [self hudWasHidden];
    
    if ([[respond objectForKey:@"Status"]isEqualToString:@"Success"]) {
        NSDictionary * dataDic = [respond objectForKey:@"data"];
        [AppUserDefaults setVoucherConstants:[dataDic objectForKey:@"voucherAmounts"]];//VOUCHERSDETAILS
        
        NSString * tillStatus = [dataDic objectForKey:@"tillEnable"];

        if ([tillStatus isEqualToString:@"1"]) {
            isTillEnable = YES;
        }else{
            isTillEnable = NO;
        }
        
        if (isTillEnable) {
             [self loadTillPickerdata:[dataDic objectForKey:@"masterTill"]];
        }else{
            Loginparams = @{@"username"    : self.CasherNameTF.text,
                            @"password"    : self.PasswordTF.text,
                            @"key" : WEBSERVICEKEY};
            [self loginProcess];
        }
        
        
        [AppUserDefaults setResturantDetails:[dataDic  objectForKey:@"restuarent"]];
        
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        
        NSArray * companyDetailsArray = [dataDic  objectForKey:@"companyUsers"];
        for (NSDictionary * company in companyDetailsArray) {
            [dbmanager insetorupdatCompanydetails:company];
        }
        
        NSString * Resname = [AppUserDefaults getResturentName];
        
        self.clientNameTL.text = Resname;
    }
    
    NSLog(@"Resturent Profile  Fatched");
}

-(void)loginProcess{
    
    NSLog(@"Login Start");
    
    [self inithud:@"Validating"];
    if ([self ReachabilityPass]) {
        if ([self.CasherNameTF.text length] > 0 && [self.PasswordTF.text length]>0) {
            
            if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
            webserviceapi.delegate = self;
            [webserviceapi Loginme:Loginparams];
            
        }else{
            [self hudWasHidden];
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Fill all fields !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
         [self hudWasHidden];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"No Internet !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)DidReceiveLogininfo:(NSArray *)respond{
    NSLog(@"LOGIN respong %@",respond);
    [self hudWasHidden];
    
    BOOL isSuccess = [[[respond firstObject] objectForKey:@"success"]boolValue];
    
    if (!isSuccess) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:[[respond firstObject] objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [self hudWasHidden];
        NSLog(@"Login Fail");
    }else{
        
        NSMutableArray *userId=[[NSMutableArray alloc]init];
        userId=[respond valueForKey:@"userid"];
        NSString *userIdValue=[userId objectAtIndex:0];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:userIdValue forKey:@"userLoginId"];
        [AppUserDefaults setUserID:userIdValue];
        [userDefaults setObject:self.CasherNameTF.text forKey:@"userName"];
        NSLog(@"Login Success");
        
        if (isTillEnable) {
            
            NSString * masterTillId = [[respond valueForKey:@"tillMasterId"] firstObject];
            NSString * sessionTillId = [[respond valueForKey:@"tillSessionId"] firstObject];
            
            [AppUserDefaults setMasterTillId:masterTillId];
            [AppUserDefaults setSessionTillId:sessionTillId];
            
        }
        
        if ([userDefaults synchronize]) {
            [self MainMenuSyncProcess];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Wrong infomation detected." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}
-(void)cleanOrderHistory{
    if (!orderlist)orderlist = [[OrderListDao alloc]init];
    [orderlist deleteAllHistoryList];
    
    if (!orderhistory)orderhistory = [[OrderHistoryItemDao alloc]init];
    [orderhistory deleteAllHistory];
}
#pragma mark -
#pragma mark Main Menu Sync

-(void)MainMenuSyncProcess{
    [self inithud:@"Loading.."];
    
    //Clean main menu
    if (!foodcategory)foodcategory = [[FoodCategoryDAO alloc]init];
    [foodcategory deleteAllFoodCategory];
    
    
    NSDictionary * params = @{@"res_id" : @"33",
                              @"key" : WEBSERVICEKEY};
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi GetMainMenus:params];
    NSLog(@"Main menu  Fatching..");
    
}
-(void)didfatchMainMenu:(BOOL)isFatched{
    [self hudWasHidden];
    
    if (isFatched) {
        NSLog(@"Main menu  Fatched");
        [self HistorySyncProcess];
    }
//    [self hudWasHidden];
}
#pragma mark -
#pragma mark History
-(void)HistorySyncProcess{
  
    [self cleanOrderHistory];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userId=[userDefaults valueForKey:@"userLoginId"];
    
    [self inithud:@"Loading.."];
    NSDictionary * params = @{@"customer_id" : userId,
                              @"last_update":[self getStartDate],
                              @"key" :WEBSERVICEKEY};
    

    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi GetHistory:params];
    
    NSLog(@"History  Fatching..");
}
-(void)DidReceiveHistory:(BOOL)isFatched{
     [self hudWasHidden];
    NSLog(@"History  Fatched");
     [self TableStatusSyncProcess];
}

#pragma mark -
#pragma mark TableStatus
-(void)TableStatusSyncProcess{

    [self inithud:@"Loading.."];
    NSDictionary * params = @{@"key" : WEBSERVICEKEY};
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi GetTableStatus:params];
    NSLog(@"Table  Fatcheing..");
}
-(void)DidfatchTableStatus:(BOOL)isFatched{
    [self hudWasHidden];
    NSLog(@"Table  Fatched");
    NSLog(@"Navigation on");

    [self GetAllItemDetails];
}
#pragma mark -
#pragma mark GetAllItems

-(void)GetAllItemDetails{
    
    [self inithud:@"Loading.."];
    NSDictionary * params = @{@"key" : WEBSERVICEKEY};
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi getAllItems:params];
    NSLog(@"Table  Fatcheing..");
}

-(void)DidReceiveAllItems:(id)respond{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    [dbmanager deleteAllItemDetails];
    
    for (NSDictionary * Item in respond) {
        [dbmanager insertAllItemData:Item];
    }

    [self navigate];
}
-(void)navigate{
    [self hudWasHidden];
    
    ViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    mainView.isParent = YES;
    [self.navigationController pushViewController:mainView animated:YES];
}

-(void)CleanUserdata{
    [AppUserDefaults setPrinterStatus:PRINTEROFF :CASHERPRINTER];
    [AppUserDefaults setPrinterStatus:PRINTEROFF :KITCHENPRINTER];
}
#pragma mark -
#pragma mark Reachability

-(BOOL)ReachabilityPass{
    
    Reachability * networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus != NotReachable) {
        return true;
    }
    return false;
}
#pragma mark -
#pragma mark TillViewActions

-(void)loadTillPickerdata :(NSArray *)Tills{
    
    if (!TillsArray)TillsArray = [[NSMutableArray alloc]init];
    [TillsArray removeAllObjects];
    
    [TillsArray addObjectsFromArray:Tills];
    [self.tillPickerView reloadAllComponents];
    
    NSDictionary * selectedTill = [TillsArray objectAtIndex:0];
    currectTill = [selectedTill objectForKey:@"id"];
    
    [self AnimateviewOpen:YES];
}
-(void)AnimateviewOpen:(BOOL)isUp{
    
    if (isUp) {
        self.TillView.hidden = NO;
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.TillView.frame;
                             frame.origin.y = 210;
                             [self.TillView setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             
                             [UIView animateWithDuration:0.2
                                                   delay:0
                                                 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^{
                                                  
                                                  CGRect frame = self.TillView.frame;
                                                  frame.origin.y = 247;
                                                  [self.TillView setFrame:frame];
                                                  
                                              }
                                              completion:^(BOOL finished){
                                                  
                                                  
                                                  
                                              }
                              ];
                            
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.TillView.frame;
                             frame.origin.y = 770;
                             [self.TillView setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             self.TillView.hidden = YES;
                         }
         ];
    }
}

- (IBAction)btnTillDoneClicks:(id)sender{
    [self AnimateviewOpen:NO];
    
    Loginparams = @{@"username"    : self.CasherNameTF.text,
                    @"password"    : self.PasswordTF.text,
                    @"mtill"            : currectTill,
                    @"key" : WEBSERVICEKEY};
    
    [self loginProcess];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"Seleted index %i",row);
    
    NSDictionary * selectedTill = [TillsArray objectAtIndex:row];
    currectTill = [selectedTill objectForKey:@"id"];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = [TillsArray count];
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    ;
//    title = [@"" stringByAppendingFormat:@"%d",row];
    
    NSDictionary * tillCodeDic = [TillsArray objectAtIndex:row];
    NSString *title = [tillCodeDic objectForKey:@"code"];
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}
-(NSString *)getStartDate{
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
    NSString *stringFromDate = [dateFormatter1 stringFromDate:[NSDate date]];
    NSString * midnight = [NSString stringWithFormat:@"%@ %@",stringFromDate,@"00:00:00"];
    
    return midnight;
}

#pragma mark -
#pragma mark HudMethods

-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
	[HUD removeFromSuperview];
    [HUD hide:YES];
	HUD = nil;
}
@end
