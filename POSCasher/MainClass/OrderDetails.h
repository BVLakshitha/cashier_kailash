//
//  OrderDetails.h
//  Cashier
//
//  Created by User on 10/02/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceApi.h"
#import "OrderListDao.h"
#import "MBProgressHUD.h"
#import "TableReceivedDao.h"

@protocol OrderDetailsDelegate <NSObject>

-(void)didAnimationOpenOrderDetails:(BOOL)isOpen;
-(void)didReceivedOrderCancelDetails :(id)respond;

@end

@interface OrderDetails : UIView<webservicedelegate>{
    
    DBManager * dbmanager;
    WebserviceApi * webservice;
    NSMutableArray * Ordershistry;
    OrderListDao * orderlistDOA;
    MBProgressHUD * HUD;
    TableReceivedDao * tblreserved;
    
}

-(void)AnimateviewOpen:(BOOL)isUp;

@property (weak, nonatomic) IBOutlet UILabel *OrderTypeTL;
@property (weak, nonatomic) IBOutlet UILabel *CreatedDateTL;
@property (weak, nonatomic) IBOutlet UILabel *OrderStatusTL;
@property (weak, nonatomic) IBOutlet UILabel *OrderODTL;
@property (weak, nonatomic) IBOutlet UILabel *UserTL;
@property (weak, nonatomic) IBOutlet UILabel *OrderIDTL;

@property (weak, nonatomic) IBOutlet UILabel *CustomerTL;
@property (weak, nonatomic) IBOutlet UILabel *EmailTL;
@property (weak, nonatomic) IBOutlet UILabel *ContactTL;
@property (weak, nonatomic) IBOutlet UILabel *AddressTL;
@property ( nonatomic, retain ) id <OrderDetailsDelegate> delegate;
@property (nonatomic,retain)NSString * orderID;


@end
