//
//  OrderDetails.m
//  Cashier
//
//  Created by User on 10/02/2015.
//  Copyright (c) 2015 User. All rights reserved.
//

#import "OrderDetails.h"
#import "OrderListDao.h"
#import "OrderList.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

@implementation OrderDetails

-(void)awakeFromNib{

    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}

-(void)AnimateviewOpen:(BOOL)isUp{
    
    if (isUp) {
        self.hidden = NO;
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.frame;
                             frame.origin.y = 202;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             [self loadData];
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 768;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                             [self.delegate didAnimationOpenOrderDetails:NO];
                         }
         ];
    }
}
-(void)loadData{
    
    if (!orderlistDOA)orderlistDOA = [[OrderListDao alloc]init];
    if (!Ordershistry)Ordershistry = [[NSMutableArray alloc]init];
    
    if (self.orderID.intValue > 0) {
        OrderList * orderlist = [orderlistDOA getCheckOutOrderList:self.orderID];
        self.OrderIDTL.text = self.orderID;
        
        if (!tblreserved)tblreserved = [[TableReceivedDao alloc]init];
        TableReceived * tableresinfo = [tblreserved getTableDetail:orderlist.tableid];
        
        //Ordertype
        if ([orderlist.deliveryType isEqualToString:COLLECTIONORDER]) {
            self.OrderTypeTL.text = [NSString stringWithFormat:@"Collection order"];
        }else if ([orderlist.deliveryType isEqualToString:DELIVERYORDER]){
            self.OrderTypeTL.text = [NSString stringWithFormat:@"Delivery order"];
        }else if ([orderlist.deliveryType isEqualToString:TABLEORDER]){
            self.OrderTypeTL.text = [NSString stringWithFormat:@"Table  %@",tableresinfo.tableName];
        }else{
            self.OrderTypeTL.text = @"";
        }
        
        //orderStatus
        NSLog(@"Status %@",orderlist.stetus);
        if ([orderlist.stetus isEqualToString:SAVE]) {
            self.OrderStatusTL.text = [NSString stringWithFormat:@"Hold"];
        }else if ([orderlist.stetus isEqualToString:SENTTOKITCHEN]){
            self.OrderStatusTL.text = [NSString stringWithFormat:@"Send to kitchen"];
        }else if ([orderlist.stetus isEqualToString:COMPLETED]){
            self.OrderStatusTL.text = [NSString stringWithFormat:@"Completed"];
        }
        
        //Customer
        NSDictionary *customerDetails = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:orderlist.customer];

        self.CustomerTL.text = [NSString stringWithFormat:@"%@ %@",[customerDetails objectForKey:@"fname"],[customerDetails objectForKey:@"lname"]];
        self.EmailTL.text = [customerDetails objectForKey:@"email"];
        self.ContactTL.text = [customerDetails objectForKey:@"tell"];
        self.AddressTL.text = [customerDetails objectForKey:@"street_address"];
        
        self.CreatedDateTL.text = [NSString stringWithFormat:@"%@",orderlist.deliveryDate];
        self.OrderODTL.text = @"IOS";
        self.UserTL.text = [customerDetails objectForKey:@"uname"];
    }
}
- (IBAction)btnDoneClicks:(id)sender{
    [self AnimateviewOpen:NO];
}
- (IBAction)btnClearClicks:(id)sender{
    
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    webservice.delegate = self;
    
    NSDictionary * orderdetails = @{@"key" : WEBSERVICEKEY,
                                    @"order_id" : self.orderID};
    
    [webservice CancelOrder:orderdetails];
    [self inithud:@"Cancelling Order"];
    
}
-(void)DidReceiveCancelOrderDetails:(id)respond{

    [self hudWasHidden];
    if ([[respond objectForKey:@"Status"] isEqualToString:@"Success"]) {
        
        NSString * orderNumber = [NSString stringWithFormat:@"%@",self.orderID];
        NSString * massage = @"Cancelled successfully";
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:orderNumber message:massage delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil, nil];
        [alert show];
        
        [self.delegate didReceivedOrderCancelDetails:respond];
        
    }else{
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self];
    [self addSubview:HUD];
    HUD.labelText = message;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}
@end
