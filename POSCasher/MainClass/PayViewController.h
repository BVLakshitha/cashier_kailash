//
//  PayViewController.h
//  POSCasher
//
//  Created by Lakshitha on 10/3/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceApi.h"
#import "MBProgressHUD.h"
#import "SelectOrderDAO.h"


@interface PayViewController : UIViewController<webservicedelegate,MBProgressHUDDelegate,UIAlertViewDelegate>{
    
    WebserviceApi * webserviceapi;
    NSString * TotalValue;
     MBProgressHUD * HUD;
    SelectOrderDAO * carttbl;
}


@property (weak, nonatomic) IBOutlet UIButton *btnCancel;


@property (weak, nonatomic) IBOutlet UILabel *OrderIDTL;
@property (weak, nonatomic) IBOutlet UILabel *CtimeTL;
@property (weak, nonatomic) IBOutlet UILabel *TotalTL;


@property (weak, nonatomic) IBOutlet UILabel *subtotalTL;
@property (weak, nonatomic) IBOutlet UILabel *taxTL;
@property (weak, nonatomic) IBOutlet UILabel *SCTL;
@property (weak, nonatomic) IBOutlet UILabel *tipTL;
@property (weak, nonatomic) IBOutlet UILabel *DiscountTL;


@property(nonatomic,retain)NSString * orderid;
@property(nonatomic,retain)NSString * Subtotal;

- (IBAction)btnCancelClicks:(id)sender;


//bill print
- (IBAction)btnPrintbilClicks:(id)sender;

@end
