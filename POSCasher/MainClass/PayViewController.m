//
//  PayViewController.m
//  POSCasher
//
//  Created by Lakshitha on 10/3/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "PayViewController.h"
#import "WebserviceApi.h"
#import "MBProgressHUD.h"
#import "MiniPrinterFunctions.h"
#import "AppDelegate.h"
#import "SelectOrderDAO.h"
#import "Printer.h"

@implementation PayViewController

@synthesize Subtotal,orderid;


#pragma mark - 
#pragma mark Delegates
-(void)viewWillAppear:(BOOL)animated{
    
    NSLog(@"order id %@ and subtotal %@",self.orderid,self.Subtotal);
    
    
//    [self Detailsgenarater];
    
    
    self.btnCancel.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnCancel.layer.masksToBounds = YES;
    self.btnCancel.layer.cornerRadius = 4;
    self.btnCancel.layer.borderWidth = 1.0f;
    
}


#pragma mark -
#pragma mark Cash pay
- (IBAction)btnCashClicks:(id)sender {
    [self inithud:@"Sending billing infomation"];
    
    NSDictionary * itemdicmy = @{@"slipte_type"    : @"cash",
                                 @"slipte_qyt"     : @"1",
                                 @"order_id"       : self.orderid};
    
    
    [self GetPaymentinfo:itemdicmy];
}

#pragma mark -
#pragma mark Card pay
- (IBAction)btnCardClicks:(id)sender {
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"This function is not available in this version." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}



//-(void)Detailsgenarater{
//    
//    float totalvalue = 0.0;
//    
//    if (self.orderid != nil && self.Subtotal != nil) {
//        
//        //taxvalue
//        float taxvalue = 8.9;
//        //servicecharge
//        float SC = 5.9;
//        
//        self.subtotalTL.text = self.Subtotal;
//        NSString * subtotalstring = self.Subtotal;
//        //Tax
//        NSString * taxcalue = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (taxvalue/100)];
//        self.taxTL.text = taxcalue;
//        //S/C
//        NSString * servicvalue = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (SC/100)];
//        self.SCTL.text = servicvalue;
//        //TIP
//        NSString * tipcal = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (3/100)];
//        self.tipTL.text = tipcal;
//        //DisCount
//        NSString * discount = [NSString stringWithFormat:@"%.2f",subtotalstring.floatValue * (0/100)];
//        self.DiscountTL.text = discount;
//   
//        
//        totalvalue = taxcalue.floatValue + subtotalstring.floatValue+servicvalue.floatValue;
//        
//        self.TotalTL.text = [NSString stringWithFormat:@"£ %.2f",totalvalue];
//        TotalValue = [NSString stringWithFormat:@"%.2f",totalvalue];
//
//        
//    }else{
//        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:@"Details missing." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}
-(void)GetPaymentinfo:(NSDictionary *)params{
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi CheckforSplit:params];
}
-(void)DidReceiveSplitinfo:(id)respond{
    
    NSLog(@"respond 12 %@",respond);
    
     NSString * Success = [[respond firstObject] objectForKey:@"Result"];
    
    if (![Success isEqualToString:@"Success"]) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:[[respond firstObject] objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.delegate = self;
        [alert show];
    }else{
        NSString * Paymentid = [[respond firstObject] objectForKey:@"payment_id"];
        
        if (Paymentid != nil) {
            NSDictionary * itemdicmy = @{@"pay_type"        : @"cash",
                                         @"paid_chair"      : @"0",
                                         @"pay_amount"      : TotalValue,
                                         @"orderdetails_id" : @"0000",
                                         @"order_id"        : self.orderid,
                                         @"qyt"             : @"10",
                                         @"item_id"         : @"10",
                                         @"payment_id"      : Paymentid};
            
            [self SendCashPayments:itemdicmy];
        }
    }
}
-(void)SendCashPayments:(NSDictionary *)params{
    
    if (!webserviceapi)webserviceapi = [[WebserviceApi alloc]init];
    webserviceapi.delegate = self;
    [webserviceapi CashPayProcess:params];
}
-(void)DidReceivePaymentinfo:(id)respond{
    
    [self hudWasHidden];
    NSLog(@"respond %@",respond);
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Done" message:@"Checkout Success." delegate:self
                                          cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    alert.delegate = self;
    [alert show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSLog(@"nutton title %i",buttonIndex);
    
    if (buttonIndex != [alertView cancelButtonIndex]){
       [self setPosition:@"paied"];
    }
    
}
- (IBAction)btnCancelClicks:(id)sender {
       [self setPosition:@"canceled"];
}


-(void)setPosition:(NSString *)position{
    
    NSUserDefaults * mydefaly = [NSUserDefaults standardUserDefaults];
    [mydefaly setObject:position forKey:@"position"];
    if ([mydefaly synchronize]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark -
#pragma mark printer

- (IBAction)btnPrintbilClicks:(id)sender {
    
    Printer * print = [[Printer alloc]init];
//    [print initWithKBillPrint:self.orderid];

}
-(NSString *)getUser{
    
    NSString * username = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    NSString * time = [NSString stringWithFormat:@"User Name : %@\n",username];
    return time;
}
-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}
@end
