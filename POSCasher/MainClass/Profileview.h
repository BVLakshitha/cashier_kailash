//
//  Profileview.h
//  POSCasher
//
//  Created by orderfood on 09/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProfileViewDelegate <NSObject>

-(void)didAnimationOpen :(BOOL)isOpenProfile;

@end

@interface Profileview : UIView
-(void)AnimatetableviewOpen:(BOOL)isUp;
@property ( nonatomic, retain ) id <ProfileViewDelegate> delegate;

-(void)switchtoEnterCustomer;
-(void)switchtoSearchCustomer;

@end
