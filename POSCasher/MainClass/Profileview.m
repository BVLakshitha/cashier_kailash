//
//  Profileview.m
//  POSCasher
//
//  Created by orderfood on 09/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "Profileview.h"

@implementation Profileview

-(void)AnimatetableviewOpen:(BOOL)isUp{
    
    if (isUp) {
       [self.delegate didAnimationOpen:YES];
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(0,64, 310, 704);
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }else{
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(-310, 64, 310, 704);
                             
                         }
                         completion:^(BOOL finished){
                            [self.delegate didAnimationOpen:NO];
                         }
         ];
    }
}
-(void)switchtoEnterCustomer{
    
    
}
-(void)switchtoSearchCustomer{
    
    
}
@end
