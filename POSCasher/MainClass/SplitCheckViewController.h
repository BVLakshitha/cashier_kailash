//
//  SplitCheckViewController.h
//  POSCasher
//
//  Created by orderfood on 07/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectOrderDAO.h"
#import "SelectOrder.h"
#import "DBManager.h"
#import "PopoverViewController.h"
#import "APPChildViewController.h"
#import "WebserviceApi.h"
#import "MBProgressHUD.h"

@interface SplitCheckViewController : UIViewController<UIAlertViewDelegate,UIPageViewControllerDataSource,Childviewdelegate,webservicedelegate,MBProgressHUDDelegate>{
    
    int GLBsplitcount;
    int Splitcouner;
    DBManager * dbmanager;
    SelectOrderDAO * selectorder;
    APPChildViewController *initialViewController;
    WebserviceApi * webapi;
    MBProgressHUD * HUD;
    
    //SPLITDATA
//    NSMutableArray * SplitRecords;
    NSMutableArray * Splitbuddle;
    NSMutableArray * splitindexarray;
}
//OrderDetails
@property (nonatomic, retain)NSString *OrderID;

@property (strong, nonatomic) UIPageViewController *pageController;
//-(void)DBSplitmanager:(int)splitcount ;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)btncancelClicks:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnprintall;
- (IBAction)btnprintclicks:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSaveSplit;
- (IBAction)btnSaveSplitClicks:(id)sender;

- (IBAction)btnOrderSplitClicks:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderSplit;

@end
