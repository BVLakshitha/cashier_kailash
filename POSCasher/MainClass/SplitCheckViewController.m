//
//  SplitCheckViewController.m
//  POSCasher
//
//  Created by orderfood on 07/10/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "SplitCheckViewController.h"
#import "SelectOrderDAO.h"
#import "CheckoutViewController.h"
#import "SplitData.h"
#import "Constants.h"
#import "WebserviceApi.h"
#import "AppUserDefaults.h"
#import "ExtraItems.h"

@interface SplitCheckViewController ()

@end

@implementation SplitCheckViewController
@synthesize OrderID = _OrderID;

- (void)viewDidLoad {
    [super viewDidLoad];
    GLBsplitcount = 1;
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    [dbmanager RemoveSplitRecrd:@"1000"];
     [self alertShows:@"How many guest ?"];
    [AppUserDefaults setEnableSplitOrderbtn:@"YES"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.title = @"Split check";
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:224.0/255 green:24.0/255 blue:32.0/255 alpha:1.0f];
    self.navigationController.navigationItem.backBarButtonItem.title = @"Back";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:  [UIColor whiteColor], UITextAttributeTextColor,nil];
    self.navigationController.navigationBar.titleTextAttributes = size;
    
    UIButton *btnBack2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack2.frame = CGRectMake(0, 0, 120, 35);
    [btnBack2 setTitle:@"Checkout All" forState:UIControlStateNormal];
    [btnBack2 addTarget:self action:@selector(checkoutAllClicks) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack2];
    
    [super viewWillAppear:YES];
    
    self.btnCancel.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnCancel.layer.masksToBounds = YES;
    self.btnCancel.layer.cornerRadius = 4;
    self.btnCancel.layer.borderWidth = 1.0f;
    
    self.btnprintall.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnprintall.layer.masksToBounds = YES;
    self.btnprintall.layer.cornerRadius = 4;
    self.btnprintall.layer.borderWidth = 1.0f;
    
    self.btnSaveSplit.layer.masksToBounds = YES;
    self.btnSaveSplit.layer.cornerRadius = 4;
//    [self BackbtnControl];
}
-(void)BackbtnControl{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    BOOL AllPaied = YES;
    BOOL isOnePaied = NO;
    NSArray * splitscount = [dbmanager selectAllSplitRecords];
    
    for (SplitData * splitdata in splitscount) {
        if ([splitdata.isPaid isEqualToString:@"NO"]){
          AllPaied = NO;
        }else{
            isOnePaied = YES;
        }
    }
    if (AllPaied)[self.btnCancel setTitle:@"Done" forState:UIControlStateNormal];
    
    if (isOnePaied)[self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    
}
-(void)alertShows:(NSString *)massge{
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Split Count" message:massge delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:@"CANCEL", nil];
    alert.alertViewStyle  = UIAlertViewStylePlainTextInput;
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"DONE"])
    {
        UITextField *splitcountTF = [alertView textFieldAtIndex:0];
        splitcountTF.keyboardType = UIKeyboardTypeNumberPad;
        NSLog(@"inputtext=%@",splitcountTF.text);
        
        int splitcount = [splitcountTF.text intValue];
        
        if (splitcount > 10) {
            
            [self alertShows:@"Wrong Split number please try again."];
        }else if (splitcount == 0){
            [self alertShows:@"Wrong Split number please try again."];
        }else{
            Splitcouner = splitcount;
            [self DBSplitmanagerMasterCreater];
            [self initSplitView:splitcount];
        }
        
        NSLog(@"splitcount %i",splitcount);
        
    }else{
        if ([title isEqualToString:@"YES"]){
            if ([self isSplitIDgenarated]){
                [self cancelSplitOrder];
                [self inithud:@"Removing..."];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        if ([title isEqualToString:@"CANCEL"])[self.navigationController popViewControllerAnimated:YES];
    }
}
-(BOOL)isSplitIDgenarated{
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * splitRs =  [dbmanager selectAllSplitRecords];
    SplitData * splitdata = [splitRs firstObject];
    
    if (splitdata.splitOrderID.intValue > 0) {
        return YES;
    }
    return NO;
}
-(void)cancelSplitOrder{
    
    if (!webapi)webapi = [[WebserviceApi alloc]init];
    webapi.delegate = self;
    NSDictionary * splitCDetails = @{@"key":WEBSERVICEKEY,
                                     @"order_id":self.OrderID};
    
    
    [webapi CancelSplitOrder:splitCDetails];
    

}
-(void)DidReceiveSplitCancelDetails:(id)respond{
    if ([[respond objectForKey:@"Status"] isEqualToString:@"Success"]) {
            [self.navigationController popViewControllerAnimated:YES];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry!" message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        alert.alertViewStyle  = UIAlertViewStylePlainTextInput;
        [alert show];
    }
    [self hudWasHidden];
}
-(void)initSplitView:(int)splitcount{
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:CGRectMake(0, 70, 1024, 700)];
    
    initialViewController = [self viewControllerAtIndex:0];
    initialViewController.checklimit = [self SplitCount];
    initialViewController.totallimit = Splitcouner;
    
    initialViewController.delegate =self;
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor clearColor];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    NSLog(@"viewControllers %i",[viewControllers count]);
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
}
- (APPChildViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    APPChildViewController *childViewController = [[APPChildViewController alloc] initWithNibName:@"APPChildViewController" bundle:nil];
    childViewController.index = index;
    childViewController.delegate = self;
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(APPChildViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    index--;
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(APPChildViewController *)viewController index];
    
    index++;
    
    if (index == [self SplitCount]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
    
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return [self SplitCount];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return 0;
}
#pragma mark-
#pragma mark DataLoader

-(void)DBSplitmanagerMasterCreater{
    
    if (!selectorder)selectorder = [[SelectOrderDAO alloc]init];
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * objects = [selectorder getSelectFoodCategorys];
    
    for (SelectOrder * select in objects) {
        
        NSNumber * splitid = [NSNumber numberWithInt:1];
        NSString * disID = select.descriptionID;
        NSString * price = select.totalPrice;
        NSString * qty = select.orderrQuantity.stringValue;
        NSString * name = select.orderName;
        NSString * recordID = select.recodId;
        NSString * extraAmout = [self returnExtraAmount:recordID];
        NSString * cancelStatus = select.cancelStatus;
        
        NSString * totalprice = [NSString stringWithFormat:@"%0.2f",price.floatValue + extraAmout.doubleValue];
        
        if (cancelStatus.intValue != 1)[dbmanager insetSplitRecord:disID :splitid :totalprice :qty :name :@"NO" :qty :recordID];
    }
    
}
-(NSString *)returnExtraAmount:(NSString *)recordID{
    NSString * amount = @"0";
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSArray * extraItems = [dbmanager GetAllExtrasByRID:recordID.intValue];
    double extraamount = 0.0;
    for (ExtraItems * itemextra in extraItems) {
        extraamount = extraamount+itemextra.extraitemPrice.doubleValue;
    }
    
    amount = [NSString stringWithFormat:@"%f",extraamount];
    return amount;
}
-(void)didchangesplit:(NSString *)reloadmassate{
    
    NSLog(@"parent_reloader %@",reloadmassate);
    GLBsplitcount = reloadmassate.intValue;
    
    
    for (UIView * subview in [self.view subviews]) {
        
        if (![subview isKindOfClass:[UIButton class]] && ![subview isKindOfClass:[UILabel class]]) {
            [subview removeFromSuperview];
        }
        NSLog(@"subview = %@",subview);
    }
    
    [initialViewController removeFromParentViewController];
    [self initSplitView:reloadmassate.intValue];
    [self.pageController reloadInputViews];
}

- (IBAction)btncancelClicks:(id)sender {
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    BOOL AllPaied = YES;
    NSArray * splitscount = [dbmanager selectAllSplitRecords];
    
    for (SplitData * splitdata in splitscount) {
        if ([splitdata.isPaid isEqualToString:@"NO"])AllPaied = NO;
    }
    if (AllPaied) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Split is not completed. Do you want to cancel?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [alert show];
    }
}
- (IBAction)btnprintclicks:(id)sender {
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Printer not detected." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark-
#pragma mark Payment

-(void)didClicksPaybtn:(NSString *)splitID :(NSString *)amount{
    
    NSArray * SplitDetails = [AppUserDefaults getSplitDetails];
    NSString * SplitorderID = [SplitDetails objectAtIndex:(splitID.intValue-1)];
    
    CheckoutViewController * payview = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
    payview.orderid = SplitorderID;
    payview.Subtotal = amount;
    payview.SplitID = splitID;
    payview.isFromSplit = YES;
    [self presentViewController:payview animated:YES completion:nil];
    
}
#pragma mark-
#pragma mark Send Split Records

- (IBAction)btnSaveSplitClicks:(id)sender {
    
    if ([self SplitCount] > 1){
        [self ServiceCall];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Only one order found." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)btnOrderSplitClicks:(id)sender {
    
}
-(void)ServiceCall{
    
    if (!webapi)webapi = [[WebserviceApi alloc]init];
    webapi.delegate = self;
    NSDictionary * SplitData = [self GenarateSplitRecords];
    
    NSLog(@"output %@",SplitData);
    [webapi SendSplitOrder:SplitData];
}
-(void)DidReceiveSplitinfo:(id)respond{
    
    if ([[respond objectForKey:@"Status"] isEqualToString:@"Success"]) {
        NSArray * splitdata = [respond objectForKey:@"data"];
        if ([splitdata count]>0) {
            [AppUserDefaults setSplitDetails:splitdata];
            int SPIT = 1;
            for (NSString * SpliorderID in splitdata) {
                if (!dbmanager)dbmanager = [[DBManager alloc]init];
                [dbmanager updateSplitOrderID:SPIT :SpliorderID.intValue];
                SPIT++;
            }
            [self didchangesplit:[NSString stringWithFormat:@"%i",GLBsplitcount]];
            [self ActivatePayBtn];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Split data not found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
       
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    NSLog(@"Resppond %@",respond);
}
-(NSDictionary *)GenarateSplitRecords{
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!Splitbuddle)Splitbuddle = [[NSMutableArray alloc]init];
    [Splitbuddle removeAllObjects];
    
    for (int i =1; i<=[self SplitCount]; i++) {
        NSMutableArray * SplitRecords = [[NSMutableArray alloc]init];
//        [SplitRecords removeAllObjects];
        
         NSArray * splitRecordData = [dbmanager selectSplitRecord:[NSNumber numberWithInt:i]];

        for (SplitData * splitrecord in splitRecordData) {
            NSDictionary * splitdata = @{@"id" : splitrecord.itemDisID,
                                         @"qty" : [self decimelconverter:splitrecord.divQty]};
            
            [SplitRecords addObject:splitdata];
        }
        
        NSDictionary * Orderfineldata = @{@"order" :SplitRecords};
        NSLog(@"order %@",Orderfineldata);
        [Splitbuddle addObject:Orderfineldata];
        NSLog(@"afterAdd %@",Splitbuddle);
    }

    NSLog(@"dataDic b4 %@",Splitbuddle);
    NSDictionary * Splitfineldata = @{@"splitOrders" :Splitbuddle};
    
    NSLog(@"dataDic %@",Splitfineldata);
    NSString * jsonStrting = [self jsonconverter:Splitfineldata];
    
    NSDictionary * data = @{@"key" :WEBSERVICEKEY,
                            @"order_id" :self.OrderID,
                            @"data" :jsonStrting};
    return data;
}
-(NSString * )decimelconverter :(NSString *)valueString{
    NSArray *subStrings = [valueString componentsSeparatedByString:@"/"]; //or rather @" - "
    NSString * finelString = @"";
    
    if ([subStrings count] > 1) {
        NSString *firstString = [subStrings objectAtIndex:0];
        NSString *lastString = [subStrings objectAtIndex:1];
        float values = firstString.floatValue/lastString.floatValue;
        finelString = [NSString stringWithFormat:@"%0.2f",values];
    }else{
        finelString = valueString;
    }
    
    return finelString;
}
-(int)SplitCount{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * records =  [dbmanager selectSplitRecord:[NSNumber numberWithInt:1000]];
    NSNumber * splitsize = nil;
    
    if (!splitindexarray)splitindexarray = [[NSMutableArray alloc]init];
    
    [splitindexarray removeAllObjects];
    
    for (SplitData * splidata in records) {
        splitsize = splidata.splitID;
        if (![splitindexarray containsObject:splitsize]) {
            [splitindexarray addObject:splitsize];
        }
    }
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [splitindexarray sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];

    NSLog(@"Splitcount %i",[splitindexarray count]);
    
    return [splitindexarray count];
    
}
-(void)ActivatePayBtn{
    
    self.btnSaveSplit.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnSaveSplit.layer.borderWidth = 1.0f;
    self.btnSaveSplit.layer.masksToBounds = YES;
    self.btnSaveSplit.layer.cornerRadius = 4;
    self.btnSaveSplit.enabled = NO;
    [self.btnSaveSplit setTitle:@"Split Saved" forState:UIControlStateNormal];
    [self.btnSaveSplit setBackgroundColor:[UIColor colorWithRed:224.0/255 green:24.0/255 blue:32.0/255 alpha:1.0f]];
    [self.btnSaveSplit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [initialViewController ActiveteButtonpanel];
}
-(NSString *)jsonconverter :(NSDictionary *)dictionary{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}

@end
