//
//  TBLView.h
//  POSCasher
//
//  Created by Lakshitha on 9/22/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APPChildViewController.h"
#import "WebserviceApi.h"
#import "MBProgressHUD.h"


@protocol TableLoaderDelegate <NSObject>

-(void)didFinishTableviewDown;
-(void)didFinishTableviewup;

@end
@interface TBLView : UIView<webservicedelegate,MBProgressHUDDelegate>{
    
    APPChildViewController *initialViewController;
    WebserviceApi * webserviceApi;
    MBProgressHUD * HUD;
    
}
-(void)AnimatetableviewUp:(BOOL)isUp;
@property (strong, nonatomic) UIPageViewController *pageController;
@property ( nonatomic, retain ) id <TableLoaderDelegate> delegate;
@end
