//
//  TBLView.m
//  POSCasher
//
//  Created by Lakshitha on 9/22/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "TBLView.h"
#import "WebserviceApi.h"
#import "Constants.h"

@implementation TBLView
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self setFrame:CGRectMake(0, 0, self.layer.bounds.size.width, self.layer.bounds.size.height)];
    }
    return self;
}
-(void)initwithcode{
    
    
    
}
-(void)AnimatetableviewUp:(BOOL)isUp{
    
    NSLog(@"Codinateni %f",self.layer.frame.origin.y);
    
    
    if (isUp) {
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.frame = CGRectMake(0, -767, 1024,768);
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                             [self.delegate didFinishTableviewup];
                         }
         ];
    }else{
        
        self.hidden = NO;
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(0, 0, 1024, 768);

                         }
                         completion:^(BOOL finished){
//                            [self loadvebservice];
                            [self.delegate didFinishTableviewDown];
                         }
         ];
    }
}
-(void)loadvebservice{
    [self inithud:@"Refreshing.."];
    if (!webserviceApi)webserviceApi = [[WebserviceApi alloc]init];
    webserviceApi.delegate =self;
    NSDictionary * params = @{@"key" : WEBSERVICEKEY};
    [webserviceApi GetTableStatus:params];
    
}
-(void)DidfatchTableStatus:(BOOL)isFatched{
    [self hudWasHidden];
}
#pragma mark -
#pragma mark HudMethods

-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self];
	[self addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
	[HUD removeFromSuperview];
    [HUD hide:YES];
	HUD = nil;
}
@end
