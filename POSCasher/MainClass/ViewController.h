//
//  ViewController.h
//  POSCasher
//
//  Created by orderfood on 22/09/2014.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBLView.h"
#import "FlowView.h"
#import "Layouttwo.h"
#import "Layoutthree.h"
#import "FoodCategoryDAO.h"
#import "MainMenuCell.h"
#import "HistryView.h"
#import "TableReceivedDao.h"
#import "SelectOrderDAO.h"
#import "OrderHistoryItemDao.h"
#import "DBManager.h"
#import "SelectOrder.h"
#import "Profileview.h"
#import "AddCustomer.h"
#import "DeliveryDetails.h"
#import "itemDetailsView.h"
#import "ExtraItems.h"
#import "GiftVoucherView.h"
#import "Printer.h"
#import "OrderDetails.h"
#import "AddItemByID.h"
#import "orderStatusView.h"
#import "CompanyView.h"

@interface ViewController : UIViewController<UIScrollViewDelegate,layoutdelegate,mainmenudelegates,UITableViewDelegate,UITableViewDataSource,webservicedelegate,MBProgressHUDDelegate,UIAlertViewDelegate,TableLoaderDelegate,ProfileViewDelegate,AddcustomerViewDelegate,DeliveryDetailsDelegate,ItemDetailsViewDelegate,GiftVoucherViewDelegate,OrderDetailsDelegate,AddItemDelegate,UITextFieldDelegate,CompanyDetailsDelegate>{
    
    WebserviceApi * webservice;
    MBProgressHUD * HUD;
    FoodCategoryDAO * foodcategory;
    TableReceivedDao * tblreserved;
    SelectOrderDAO * addcart;
    OrderHistoryItemDao * orderhistory;
    SelectOrder * cartSelectedOrder;
    ExtraItems * extraitem;
    Printer * printer;
    OrderListDao * OLDAO;
    OrderList * selectedOrderDetails;
    
    
    
    NSMutableArray * MainMenus;
    NSMutableArray * MenuItems;
    NSMutableArray * Ordershistry;
    NSMutableArray * CartLoader;
    NSMutableArray * SelectedItemDics;
    NSMutableArray * addonsArray;
    NSMutableArray * processcollection;
    DBManager * dbmanager;
    BOOL isOpen;
    BOOL isProfileOpen;
    BOOL isAddCustomerOpen;
    BOOL isinstantPay;
    BOOL isGiftVoucherViewOpen;
    BOOL isOrderCancel;
    BOOL isOrderpaied;
    BOOL isHoldOrder;
    
    NSDictionary * containerHeader;
    NSString * removedRecordID;
}

@property (weak, nonatomic) IBOutlet TBLView *TableDisplayView;

@property (weak, nonatomic) IBOutlet UIScrollView *TBLScroller;
@property (weak, nonatomic) IBOutlet UIScrollView *MainMenuScroller;
@property (weak, nonatomic) IBOutlet UITableView *MenuitemTV;
@property (weak, nonatomic) IBOutlet UILabel *MenuItemName;

@property (weak, nonatomic) IBOutlet HistryView *histryView;
@property (weak, nonatomic) IBOutlet Profileview *ProfileView;

@property (weak, nonatomic) IBOutlet UITableView *OrderHistryTBL;
@property (weak, nonatomic) IBOutlet UITableView *CartTV;
@property (weak, nonatomic) IBOutlet UILabel *titleTL;
@property (nonatomic,retain)NSString * table;
@property (nonatomic,retain)NSString * orderID;
@property (nonatomic,retain)NSString * Subtotal;
@property (nonatomic,retain)NSString * SelectedCustomer;
@property (nonatomic,assign)BOOL isParent;

@property (weak, nonatomic) IBOutlet UIView *blureview;


@property (weak, nonatomic) IBOutlet UILabel *totalitemTF;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceTF;
@property (weak, nonatomic) IBOutlet UILabel *orderidTL;

//buttom top set.
@property (weak, nonatomic) IBOutlet UIButton *btnTakeAway;
@property (weak, nonatomic) IBOutlet UIButton *btnTable;
@property (weak, nonatomic) IBOutlet UIButton *btnHold;

//buttom bottom set.
@property (weak, nonatomic) IBOutlet UIButton *btnHoldOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnSplitOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnPayOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCustomer;

@property (weak, nonatomic) IBOutlet UISegmentedControl *historySegController;

//profile
@property (weak, nonatomic) IBOutlet UIImageView *ProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *UserNameTL;
@property (weak, nonatomic) IBOutlet UILabel *UserIDTL;
@property (weak, nonatomic) IBOutlet UIButton *btnlogout;

//AddCustomer
- (IBAction)btnAddcustomerClicks:(id)sender ;
@property (weak, nonatomic) IBOutlet AddCustomer *addcustomer;

//DeliveryDetails
@property (weak, nonatomic) IBOutlet DeliveryDetails *deliverydetails;
@property (weak, nonatomic) IBOutlet UIButton *btnDeliveryDetails;

- (IBAction)btndeliveryDetails:(id)sender;

//ItemDetalsView
@property (weak, nonatomic) IBOutlet itemDetailsView *ItemDetailsView;

//GiftVoucher
- (IBAction)btnGiftVoucherClicks:(id)sender;
@property (weak, nonatomic) IBOutlet GiftVoucherView *GiftVoucherView;

//OrderDetails
- (IBAction)btnOrderIDClicks:(id)sender;
@property (weak, nonatomic) IBOutlet OrderDetails *orderdetailsview;

//AddItemByItemId
@property (weak, nonatomic) IBOutlet UIButton *btnAddItem;
- (IBAction)btnAddItemClicks:(id)sender;
@property (weak, nonatomic) IBOutlet AddItemByID *addItemById;

//TillView
@property (weak, nonatomic) IBOutlet UIView *TillFinalizeView;
@property (weak, nonatomic) IBOutlet UITextField *tillAmtTF;
@property (weak, nonatomic) IBOutlet UIButton *btnTillDone;
@property (weak, nonatomic) IBOutlet UIButton *btnTillCancel;

- (IBAction)btnTillDoneClicks:(id)sender;


//get item status

@property (weak, nonatomic) IBOutlet CompanyView *comapanydetails;
- (IBAction)btnCompanyClicks:(id)sender;


@end
