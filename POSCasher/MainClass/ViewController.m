//
//  ViewController.m
//  POSCasher
//
//  Created by orderfood on 22/09/2014.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import "FlowView.h"
#import "Layouttwo.h"
#import "Layoutthree.h"
#import "WebserviceApi.h"
#import "FoodCategory.h"
#import "MainMenuCell.h"
#import "MenuItemCell.h"
#import "HistoryCell.h"
#import "AFImageRequestOperation.h"
#import "Reachability.h"
#import "TableReceivedDao.h"
#import "TableReceived.h"
#import "AddbasketViewCell.h"
#import "AddbasketViewminiCell.h"
#import "SelectOrder.h"
#import "Location.h"
#import "OrderList.h"
#import "PayViewController.h"
#import "SplitCheckViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Printer.h"
#import "CheckoutViewController.h"
#import "SettingsViewController.h"
#import "ExtraItems.h"
#import "CartExtraCell.h"
#import "Constants.h"
#import "AppUserDefaults.h"
#import "Flurry.h"
#import "SelectOrder.h"


@interface ViewController ()

@end

@implementation ViewController
@synthesize table,orderID,Subtotal;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self PanelLoader];
    
    NSLog(@"Loadind screen");

	// Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated{
    
//     [self.TableDisplayView AnimatetableviewUp:YES];
    
    if ([[AppUserDefaults getCurrentpayment] isEqualToString:@"SUCCESS"])[self takeAwayClikcs:nil];

   [self reloadTableData];
}

-(void)PanelLoader{
    
    [self.MainMenuScroller setScrollEnabled:YES];
    [self loadMainMenu];
    
    isOpen = NO;
    isProfileOpen = NO;
    isAddCustomerOpen = NO;
    isGiftVoucherViewOpen = NO;
    self.blureview.hidden = YES;
    
    [super viewWillAppear:YES];
    self.CartTV.allowsMultipleSelectionDuringEditing = NO;
    [self.histryView AnimatetableviewOpen:NO];
    
    self.TableDisplayView.delegate = self;
    self.ProfileView.delegate = self;
    self.addcustomer.delegate = self;
    self.deliverydetails.delegate = self;
    self.ItemDetailsView.delegate = self;
    self.GiftVoucherView.delegate = self;
    self.orderdetailsview.delegate = self;
    
    [self.orderidTL setText:@""];
    //    [self loadCart];
    table = nil;
    self.titleTL.text = @"Takeaway Order";
    //
    [self OrderSentToKitchen:NO];//Take away ready to send order.
    
    self.btnTakeAway.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnTakeAway.layer.masksToBounds = YES;
    self.btnTakeAway.layer.cornerRadius = 4;
    self.btnTakeAway.layer.borderWidth = 1.0f;
    
    self.btnHold.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnHold.layer.masksToBounds = YES;
    self.btnHold.layer.cornerRadius = 4;
    self.btnHold.layer.borderWidth = 1.0f;
    
    self.btnTable.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnTable.layer.masksToBounds = YES;
    self.btnTable.layer.cornerRadius = 4;
    self.btnTable.layer.borderWidth = 1.0f;
    
    
    self.btnTillDone.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnTillDone.layer.masksToBounds = YES;
    self.btnTillDone.layer.cornerRadius = 4;
    self.btnTillDone.layer.borderWidth = 1.0f;
    
    self.btnTillCancel.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnTillCancel.layer.masksToBounds = YES;
    self.btnTillCancel.layer.cornerRadius = 4;
    self.btnTillCancel.layer.borderWidth = 1.0f;
    
    
    [self managemyprofileImage];
    [self managemyprofileDetails];
    [self setNavigationControls];
    [self OrderbtnActionSelected:YES];
    [self SegmentVCtitls];
    [self.ProfileView AnimatetableviewOpen:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark Navigation Controls

-(void)setNavigationControls{
    NSUserDefaults * mydefaly = [NSUserDefaults standardUserDefaults];
    NSString * position = [mydefaly objectForKey:@"position"];
    
    if ([position isEqualToString:@"paied"]) {
        [self takeAwayClikcs:nil];
    }else if ([position isEqualToString:@"canceled"]){
//        [self removeAllRecords];
//        [self loadCart];
        self.btnHoldOrder.enabled  = YES;
        self.btnSaveOrder.enabled = YES;
        self.btnPayOrder.enabled = YES;
        self.btnSplitOrder.enabled = YES;
//         [self takeAwayClikcs:nil];
    }else{
         [self removeAllRecords];
        self.totalPriceTF.text = @"00.00";
        self.totalitemTF.text = @"0";
         [self loadLocations];
    }
    [mydefaly removeObjectForKey:@"position"];
}

#pragma mark -
#pragma mark Table Controles
-(void)loadLocations{
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    webservice.delegate = self;
    [webservice GetLocationInfo];
}


- (IBAction)TableshowAction:(id)sender {
    
    [self.TableDisplayView AnimatetableviewUp:NO];
}
- (IBAction)TBLViewActionUp:(id)sender {
     [self.TableDisplayView AnimatetableviewUp:YES];
}
-(void)MainScrollerControl{
    
    self.TBLScroller.delegate = self;
    [self.TBLScroller setScrollEnabled:YES];
    
    [self removeSubviews];
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * alllocation = [dbmanager getAllLocationinfo];
    int i = 0;
    for (Location * location in alllocation) {
        
//        NSLog(@"printing_location %@",location.location);
        
        if (!tblreserved)tblreserved = [[TableReceivedDao alloc]init];
        NSArray * tableinfos = [tblreserved getTableinfomationByArea:location.locationId.stringValue];
        
        FlowView * firstflow = [[FlowView alloc]init];
        firstflow.frame = CGRectMake(i*1024, 0, 1024, 768);
        firstflow.delegate = self;
        firstflow.PlaceTF.text = location.location;
        
        [firstflow initCollectionviewWithObject:tableinfos];
        [self.TBLScroller addSubview: firstflow];
        i++;
    }

    self.TBLScroller.contentSize = CGSizeMake(1024*i, 760);
    [self.TBLScroller reloadInputViews];
    [self hudWasHidden];
}
-(void)removeSubviews{
    
    for (UIView *v in self.TBLScroller.subviews) {
        if (![v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    
}
-(void)didSelectTable:(TableReceived *)tableinfo{
    
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * holdorders = [addcart getSelectFoodCategorys];
    
    [self.addcustomer removeSecectedCustomer];
    [self.deliverydetails removeDeliveryDetails];
    
    if (holdorders.count > 0) {
        [self removeAllRecords];
    }else{
        
        NSLog(@"tblStatus id %@",tableinfo.tblstatus);
        [AppUserDefaults setDeliveryType:TABLEORDER];
        
        if (tableinfo.tblstatus.intValue == 13) {//Save Orders
            
            if (!orderhistory)orderhistory = [[OrderHistoryItemDao alloc]init];
            NSArray * Historyitems = [orderhistory getAllCheckOutOrderList:tableinfo.orderid];
            for (OrderHistoryItem * item in Historyitems) {
                [self addItemsToCartFromHistory:item :tableinfo.tableId];
            }
            //buttonsActons
            self.btnHoldOrder.enabled  = YES;
            self.btnSaveOrder.enabled = YES;
            self.btnPayOrder.enabled = NO;
            self.btnSplitOrder.enabled = NO;
            [self.orderidTL setText:[NSString stringWithFormat:@"Order ID : %@",tableinfo.orderid]];
            
            
            //self select Delivery details;
            if (!OLDAO)OLDAO = [[OrderListDao alloc]init];
            OrderList * orderinfo = [OLDAO getCheckOutOrderList:tableinfo.orderid];
            
            self.deliverydetails.isFromhistory = YES;
            self.deliverydetails.orderId = tableinfo.orderid;
            self.deliverydetails.OrderlistDetails = orderinfo;
            self.deliverydetails.DeliveryType = TABLEORDER;
            [self.deliverydetails setDeliveryDetails];
            
        }else if (tableinfo.tblstatus.intValue == 0){//Empty Tables
            

            
            //buttonsActons
            self.btnHoldOrder.enabled  = YES;
            self.btnSaveOrder.enabled = YES;
            self.btnPayOrder.enabled = NO;
            self.btnSplitOrder.enabled = NO;
            [self.orderidTL setText:@""];
        
        }else{
            
            if (!orderhistory)orderhistory = [[OrderHistoryItemDao alloc]init];
            NSArray * Historyitems = [orderhistory getAllCheckOutOrderList:tableinfo.orderid];
            for (OrderHistoryItem * item in Historyitems) {
                [self addItemsToCartFromHistory:item :tableinfo.tableId];
            }
            
            if (tableinfo.tblstatus.intValue == 4) {
                
                self.btnHoldOrder.enabled  = NO;
                self.btnSaveOrder.enabled = NO;
                self.btnPayOrder.enabled = YES;
                self.btnSplitOrder.enabled = YES;
                
            }else{
                self.btnHoldOrder.enabled  = YES;
                self.btnSaveOrder.enabled = YES;
                self.btnPayOrder.enabled = YES;
                self.btnSplitOrder.enabled = YES;
            }
            

            [self.orderidTL setText:[NSString stringWithFormat:@"Order ID : %@",tableinfo.orderid]];
            
            
            //self select Delivery details;
            if (!OLDAO)OLDAO = [[OrderListDao alloc]init];
            OrderList * orderinfo = [OLDAO getCheckOutOrderList:tableinfo.orderid];
            
            self.deliverydetails.isFromhistory = YES;
            self.deliverydetails.orderId = tableinfo.orderid;
            self.deliverydetails.OrderlistDetails = orderinfo;
            self.deliverydetails.DeliveryType = orderinfo.deliveryType;
            [self.deliverydetails setDeliveryDetails];
        }
        self.table = tableinfo.tableId;
        self.orderID = tableinfo.orderid;
        
//        Location * location = [dbmanager getLocationinfo:tableinfo.locationID];
//        self.titleTL.text = [NSString stringWithFormat:@"%@ in %@",tableinfo.tableName,location.location];
        self.titleTL.text = [NSString stringWithFormat:@"%@",tableinfo.tableName];
        [self.TableDisplayView AnimatetableviewUp:YES];
        [self loadCart];
    }
}
-(void)reloadTableData{
    
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    webservice.delegate = self;
    [webservice GetTableStatus:nil];
}
-(void)DidfatchTableStatus:(BOOL)isFatched{
    [self hudWasHidden];
    [self HistorySyncProcess];

}
-(void)didFinishTableviewDown{
//    [self inithud:@"Refreshing"];
   [self reloadTableData];
//     [self HistorySyncProcess];
//     [self MainScrollerControl];
}
-(void)didFinishTableviewup{
//  [self reloadTableData];
}
#pragma mark -
#pragma mark Takeaway
- (IBAction)takeAwayClikcs:(id)sender {
    
    self.table = nil;
    self.orderID = @"0";
    self.titleTL.text = @"Takeaway Order";
    self.orderidTL.text = @"";
    selectedOrderDetails = nil;
    self.deliverydetails.isFromhistory = NO;
    
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * holdorders = [addcart getSelectFoodCategorys];
    if (holdorders.count > 0)[self removeAllRecords];
    
    self.btnHoldOrder.enabled  = YES;
    self.btnSaveOrder.enabled = YES;
    self.btnPayOrder.enabled = YES;
    self.btnSplitOrder.enabled = YES;
    
    
    [self loadCart];
    [self.deliverydetails setAsCollection];
    [self.addcustomer removeSecectedCustomer];
    [self.deliverydetails removeDeliveryDetails];
    [AppUserDefaults setDeliveryType:COLLECTIONORDER];
    [AppUserDefaults setCurrentpayment:@"NOTSUCCESS"];
    
}
-(void)OrderSentToKitchen :(BOOL)isForPay{
    
    if (isForPay) {
        self.btnHoldOrder.enabled = NO;
        self.btnSaveOrder.enabled = YES;
        self.btnSplitOrder.enabled = YES;
        self.btnPayOrder.enabled = YES;
    }else{
        self.btnHoldOrder.enabled = YES;
        self.btnSaveOrder.enabled = YES;
        self.btnSplitOrder.enabled = YES;
        self.btnPayOrder.enabled = YES;
    }
}
#pragma mark -
#pragma mark MainMenu

-(void)loadMainMenu{
    
    int CellWidth = 120;
    
    if (!MainMenus)MainMenus = [[NSMutableArray alloc]init];
    
    [MainMenus removeAllObjects];
    
    if (!foodcategory)foodcategory = [[FoodCategoryDAO alloc]init];
    NSArray * foodCategories = [foodcategory getAllFoodCategory];
    [MainMenus addObjectsFromArray:foodCategories];
    int i = 0;
    for (FoodCategory * category in foodCategories) {
        
        MainMenuCell * singleCategory = [[MainMenuCell alloc]initWithFrame:CGRectMake(i*CellWidth, 15, CellWidth, 90)];
        singleCategory.MenuName.text = category.name;
        singleCategory.tag = i;
        singleCategory.delegate = self;
        [singleCategory downloadImageWithCompletionBlock:category.imagePath];
        [self.MainMenuScroller addSubview:singleCategory];
        i++;
    }
    [self.MainMenuScroller setContentSize:CGSizeMake(CellWidth*(i+1), 100)];
    [self.MainMenuScroller reloadInputViews];
    
    if ([MainMenus count] > 0)[self didSelectMenu2:[NSNumber numberWithInt:0]];                            //Firsttime Select Item;
}
-(void)didSelectMenu2:(NSNumber *)tabvalue{
    
    NSLog(@"Selected_menu is %i",tabvalue.intValue);
    FoodCategory * selectedcat = [MainMenus objectAtIndex:tabvalue.intValue];
    
    NSLog(@"Catid %@",selectedcat.name);
    
    if (selectedcat != nil){
        if(selectedcat.name != nil)self.MenuItemName.text = selectedcat.name;
        [self inithud:selectedcat.name];
        [self loadMenuItem:selectedcat.categoryId];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"No items found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
-(void)didSelectMenu:(NSNumber *)tabvalue{
    
    NSLog(@"Selected_menu is %i",tabvalue.intValue);
    FoodCategory * selectedcat = [MainMenus objectAtIndex:tabvalue.intValue];
//
    NSLog(@"Catid %@",selectedcat.name);
//
    if (selectedcat != nil){
        if(selectedcat.name != nil)self.MenuItemName.text = selectedcat.name;
        [self inithud:selectedcat.name];
        [self loadMenuItem:selectedcat.categoryId];
    }else{
        [self hudWasHidden];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"No items found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}


#pragma mark -
#pragma mark MenuItem TableView
-(void)loadMenuItem :(NSString *)cagegoryID{
    
    if (cagegoryID != nil) {
        NSDictionary * params = @{@"catagory_id" : cagegoryID,
                                  @"key" :WEBSERVICEKEY};
        NSLog(@"Item no %@",cagegoryID);
        if (!webservice)webservice = [[WebserviceApi alloc]init];
        webservice.delegate = self;
        [webservice GetMenuItems:params];
    }else{
        [self hudWasHidden];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"No items found." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

    

}
-(void)DidReceiveMenuItems:(NSArray *)respond{

    NSLog(@"Items Array %@",respond);
    
    if (!MenuItems)MenuItems = [[NSMutableArray alloc]init];
   
    [MenuItems removeAllObjects];
    
    [MenuItems addObjectsFromArray:respond];
    [self.MenuitemTV reloadData];
    [self hudWasHidden];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.MenuitemTV) {
        return MenuItems.count;
    }else if (tableView == self.OrderHistryTBL){
        return Ordershistry.count;
    }else{
        return CartLoader.count;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.MenuitemTV) {
         return 102;
    }else if (tableView == self.OrderHistryTBL){
         return 62;
    }else{
        return 56;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.MenuitemTV) {
        
        static NSString *simpleTableIdentifier = @"MenuItemCell";
        MenuItemCell *cell = (MenuItemCell *)[self.MenuitemTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuItemCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
        NSDictionary * menuitem = [MenuItems objectAtIndex:indexPath.row];
        
        NSLog(@"Processing %@",menuitem);
        
        [cell fatchfromurl:[menuitem objectForKey:@"item_image"]];
        cell.itemname.text = [menuitem objectForKey:@"item_name"];
        cell.backgroundColor = [UIColor clearColor];
        cell.itemprice.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,[menuitem objectForKey:@"item_price"]];
        cell.itemdiscription.text = [menuitem objectForKey:@"item_discription"];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgColorView];
        
        return cell;
        
    }else  if (tableView == self.OrderHistryTBL){
        
        static NSString *simpleTableIdentifier = @"HistoryCell";
        HistoryCell *cell = (HistoryCell *)[self.OrderHistryTBL dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];  
        }
        cell.backgroundColor = [UIColor colorWithRed:210.0/255.0 green:57.0/255.0 blue:47.0/255.0 alpha:1];
        OrderList * orderinfo = [Ordershistry objectAtIndex:indexPath.row];
        
        cell.OrderIDTL.text = [NSString stringWithFormat:@"Order # %@",orderinfo.orderId];

        if ([orderinfo.deliveryType isEqualToString:COLLECTIONORDER]) {
            cell.tabeidTL.text = [NSString stringWithFormat:@"Takeaway order"];
        }else if ([orderinfo.deliveryType isEqualToString:DELIVERYORDER]){
            cell.tabeidTL.text = [NSString stringWithFormat:@"Delivery order"];
        }else if ([orderinfo.deliveryType isEqualToString:TABLEORDER]){
            
            if (tblreserved)tblreserved = [[TableReceivedDao alloc]init];
            TableReceived * tableinfomation = [tblreserved getTableDetail:orderinfo.tableid];

            cell.tabeidTL.text = [NSString stringWithFormat:@"Table %@",tableinfomation.tableName];
        }else{
            cell.tabeidTL.text = @"";
        }

        //Active Status
        
        if ([orderinfo.activeSt isEqualToString:@"0"]) {
            cell.paiedImageView.hidden = NO;
            [cell.paiedImageView setImage:[UIImage imageNamed:@"icon_canceled~ipad.png"]];
        }else{
            if ([orderinfo.paymentSt isEqualToString:@"Paid"]) {
                cell.paiedImageView.hidden = NO;
                [cell.paiedImageView setImage:[UIImage imageNamed:@"icon_paid~ipad.png"]];
            }else{
                cell.paiedImageView.hidden = YES;
            }
        }

//        cell.timeTL.text = [NSString stringWithFormat:@"%@ : %@",orderinfo.createDate,orderinfo.createTime];
        cell.timeTL.text = orderinfo.createDate;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor clearColor];
        [cell setSelectedBackgroundView:bgColorView];
        
        return cell;
    }else if (tableView == self.CartTV){
        
        
        NSDictionary * cartData = [CartLoader objectAtIndex:indexPath.row];
        
        if ([[cartData objectForKey:@"Category"]isEqualToString:@"item"]) {
    
            
            static NSString *simpleTableIdentifier = @"AddbasketViewminiCell";
            AddbasketViewminiCell *cell = (AddbasketViewminiCell *)[self.CartTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddbasketViewminiCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            
            SelectOrder * selectedorder = [cartData objectForKey:@"data"];
            
            NSString * Itemname = selectedorder.orderName;
            cell.lblItermName.text = selectedorder.orderName;
            
            if ([Itemname isEqualToString:@"No items found."]) {
                cell.RecordIDTL.text = @"";
                cell.lblQuantity.text = @"";
                cell.lblPerOnePrice.text = @"";
                cell.lblTotalPrice.text = @"";
                cell.lblDiscount.text = @"";
            }else{
                
                cell.RecordIDTL.text = selectedorder.recodId;
                cell.lblQuantity.text = selectedorder.orderrQuantity.stringValue;
                cell.lblPerOnePrice.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,selectedorder.orderPrice];
                cell.lblTotalPrice.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,selectedorder.totalPrice];
                
                if (selectedorder.itemDiscount.doubleValue > 0) {
                    cell.lblDiscount.text = [NSString stringWithFormat:@"%@ %@",selectedorder.itemDiscount,@"%"];
                }else{
                    cell.lblDiscount.text = @"0.00 %";
                }
                
                
                if (selectedorder.cancelStatus.intValue == 1) {
                    cell.CancelIMG.hidden = NO;
                }else{
                    cell.CancelIMG.hidden = YES;
                }
            }

            return cell;
        }else if ([[cartData objectForKey:@"Category"]isEqualToString:@"addon"]){
            
            ExtraItems * addon = [cartData objectForKey:@"data"];
            
            static NSString *simpleTableIdentifier = @"CartExtraCell";
            CartExtraCell *cell = (CartExtraCell *)[self.CartTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CartExtraCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            cell.extraNameTL.text = addon.extraitemName;
            cell.extraPriceTL.text =[NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,addon.extraitemPrice];
            
            return cell;
        }else{
            SelectOrder * selectedorder = [cartData objectForKey:@"data"];
            
            static NSString *simpleTableIdentifier = @"CartExtraCell";
            CartExtraCell *cell = (CartExtraCell *)[self.CartTV dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CartExtraCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            cell.extraNameTL.text = [NSString stringWithFormat:@"Note : %@",selectedorder.ordernote];
            cell.extraPriceTL.text = @"";
            
            return cell;
        }
    }else{
        return nil;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    if (tableView == self.MenuitemTV) {

        if (![selectedOrderDetails.paymentSt isEqualToString:@"Paid"]) {
            
            if (![selectedOrderDetails.activeSt isEqualToString:@"0"]) {
                if (!addcart)addcart = [[SelectOrderDAO alloc]init];
                NSArray * holdorders = [addcart getSelectFoodCategorys];
                
                //Remove Empty place holder
                if (holdorders.count == 1) {
                    SelectOrder * order = [holdorders firstObject];
                    if ([order.orderName isEqualToString:@"No items found."])[addcart removeLocationObject];
                }
                
                NSDictionary * selectedItem = [MenuItems objectAtIndex:indexPath.row];
                [self addItemsToCart:selectedItem];
                NSLog(@"Processing %@",selectedItem);
            }else{
                UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Order cancelled ." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
                [alertview show];
            }
        }else{
            UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Order paid" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertview show];
        }
        
    }else  if (tableView == self.OrderHistryTBL){
        
        if (!addcart)addcart = [[SelectOrderDAO alloc]init];
        NSArray * holdorders = [addcart getSelectFoodCategorys];
        
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        
        if (holdorders.count > 0) {
            [addcart removeLocationObject];
            [dbmanager deleteAllExtras];
            SelectOrder * order = [holdorders firstObject];
            if ([order.orderName isEqualToString:@"No items found."])[addcart removeLocationObject];
        }
        OrderList * tableinfo = [Ordershistry objectAtIndex:indexPath.row];
        if (!orderhistory)orderhistory = [[OrderHistoryItemDao alloc]init];
        if (!tblreserved)tblreserved = [[TableReceivedDao alloc]init];
        TableReceived * tableresinfo = [tblreserved getTableDetail:tableinfo.tableid];
        
        
        if ([tableinfo.deliveryType isEqualToString:COLLECTIONORDER]) {
            self.titleTL.text = [NSString stringWithFormat:@"Takeaway order"];
        }else if ([tableinfo.deliveryType isEqualToString:DELIVERYORDER]){
            self.titleTL.text = [NSString stringWithFormat:@"Delivery order"];
        }else if ([tableinfo.deliveryType isEqualToString:TABLEORDER]){
            self.titleTL.text = [NSString stringWithFormat:@"Table  %@",tableresinfo.tableName];
        }else{
            Location * locationdetails = [dbmanager getLocationinfo:tableresinfo.locationID];
            self.titleTL.text = [NSString stringWithFormat:@"%@ in %@",tableresinfo.tableName,locationdetails.location];
        }

        self.orderidTL.text = [NSString stringWithFormat:@"Order ID : %@",tableinfo.orderId];
        
        NSLog(@"Delivery_Type %@",tableinfo.deliveryType);
        
        //self select Delivery details;
        self.deliverydetails.isFromhistory = YES;
        self.deliverydetails.orderId = tableinfo.orderId;
        self.deliverydetails.OrderlistDetails = tableinfo;
        self.deliverydetails.DeliveryType = tableinfo.deliveryType;
        [self.deliverydetails setDeliveryDetails];
        
//        self.table = tableinfo.orderId;
        NSArray * HistryItems = [orderhistory getAllCheckOutOrderList:tableinfo.orderId];
        
        if (HistryItems.count > 0) {
            for (OrderHistoryItem * histryorders in HistryItems) {
                [self addItemsToCartFromHistory:histryorders :tableinfo.orderId];
                self.orderID = histryorders.orderId;
                selectedOrderDetails = tableinfo;
            }
            
        }else{
            [self addItemsToCartFromHistoryEmpty:tableinfo.orderId];
           
        }
        
        //button control

        if(tableinfo.activeSt.intValue == 0){    //orderCanceled

            self.btnHoldOrder.enabled  = NO;
            self.btnSaveOrder.enabled = NO;
            self.btnPayOrder.enabled = NO;
            self.btnSplitOrder.enabled = NO;
            isOrderCancel = YES;
        }else{
            isOrderCancel = NO;
            
            if (tableinfo.stetus.intValue == 13) {//Save order
                
                self.btnHoldOrder.enabled  = YES;
                self.btnSaveOrder.enabled = YES;
                self.btnPayOrder.enabled = NO;
                self.btnSplitOrder.enabled = NO;
                isHoldOrder = YES;
                
            }else{
                
                isHoldOrder = NO;
                if ([self isNotPaied]) {
                    self.btnSplitOrder.enabled = YES;
                    self.btnPayOrder.enabled = YES;
                    self.btnSaveOrder.enabled = YES;
                    self.btnHoldOrder.enabled  = NO;
                    isOrderpaied = NO;
                }else{
                    self.btnSplitOrder.enabled = NO;
                    self.btnPayOrder.enabled = NO;
                    self.btnSaveOrder.enabled = NO;
                    self.btnHoldOrder.enabled  = NO;
                    isOrderpaied = YES;
                }
               
            }
        }
        
        [self loadCart];
        
    }else  if (tableView == self.CartTV){
        
        NSDictionary * cartData = [CartLoader objectAtIndex:indexPath.row];
        
        if ([[cartData objectForKey:@"Category"]isEqualToString:@"item"]) {
            
             SelectOrder * selectedorder = [cartData objectForKey:@"data"];
            NSLog(@"Selected ID %@",selectedorder.descriptionID);
            
            if ([selectedorder.descriptionID isEqualToString:@"0"]) {
                [self.ItemDetailsView setItemID:selectedorder.recodId];
                [self.ItemDetailsView AnimateOpen:YES];
            }else{
                //OldItems
            }
        }
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    
    if (tableView == self.CartTV) {
        if (isOrderCancel)return NO;
        if (isOrderpaied)return NO;
        
        NSDictionary * ItemDic = [CartLoader objectAtIndex:indexPath.row];
        SelectOrder * selectedorder = [ItemDic objectForKey:@"data"];
        if ([[ItemDic objectForKey:@"Category"]isEqualToString:@"item"]) {
            if (selectedorder.cancelStatus.intValue == 1)return NO;
        }
        return YES;
    }
    return NO;
}
-(void)didSelectExtraItemView:(BOOL)isSelected{
    
    if (isSelected) {
        self.blureview.hidden = NO;
    }else{
        self.blureview.hidden = YES;
         [self loadCart];
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (tableView == self.CartTV) {
            if (!dbmanager)dbmanager = [[DBManager alloc]init];
             if (!addcart)addcart = [[SelectOrderDAO alloc]init];
            
            NSDictionary * ItemDic = [CartLoader objectAtIndex:indexPath.row];
            

            if ([[ItemDic objectForKey:@"Category"]isEqualToString:@"item"]) {
                SelectOrder * selectedorder = [ItemDic objectForKey:@"data"];
                
                
                if (selectedorder.cancelStatus.intValue == 1) {
                    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertview show];
                }else{
                    
                    if ([selectedorder.status isEqualToString:PREPARED]) {
                        
                        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"Item can't be cancelled, as being prepared!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alertview show];
                        
                    }else if ([selectedorder.status isEqualToString:COMPLETED]){
                    
                        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"Order prepared.unable to cancel.!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                        [alertview show];
                    }else{
                        
                        int itemRecordID = selectedorder.descriptionID.intValue;
                        int recordID = selectedorder.recodId.intValue;
                        
                        
                        if (itemRecordID != 0) {
                            [self removeItemfromCart:selectedorder.descriptionID];
                            removedRecordID = selectedorder.recodId;
                        }
                        
                        [addcart removeLocationObjectByRecorID:selectedorder.recodId];
                        
                        NSArray * addons = [dbmanager GetAllExtrasByRID:selectedorder.recodId.intValue];
                        
                        for (ExtraItems * addon in addons) {
                            [dbmanager RemoveExtraItem:addon.extraitemID.intValue :addon.itemRecordID.intValue];
                        }
                    }
                    
//                    if (selectedorder.orderId.intValue == 0) {
//                        
//                        [addcart removeLocationObjectByRecorID:selectedorder.recodId];
//                        
//                        NSArray * addons = [dbmanager GetAllExtrasByRID:selectedorder.recodId.intValue];
//                        
//                        for (ExtraItems * addon in addons) {
//                            [dbmanager RemoveExtraItem:addon.extraitemID.intValue :addon.itemRecordID.intValue];
//                        }
//                    }else{
//                    [self removeItemfromCart:selectedorder.descriptionID];
//                    removedRecordID = selectedorder.recodId;

//                    }
                }
            }else{
                
                if ([[ItemDic objectForKey:@"data"] isKindOfClass:[SelectOrder class]]) {
                     SelectOrder * selectedorder = [ItemDic objectForKey:@"data"];
                    
                    if (selectedorder.orderId.intValue == 0){
                        if (!addcart)addcart = [[SelectOrderDAO alloc]init];
                        [addcart insertOrupdatenoteRID:@"" :selectedorder.recodId];
                    }
                    
                }else{
                    ExtraItems * addon = [ItemDic objectForKey:@"data"];
                    SelectOrder * order = [addcart getSelectFoodCategoryByRecordID:addon.itemRecordID.stringValue];
                    if (order.orderId.intValue == 0)[dbmanager RemoveExtraItem:addon.extraitemID.intValue :addon.itemRecordID.intValue];
                   
                }
            }
            [self loadCart];

        }else if (editingStyle == UITableViewCellEditingStyleInsert){
            
            
        }
    }
}
-(void)removeItemfromCart :(NSString *)itemDetailID{
    
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    webservice.delegate = self;
    
    NSDictionary * orderdetails = @{@"key" : WEBSERVICEKEY,
                                    @"id" : itemDetailID};
    
    [webservice CancelOrderItem:orderdetails];
    [self inithud:@"Cancelling"];
    
}
-(void)DidReceiveCancelOrderItemDetails:(id)respond{

    
    if ([[respond objectForKey:@"Status"]isEqualToString:@"Success"]) {
        
        if (!addcart)addcart = [[SelectOrderDAO alloc]init];
        [addcart removeLocationObjectByRecorID:removedRecordID];
        NSArray * addons = [dbmanager GetAllExtrasByRID:removedRecordID.intValue];
        
        for (ExtraItems * addon in addons) {
            [dbmanager RemoveExtraItem:addon.extraitemID.intValue :addon.itemRecordID.intValue];
        }
        [self loadCart];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    [self hudWasHidden];
}
#pragma mark -
#pragma mark OrderHistory

- (IBAction)btnHistoryClicks:(id)sender {
    
    if (isOpen) {
        [self.histryView AnimatetableviewOpen:NO];
        isOpen = NO;
        [self OrderbtnActionSelected:YES];
    }else{
        [self OrderbtnActionSelected:NO];
        [self.histryView AnimatetableviewOpen:YES];
        isOpen = YES;
        [self HistorySyncProcess];
    }
}
- (IBAction)HistrySegControllerClicks:(id)sender {
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!Ordershistry)Ordershistry = [[NSMutableArray alloc]init];
    
    [Ordershistry removeAllObjects];

    if (self.historySegController.selectedSegmentIndex == 0) {
        
        NSArray * holdorders = [dbmanager getOrderHistorybystatus:SAVE];//Hold Orders
        [Ordershistry addObjectsFromArray:[self AscendingOrders:holdorders]];
        
    }else if (self.historySegController.selectedSegmentIndex == 1){
        
        if (!processcollection)processcollection = [[NSMutableArray alloc]init];
        [processcollection removeAllObjects];
        
        NSArray * holdorders = [dbmanager getOrderHistorybystatus:SENTTOKITCHEN];//Current Orders
        [processcollection addObjectsFromArray:holdorders];
        
        NSArray * prepardorders = [dbmanager getOrderHistorybystatus:PREPARED]; // Prepared Orders
        [processcollection addObjectsFromArray:prepardorders];
        
        [Ordershistry addObjectsFromArray:[self AscendingOrders:processcollection]];
        
    }else if (self.historySegController.selectedSegmentIndex == 2){
        
        NSArray * holdorders = [dbmanager getOrderHistorybystatus:COMPLETED];//completed Orders
        [Ordershistry addObjectsFromArray:[self AscendingOrders:holdorders]];
        
    }
       [self.OrderHistryTBL reloadData];
}
-(NSArray *)AscendingOrders:(NSArray *)rowdata{
    
    NSMutableArray * rowdataArray = [[NSMutableArray alloc]initWithArray:rowdata];
    NSMutableArray * ascender = [[NSMutableArray alloc]init];
    OrderList * highestOrder;
    int highestOrderIndex = 0;
    int orderidtwo = 0;

    while (rowdataArray.count > 0) {
            int orderidone = 0;
        for (int i = 0; i<rowdataArray.count; i++) {
            OrderList * orderinfo = [rowdataArray objectAtIndex:i];
            orderidtwo = orderinfo.orderId.intValue;
            if (orderidtwo > orderidone) {
                orderidone = orderidtwo;
                highestOrder = orderinfo;
                highestOrderIndex = i;
            }
        }
        [rowdataArray removeObjectAtIndex:highestOrderIndex];
        [ascender addObject:highestOrder];
    }
    
    return [NSArray arrayWithArray:ascender];
}
-(void)loadHoldOrders{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!Ordershistry)Ordershistry = [[NSMutableArray alloc]init];
    
    [Ordershistry removeAllObjects];

    NSArray * holdorders = [dbmanager getOrderHistorybystatus:@"13"];//Hold Orders
    [Ordershistry addObjectsFromArray:[self AscendingOrders:holdorders]];
    [self AscendingOrders:holdorders];
    [self.OrderHistryTBL reloadData];
    
}
-(void)SegmentVCtitls{
     if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * holdorders = [dbmanager getOrderHistorybystatus:SAVE];
    NSArray * currentorders = [dbmanager getOrderHistorybystatus:SENTTOKITCHEN];
    NSArray * processingorders = [dbmanager getOrderHistorybystatus:PREPARED];
    NSArray * completed = [dbmanager getOrderHistorybystatus:COMPLETED];
    
    NSString * holdtitle = [NSString stringWithFormat:@"Hold (%lu)",(unsigned long)holdorders.count];
    NSString * currenttotle = [NSString stringWithFormat:@"Waiting (%lu)",(unsigned long)currentorders.count + (unsigned long)processingorders.count];
    NSString * compledtitle = [NSString stringWithFormat:@"Complete (%lu)",(unsigned long)completed.count];
    
    [self.historySegController setTitle:holdtitle forSegmentAtIndex:0];
    [self.historySegController setTitle:currenttotle forSegmentAtIndex:1];
    [self.historySegController setTitle:compledtitle forSegmentAtIndex:2];
    
    
}
-(void)HistorySyncProcess{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userId=[userDefaults valueForKey:@"userLoginId"];
    NSString * LMD = [AppUserDefaults getHistryDateTime];//[userDefaults valueForKey:@"LMD"];
    
//    [self inithud:@"Refreshing.."];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSDictionary * params = @{@"customer_id" : userId,
                              @"last_update":LMD,
                              @"key" : WEBSERVICEKEY
                              };
    
    NSLog(@"Histor LMD %@",LMD);
    
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    webservice.delegate = self;
    [webservice GetHistory:params];
}
-(void)DidReceiveHistory:(BOOL)isFatched{
   [self loadHoldOrders];
    self.historySegController.selectedSegmentIndex = 0;
   [self MainScrollerControl];
   [self SegmentVCtitls];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}
-(void)OrderbtnActionSelected :(BOOL)isSelected{
    
    if (isSelected) {
        self.btnHold.backgroundColor = [UIColor colorWithRed:210/255.0 green:57/255.0 blue:47/255.0 alpha:1.0];
        [self.btnHold setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        self.btnHold.backgroundColor = [UIColor whiteColor];
        [self.btnHold setTitleColor:[UIColor colorWithRed:210/255.0 green:57/255.0 blue:47/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
}
#pragma mark -
#pragma mark Cart Functions

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        NSLog(@"index 0");
    }else{
       
        NSString * newqty = [alertView textFieldAtIndex:0].text;
        
        if ([[NSScanner scannerWithString:newqty] scanInt:nil]) {
            
            if (newqty.intValue > 0) {
                [self addItemsToCartMoreQty:cartSelectedOrder :newqty];
            }else{
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"Value is not accepted." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }else{
            
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Sorry !" message:@"Value is not accepted." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
}
-(void)addItemsToCartCallback:(NSDictionary *)SelectedItem :(NSString *)orderstatus{
    
    NSString * itemid = [SelectedItem objectForKey:@"item_id"];
    NSString * Orderid = [SelectedItem objectForKey:@"order_id"];
    NSString * recordid = [self recordidGen];
    NSString * itemname = [SelectedItem objectForKey:@"itemname"];
    NSString * itemqty = [SelectedItem objectForKey:@"qyt"];
    NSString * descit = @"";
    NSString * note = [SelectedItem objectForKey:@"note"];
    if (note == nil)note = @"";
    NSString * itemPrice = [SelectedItem objectForKey:@"price"];
    NSString * discID = [SelectedItem objectForKey:@"orderdetails_id"];
    NSString * itemlocation = [SelectedItem objectForKey:@"item_location"];
    NSString * itemDiscount = [SelectedItem objectForKey:@"discount"];
    NSString * defaultDiscountStatus = [SelectedItem objectForKey:@"item_default_discount"];

    
    NSDictionary* itemdicmy = @{@"itemId"         : itemid,
                                @"itemQty"        : itemqty,
                                @"RecordId"       : recordid,
                                @"itemname"       : itemname,
                                @"tableid"        : @"12",
                                @"noofchairs"     : @"0",
                                @"OrderID"        : Orderid,
                                @"descit"         : descit,
                                @"itemPrice"      : itemPrice,
                                @"itemDiscount"   : itemDiscount,
                                @"isEdited"       : @NO,
                                @"chair"          : @"0",
                                @"status"         : orderstatus,
                                @"ActiveSt"       : @"0",
                                @"note"           : note,
                                @"orderdetailsid" : discID,
                                @"hasNote"        : @YES,
                                @"itemUpdate"     : @NO,
                                @"itemLocation"   : itemlocation,
                                @"defaultDiscountStatus" : defaultDiscountStatus};
    
    
    
    [addcart addNewSelectOrderItermfromdic:itemdicmy :@"Table one"];
    [self loadCart];
}
-(void)addItemsToCartMoreQty:(SelectOrder *)SelectedItem :(NSString *)newQty
{
    //    NSLog(@"Incomming dic %@",SelectedItem);
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    
    NSString * itemid = SelectedItem.categoryId;

    NSString * discID = SelectedItem.descriptionID;
    if (discID == nil)discID = @"0";
    NSString * Orderid = SelectedItem.orderId;
    
    if (Orderid == nil)Orderid = @"0";
    
    NSString * recordid = SelectedItem.recodId;
    
    NSString * itemname = SelectedItem.orderName;
    NSString * itemPrice = SelectedItem.orderPrice;
    NSString * defaultDiscountStatus = SelectedItem.defaultDiscountStatus;
    NSString * itemDiscount = SelectedItem.itemDiscount;
    NSString * orderstatus =SelectedItem.status;
    
    NSDictionary* itemdicmy = @{@"itemId"         : itemid,
                                @"itemQty"        : newQty,
                                @"RecordId"       : recordid,
                                @"itemname"       : itemname,
                                @"tableid"        : @"12",
                                @"noofchairs"     : @"0",
                                @"OrderID"        : Orderid,
                                @"descit"         : @"",
                                @"itemPrice"      : itemPrice,
                                @"status"         : orderstatus,
                                @"itemDiscount"   : itemDiscount,
                                @"isEdited"       : @NO,
                                @"chair"          : @"0",
                                @"ActiveSt"       : @"0",
                                @"orderdetailsid" : discID,
                                @"hasNote"        : @NO,
                                @"itemUpdate"     : @NO,
                                @"defaultDiscountStatus" : defaultDiscountStatus};
    
    
    
//    [addcart addNewSelectOrderItermNewQty:itemdicmy :SelectedItem.tableName];
     [addcart addNewSelectOrderItermNewQtyByRecordid:itemdicmy :SelectedItem.tableName];
    [self loadCart];
}

-(void)addItemsToCart:(NSDictionary *)SelectedItem
{
//    NSLog(@"Incomming dic %@",SelectedItem);
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
     if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    
    NSString * itemid = [SelectedItem objectForKey:@"item_id"];
    
    SelectOrder * item = [addcart getSelectFoodCategory:itemid];
    NSString * discID = @"0";

    NSString * Orderid = item.orderId;
    if (Orderid == nil)Orderid = @"0";
    
    NSString * recordid = [self recordidGen];
    //item_id
    NSString * itemname = [SelectedItem objectForKey:@"item_name"];
    NSString * descit = [SelectedItem objectForKey:@"item_discription"];
    NSString * itemPrice = [SelectedItem objectForKey:@"item_price"];
    NSString * img_url = [SelectedItem objectForKey:@"item_image"];
    NSString * itemlocation = [SelectedItem objectForKey:@"itemLocation"];
    NSString * itemDiscount = [SelectedItem objectForKey:@"discount"];
    NSString * defaultDiscountStatus = [SelectedItem objectForKey:@"addDefaultDiscount"];
    
    NSDictionary* itemdicmy = @{@"itemId"         : itemid,
                                @"itemQty"        : @"1",
                                @"RecordId"       : recordid,
                                @"itemname"       : itemname,
                                @"tableid"        : @"12",
                                @"noofchairs"     : @"0",
                                @"OrderID"        : Orderid,
                                @"descit"         : descit,
                                @"itemPrice"      : itemPrice,
                                @"itemDiscount"   : itemDiscount,
                                @"img_url"        : img_url,
                                @"status"         : @"1",
                                @"isEdited"       : @NO,
                                @"chair"          : @"0",
                                @"orderdetailsid" : discID,
                                @"ActiveSt"       : @"0",
                                @"hasNote"        : @NO,
                                @"itemUpdate"     : @NO,
                                @"itemLocation"   : itemlocation,
                                @"defaultDiscountStatus" : defaultDiscountStatus};
    
   
    
    [addcart addNewSelectOrderItermfromdic:itemdicmy :@"Table one"];
    [self loadCart];
}
-(void)addItemsToCartFromHistory:(OrderHistoryItem *)item :(NSString *)tableid
{
    NSString * recordid = [self recordidGen];
    NSString * note = @"";
    BOOL isNoteHave = NO;
    
    if (item.itemNote.length > 0) {
        note = item.itemNote;
        isNoteHave = YES;
    }
    
    //itemstatus
    if (!OLDAO)OLDAO = [[OrderListDao alloc]init];
    OrderList * orderdetails = [OLDAO getCheckOutOrderList:item.orderId];
    
    NSDictionary* itemdicmy = @{@"itemId"         : item.iterm_id,
                                @"itemQty"        : item.itemQuantity,
                                @"RecordId"       : recordid,
                                @"itemname"       : item.itemName,
                                @"tableid"        : tableid,
                                @"noofchairs"     : @"0",
                                @"OrderID"        : item.orderId,
                                @"descit"         : @"",
                                @"itemPrice"      : item.price,
                                @"itemDiscount"   : item.itemDiscount,
                                @"isEdited"       : @NO,
                                @"chair"          : @"0",
                                @"status"         : orderdetails.stetus,
                                @"note"           : note,
                                @"ActiveSt"       : item.activeST,
                                @"orderdetailsid" : item.itemDesID,
                                @"hasNote"        : isNoteHave ? @YES :@NO,
                                @"itemUpdate"     : @NO,
                                @"itemLocation"   : item.itemLocation,
                                @"defaultDiscountStatus" : item.defaultDiscountStatus};
    
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    
    [addcart addNewSelectOrderItermfromdic:itemdicmy :tableid];
    [self ProcessExtraItems:itemdicmy :recordid.intValue];
}
-(void)addItemsToCartFromHistoryEmpty:(NSString *)tableid
{
    
    NSDictionary* itemdicmy = @{@"itemId"         : @"",
                                @"itemQty"        : @"",
                                @"RecordId"       : @"0",
                                @"itemname"       : @"No items found.",
                                @"tableid"        : @"",
                                @"noofchairs"     : @"0",
                                @"OrderID"        : @"",
                                @"descit"         : @"",
                                @"itemPrice"      : @"",
                                @"isEdited"       : @NO,
                                @"chair"          : @"0",
                                @"ActiveSt"       : @"0",
                                @"orderdetailsid" : @"",
                                @"hasNote"        : @NO,
                                @"itemUpdate"     : @NO};
    
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    
    [addcart addNewSelectOrderItermfromdic:itemdicmy :tableid];
}
-(NSString *)recordidGen{
    int Trecordid = 0;
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * items = [addcart getSelectFoodCategorys];
    for (SelectOrder * order in items) {
        int CRecordid = order.recodId.intValue;
        if (CRecordid != 0) {
           
            if (CRecordid > Trecordid) {
                Trecordid = CRecordid;
            }
        }
    }
    
    int NewRecordid = Trecordid +1;
 
    return [NSString stringWithFormat:@"%i",NewRecordid];
}
-(void)ProcessExtraItems:(NSDictionary *)itemdetails :(int)RecordID{
     if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * extraArray = [dbmanager GetExtraHistoryByDetailID:[[itemdetails objectForKey:@"orderdetailsid"] intValue]];
    
    
    for (ExtraHistory * extrahistory in extraArray) {
        [dbmanager insertExtraByHistory:extrahistory :RecordID];
    }
    
}

-(void)loadCart{
    float totalPrice = 0.0;
    int totalQty = 0;
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    if (!extraitem)extraitem = [[ExtraItems alloc]init];
    if (!CartLoader)CartLoader = [[NSMutableArray alloc]init];
    
    [CartLoader removeAllObjects];
    NSArray * holdorders = [addcart getSelectFoodCategorys];

    for (SelectOrder * item in holdorders) {
        
        NSDictionary* itemdicmy = @{@"Category"       : @"item",
                                    @"data"           : item};
        
        [CartLoader addObject:itemdicmy];
        float subitemQty = 0;
        
        NSArray * addons = [dbmanager GetAllExtrasByRID:item.recodId.intValue];
        
        for (ExtraItems * addon in addons) {
            
            NSDictionary* addonDic = @{@"Category"       : @"addon",
                                        @"data"           : addon};
            [CartLoader addObject:addonDic];
            subitemQty = subitemQty + addon.extraitemPrice.floatValue;
        }
        
        if (item.ordernote.length > 0) {
            NSDictionary* addonDicnote = @{@"Category"       : @"note",
                                           @"data"           : item};
            [CartLoader addObject:addonDicnote];
        }
        
        if (item.cancelStatus.intValue == 0) {
            totalPrice = totalPrice +[item.totalPrice floatValue]+subitemQty;
            totalQty = totalQty + [item.orderrQuantity intValue];
        }
        
        if ([item.status isEqualToString:NEWITEM]){
            self.btnSaveOrder.enabled  = YES;
        }else if ([item.status isEqualToString:SAVE]){
            self.btnSaveOrder.enabled  = YES;
        }else{
            self.btnSaveOrder.enabled  = NO;
        }
        
//            self.btnHoldOrder.enabled  = YES;
//        self.btnSaveOrder.enabled = YES;
    }
    Subtotal = [NSString stringWithFormat:@"%0.2f",totalPrice];
    self.totalPriceTF.text = [NSString stringWithFormat:@"%@ %0.2f",CURRENCYSYMBOL,totalPrice];
    self.totalitemTF.text = [NSString stringWithFormat:@"%i",totalQty];
    
    

    [self.CartTV reloadData];
}
-(void)createTotalandQty :(NSArray *)items{
    
    float totalPrice = 0.0;
    int totalQty = 0;
    
    for (SelectOrder * item in items) {
        
        totalPrice = totalPrice +[item.totalPrice floatValue];
        totalQty = totalQty + [item.orderrQuantity intValue];
    }
    
    Subtotal = [NSString stringWithFormat:@"%0.2f",totalPrice];
    self.totalPriceTF.text = [NSString stringWithFormat:@"£ %0.2f",totalPrice];
    self.totalitemTF.text = [NSString stringWithFormat:@"%i",totalQty];
}
-(void)removeAllRecords{
    
    self.orderidTL.text = @"";
     if (!addcart)addcart = [[SelectOrderDAO alloc]init];
     if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    [addcart removeLocationObject];
    [dbmanager deleteAllExtras];
    [self loadCart];
}
#pragma mark -
#pragma mark MenuItem TableView
-(void)DidReceiveLocationinfo:(BOOL)isFatched{

     [self hudWasHidden];
     [self MainScrollerControl];
}

#pragma mark -
#pragma mark OrderCancelStatusProcess

-(void)didReceivedOrderCancelDetails:(id)respond{
    [self removeAllRecords];
}

#pragma mark -
#pragma mark Order Sending Process

- (IBAction)btnHoldClicks:(id)sender {
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * items = [addcart getSelectFoodCategorys];
    if ([items count]>0) {
         [self sendSelectedItems:SAVE];
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Cart empty." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    isinstantPay = NO;
}
- (IBAction)btnSendClicks:(id)sender {
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * items = [addcart getSelectFoodCategorys];
    if ([items count]>0) {
        
        if ([self isPrinterOn]) {
            [self sendSelectedItems:SENTTOKITCHEN];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Cashier/Kitchen printer error !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Cart empty." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    isinstantPay = NO;
}


-(void)sendSelectedItems :(NSString *)OrderStatus{
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    BOOL isItemEdit = NO;
    BOOL isMainOrderEdit = NO;
    int customerID;
    
    [Flurry logEvent:[NSString stringWithFormat:@"OrderProcess = %@",self.orderID]];
    
    
    //ItemsArray
    [SelectedItemDics removeAllObjects];
    if (!SelectedItemDics)SelectedItemDics = [[NSMutableArray alloc]init];
 
    NSArray * SelectedItems= [addcart getSelectFoodCategorys];
    
    for (SelectOrder * Selecteditem in SelectedItems) {
        NSDictionary * addonDic = [self returnAddonDic:Selecteditem.recodId];
        NSLog(@"Converted Addon Json = %@",addonDic);
        
        BOOL hasNote = NO;
        NSString * note = @"";
        if (Selecteditem.ordernote.length > 0) {
            hasNote = YES;
            note =Selecteditem.ordernote;
        }
        
        NSDictionary * ItemCollectionDictionary  = @{@"itemId"          : Selecteditem.categoryId,
                                                     @"itemQty"         : Selecteditem.orderrQuantity,
                                                     @"orderdetails_id" : [f numberFromString:Selecteditem.descriptionID],
                                                     @"itemPrice"       : Selecteditem.orderPrice,
                                                     @"addons"          : addonDic,
                                                     @"itemNote"        : note,
                                                     @"chair"           : [f numberFromString:@"0"],
                                                     @"hasNote"         : hasNote ? @YES :@NO,
                                                     @"isEdited"        : isItemEdit ? @YES : @NO };
        
        
        if (Selecteditem.cancelStatus.intValue != 1)[SelectedItemDics addObject:ItemCollectionDictionary];
    }
    //if (selectedorder.cancelStatus.intValue == 1) {
    
    //Json Converter
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:SelectedItemDics forKey:@"items"];
    NSString * JsonString = [self jsonconverter:postDict];
    
    NSLog(@"printed json %@",JsonString);
    
    
    //DictionaryHeader

    //Components
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * userid = [userDefaults objectForKey:@"userLoginId"];
    NSString * delivery_type = COLLECTIONORDER;
    
    //If Send to kichen
        NSLog(@"Send to kitchen");
    
    //SET DELIVERY TYPE

        if (self.table == nil || self.table.intValue == 0){//select TakeAway and Dining;
           self.table = @"0";
          delivery_type = @"0";
          
            if ([AppUserDefaults getDeliveryType] != nil) {
                delivery_type = [AppUserDefaults getDeliveryType];
            }else{
                delivery_type = COLLECTIONORDER;
            }
            
        }else{
            delivery_type = TABLEORDER;
        }
    
    //SET CUSTPMER DETAILS
    
    customerID = [[self getCustomerID] intValue];

    
    NSDictionary * tempDeliveryDetails =nil;
    NSString * DeliveryjsonString = nil;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"SDD"] != nil) {
        NSDictionary * deliveryDetails = [[NSUserDefaults standardUserDefaults] objectForKey:@"SDD"];
        DeliveryjsonString = [self jsonconverter:deliveryDetails];
    }else{
        tempDeliveryDetails = @{@"deliveryDate"         : @"0",
                                @"deliveryAddress"      : @"0",
                                @"deliveryPerson"       : @"0",
                                @"deliveryPhone"        : @"0",
                                @"deliveryEmail"        : @"0"};
        
        
        DeliveryjsonString = [self jsonconverter:tempDeliveryDetails];
        
    }
    
    NSLog(@"DD = %@",DeliveryjsonString);
    
    if (self.orderID == nil){
       self.orderID = @"0";
    }

        NSString * totalPrice = [self Totalprice];
    
        NSLog(@"tableid %@",self.orderID);
    
    
    if ([OrderStatus isEqualToString:SENTTOKITCHEN]) {                                      //Send to Kichen orders.
        
        
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        [dbmanager ActvePrinterFeeds:isHoldOrder];
        
        if (self.orderID.intValue != 0)isMainOrderEdit = YES;
        
        containerHeader = @{@"table"             : self.table,
                            @"total_value"       : totalPrice,
                            @"orderId"           : self.orderID,
                            @"os"                : @"IOSCashierApp",
                            @"status"            : SENTTOKITCHEN,
                            @"delivery_type"     : delivery_type,
                            @"isEdited"          : isMainOrderEdit ? @YES : @NO,
                            @"person_id"         : [f numberFromString:userid],
                            @"customer_id"       : [NSNumber numberWithInt:customerID],
                            @"data"              : JsonString,
                            @"key"               : WEBSERVICEKEY,
                            @"delievery_details" : DeliveryjsonString};
        
        if (!webservice)webservice = [[WebserviceApi alloc]init];
        webservice.delegate = self;
        [webservice SendOrderToServer:containerHeader];
        [self inithud:@"Sending order to kitchen"];
        
        isHoldOrder = NO;
        
    }else if([OrderStatus isEqualToString:SAVE]){                                           //save orders.
        
        isHoldOrder = YES;
        
        if (self.orderID.intValue != 0)isMainOrderEdit = YES;

            containerHeader = @{@"table"             : self.table,
                                @"total_value"       : totalPrice,
                                @"orderId"           : self.orderID,
                                @"os"                : @"IOSCashierApp",
                                @"status"            : SAVE,
                                @"delivery_type"     : delivery_type,
                                @"isEdited"          : isMainOrderEdit ? @YES : @NO,
                                @"person_id"         : [f numberFromString:userid],
                                @"customer_id"       : [NSNumber numberWithInt:customerID],
                                @"data"              : JsonString,
                                @"key"               : WEBSERVICEKEY,
                                @"delievery_details" : DeliveryjsonString};
        
        if (!webservice)webservice = [[WebserviceApi alloc]init];
        webservice.delegate = self;
        [webservice SendOrderToServer:containerHeader];
        [self inithud:@"Send order to Save"];
        
    }else{                                                                                  //Pay before Send to kitchen.
        
        containerHeader = @{@"table"             : self.table,
                            @"total_value"       : totalPrice,
                            @"orderId"           : self.orderID,
                            @"os"                : @"IOSCashierApp",
                            @"status"            : COMPLETED,
                            @"delivery_type"     : delivery_type,
                            @"isEdited"          : isMainOrderEdit ? @YES : @NO,
                            @"person_id"         : [f numberFromString:userid],
                            @"customer_id"       : [NSNumber numberWithInt:customerID],
                            @"data"              : JsonString,
                            @"key"               : WEBSERVICEKEY,
                            @"delievery_details" : DeliveryjsonString};
        
        if (!webservice)webservice = [[WebserviceApi alloc]init];
        webservice.delegate = self;
        [webservice SendOrderToServer:containerHeader];
        [self inithud:@"Processing.."];
    }
    
    NSLog(@"finelContainer %@",containerHeader);

}

-(NSDictionary *)returnAddonDic :(NSString *)RecordID{
    
     if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!addonsArray)addonsArray = [[NSMutableArray alloc]init];
    [addonsArray removeAllObjects];
    
    NSArray * addons = [dbmanager GetAllExtrasByRID:RecordID.intValue];
    
    for (ExtraItems * addon in addons) {
        
        NSString * ctype = addon.cType;
        if (ctype == nil)ctype = @"";
        
        NSNumber * extraID = addon.extraitemID;
        if (extraID == nil)extraID = 0;
        
        NSDictionary * addonsDic = @{@"addons_type" :ctype,
                                     @"addons_id"   :extraID};
        [addonsArray addObject:addonsDic];
    }
    
    NSDictionary * addonsDic = @{@"addons" :[NSArray arrayWithArray:addonsArray]};
    NSLog(@"addonsDic %@",addonsDic);
    return addonsDic;
    
}
-(NSString *)getCustomerID{
    
    NSUserDefaults * customerdefalut = [NSUserDefaults standardUserDefaults];
    NSString * CID = [customerdefalut objectForKey:@"SCD"];
    
    if (CID == nil)CID = 0;
    
    return CID;
    
}
-(NSString *)getOrderType :(NSString *)ordertypeNo{

    int orderNo = ordertypeNo.intValue;
    
    switch (orderNo) {
        case 0:
            return @"TAKEAWAY";
            break;
            
        case 1:
            
            return @"DELIVERY";
            break;
            
        case 2:
            
            return @"DINING";
            break;
            
        default:
            
            return @"NOT DEFINE";
            break;
    }
}
-(void)DidReceiveSendinfo:(id)respond{
    
    NSLog(@"infomation %@",respond);
    
    
    if ([respond isKindOfClass:[NSString class]]) {
        [self hudWasHidden];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Sending order to kitchen failed !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        return ;
    }
    
    BOOL reslt = [[respond objectForKey:@"Result"]boolValue];
    
    if (reslt) {
        
        [self.addcustomer removeSecectedCustomer];
        [self.deliverydetails removeDeliveryDetails];
        
        NSDictionary * dataDic = [respond objectForKey:@"Data"];
        
        NSLog(@"dataDic %@",dataDic);
        
        //CartUpdate
        
        NSArray * detailArray = [dataDic objectForKey:@"order_details"];
        if (!addcart)addcart = [[SelectOrderDAO alloc]init];
        [addcart removeLocationObject];
        
        for (NSDictionary * item in detailArray) {
            [self addItemsToCartCallback:item : [dataDic objectForKey:@"order_st"]];
        }
        
        NSString * tableid = [dataDic objectForKey:@"table"];
        self.orderID = nil;
        HUD.mode = MBProgressHUDModeIndeterminate;
        HUD.labelText = @"Refresing..";
        [self reloadTableData];
       
        NSLog(@"tableid = %@",tableid);
        
        NSString * orderId = [dataDic objectForKey:@"order_id"];
        self.orderidTL.text = [NSString stringWithFormat:@"Order ID : %@",orderId];
        
        self.orderID = orderId;
        
        if ([[dataDic objectForKey:@"order_st"] isEqualToString:SENTTOKITCHEN]){
            NSString * delType = [self getOrderType:[dataDic objectForKey:@"delivery_type"]];
            
            NSDictionary * Deliveryinfomations = [dataDic objectForKey:@"deleveryDetail"];
            
            NSString * registedCustomer = [Deliveryinfomations objectForKey:@"deliveryPerson"];
            self.SelectedCustomer = registedCustomer;
            [self Printing:orderId :delType :tableid :registedCustomer];
        }
        
        if (tableid.intValue != 0) [self OrderSentToKitchen:YES];
        
        
        [self hudWasHidden];
        
        if (isinstantPay) {
            [self btnPayClicks:nil];
        }
        
        [self respondBtnControl:[dataDic objectForKey:@"order_st"]];
        

    }else{
        [self hudWasHidden];
        NSString * errormsg = [respond objectForKey:@"Error"];
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:errormsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
  
}
-(void)respondBtnControl :(NSString *)status{
    
    int realstatus = status.intValue;
    
    switch (realstatus) {
        case 4:
            
            self.btnHoldOrder.enabled  = NO;
            self.btnSaveOrder.enabled = NO;
            self.btnPayOrder.enabled = YES;
            self.btnSplitOrder.enabled = YES;
            
            break;
            
        case 13:
            
            self.btnHoldOrder.enabled  = YES;
            self.btnSaveOrder.enabled = YES;
            self.btnPayOrder.enabled = NO;
            self.btnSplitOrder.enabled = NO;
            
            break;
            
        case 5:
            
            self.btnHoldOrder.enabled  = NO;
            self.btnSaveOrder.enabled = NO;
            self.btnPayOrder.enabled = NO;
            self.btnSplitOrder.enabled = NO;
            
            break;
            
        case 6:
            
            
            
            break;
            
        case 7: //add new item for send to kitchen
            
            self.btnHoldOrder.enabled  = NO;
            self.btnSaveOrder.enabled = YES;
            self.btnPayOrder.enabled = YES;
            self.btnSplitOrder.enabled = YES;
            
            break;
            
        default:
            
            
            break;
    }
    
}
#pragma mark - 
#pragma mark Printer Feeds

-(void)Printing :(NSString *)Orderid :(NSString *)OrderType :(NSString *)tableId :(NSString *)customer{
    
    NSLog(@"OrderType %@",OrderType);
    NSString * tableName = @"";
    
    if (tableId.intValue != 0) {
        if (!tblreserved)tblreserved = [[TableReceivedDao alloc]init];
      TableReceived * tableresinfo = [tblreserved getTableDetail:tableId];
        tableName = tableresinfo.tableName;
    }
    
    if ([[AppUserDefaults getPrinterStatus:CASHERPRINTER] isEqualToString:PRINTERON]) {
        NSDictionary * casherPrinterDic = [[NSUserDefaults standardUserDefaults] objectForKey:CASHERPRINTER];
        if (casherPrinterDic !=nil) {
            if (!printer)printer = [[Printer alloc]init];
            
            printer.TableId = tableId;
            printer.TableName = tableName;
            printer.Customer = customer;
            
            [printer initWithKOTPrint:Orderid :[casherPrinterDic objectForKey:@"portName"] :@"Standard" : OrderType];
            
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Check KOT printer settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    if ([[AppUserDefaults getKOTPrintinCashier] isEqualToString:PRINTERON]) {
        
        NSDictionary * casherPrinterDic = [[NSUserDefaults standardUserDefaults] objectForKey:KITCHENPRINTER];
        if (casherPrinterDic !=nil) {
            if (!printer)printer = [[Printer alloc]init];
            
            printer.TableId = tableId;
            printer.TableName = tableName;
            
            [printer initWithKOTPrintInCashierPrinter:Orderid :[casherPrinterDic objectForKey:@"portName"] :@"Standard" :OrderType];
            
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Check cashier printer settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

-(BOOL)isPrinterOn {
    
    BOOL isOn = NO;
    
    if ([[AppUserDefaults getKOTPrintinCashier] isEqualToString:PRINTERON] && [[AppUserDefaults getPrinterStatus:CASHERPRINTER] isEqualToString:PRINTERON]) {
        isOn = YES;
    }
    return isOn;
}

-(NSString *)jsonconverter :(NSDictionary *)dictionary{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];

    if (! jsonData) {
        //        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
-(BOOL)isItemEdit:(NSString *)itemDisID :(NSString *)CQty{
    
    if (!orderhistory)orderhistory = [[OrderHistoryItemDao alloc]init];
    if (itemDisID != nil) {
        OrderHistoryItem *item = [orderhistory getCheckOutOrderList:itemDisID];
        int oldqty = [item.itemQuantity intValue];
        
        if (oldqty == 0)return NO;
        
        int newqty = [CQty intValue];
        if (oldqty !=newqty ){
                return YES;
        }
    }else{
        return YES;
    }
    return NO;
}
#pragma mark -
#pragma mark TotalPrice

-(NSString *)Totalprice {
    
     if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * SelectedItems= [addcart getSelectFoodCategorys];
    double totalP = 0.0;
    for (SelectOrder * Selecteditem in SelectedItems) {

        totalP = totalP + [Selecteditem.totalPrice doubleValue];
    }
    return [NSString stringWithFormat:@"%0.2f",totalP];
}
#pragma mark -
#pragma mark Pay and Split Section
- (IBAction)btnSplitClicks:(id)sender {
    
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    
    NSArray * bucketdata = [addcart getSelectValidFoodCategorys];
    
    if ([bucketdata count]== 0) {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Shopping cart empty or all items cancelled!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        
    }else{
        if (self.orderID.intValue > 0) {
            SplitCheckViewController * splitview = [self.storyboard instantiateViewControllerWithIdentifier:@"SplitCheckViewController"];
            splitview.OrderID = self.orderID;
            [self.navigationController pushViewController:splitview animated:YES];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Order not process !" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alert show];
        }
    }
}
- (IBAction)btnPayClicks:(id)sender {
    
    NSLog(@"View %@",self.orderID);
    if (!addcart)addcart = [[SelectOrderDAO alloc]init];
    NSArray * items = [addcart getSelectValidFoodCategorys];
    if ([items count]>0) {
        
        if (self.orderID == nil || [self.orderID isEqualToString:@"0"]) {
            isinstantPay = YES;
            [self sendSelectedItems:COMPLETED];
        }else{
            if (!addcart)addcart = [[SelectOrderDAO alloc]init];
            NSArray * bucketdata = [addcart getSelectFoodCategorys];
            
            if ([bucketdata count]== 0) {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Shopping cart empty!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alert show];
                
            }else{
                NSLog(@"Loghere %@",self.orderID);
                
                if (self.orderID == nil) {
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"No order id found." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }else if(Subtotal == nil){
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"No Subtotal found." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }else{
                    CheckoutViewController * payview = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
                    payview.orderid = self.orderID;
                    payview.Subtotal = Subtotal;
                    payview.Customer = self.SelectedCustomer;
                    payview.isFromSplit = NO;
                    [self.navigationController pushViewController:payview animated:YES];
                }
            }
        }
    }else{
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Shopping cart empty!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

-(BOOL)isNotPaied{
    
    if (!OLDAO)OLDAO = [[OrderListDao alloc]init];
   OrderList * orderlist = [OLDAO getCheckOutOrderList:self.orderID];
    
    if ([orderlist.paymentSt isEqualToString:@"Not paid"] || [orderlist.paymentSt isEqualToString:@"1"]) {
        return YES;
    }
    return NO;
}

#pragma mark -
#pragma mark Reachability

-(BOOL)ReachabilityPass{
    
    Reachability * networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus != NotReachable) {
        return true;
    }
    return false;
}
#pragma mark -
#pragma mark Profile
- (IBAction)btnProfileClicks:(id)sender {
    
    if (isProfileOpen) {
         [self.ProfileView AnimatetableviewOpen:NO];
        isProfileOpen = NO;
    }else{
        [self.ProfileView AnimatetableviewOpen:YES];
        isProfileOpen = YES;
    }
}

-(void)didAnimationOpen:(BOOL)isOpenProfile{
    
    if (isOpenProfile) {
        self.blureview.hidden = NO;
    }else{
        self.blureview.hidden = YES;
    }
}
-(void)managemyprofileImage{
    
    self.ProfileImage.layer.cornerRadius = self.ProfileImage.frame.size.width / 2;
    self.ProfileImage.clipsToBounds = YES;
    self.ProfileImage.layer.borderWidth = 3.0f;
    self.ProfileImage.layer.borderColor = [UIColor whiteColor].CGColor;
    
}
-(void)managemyprofileDetails{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString * myname = [userDefaults objectForKey:@"userName"];
    NSString * myid = [userDefaults objectForKey:@"userLoginId"];

    self.UserNameTL.text = myname;
    self.UserIDTL.text = [NSString stringWithFormat:@"ID : %@",myid];
    
}
- (IBAction)btnLogoutClicks:(id)sender {
    
    [self AnimateTillFinalizeView:YES];
}
#pragma mark -
#pragma mark AddCustomer
- (IBAction)btnAddcustomerClicks:(id)sender {
    [self.addcustomer AnimatetableviewOpen:YES];
    isAddCustomerOpen = YES;
}

-(void)didAnimationOpenAddcustomer:(BOOL)isOpenAddcustomer
{
    if (isOpenAddcustomer) {
        self.blureview.hidden = NO;
        isAddCustomerOpen = NO;
    }else{
        self.blureview.hidden = YES;
    }
}
-(void)didSelectCustomer:(BOOL)isSelected{
    
    if (isSelected) {
        [self.btnAddCustomer setBackgroundImage:[UIImage imageNamed:@"icon_add_customer~ipad.png"] forState:UIControlStateNormal];
    }else{
        [self.btnAddCustomer setBackgroundImage:[UIImage imageNamed:@"btn_add_customer~ipad.png"] forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark DeliveryDetails

- (IBAction)btndeliveryDetails:(id)sender {
    
//    self.deliverydetails.isFromhistory = NO;
    [self.deliverydetails AnimatetableviewOpen:YES];
    
}
-(void)didAnimationOpenDeliveryDetails:(BOOL)isOpenAddcustomer
{
    if (isOpenAddcustomer) {
        self.blureview.hidden = NO;
    }else{
        self.blureview.hidden = YES;
    }
}
-(void)didSelectDeleveryDetails:(BOOL)isSelected{
    
    if (isSelected) {
        [self.btnDeliveryDetails setBackgroundImage:[UIImage imageNamed:@"icon_delivery~ipad.png"] forState:UIControlStateNormal];
    }else{
        [self.btnDeliveryDetails setBackgroundImage:[UIImage imageNamed:@"btn_delivery@2x~ipad.png"] forState:UIControlStateNormal];
    }
    
    //SetOrderType
    
    if ([[AppUserDefaults getDeliveryType]isEqualToString:COLLECTIONORDER]) {
        self.titleTL.text = @"Takeaway Order";
    }else if([[AppUserDefaults getDeliveryType]isEqualToString:TABLEORDER]){
//        self.titleTL.text = @"Delivery Order";
    }else{
        self.titleTL.text = @"Delivery Order";
    }
}
-(void)didUpdateDeliverDetails:(BOOL)updated{
    [self HistorySyncProcess];
}
#pragma mark -
#pragma mark profileSettings
- (IBAction)SettingsClicks:(id)sender {
    
    static NSString *tabViewControllerIdentifier = @"SetupViewController";
    SettingsViewController * mainView = [self.storyboard instantiateViewControllerWithIdentifier:tabViewControllerIdentifier];
    [self presentViewController:mainView animated:YES completion:nil];
}
#pragma mark -
#pragma mark ExtraItems
-(void)didSelectExtraItems:(BOOL)isSelected{
    [self loadCart];
}
-(void)didChangeItemQty:(SelectOrder *)order :(int)Qty{
    NSString * Quantity = [NSString stringWithFormat:@"%i",Qty];
    [self addItemsToCartMoreQty:order :Quantity];
}
#pragma mark -
#pragma mark GiftVoucher

- (IBAction)btnGiftVoucherClicks:(id)sender {
    
    if (isGiftVoucherViewOpen) {
        self.blureview.hidden = YES;
        [self.GiftVoucherView AnimateviewOpen:NO];
        isGiftVoucherViewOpen = NO;
    }else{
        self.blureview.hidden = NO;
        [self.GiftVoucherView AnimateviewOpen:YES];
         isGiftVoucherViewOpen = YES;
    }
}
-(void)didAnimationOpenGiftView:(BOOL)isOpen{
    self.blureview.hidden = YES;
    isGiftVoucherViewOpen = NO;
}
#pragma mark -
#pragma mark OrderDetails

- (IBAction)btnOrderIDClicks:(id)sender {
    
    int orderid = self.orderID.intValue;
    
    if (orderid > 0) {
        self.orderdetailsview.orderID = self.orderID;
        [self.orderdetailsview AnimateviewOpen:YES];
        self.orderdetailsview.delegate = self;
        self.blureview.hidden = NO;
    }
}

#pragma mark -
#pragma mark Add Item By ItemID

- (IBAction)btnAddItemClicks:(id)sender {
    
    self.blureview.hidden = NO;
    self.addItemById.delegate = self;
    [self.addItemById AnimateviewOpen:YES];
}


-(void)didAnimationOpenOrderDetails:(BOOL)isOpen{
    
        self.blureview.hidden = YES;
//        isGiftVoucherViewOpen = NO;
}
-(void)didAnimationclose{
    self.blureview.hidden = YES;
}
-(void)didSelectItemByItemID{
    [self loadCart];
}

#pragma mark -
#pragma mark Tillmangment

-(void)AnimateTillFinalizeView :(BOOL)isUp{
    
    if (isUp) {
        self.TillFinalizeView.hidden = NO;
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.TillFinalizeView.frame;
                             frame.origin.x = 601;
                             [self.TillFinalizeView setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                            
                         }
         ];
    }else{
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.TillFinalizeView.frame;
                             frame.origin.x = 1030;
                             [self.TillFinalizeView setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             self.TillFinalizeView.hidden = YES;
                         }
         ];
    }
    
}

- (IBAction)btnTillDoneClicks:(id)sender {
    
    [self AnimateTillFinalizeView:NO];
    
    NSString * tillAmount = self.tillAmtTF.text;
    
    if ([tillAmount isEqualToString:@""])tillAmount = @"0";
    
    
    [self inithud:@"Finalizing.."];

    NSString * sessionId = [AppUserDefaults getSessionTillId];
    
    NSString * userId = [AppUserDefaults getUserID];

    NSDictionary * params = @{@"key"           : WEBSERVICEKEY,
                              @"sessionId"     : sessionId,
                              @"status"        : ENDTILLSESSION,
                              @"final_amount"  : tillAmount,
                              @"userId"        : userId};
    
     NSLog(@"prames %@",params);
//
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    [webservice finalizeTillSession:params];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.tillAmtTF)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
    }
    
    return YES;
}
-(void)DidReceiveFinalizeData:(id)respond{
    
    NSLog(@"Respond %@",respond);
    
    NSString * Status = [respond objectForKey:@"Status"];
    [self hudWasHidden];
    if ([Status isEqualToString:@"Success"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oop" message:[respond objectForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (IBAction)btnCancelClicks:(id)sender {
     [self AnimateTillFinalizeView:NO];
}

#pragma mark -
#pragma mark CompanyDetails


- (IBAction)btnCompanyClicks:(id)sender{
    
    [self.comapanydetails AnimateviewOpen:YES];
    self.comapanydetails.delegate = self;
    
}
-(void)didSelectCompanyDetails:(Companies *)company{
    
    NSLog(@"CompanyDetails %@",company.cName);
}

#pragma mark -
#pragma mark HudMethods

-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
	[HUD removeFromSuperview];
    [HUD hide:YES];
	HUD = nil;
}








@end
