//
//  itemDetailsView.h
//  Cashier
//
//  Created by User on 06/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebserviceApi.h"
#import "DBManager.h"
#import "SelectOrderDAO.h"
#import "MBProgressHUD.h"
#import "SelectOrder.h"


@protocol ItemDetailsViewDelegate <NSObject>

-(void)didSelectExtraItemView:(BOOL)isSelected;
-(void)didSelectExtraItems:(BOOL)isSelected;
-(void)didChangeItemQty :(SelectOrder *)order :(int)Qty;

@end

@interface itemDetailsView : UIView<webservicedelegate,MBProgressHUDDelegate,UITextFieldDelegate,UITextViewDelegate>{
    
    WebserviceApi * webservice;
    DBManager * dbmanager;
    SelectOrderDAO * selectedOrder;
    MBProgressHUD * HUD;
    SelectOrder * currectorder;
    
    
    
    NSMutableArray * ExtraCategory;
    NSMutableArray * ExtraCategoryName;
    NSMutableArray * ExtraCategoryitems;
    BOOL isMultipleSelection;
    BOOL isExtrashave;
    BOOL isActiveMoveUP;
    BOOL isExtraExsis;
    NSMutableArray * MultipleSelectionArray;
    NSDictionary * SingleSelectionDictionary;
    
    NSDictionary * ExtracategoryInfo;
    NSDictionary * Iteminfo;
    
    NSString * RecordID;
    
    
    
}
@property ( nonatomic, retain ) id <ItemDetailsViewDelegate,UITextFieldDelegate> delegate;

@property(nonatomic,retain)NSString * itemID;

@property(weak,nonatomic)IBOutlet UIScrollView * extraCategotyScroller;
@property(weak,nonatomic)IBOutlet UITableView * extraCategotyTable;
@property(weak,nonatomic)IBOutlet UIImageView * ItemImage;
@property(weak,nonatomic)IBOutlet UITextView * ItemdetailsTV;
@property(weak,nonatomic)IBOutlet UITextField * QuantityTF;
@property(weak,nonatomic)IBOutlet UILabel * ItemNameTL;
@property(weak,nonatomic)IBOutlet UITextField * NoteTF;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCtrl1;
@property (weak, nonatomic) IBOutlet UIButton *btnCtrl2;
@property (weak, nonatomic) IBOutlet UIButton *btnCtrl3;

-(void)AnimateOpen:(BOOL)isUp;

- (IBAction)btnCancelClicks:(id)sender;
@end
