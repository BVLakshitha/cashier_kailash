//
//  itemDetailsView.m
//  Cashier
//
//  Created by User on 06/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import "itemDetailsView.h"
#import "TableViewCell.h"
#import "SelectOrder.h"
#import "SelectOrderDAO.h"
#import "AFImageRequestOperation.h"
#import "ExtraItems.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"


@implementation itemDetailsView
@synthesize itemID = _itemID;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
-(void)awakeFromNib{
    
    self.btnCancel.layer.masksToBounds = YES;
    self.btnCancel.layer.cornerRadius = 4;
    
    self.btnSave.layer.masksToBounds = YES;
    self.btnSave.layer.cornerRadius = 4;
    
    self.btnCtrl1.layer.masksToBounds = YES;
    self.btnCtrl1.layer.cornerRadius = 4;
    
    self.btnCtrl2.layer.masksToBounds = YES;
    self.btnCtrl2.layer.cornerRadius = 4;
    
    self.btnCtrl3.layer.masksToBounds = YES;
    self.btnCtrl3.layer.cornerRadius = 4;
    

    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}
#pragma mark -
#pragma mark BtnActions

- (IBAction)btnCancelClicks:(id)sender {
    [self AnimateOpen:NO];
    
    if (self.NoteTF.text.length > 0) {
         if (!selectedOrder)selectedOrder = [[SelectOrderDAO alloc]init];
        [selectedOrder insertOrupdatenoteRID:self.NoteTF.text :RecordID];
        self.NoteTF.text = @"";
    }
    
    
}

-(void)AnimateOpen:(BOOL)isUp{ //Open and Close View
    
    if (isUp) {
        [self loadItemDetails];
        self.hidden = NO;
        [self.delegate didSelectExtraItemView:YES];
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(157,109, 709, 487);
                         }
                         completion:^(BOOL finished){
                             [self addGuesture:YES];
                             isExtraExsis = NO;
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(157,770, 709, 487);
                             
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                             [self.delegate didSelectExtraItemView:NO];
                              [self addGuesture:NO];
                         }
         ];
    }
}
#pragma mark -
#pragma mark ItemDetails

-(void)loadItemDetails{
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!selectedOrder)selectedOrder = [[SelectOrderDAO alloc]init];
    
    SelectOrder * orderDetails = [selectedOrder getSelectFoodCategoryByRecordID:self.itemID];
    NSString * itemid = orderDetails.categoryId;
    RecordID = orderDetails.recodId;
    [self.ItemNameTL setText:orderDetails.orderName];
    [self.NoteTF setText:orderDetails.ordernote];
    [self.QuantityTF setText:orderDetails.orderrQuantity.stringValue];
    currectorder = orderDetails;
    [self.QuantityTF addTarget:self
                        action:@selector(textFieldDidChange:)
              forControlEvents:UIControlEventEditingChanged];
    
    //SetItem Details
    [self fatchfromurl:orderDetails.img_URL];
    [self.ItemdetailsTV setText:orderDetails.orderdescription];
    
    if (orderDetails.orderrQuantity.intValue <= 1)[self.QuantityTF setText:@"1"];
    
    [self callExtraItems:itemid];
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    NSLog( @"text changed: %@", theTextField.text);
    
    if (theTextField.text.intValue > 0) {
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        [dbmanager UpdateQuantity:currectorder :theTextField.text.intValue];
        [dbmanager updateExtraQty:RecordID :theTextField.text];
        [self.delegate didChangeItemQty:currectorder :theTextField.text.intValue];
    }

}
-(void)callExtraItems :(NSString *)itemDeital{

    [self inithud:@"Loading"];
    
        Iteminfo = @{@"item_id" : itemDeital,
                    @"RecordID" : self.itemID,
                    @"ItemDisID" : currectorder.recodId,
                     @"key" : WEBSERVICEKEY};
    
    NSLog(@"ItemDic_record_id %@",Iteminfo);
    
    if (!webservice)webservice = [[WebserviceApi alloc]init];
    webservice.delegate = self;
    [webservice GetExtraItemsbyId:Iteminfo];
}
-(void)fatchfromurl :(NSString *)imgURL{
//    NSLog(@"Image %@",imgURL);
    
    if ([imgURL isEqualToString:@""]) {
        [self.ItemImage setImage:[UIImage imageNamed:@"placeholder.png"]];
    }else{
        AFImageRequestOperation* operation = [AFImageRequestOperation imageRequestOperationWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imgURL]] imageProcessingBlock:nil
                                                                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
                                              {
                                                  [self.ItemImage setImage:image];
                                                  
                                              }
                                                                                               failure:nil];
        
        [operation start];
    }
}
-(void)DidReceiveExtraItems:(id)respond{
    
    NSLog(@"Respond %@",respond);
    NSDictionary * dataDic = [respond objectForKey:@"data"];
    NSArray * addonsArray = [dataDic objectForKey:@"addons"];
    
    if (!ExtraCategory)ExtraCategory = [[NSMutableArray alloc]init];
    [ExtraCategory removeAllObjects];
    
    if ([addonsArray count]>0 && [addonsArray isKindOfClass:[NSArray class]]) {
        [ExtraCategory addObjectsFromArray:addonsArray];
        isExtrashave = YES;
    }else{
        isExtrashave = NO;
        NSLog(@"No extraitem Found");
    }
    [self LoadextraCatogoryName:ExtraCategory];
}
-(void)LoadextraCatogoryName:(NSArray *)extraArray{
    
    if (!ExtraCategoryName)ExtraCategoryName = [[NSMutableArray alloc]init];
    [ExtraCategoryName removeAllObjects];
    
    if (isExtrashave) {
        
        for (NSDictionary * extras in extraArray) {
            [ExtraCategoryName addObject:[extras objectForKey:@"name"]];
        }
        [self loadextraSubItems:0];
    }else{
        [ExtraCategoryName addObject:@"No Extras."];
        [self loadScroller:0];
        [ExtraCategoryitems removeAllObjects];
        [self.extraCategotyTable reloadData];
        [self hudWasHidden];
    }
 //Start Load
}
-(void)loadextraSubItems:(int)CatID{
    NSDictionary * ExtraDictionary = [ExtraCategory objectAtIndex:CatID];
    
    if (!ExtraCategoryitems)ExtraCategoryitems = [[NSMutableArray alloc]init];
    [ExtraCategoryitems removeAllObjects];
    
    if (ExtraDictionary != nil) {
        [ExtraCategoryitems addObjectsFromArray:[ExtraDictionary objectForKey:@"child"]];
        
        ExtracategoryInfo = @{@"catType" : [ExtraDictionary objectForKey:@"type"],
                              @"catID" : [ExtraDictionary objectForKey:@"id"],
                              @"catPrice" : [ExtraDictionary objectForKey:@"additional_price"],
                              @"catName" : [ExtraDictionary objectForKey:@"name"]};
        
       int catType = [[ExtraDictionary objectForKey:@"type"] intValue];
        if (catType == 2) {
            isMultipleSelection = NO;
        }else{
            isMultipleSelection = YES;
        }
        //additionprice
    }
    
    [self.extraCategotyTable reloadData];
    [self loadScroller:CatID];
    [self hudWasHidden];
}
#pragma mark -
#pragma mark extraScroller

-(void)loadScroller:(int)selectedtag{
    
    NSMutableArray * namearray = ExtraCategoryName;
    [self.extraCategotyScroller setScrollEnabled:YES];
    
    int totallenght = 0;
    [self.extraCategotyScroller.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int btntag = 0;
    for (NSString * category in namearray) {
        
        int stringlengh = (int)category.length;
        int panellength = 100;
        int extraperlength = 10;
        
        if (stringlengh >= 5) {
            int extrachars = stringlengh - 5;
            panellength = 100 + extraperlength *extrachars;
        }
        
        UIView * CategoryView = [[UIView alloc]initWithFrame:CGRectMake(totallenght,0, panellength+10, 50)];
        totallenght = totallenght +panellength;
        
        UIButton * touchbtn = [[UIButton alloc]initWithFrame:CGRectMake(2, 0, panellength, 50)];
        touchbtn.backgroundColor = [UIColor clearColor];

        if (btntag == selectedtag) {
            [touchbtn setBackgroundImage:[UIImage imageNamed:@"tab_add_extra_slected~ipad.png"]
                                forState:UIControlStateNormal];
            [touchbtn setTitleColor:[UIColor colorWithRed:209.0/255.0 green:40.0/255.0 blue:41.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        }else{
            [touchbtn setBackgroundImage:[UIImage imageNamed:@"tab_add_extra~ipad.png"]
                                forState:UIControlStateNormal];
            [touchbtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        [touchbtn setTag:btntag];
        [touchbtn setTitle:category forState:UIControlStateNormal];
        [touchbtn addTarget:self action:@selector(touchbtnClicks:) forControlEvents:UIControlEventTouchUpInside];
         btntag++;
        [CategoryView addSubview:touchbtn];

        CategoryView.backgroundColor = [UIColor clearColor];
        [self.extraCategotyScroller addSubview:CategoryView];
    }
    [self.extraCategotyScroller setContentSize:CGSizeMake(totallenght, 50)];
}
-(void)touchbtnClicks:(UIButton *)btn{
    if (isExtrashave)[self loadextraSubItems:(int)btn.tag];
}

#pragma mark -
#pragma mark extratableview

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ExtraCategoryitems count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *simpleTableIdentifier = @"TableViewCell";
    TableViewCell *cell = (TableViewCell *)[self.extraCategotyTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
    }
    NSDictionary * itemDic = [ExtraCategoryitems objectAtIndex:indexPath.row];
    int Exitemid =[[itemDic objectForKey:@"cid"] intValue];
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    ExtraItems * selectedEx = [dbmanager isRecordExistanceExtraItemID:Exitemid :self.itemID.intValue];
    
    if (selectedEx) {
        isExtraExsis = YES;
        cell.checkimg.image = [UIImage imageNamed:@"checkbox_on~ipad.png"];
    }else{
        cell.checkimg.image = [UIImage imageNamed:@"checkbox_off~ipad.png"];
    }
    
    cell.nameTL.text = [itemDic objectForKey:@"cname"];
    cell.priceTL.text = [NSString stringWithFormat:@"%@ %@",CURRENCYSYMBOL,[itemDic objectForKey:@"cadditional_price"]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary * selectedItem = [ExtraCategoryitems objectAtIndex:indexPath.row];
    NSString * itemQty = self.QuantityTF.text;
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    //CategoryDetails
    
    NSMutableDictionary * ExtraDictionary = [[NSMutableDictionary alloc]initWithDictionary:selectedItem];
    [ExtraDictionary setObject:itemQty forKey:@"ExtraQty"];
    
    if (isMultipleSelection) {
        [dbmanager insertMultipleSelection:ExtracategoryInfo :ExtraDictionary :Iteminfo];
    }else{
        [dbmanager insertExtraSingleSelection:ExtracategoryInfo :ExtraDictionary :Iteminfo];
    }
    [self.extraCategotyTable reloadData];
    [self.delegate didSelectExtraItems:YES];
    
}

#pragma mark -
#pragma mark TextFieldDelegate

//-(void)textViewDidBeginEditing:(UITextView *)textView{
//     [self AnimateUP:YES];
//
//}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == self.QuantityTF) {
//        isActiveMoveUP = NO;
    }else{
         [self AnimateUP:YES];
//        isActiveMoveUP = YES;
    }
    
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
   [self AnimateUP:NO];
    [self.NoteTF resignFirstResponder];
    return true;
}
-(void)AnimateUP:(BOOL)isUp{ //Open and Close View
    
    if (isUp) {
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(157,-120, 709, 487);
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             self.frame = CGRectMake(157,109, 709, 487);
                             
                         }
                         completion:^(BOOL finished){

                         }
         ];
    }
}
-(void)addGuesture :(BOOL)iSadd{
    
    if (iSadd) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        
    }else{
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillShowNotification
                                                      object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:UIKeyboardWillHideNotification
                                                      object:nil];
    }
    
}
-(void)keyboardWillShow {
   
}
-(void)keyboardWillHide {
    [self AnimateUP:NO];
}

#pragma mark -
#pragma mark HudMethods

-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self];
    [self addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}

@end
