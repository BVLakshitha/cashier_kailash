//
//  orderStatusView.h
//  Cashier
//
//  Created by Lakshitha on 7/11/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface orderStatusView : UIView

- (IBAction)btnDoneClicks:(id)sender;
-(void)AnimateviewOpen:(BOOL)isUp;

@end
