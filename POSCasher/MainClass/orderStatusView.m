//
//  orderStatusView.m
//  Cashier
//
//  Created by Lakshitha on 7/11/15.
//  Copyright (c) 2015 Lakshitha. All rights reserved.
//

#import "orderStatusView.h"

@implementation orderStatusView

-(void)awakeFromNib{
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
}

-(void)AnimateviewOpen:(BOOL)isUp{
    
    if (isUp) {
        
//        [self dataLoader];
        
        self.hidden = NO;
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             CGRect frame = self.frame;
                             frame.origin.y = 75;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             
                         }
         ];
    }else{
        [UIView animateWithDuration:0.2
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             CGRect frame = self.frame;
                             frame.origin.y = 768;
                             [self setFrame:frame];
                             
                         }
                         completion:^(BOOL finished){
                             self.hidden = YES;
                         }
         ];
    }
}


- (IBAction)btnDoneClicks:(id)sender{
    [self AnimateviewOpen:NO];
}
@end
