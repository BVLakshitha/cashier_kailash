//
//  MasterViewController.m
//  DetailViewSwitch
//
//  Created by Tim Harris on 1/17/14.
//  Copyright (c) 2014 Tim Harris. All rights reserved.
//

#import "MasterViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ViewController.h"

@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.splitViewController setDelegate:self];
    
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);

}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UIStoryboard *storyboard = [self storyboard];
        DetailViewController *newController = nil;
        
        switch ([indexPath row]) {
            case 0:
                newController = [storyboard instantiateViewControllerWithIdentifier:@"firstviewcontroller"];
                break;
            case 1:
                newController = [storyboard instantiateViewControllerWithIdentifier:@"secondviewcontroller"];
                break;
        }
        
        // now set this to the navigation controller
        UINavigationController *navController = [[[self splitViewController ] viewControllers ] lastObject ];
//        DetailViewController *oldController = [[navController viewControllers] firstObject];
        
        NSArray *newStack = [NSArray arrayWithObjects:newController, nil ];
        [navController setViewControllers:newStack];
        
    }
}
- (IBAction)btnDoneClicks:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
