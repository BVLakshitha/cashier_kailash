//
//  PrinterDetailsVC.h
//  Cashier
//
//  Created by User on 25/11/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StarIO/SMPort.h>
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"

@interface PrinterDetailsVC : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>{
    
    NSMutableArray * modelsArray;
    NSMutableArray * PrintersArray;
    
    NSMutableArray * masterDetails;
    int modelnumber;
    PortInfo * port;
    NSString * selectedString;
    MBProgressHUD * HUD;
    
}
@property(nonatomic,retain)NSString * model;
- (IBAction)refreshPrinterView:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *TBLPrinterDetail;

@end
