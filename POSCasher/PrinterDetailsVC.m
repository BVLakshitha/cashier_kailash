//
//  PrinterDetailsVC.m
//  Cashier
//
//  Created by User on 25/11/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import "PrinterDetailsVC.h"
#import "Constants.h"
#import <StarIO/SMPort.h>
#import <QuartzCore/QuartzCore.h>
#import "PrinterFunctions.h"

@interface PrinterDetailsVC ()

@end

@implementation PrinterDetailsVC
@synthesize model = _model;


- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self inithud:@"Searching..."];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self setValesFortableLoader];
        [self hudWasHidden];
    });
    
}
-(void)setValesFortableLoader{//Manage Printer Modes 
    
    modelsArray = [[NSMutableArray alloc]initWithObjects:@"Line Mode",@"Raster Mode", nil];
    
    if (!masterDetails)masterDetails = [[NSMutableArray alloc]init];
    [masterDetails removeAllObjects];
    
    modelnumber = self.model.intValue;
    NSDictionary * printer = nil;
    
    switch (modelnumber) {
        case 1:
            self.title = @"Printer Mode";
             [self hideAndDisableRightNavigationItem];
            [masterDetails addObjectsFromArray:modelsArray];
            selectedString = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_C];
            break;
            
        case 2:
            self.title = @"Printer Mode";
             [self hideAndDisableRightNavigationItem];
            [masterDetails addObjectsFromArray:modelsArray];
            selectedString = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_K];
            break;
            
        case 3:
            self.title = @"Online Printers";
            
            printer = [[NSUserDefaults standardUserDefaults] objectForKey:CASHERPRINTER];
            selectedString = [printer objectForKey:@"macAddress"];
            
            [masterDetails addObjectsFromArray:[self printerDelails]];
            break;
            
        case 4:
            self.title = @"Online Printers";
            
            printer = [[NSUserDefaults standardUserDefaults] objectForKey:KITCHENPRINTER];
            selectedString = [printer objectForKey:@"macAddress"];
            
            [masterDetails addObjectsFromArray:[self printerDelails]];
            break;
            
        default:
            break;
    }
     [self.TBLPrinterDetail reloadData];
}
#pragma mark NavigationBTNControl
#pragma mark -
- (IBAction)refreshPrinterView:(id)sender {
    
    [self inithud:@"Searching..."];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self setValesFortableLoader];
        [self hudWasHidden];
    });
}
-(void) hideAndDisableRightNavigationItem
{
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor clearColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
}

-(void) showAndEnableRightNavigationItem
{
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor blackColor]];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
}
#pragma mark GetPrinterdetals
#pragma mark -
-(NSArray *)printerDelails{
    NSArray *array = [SMPort searchPrinter];
    return array;
}

#pragma mark UITableView
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [masterDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSString * currentString = @"";
    
    // Configure the cell...

    switch (modelnumber) {
        case 1:
            currentString = [masterDetails objectAtIndex:indexPath.row];
            cell.textLabel.text = currentString;
            
            if ([selectedString isEqualToString:currentString]){
               cell.imageView.image = [UIImage imageNamed:@"tickmark.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@""];
            }
            break;
            
        case 2:
            currentString = [masterDetails objectAtIndex:indexPath.row];
            cell.textLabel.text = currentString;
            
            if ([selectedString isEqualToString:currentString]){
                cell.imageView.image = [UIImage imageNamed:@"tickmark.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@""];
            }
            break;
            
        case 3:
            
            port = [masterDetails objectAtIndex:indexPath.row];
            
            cell.textLabel.text = port.modelName;
            cell.detailTextLabel.text = port.portName;
            
            if ([selectedString isEqualToString:port.macAddress]){
                cell.imageView.image = [UIImage imageNamed:@"tickmark.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@""];
            }

            break;
            
        case 4:
            
            port = [masterDetails objectAtIndex:indexPath.row];
            
            cell.textLabel.text = port.modelName;
            cell.detailTextLabel.text = port.portName;
            
            if ([selectedString isEqualToString:port.macAddress]){
                cell.imageView.image = [UIImage imageNamed:@"tickmark.png"];
            }else{
                cell.imageView.image = [UIImage imageNamed:@""];
            }
            
            break;
            
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSUserDefaults * detailbinder = [NSUserDefaults standardUserDefaults];
    NSDictionary * printer =nil;
    
    switch (modelnumber) {
        case 1:
            
            selectedString = [masterDetails objectAtIndex:indexPath.row];
            [detailbinder setObject:selectedString forKey:PRINTERMODE_C];
            [detailbinder synchronize];

            break;
            
        case 2:
            
            selectedString = [masterDetails objectAtIndex:indexPath.row];
            [detailbinder setObject:selectedString forKey:PRINTERMODE_K];
            [detailbinder synchronize];

            break;
            
        case 3:
            
            port = [masterDetails objectAtIndex:indexPath.row];
            selectedString = port.macAddress;
            printer  = @{@"macAddress" : port.macAddress,
                         @"portName"   : port.portName,
                         @"modelName"  : port.modelName};
            
            [detailbinder setObject:printer forKey:CASHERPRINTER];
            [detailbinder synchronize];

            break;
            
        case 4:
            
            port = [masterDetails objectAtIndex:indexPath.row];
            selectedString = port.macAddress;
            
            printer  = @{@"macAddress" : port.macAddress,
                         @"portName"   : port.portName,
                         @"modelName"  : port.modelName};
            
            [detailbinder setObject:printer forKey:KITCHENPRINTER];
            [detailbinder synchronize];
            
            break;
            
        default:
            break;
    }
    [self.TBLPrinterDetail reloadData];
}
#pragma mark SamplePrint
#pragma mark -

-(void)CashierPrinterSamplePrint{
    
   NSDictionary * printer = [[NSUserDefaults standardUserDefaults] objectForKey:CASHERPRINTER];
    NSString * p_portName = [printer objectForKey:@"portName"];
    NSString * p_portSettings = @"Standard";
    
    [PrinterFunctions PrintRasterSampleReceipt3InchWithPortname:p_portName portSettings:p_portSettings];
}
-(void)KashierPrinterSamplePrint{
    
    
}
#pragma mark -
#pragma mark HudMethods

-(void)inithud :(NSString *)message{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.labelText = message;
    HUD.delegate = self;
    [HUD show:YES];
    
}
- (void)hudWasHidden{
    [HUD removeFromSuperview];
    [HUD hide:YES];
    HUD = nil;
}
@end
