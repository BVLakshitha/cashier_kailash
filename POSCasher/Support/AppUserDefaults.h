//
//  AppUserDefaults.h
//  RsturantApp
//
//  Created by Ganidu Ashen on 2/22/13.
//  Copyright (c) 2013 JBMdigital(pvt)Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUserDefaults : NSObject

+ ( void ) setResturantDetails:(NSDictionary *) Record;
+ ( NSDictionary *) getResturantDetails;

+ ( NSString *) getResturentName;
+ ( NSString *) getResturentAddress;
+ ( NSString *) getResturentEmail;
+ ( NSString *) getResturentWeb;
+ ( NSString *) getResturentTell;

+ ( void ) setUserID:(NSString *) Record;
+ ( NSString *) getUserID;

+ ( void ) setPaymentdetails:(NSDictionary *) Record;
+ ( NSDictionary *) getPaymentdetails;
+( void )removeOrderDetails;

+ ( void ) setCardDetails:(NSArray *) Record;
+ ( NSArray *) getCardDetails;

+ ( void ) setDiscoutn:(NSArray *) Record;
+ ( NSArray *) getDiscoutn;

+ ( void ) setDeliveryType:(NSString *) Record;
+ ( NSString *) getDeliveryType;

+ ( void ) setSplitDetails:(NSArray *) Record;
+ ( NSArray *) getSplitDetails;

+ ( void ) setHistryDateTime:(NSString *) Record;
+ ( NSString *) getHistryDateTime;

+ ( void ) setPrinterStatus:(NSString *) Status :(NSString *)PrinterType;
+ ( NSString *) getPrinterStatus :(NSString *)PrinterType;

+ ( void )setKOTPrintinCashierStatus :(NSString *)Status;
+ ( NSString *)getKOTPrintinCashier;

+ ( void ) setCashDrawerStatus:(NSString *) Status;
+ ( NSString *) getCashDrawerStatus;

+ ( void ) setVoucherConstants:(NSArray *) vouchers;
+ ( NSArray *) getVoucherConstants;

+ ( void ) setCurrentpayment:(NSString *) params;
+ ( NSString *) getCurrentpayment;

+ ( void ) setMasterTillId:(NSString *) params;
+ ( NSString *) getMasterTillId;

+ ( void ) setSessionTillId:(NSString *) params;
+ ( NSString *) getSessionTillId;

+ ( void ) setEnableSplitOrderbtn:(NSString *) params;
+ ( NSString *) getEnableSplitOrderbtn;

//+(void)setLatSyncTime:(NSString *)DateTime;
//+(NSString *)getLatSyncTime;
@end
