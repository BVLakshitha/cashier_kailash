//
//  AppUserDefaults.m
//  RsturantApp
//
//  Created by Ganidu Ashen on 2/22/13.
//  Copyright (c) 2013 JBMdigital(pvt)Ltd. All rights reserved.
//

#import "AppUserDefaults.h"
#import "Constants.h"
#define USER_DEFAULTS [NSUserDefaults standardUserDefaults]

@implementation AppUserDefaults

+ ( void ) setResturantDetails:(NSDictionary *) Record
{
    [USER_DEFAULTS removeObjectForKey:@"RESTURENT_DATA"];
    [USER_DEFAULTS synchronize];
    
    [USER_DEFAULTS setObject:Record forKey:@"RESTURENT_DATA"];
    [USER_DEFAULTS synchronize];
}
+ ( NSDictionary *) getResturantDetails
{
     return [USER_DEFAULTS objectForKey:@"RESTURENT_DATA"];
}

+ ( NSString *) getResturentName{
    
    NSString * ResturentName = [[USER_DEFAULTS objectForKey:@"RESTURENT_DATA"] objectForKey:@"restuarentName"];
    
    return ResturentName;
}

+ ( NSString *) getResturentAddress{
    return [[USER_DEFAULTS objectForKey:@"RESTURENT_DATA"] objectForKey:@"restuarentAddress"];
}

+ ( NSString *) getResturentEmail{
    return [[USER_DEFAULTS objectForKey:@"RESTURENT_DATA"] objectForKey:@"restuarentEmail"];
}

+ ( NSString *) getResturentWeb{
    return [[USER_DEFAULTS objectForKey:@"RESTURENT_DATA"] objectForKey:@"restuarentWebSite"];
}

+ ( NSString *) getResturentTell{
    return [[USER_DEFAULTS objectForKey:@"RESTURENT_DATA"] objectForKey:@"restuarentTell"];
}

+ ( void ) setUserID:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"USERID"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getUserID
{
    return [USER_DEFAULTS objectForKey:@"USERID"];
}

+ ( void ) setPaymentdetails:(NSDictionary *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"PAYMENT_DATA"];
    [USER_DEFAULTS synchronize];
}
+ ( NSDictionary *) getPaymentdetails
{
    return [USER_DEFAULTS objectForKey:@"PAYMENT_DATA"];
}

+ ( void ) setCardDetails:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"CARDDETAILS"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getCardDetails
{
    return [USER_DEFAULTS objectForKey:@"CARDDETAILS"];
}
+ ( void ) setDiscoutn:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"ORDERDISCOUNT"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getDiscoutn
{
    return [USER_DEFAULTS objectForKey:@"ORDERDISCOUNT"];
}
+( void )removeOrderDetails{
    
    [USER_DEFAULTS removeObjectForKey:@"PAYMENT_DATA"];
    [USER_DEFAULTS synchronize];
}

+ ( void ) setDeliveryType:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"DELIVERYTYPE"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getDeliveryType
{
    return [USER_DEFAULTS objectForKey:@"DELIVERYTYPE"];
}

+ ( void ) setSplitDetails:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"SPLITDATA"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getSplitDetails
{
    return [USER_DEFAULTS objectForKey:@"SPLITDATA"];
}
+ ( void ) setHistryDateTime:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"HLMD"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getHistryDateTime
{
    return [USER_DEFAULTS objectForKey:@"HLMD"];
}
+ ( void ) setPrinterStatus:(NSString *) Status :(NSString *)PrinterType
{
    if ([PrinterType isEqualToString:CASHERPRINTER]) {
        [USER_DEFAULTS setObject:Status forKey:@"CPSTATUS"];
    }else{
        [USER_DEFAULTS setObject:Status forKey:@"KPSTATUS"];
    }
    [USER_DEFAULTS synchronize];
}
+(void)setKOTPrintinCashierStatus :(NSString *)Status{
    
    [USER_DEFAULTS setObject:Status forKey:@"CKOTPSTATUS"];
    [USER_DEFAULTS synchronize];
}
+(NSString *)getKOTPrintinCashier{
    
    return [USER_DEFAULTS objectForKey:@"CKOTPSTATUS"];
}

+ ( NSString *) getPrinterStatus :(NSString *)PrinterType
{
    if ([PrinterType isEqualToString:CASHERPRINTER]) {
        
        return [USER_DEFAULTS objectForKey:@"CPSTATUS"];
    }else{
        return [USER_DEFAULTS objectForKey:@"KPSTATUS"];
    }
}
+ ( void ) setCashDrawerStatus:(NSString *) Status
{
    [USER_DEFAULTS setObject:Status forKey:@"DRAWERSTATUS"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getCashDrawerStatus
{
return [USER_DEFAULTS objectForKey:@"DRAWERSTATUS"];
}

+ ( void ) setVoucherConstants:(NSArray *) vouchers
{
    [USER_DEFAULTS setObject:vouchers forKey:@"VOUCHERS"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getVoucherConstants
{
    return [USER_DEFAULTS objectForKey:@"VOUCHERS"];
}

#pragma mark - Parmentstatus

+ ( void ) setCurrentpayment:(NSString *) params
{
    [USER_DEFAULTS setObject:params forKey:@"PAYMENTSTATUS"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getCurrentpayment
{
    return [USER_DEFAULTS objectForKey:@"PAYMENTSTATUS"];
}

#pragma mark - Till

+ ( void ) setMasterTillId:(NSString *) params
{
    [USER_DEFAULTS setObject:params forKey:@"MASTERTILLID"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getMasterTillId
{
    return [USER_DEFAULTS objectForKey:@"MASTERTILLID"];
}

+ ( void ) setSessionTillId:(NSString *) params
{
    [USER_DEFAULTS setObject:params forKey:@"SETTIONTILLID"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getSessionTillId
{
    return [USER_DEFAULTS objectForKey:@"SETTIONTILLID"];
}

+ ( void ) setEnableSplitOrderbtn:(NSString *) params
{
    [USER_DEFAULTS setObject:params forKey:@"ORDERPLITBTN"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getEnableSplitOrderbtn
{
    return [USER_DEFAULTS objectForKey:@"ORDERPLITBTN"];
}

@end
