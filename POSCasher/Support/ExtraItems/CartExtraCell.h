//
//  CartExtraCell.h
//  Cashier
//
//  Created by User on 11/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartExtraCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *extraNameTL;
@property (weak, nonatomic) IBOutlet UILabel *extraPriceTL;

@end
