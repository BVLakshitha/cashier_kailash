//
//  CVCell.h
//  CollectionViewExample
//
//  Created by Tim on 9/5/12.
//  Copyright (c) 2012 Charismatic Megafauna Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCell : UICollectionViewCell{
    
}

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *processimage;
@property (weak, nonatomic) IBOutlet UILabel *tablenameTL;
@property (weak, nonatomic) IBOutlet UILabel *waierid;
@property (strong, nonatomic) IBOutlet UILabel *statusTL;
@property (weak, nonatomic) IBOutlet UILabel *tableidTL;
@end
