//
//  FlowView.h
//  POSCasher
//
//  Created by orderfood on 23/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableReceived.h"

@protocol layoutdelegate <NSObject>

//-(void)didSelectTable :(NSString *)flowid :(NSString *)tableid :(NSString *)type;
//-(void)didSelectTable :(NSString *)flowid :(NSString *)tableid;
-(void)didSelectTable :(TableReceived *)tableinfo;

@end

#define loadView() \
NSBundle *mainBundle = [NSBundle mainBundle]; \
NSArray *views = [mainBundle loadNibNamed:NSStringFromClass([self class]) owner:self options:nil]; \
[self addSubview:views[0]];

@interface FlowView : UIView<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate>{
    
    NSMutableArray * tableData;
    
}

@property ( nonatomic, retain ) id <layoutdelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *TablesCollectionsView;

-(void)initCollectionviewWithObject :(NSArray *)sources;
@property (weak, nonatomic) IBOutlet UILabel *PlaceTF;

@end
