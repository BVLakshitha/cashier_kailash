//
//  FlowView.m
//  POSCasher
//
//  Created by orderfood on 23/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import "FlowView.h"
#import "CVCell.h"
#import "TableReceived.h"

@implementation FlowView
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
   
    //loading menu
    loadView()
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    loadView()
    
    
    return self;
}
#pragma mark -
#pragma Setinstrance Property

- (IBAction)tableoneClicks:(id)sender {
  
//        [self.delegate didSelectTable:[NSNumber numberWithInt:1] :[NSNumber numberWithInt:2]];
}
-(void)initCollectionviewWithObject :(NSArray *)sources{
    
    if (!tableData)tableData = [[NSMutableArray alloc]init];
    
    [tableData removeAllObjects];
    [tableData addObjectsFromArray:sources];
    
    [self.TablesCollectionsView registerClass:[CVCell class] forCellWithReuseIdentifier:@"cvCell"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(150, 150)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.TablesCollectionsView setCollectionViewLayout:flowLayout];
}

//collection view controllers.

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   
    int count = [tableData count];
    
    return count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"cvCell";
    
    CVCell *cell = (CVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    TableReceived *objTableReceived=[tableData objectAtIndex:indexPath.row];
    
//    NSLog(@"Oooderid %@",objTableReceived.orderid);
//    NSLog(@"Oooderid %@",objTableReceived.tableName);
    [cell.tablenameTL setText:objTableReceived.tableName];
    [cell.tableidTL setText:objTableReceived.tableId];
    
    [cell.statusTL setHidden:YES];
    if ([objTableReceived.tblstatus isEqualToString:@"4"]) {
//        [cell.statusTL setText:@"W"];
//        cell.statusTL.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:48.0/255.0 blue:237.0/255.0 alpha:1];
        cell.processimage.image=[UIImage imageNamed:@"icon_table_waiting~ipad.png"];
    }else if ([objTableReceived.tblstatus isEqualToString:@"13"]){
//        [cell.statusTL setText:objTableReceived.tblstatus];
//        [cell.statusTL setText:@"H"];
//        cell.statusTL.backgroundColor = [UIColor colorWithRed:8.0/255.0 green:67.0/255.0 blue:133.0/255.0 alpha:1];
        
        cell.processimage.image=[UIImage imageNamed:@"icon_table_hold~ipad.png"];
    }else if ([objTableReceived.tblstatus isEqualToString:@"11"]){
        
        cell.processimage.image=[UIImage imageNamed:@"icon_table_complete~ipad.png"];
//        [cell.statusTL setText:@"R"];
//        cell.statusTL.backgroundColor = [UIColor colorWithRed:7.0/255.0 green:174.0/255.0 blue:46.0/255.0 alpha:1];
    }else if ([objTableReceived.tblstatus isEqualToString:@"0"]){
        [cell.statusTL setHidden:YES];
        cell.processimage.image=[UIImage imageNamed:@"icon_table_free~ipad.png"];
        //        [cell.statusTL setText:@"W"];
        //        cell.statusTL.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:48.0/255.0 blue:237.0/255.0 alpha:1];
    }else{
//        [cell.statusTL setText:@"W"];
//        cell.statusTL.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:48.0/255.0 blue:237.0/255.0 alpha:1];
        cell.processimage.image=[UIImage imageNamed:@"icon_table_free~ipad.png"];
    }
    
//    if ([objTableReceived.tableStetus isEqualToString:@"0"])
//    {
//        cell.waierid.text = @"";
//        cell.processimage.image=[UIImage imageNamed:@"not-reserved.png"];
//    }else{
//        cell.waierid.text = objTableReceived.waiterid;
//        cell.processimage.image=[UIImage imageNamed:@"reserved.png"];
//    }

    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Clicks");
    TableReceived *objTableReceived=[tableData objectAtIndex:indexPath.row];
    
    [self.delegate didSelectTable:objTableReceived];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(40, 40, 0, 0);
}

#pragma mark Collection view layout things
// Layout: Set cell size
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //    NSLog(@"SETTING SIZE FOR ITEM AT INDEX %d", indexPath.row);
    CGSize mElementSize = CGSizeMake(150, 150);
    return mElementSize;
}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 20;
//}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 30;
}
-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 30;
}
//- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
//    
//    return 2;
//}
@end
