//
//  Layoutthree.h
//  POSCasher
//
//  Created by orderfood on 23/09/2014.
//  Copyright (c) 2014 orderfood. All rights reserved.
//

#import <UIKit/UIKit.h>



#define loadView() \
NSBundle *mainBundle = [NSBundle mainBundle]; \
NSArray *views = [mainBundle loadNibNamed:NSStringFromClass([self class]) owner:self options:nil]; \
[self addSubview:views[0]];

@interface Layoutthree : UIView


@end
