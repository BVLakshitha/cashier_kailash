//
//  Printer.h
//  Cashier
//
//  Created by Lakshitha on 29/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SelectOrderDAO.h"
#import "DBManager.h"
#import "SplitData.h"

@interface Printer : NSObject{
    
    SelectOrderDAO * carttbl;
    DBManager * dbmanager;
    NSString * Orderid;
    NSString * totalValues;
    
    UIImage * HeaderImage;
    UIImage * bodyImage;
    
    NSString * portname;
    NSString * PortSettings;
    NSString * deliveryType;
    
    BOOL isToKitchen;
    BOOL isPrint;
    
    
}

@property(nonatomic,retain)NSString * Subtotal;
@property(nonatomic,retain)NSString * Tax;
@property(nonatomic,retain)NSString * Servicecharge;
@property(nonatomic,retain)NSString * Discount;
@property(nonatomic,retain)NSString * Cash;
@property(nonatomic,retain)NSString * Card;
@property(nonatomic,retain)NSString * GiftVoucher;
@property(nonatomic,retain)NSString * Change;
@property(nonatomic,retain)NSString * Total;
@property(nonatomic,retain)NSString * TableName;
@property(nonatomic,retain)NSString * TableId;
@property(nonatomic,retain)NSString * SplitId;
@property(nonatomic,retain)NSString * PaymentStatus;
@property(nonatomic,retain)NSString * Customer;

-(void)initWithKOTPrint:(NSString *)orderid :(NSString *)pname :(NSString *)psettings :(NSString *)Ordertype; //KOT Print
-(void)initWithKOTPrintInCashierPrinter :(NSString *)orderid :(NSString *)pname :(NSString *)psettings :(NSString *)Ordertype; //KOT print in cashier App

-(void)initWithKBillPrint:(NSString *)orderid :(NSString *)pname :(NSString *)psettings :(BOOL)isCashDrowerOpen; // Bill Print

@end
