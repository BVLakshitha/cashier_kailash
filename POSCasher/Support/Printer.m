//
//  Printer.m
//  Cashier
//
//  Created by Lakshitha on 29/10/2014.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "Printer.h"
#import "MiniPrinterFunctions.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "ExtraItems.h"
#import "PrinterFunctions.h"
#import "AppUserDefaults.h"
#import "PrinterFeeds.h"
#import "Constants.h"
#import "SplitData.h"

@implementation Printer

#pragma mark-
#pragma mark InitPrinter

-(void)initWithKOTPrint:(NSString *)orderid :(NSString *)pname :(NSString *)psettings :(NSString *)Ordertype {
    
    [self setPortInfo];
    portname = pname;
    PortSettings = psettings;
    Orderid = orderid;
    deliveryType = Ordertype;
    NSMutableString * string = nil;
    
     NSString * printermode_C = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_C];
    
    if ([printermode_C isEqualToString:@"Raster Mode"]) {
        isToKitchen = YES;
        [self setRasterCashierBill:portname portSettings:PortSettings];
    }else{
        NSData *commands = [self KOTPrinterDetails];
        [MiniPrinterFunctions sendCommand:commands portName:portname portSettings:PortSettings timeoutMillis:10000 errorMessage:string];
    }
}
-(void)initWithKOTPrintInCashierPrinter :(NSString *)orderid :(NSString *)pname :(NSString *)psettings :(NSString *)Ordertype {
    
    [self setPortInfo];
    portname = pname;
    PortSettings = psettings;
    Orderid = orderid;
    deliveryType = Ordertype;
    NSMutableString * string = nil;
    
    NSString * printermode_K = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_K];
    
    if ([printermode_K isEqualToString:@"Raster Mode"]) {
        isToKitchen = NO;
        [self setRasterCashierBill:portname portSettings:PortSettings];
    }else{
        NSData *commands = [self KOTPrinterDetails];
        [MiniPrinterFunctions sendCommand:commands portName:portname portSettings:PortSettings timeoutMillis:10000 errorMessage:string];
    }
    
}

-(void)initWithKBillPrint:(NSString *)orderid :(NSString *)pname :(NSString *)psettings :(BOOL)isCashDrowerOpen{
    [self setPortInfo];
    NSString *portName = pname;
    NSString *portSettings = psettings;
    NSMutableString * string = nil;
    Orderid = orderid;
    
    NSString * printermode_K = [[NSUserDefaults standardUserDefaults] objectForKey:PRINTERMODE_K];
    
    if ([printermode_K isEqualToString:@"Raster Mode"]) {
        [self setRasterBillPrint:portName portSettings:portSettings];
    }else{
        NSData *commands = [self BillPrinterDetails];
        [MiniPrinterFunctions sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000 errorMessage:string];
    }
    if ([[AppUserDefaults getCashDrawerStatus]isEqualToString:@"YES"]) {
        if (isCashDrowerOpen){
         [PrinterFunctions OpenCashDrawerWithPortname:portName portSettings:portSettings drawerNumber:1];
         [PrinterFunctions OpenCashDrawerWithPortname:portName portSettings:portSettings drawerNumber:2];
        }
    }
}

#pragma mark-
#pragma mark Getters/Setters

+ (void)setPortName:(NSString *)m_portName
{
    [AppDelegate setPortName:m_portName];
}

+ (void)setPortSettings:(NSString *)m_portSettings
{
    [AppDelegate setPortSettings:m_portSettings];
}
- (void)setPortInfo
{
    
    NSString *localPortName = @"BT:PRNT Star";
    [Printer setPortName:localPortName];
    
    NSString *localPortSettings = @"mini";
    
    [Printer setPortSettings:localPortSettings];
}

#pragma mark-
#pragma mark KOT Details

-(NSData *)KOTPrinterDetails{
    NSMutableData *commands = [NSMutableData new];
    
    [commands appendBytes:"\x1d\x57\x40\x32"
                   length:sizeof("\x1d\x57\x40\x32") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 64, nH = 2)
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    [commands appendBytes:"\x1b\x45\x01"
                   length:sizeof("\x1b\x45\x01") - 1];    // Set Emphasized Printing ON
    
    [commands appendData:[@"Cashier-IOS \n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendBytes:"\x1b\x45\x00"
                   length:sizeof("\x1b\x45\x00") - 1];    // Set Emphasized Printing OFF (same command as on)
    
    [commands appendBytes:"\x1b\x61\x00"
                   length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    
    [commands appendBytes:"\x1b\x44\x02\x10\x22\x00"
                   length:sizeof("\x1b\x44\x02\x10\x22\x00") - 1];    // Setting Horizontal Tab
    
    [commands appendBytes:"\x09"
                   length:sizeof("\x09") - 1];    // Left Alignment"
    
        [commands appendData:[@""
                               "-----------------------------\n"
                              dataUsingEncoding:NSASCIIStringEncoding]];
    
        [commands appendData:[[self getOrderid] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendData:[[self getUser] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendData:[[self getCurrecttime] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendData:[@"\n\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    [commands appendData:[@"-------------------------------\n"
                          " Qty         Item Description    \n"
                          "-------------------------------\n"dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendData:[[self getCartData] dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    [commands appendBytes:"\x1b\x45\x01"
                   length:sizeof("\x1b\x45\x01") - 1];    // Set Emphasized Printing ON
    
    [commands appendData:[@"\n"
                          "-----------------------------\n"
                          dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendData:[@"\n\n\n*****END*****" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendBytes:"\x1b\x45\x00"
                   length:sizeof("\x1b\x45\x00") - 1];    // Set Emphasized Printing OFF (same command as on)

    [commands appendData:[@"\n\n\n\n\n" dataUsingEncoding:NSASCIIStringEncoding]];

    return commands;

}
/*
-(NSString *)getCartData{
    NSString * dataString = @"";
    
//    if (!carttbl)carttbl = [[SelectOrderDAO alloc]init];
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    
    NSArray * cartItems = [carttbl getSelectFoodCategorysforPrinter];
    int totalprice = 0;
                                                
    for (SelectOrder * Item in cartItems) {

        NSLog(@"Attaching %@ and %@",Item.orderrQuantity,Item.ordernote);
       dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@" %@  %@\n",Item.orderrQuantity,Item.orderName]];
        NSString * ExtraString = [self getExtraString:Item.recodId :Item.ordernote];
        
        if (![ExtraString isEqualToString:@""] && ![ExtraString isEqualToString:@","]) {
            dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@"-(%@)\n",ExtraString]];
        }
        dataString = [dataString stringByAppendingString:@"\n"];
        totalprice = totalprice+Item.totalPrice.intValue;
    }
    return dataString;
}
 */

-(NSString *)getCartData{
    NSString * dataString = @"";

    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    
    NSArray * feedItems = nil;
    
    if (isToKitchen) {
        feedItems = [dbmanager GetAllKitchenPrinterFeeds];
        
        if (feedItems.count > 0) {
            isPrint = YES;
        }else{
            isPrint = NO;
        }
        
    }else{
        feedItems = [dbmanager getAllPrinterFeeds];
        isPrint = YES;
    }
    
    for (PrinterFeeds * Item in feedItems) {
        
        NSLog(@"Attaching %@ and %@",Item.itemQty,Item.itemNote);
        dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@" %@  %@\n",Item.itemQty,Item.itemName]];
        NSString * ExtraString = [self getExtraString:Item.itemRecordID :Item.itemNote];
        
        if (![ExtraString isEqualToString:@""] && ![ExtraString isEqualToString:@","]) {
            dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@"-(%@)\n",ExtraString]];
        }
        dataString = [dataString stringByAppendingString:@"\n"];
    }
    return dataString;
}

-(NSString *)getExtraString :(NSString *)itemID :(NSString *)noteString{
    NSString * extras = noteString;
    
    if (extras == nil){
      extras = @"";
    }else{
      extras = [NSString stringWithFormat:@"%@,",extras];
    }
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    NSArray * extrasArray = [dbmanager GetAllExtrasByRID:itemID.intValue];
    
    for (ExtraItems * extraItem in extrasArray) {
        extras = [extras stringByAppendingString:[NSString stringWithFormat:@"%@,",extraItem.extraitemName]];
    }
    return extras;
}
#pragma mark-
#pragma mark CashierBill Details

-(NSData *)BillPrinterDetails{
    NSMutableData *commands = [NSMutableData new];
    
    [commands appendBytes:"\x1d\x57\x40\x32"
                   length:sizeof("\x1d\x57\x40\x32") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 64, nH = 2)
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    [commands appendBytes:"\x1b\x45\x01"
                   length:sizeof("\x1b\x45\x01") - 1];    // Set Emphasized Printing ON
    
    [commands appendData:[@"BA-POS\n"
                          "Trinity House"
                          "Heather Park Drive\n"
                          "Wembley Ha0 1SU HA0 1SU*\n\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendBytes:"\x09"
                   length:sizeof("\x09") - 1];    // Left Alignment"
    
    [commands appendData:[@""
                          "\n"
                          dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendData:[[self getOrderid] dataUsingEncoding:NSASCIIStringEncoding]];
    [commands appendData:[[self getUser] dataUsingEncoding:NSASCIIStringEncoding]];
    [commands appendData:[[self getCurrecttime] dataUsingEncoding:NSASCIIStringEncoding]];
    [commands appendData:[@"\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendData:[@"===============================\n"
                          "        Billing Details         \n"
                          "===============================\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendData:[[self getCartBillPrintData] dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    [commands appendBytes:"\x1b\x45\x01"
                   length:sizeof("\x1b\x45\x01") - 1];    // Set Emphasized Printing ONalignment
    
    [commands appendData:[@"\n"
                          "Total :"
                          dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendBytes:"\x1d\x21\x11"
                   length:sizeof("\x1d\x21\x11") - 1];    // Width and Height Character Expansion  <GS>  !  n
    
    [commands appendData:[[self getTotal] dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendBytes:"\x1d\x21\x00"
                   length:sizeof("\x1d\x21\x00") - 1];    // Cancel Expansion - Reference Star Portable Printer Programming Manual
    
    
    
    [commands appendData:[@"\n\n*****END*****" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [commands appendBytes:"\x1b\x45\x00"
                   length:sizeof("\x1b\x45\x00") - 1];    // Set Emphasized Printing OFF (same command as on)
    
    [commands appendData:[@"\n\n\n\n" dataUsingEncoding:NSASCIIStringEncoding]];

    return commands;
}
-(NSString *)getCartBillPrintData{
    NSString * dataString = @"";
    
    if (self.SplitId.intValue > 0) {
        dataString = [self getSplitBillPrintData];
    }else{
        if (!carttbl)carttbl = [[SelectOrderDAO alloc]init];
        NSArray * cartItems = [carttbl getSelectFoodCategorys];
        double totalprice = 0;
        
        for (SelectOrder * Item in cartItems) {
            
            dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@"%@\n",Item.orderName]];
            dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@"%@  *  %@ = %@\n",Item.orderrQuantity,Item.orderPrice,Item.totalPrice]];
            dataString = [dataString stringByAppendingString:@"\n"];
            totalprice = totalprice+Item.totalPrice.doubleValue;
        }
        totalValues = [NSString stringWithFormat:@"%0.2f",totalprice];
    }
    
    return dataString;
}

-(NSString *)getSplitBillPrintData{
    NSString * dataString = @"";
    
    if (!dbmanager)dbmanager = [[DBManager alloc]init];
    if (!carttbl)carttbl = [[SelectOrderDAO alloc]init];
    NSArray * SplitdataArray = [dbmanager selectSplitRecord:[NSNumber numberWithInt:self.SplitId.intValue]];
    double totalprice = 0;
    for (SplitData * item in SplitdataArray) {
        
        SelectOrder * currentItem = [carttbl getSelectFoodCategoryByRecordID:item.itemRecordID];
        dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@"%@\n",item.itemname]];
        dataString = [dataString stringByAppendingString:[NSString stringWithFormat:@"%@  *  %@ = %@\n",currentItem.totalPrice,item.qty,item.price]];
        dataString = [dataString stringByAppendingString:@"\n"];
        totalprice = totalprice+item.price.doubleValue;
    }
    totalValues = [NSString stringWithFormat:@"%0.2f",totalprice];
    return dataString;
}

#pragma mark-
#pragma mark Common Details

-(NSString *)getCurrecttime{
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *stringFromDate = [dateFormatter1 stringFromDate:[NSDate date]];
    
    NSString * time = [NSString stringWithFormat:@"%@\n",stringFromDate];
    return time;
}
-(NSString *)getOrderid{
    
    NSString * time = [NSString stringWithFormat:@"Order No : %@\n",Orderid];
    
    if (self.SplitId.intValue > 0)time = [NSString stringWithFormat:@"Order No : %@ - #%@\n",Orderid,self.SplitId];
    
    return time;
}
-(NSString *)getUser{
    
    NSString * username = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
    NSString * time = [NSString stringWithFormat:@"User Name : %@\n",username];
    return time;
}
-(NSString *)getCustomer{
    
    NSLog(@"customerDetails %@",self.Customer);
    
    if (self.Customer == NULL)self.Customer = @" ";
    
    
    NSString * cuntomername = [NSString stringWithFormat:@"Customer : %@",self.Customer];
    return cuntomername;
    
}
-(NSString *)getTotal{
    
    NSString * totalval = @"00.00";
    
    if (totalValues.intValue > 0) {
        totalval = totalValues;
    }
    return totalval;
}

-(void)setRasterCashierBill:(NSString *)portName portSettings:(NSString *)portSettings{
    
    [self CombineImagestop:portName portSettings:portSettings];
}

-(void)CombineImagestop :(NSString *)portName portSettings:(NSString *)portSettings{
    
    NSString *textToPrint = @"";
    
    if (self.TableId.intValue == 0) {
        
     textToPrint =[NSString stringWithFormat:@"%@ \r\n"
          "%@\r\n"
          "-----------------\r\n"
          "%@"
          ,[AppUserDefaults getResturentName],[self getOrderid],deliveryType];
        
    }else{
        textToPrint =[NSString stringWithFormat:@"%@ \r\n"
                      "%@\r\n"
                      "-----------------\r\n"
                      "%@\r\n"
                      "Table : %@/%@"
                      ,[AppUserDefaults getResturentName],[self getOrderid],deliveryType,self.TableId,self.TableName];
    }
    
    int width = 576;
    
    NSString *fontName = @"Courier";
    
    double fontSize = 28.0;
    
    //  fontSize *= multiple;
    fontSize *= 2;
    
    UIFont *font = [UIFont fontWithName:fontName size:fontSize];
    
    CGSize size = CGSizeMake(width, 10000);
    CGSize messuredSize = [textToPrint sizeWithFont:font constrainedToSize:size];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(messuredSize, NO, 1.0);
        } else {
            UIGraphicsBeginImageContext(messuredSize);
        }
    } else {
        UIGraphicsBeginImageContext(messuredSize);
    }
    
    CGContextRef ctr = UIGraphicsGetCurrentContext();
    UIColor *color = [UIColor whiteColor];
    [color set];
    
    CGRect rect = CGRectMake(0, 0, messuredSize.width + 1, messuredSize.height + 1);
    CGContextFillRect(ctr, rect);
    
    color = [UIColor blackColor];
    [color set];
    
    [textToPrint drawInRect:rect withFont:font];
    
//    UIImage *imageToPrint = UIGraphicsGetImageFromCurrentImageContext();
    
    HeaderImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self CombineImagesbody];
    
//dev_frenndo 0382235659 dilukshan.
    
//    [self BindingImages];
//    [PrinterFunctions PrintImageWithPortname:portName portSettings:portSettings imageToPrint:imageToPrint maxWidth:width compressionEnable:YES withDrawerKick:NO];
    
}
-(void)CombineImagesbody{
    
//    NSString *textToPrint =[NSString stringWithFormat:@"PALMBEACH \r\n"
//                            "%@\r\n"
//                            "------------------\r\n"
//                            "%@\r\n"
//                            ,[self getOrderid],@"TAKE AWAY"];
    
    
        NSString *textToPrint =[NSString stringWithFormat:@"DateTime: %@\r\n"
                                " %@\r\n"
                                "--------------------------\r\n"
                                " Qty       Item\r\n"
                                 "-------------------------\r\n"
                                 " %@\r\n"
                                 "------------END----------\r\n"
                                 ,[self getCurrecttime],[self getCustomer],[self getCartData]];
    
    
    int width = 576;
    
    NSString *fontName = @"Courier";
    
    double fontSize = 18.0;
    
    //  fontSize *= multiple;
    fontSize *= 2;
    
    UIFont *font = [UIFont fontWithName:fontName size:fontSize];
    
    CGSize size = CGSizeMake(width, 10000);
    CGSize messuredSize = [textToPrint sizeWithFont:font constrainedToSize:size];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(messuredSize, NO, 1.0);
        } else {
            UIGraphicsBeginImageContext(messuredSize);
        }
    } else {
        UIGraphicsBeginImageContext(messuredSize);
    }
    
    CGContextRef ctr = UIGraphicsGetCurrentContext();
    UIColor *color = [UIColor whiteColor];
    [color set];
    
    CGRect rect = CGRectMake(0, 0, messuredSize.width + 1, messuredSize.height + 1);
    CGContextFillRect(ctr, rect);
    
    color = [UIColor blackColor];
    [color set];
    
    [textToPrint drawInRect:rect withFont:font];
    
    //    UIImage *imageToPrint = UIGraphicsGetImageFromCurrentImageContext();
    
    bodyImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (isPrint)[self BindingImages];
    
}
-(void)BindingImages{
    
        UIImage *image1 = HeaderImage;//[UIImage imageNamed:@"image1.jpg"];
        UIImage *image2 = bodyImage;//[UIImage imageNamed:@"image2.jpg"];
    
        CGSize size_main = CGSizeMake(image1.size.width, image1.size.height + image2.size.height);
    
        UIGraphicsBeginImageContext(size_main);
    
        [image1 drawInRect:CGRectMake(0,0,size_main.width, image1.size.height)];
        [image2 drawInRect:CGRectMake(0,image1.size.height,size_main.width, image2.size.height)];
    
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
        UIGraphicsEndImageContext();
    
       [PrinterFunctions PrintImageWithPortname:portname portSettings:PortSettings imageToPrint:finalImage maxWidth:500.0 compressionEnable:YES withDrawerKick:NO];
}
-(void)setRasterBillPrint:(NSString *)portName portSettings:(NSString *)portSettings{
    
    
//    NSString * totalPrice = [NSString stringWithFormat:@"\n"
//                             "Total : %@",[self getTotal]];
    
    NSString *textToPrint =[NSString stringWithFormat:@"%@\n"
                            "%@\n"
                            "Tel :%@\n"
                            "WEB :%@\n"
                            "Email : %@\n\n"
                            "%@"
                            "Billing Date: %@\n"
                            "%@\n"
                            "-----------------------------------------------------------\r\n"
                            "%@\n"
                            "-----------------------------------------------------------\r\n"
                            "Sub total                                  : %@\n"
                            "Discount                                  : %@\n"
                            "Tax                                           : %@\n"
                            "Service C                                 : %@\n\n"
                            "Total                                         : %@\n\n"
                            "Cash                                        : %@\n"
                            "Card                                         : %@\n"
                            "GiftVoucher                              : %@\n\n"
                            "Change                                    : %@\n\n"
                            "Action               : %@\n"
                            "\r\n"
                            "---------THANK YOU.COME AGAIN--------\r\n"
                            
                            ,[AppUserDefaults getResturentName],[AppUserDefaults getResturentAddress],[AppUserDefaults getResturentTell],[AppUserDefaults getResturentWeb],[AppUserDefaults getResturentEmail],[self getOrderid],[self getCurrecttime],[self getCustomer],[self getCartBillPrintData],self.Subtotal,self.Discount,self.Tax,self.Servicecharge,self.Total,self.Cash,self.Card,self.GiftVoucher,self.Change,self.PaymentStatus];
    
    int width = 576;
    
    NSString *fontName = @"Arial";//@"Courier";
    
    NSLog(@"Printerd %@",textToPrint);
    
    double fontSize = 14.0;
    
    //  fontSize *= multiple;
    fontSize *= 2;
    
    UIFont *font = [UIFont fontWithName:fontName size:fontSize];
    
    CGSize size = CGSizeMake(width, 10000);
    CGSize messuredSize = [textToPrint sizeWithFont:font constrainedToSize:size];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            UIGraphicsBeginImageContextWithOptions(messuredSize, NO, 1.0);
        } else {
            UIGraphicsBeginImageContext(messuredSize);
        }
    } else {
        UIGraphicsBeginImageContext(messuredSize);
    }
    
    CGContextRef ctr = UIGraphicsGetCurrentContext();
    UIColor *color = [UIColor whiteColor];
    [color set];
    
    CGRect rect = CGRectMake(0, 0, messuredSize.width + 1, messuredSize.height + 1);
    CGContextFillRect(ctr, rect);
    
    color = [UIColor blackColor];
    [color set];
    
    [textToPrint drawInRect:rect withFont:font];
    
    UIImage *imageToPrint = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    [PrinterFunctions PrintImageWithPortname:portName portSettings:portSettings imageToPrint:imageToPrint maxWidth:width compressionEnable:YES withDrawerKick:NO];
}
@end
