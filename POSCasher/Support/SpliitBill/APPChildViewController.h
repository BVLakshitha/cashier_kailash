//
//  APPChildViewController.h
//  PageApp
//
//  Created by Rafael Garcia Leiva on 10/06/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "PopoverViewController.h"
#import "SelectOrderDAO.h"


@protocol Childviewdelegate <NSObject>

-(void)didchangesplit :(NSString *)reloadmassate;
-(void)didClicksPaybtn :(NSString *)splitID :(NSString *)amount;

@end
@interface APPChildViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,Popoverdelegate>{
    
    DBManager * manager;
    NSString * toucheditemID;
    NSMutableArray * splitindexarray;
    int lastcheckno;
    float totalprices;
    PopoverViewController *PopoverView;
    SelectOrderDAO * selectedDAO;
    
}

@property (assign, nonatomic) NSInteger index;
@property (assign, nonatomic) NSInteger checklimit;
@property (assign, nonatomic) NSInteger totallimit;
@property (strong, nonatomic) IBOutlet UILabel *screenNumber;
@property (strong, nonatomic) IBOutlet UITableView *splittable;
@property(nonatomic,retain)NSMutableArray * dataarray;
@property (strong, nonatomic) IBOutlet UILabel *titleTL;
@property (nonatomic,strong) UIPopoverController *popOver;

@property ( nonatomic, retain ) id <Childviewdelegate> delegate;


@property (weak, nonatomic) IBOutlet UILabel *subtotalTF;
@property (weak, nonatomic) IBOutlet UILabel *scTF;
@property (weak, nonatomic) IBOutlet UILabel *totalTF;


@property (weak, nonatomic) IBOutlet UIButton *leftsidebtn;
@property (weak, nonatomic) IBOutlet UIButton *rightsidebtn;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckout;

@property (weak, nonatomic) IBOutlet UIButton *btnprint;
- (IBAction)btnprintClicks:(id)sender;
- (IBAction)checkoutclicks:(id)sender;

-(void)ActiveteButtonpanel;

- (IBAction)btnOrdeSplitClicks:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *OrderSplit;



@end
