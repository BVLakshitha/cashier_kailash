//
//  APPChildViewController.m
//  PageApp
//
//  Created by Rafael Garcia Leiva on 10/06/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "APPChildViewController.h"
#import "SplitData.h"
#import "DBManager.h"
#import "PopoverViewController.h"
#import "AddbasketViewCell.h"
#import "SelectOrder.h"
#import "ExtraItems.h"
#import "AppUserDefaults.h"


@interface APPChildViewController ()

@end

@implementation APPChildViewController
@synthesize dataarray;
@synthesize delegate = _delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
    }
    
    return self;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.


}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self reloaddata];

    [self genarateDashborddata];
//    [self arrowcontrol];
    [self manageSplitOrderFunction];

}

-(void)manageSplitOrderFunction{
    BOOL isSplited = NO;
    
    if (!manager)manager = [[DBManager alloc]init];
    NSArray * splitrecords = [manager selectAllSplitRecords];
    
    for (SplitData * record in splitrecords) {
        if ([record.isSplited isEqualToString:@"YES"])isSplited = YES;
    }
    
    if (isSplited) {
        self.OrderSplit.hidden = YES;
    }else{
        self.OrderSplit.hidden = NO;
    }
}
-(void)reloaddata{
    if (!manager)manager = [[DBManager alloc]init];
    
    int splitid =self.index+1;
    
    NSArray * databundle = [manager selectSplitRecord:[NSNumber numberWithInt:splitid]];
    
    NSLog(@"databundle count =%i",[databundle count]);
    
    //TITLECONTROL
    SplitData * splitdata = [databundle firstObject];
    NSLog(@"SplitID %@",splitdata.splitOrderID);
    if (splitdata.splitOrderID.intValue == 0 ||splitdata.splitOrderID == nil) {
            self.titleTL.text = [NSString stringWithFormat:@"Check #%d", self.index+1];
        [self checkoutbtnControl:1];
    }else{
        //BUTTONCONTROL
        if ([splitdata.isPaid isEqualToString:@"YES"]) {
            self.titleTL.text = [NSString stringWithFormat:@"Check #%d", self.index+1];
            [self checkoutbtnControl:3];
        }else{
            self.titleTL.text = [NSString stringWithFormat:@"Check #%i Check OrderId %@", self.index+1, splitdata.splitOrderID];
            [self checkoutbtnControl:2];
        }
    }
    
    if (self.dataarray.count > 0) {
        [self.dataarray removeAllObjects];
        [self.dataarray addObjectsFromArray:databundle];
    }else{
        self.dataarray = [[NSMutableArray alloc]initWithArray:databundle];
    }
    [self.splittable reloadData];
}
-(void)checkoutbtnControl :(int)position{
    
    switch (position) {
        case 1:
            
            self.btnCheckout.hidden = YES;
            
            break;
        case 2:
            
            self.btnCheckout.hidden = NO;
            
            break;
        case 3:
            
            self.btnCheckout.hidden = NO;
            self.btnCheckout.enabled = NO;
            [self.btnCheckout setTitle:@"Paid" forState:UIControlStateNormal];
            
            break;
            
        default:
            break;
    }
    
    //btn Pay
    
    self.btnCheckout.layer.masksToBounds = YES;
    self.btnCheckout.layer.cornerRadius = 4;
    
    //btn Print
    self.btnprint.layer.masksToBounds = YES;
    self.btnprint.layer.cornerRadius = 4;
    self.btnprint.enabled = NO;
    
    //btn Print
    
    
    self.OrderSplit.layer.masksToBounds = YES;
    self.OrderSplit.layer.cornerRadius = 4;
    
}
#pragma mark -
#pragma mark Tablview deligates{

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [dataarray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddbasketViewCell" owner:self options:nil];
    AddbasketViewCell *cell = [nib objectAtIndex:0];

    SplitData * splidata = [dataarray objectAtIndex:indexPath.row];
      cell.lblItermName.text=splidata.itemname;
      cell.lblTotalPrice.text=splidata.price;
      cell.lblQuantity.text=splidata.qty;
      cell.lblPerOnePrice.text = [self eachPrice:splidata.itemDisID];
    
    return cell;
}
-(NSString *)eachPrice :(NSString *)IDSID{
    NSString * ID = @"0";
    if (!selectedDAO)selectedDAO = [[SelectOrderDAO alloc]init];
    SelectOrder * Item = [[selectedDAO getFoodCategoryByDisID:IDSID] firstObject];
    ID = Item.orderPrice;
    return ID;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (self.index == 0) {
        
        SplitData * splidata = [dataarray objectAtIndex:indexPath.row];

        if ([splidata.isSplited isEqualToString:@"NO"]) {
            toucheditemID = splidata.itemDisID;
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            CGRect rect=CGRectMake(cell.bounds.origin.x+400, cell.bounds.origin.y+10, 50, 30);
            
            PopoverView =[[PopoverViewController alloc] initWithNibName:@"PopoverViewController" bundle:nil];
            PopoverView.delegate = self;
            PopoverView.counter = [[NSMutableArray alloc]initWithArray:[self titleCreater]];
            self.popOver =[[UIPopoverController alloc] initWithContentViewController:PopoverView];
            [self.popOver presentPopoverFromRect:rect inView:cell permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"You cannot split more." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}
#pragma mark -
#pragma mark OrderSplit;

- (IBAction)btnOrdeSplitClicks:(id)sender {
    [self orderEquallySplit];
}

-(void)orderEquallySplit{
    
    NSArray * dataArray = [[NSArray alloc]initWithArray:dataarray];
    
    for (SplitData * splitdata in dataArray) {
        toucheditemID = splitdata.itemDisID;
        [self didTouchtestfield:@"Split equaly"];
    }
}
-(NSArray *)titleCreater{
    
     if (!manager)manager = [[DBManager alloc]init];
    NSArray * records =  [manager selectSplitRecord:[NSNumber numberWithInt:1000]];
    NSMutableArray * master = [[NSMutableArray alloc]init];

    NSNumber * splitsize = nil;
    
    if (!splitindexarray)splitindexarray = [[NSMutableArray alloc]init];
    
    [splitindexarray removeAllObjects];
    
    for (SplitData * splidata in records) {
        splitsize = splidata.splitID;
        if (![splitindexarray containsObject:splitsize]) {
            [splitindexarray addObject:splitsize];
        }
    }
    NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [splitindexarray sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
    
    NSLog(@"CplitArray %@",splitindexarray);
    NSLog(@"limit %i",self.totallimit);
    
    for (NSNumber * number in splitindexarray) {
        
        if (number.intValue == 1) {
            [master addObject:@"Split equaly"];
            if (splitindexarray.count < self.totallimit )[master addObject:@"New Check"];
        }else{
            [master addObject:[NSString stringWithFormat:@"Check %i",number.intValue]];
        }
    }
    NSLog(@"Msster = %@",master);
    
    return master;
}
-(void)didTouchtestfield:(NSString *)mmassage{
    
    [self updateDBasrequest:mmassage];
    [self.delegate didchangesplit:[NSString stringWithFormat:@"%i",lastcheckno]];
    [self.splittable reloadData];
    [self.popOver dismissPopoverAnimated:YES];
    
}

-(void)updateDBasrequest :(NSString *)massage{
    
    SplitData * splidata =  [manager isRecordExistanceSplit:toucheditemID];
    
    NSString * qty = splidata.qty;
    NSString * price = splidata.price;
    NSString * name = splidata.itemname;
    NSString * recordID = splidata.itemRecordID;
    NSString * extraprice = [self returnExtraAmount:recordID];

    if (!manager)manager = [[DBManager alloc]init];
    
    if ([massage isEqualToString:@"Split equaly"]) {
        
        NSLog(@"totallimit %i",self.totallimit);
        
         [manager RemoveSplitRecrd:toucheditemID];
        for (int i = 0; i <self.totallimit; i++) {
            NSString * newqty = @"0";
            
            if (qty.intValue == self.totallimit) {
                 newqty = [NSString stringWithFormat:@"%i",self.totallimit/qty.intValue];
            }else{
                newqty = [NSString stringWithFormat:@"%@ / %i",qty,self.totallimit];
            }

            NSString * dicreiptionid = toucheditemID;
            
            //new Price adjustmentExtra
            
            NSString * Divqty = [NSString stringWithFormat:@"%.2f",qty.doubleValue/self.totallimit];
            
            double mainvalue = Divqty.doubleValue * price.doubleValue;
//            double Extra = Divqty.doubleValue * extraprice.doubleValue;
//            double totalValue = mainvalue +Extra;
            
            NSString * newprice = [NSString stringWithFormat:@"%.2f",mainvalue];
            
            NSNumber * newsplitid = [NSNumber numberWithInt:(i+1)];
            [manager insetSplitRecord:dicreiptionid :newsplitid :newprice :newqty :name :@"YES" :Divqty :recordID];
            lastcheckno = self.totallimit;
            self.checklimit = lastcheckno;
        }

    }else if([massage isEqualToString:@"New Check"]){
        
        int newcheckno = [[splitindexarray lastObject] intValue]+1;
        lastcheckno = newcheckno;
        if (newcheckno <= self.totallimit) {
          [manager updateSplitRecord:toucheditemID :price :qty :[NSNumber numberWithInt:newcheckno]];
        }else{
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Check limit reached." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        
        NSString *aString = massage;
        NSArray *array = [aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        array = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
        NSString * laststring = [array lastObject];
         [manager updateSplitRecord:toucheditemID :price :qty :[NSNumber numberWithInt:laststring.intValue]];
        NSLog(@"Check no = %@",laststring);
        
    }

    [self  genarateDashborddata];
     [self reloaddata];
}

-(NSString *)returnExtraAmount:(NSString *)recordID{
    NSString * amount = @"0";
    if (!manager)manager = [[DBManager alloc]init];
    
    NSArray * extraItems = [manager GetAllExtrasByRID:recordID.intValue];
    double extraamount = 0.0;
    for (ExtraItems * itemextra in extraItems) {
        extraamount = extraamount+itemextra.extraitemPrice.doubleValue;
    }

    amount = [NSString stringWithFormat:@"%f",extraamount];
    return amount;
}
 
-(void)arrowcontrol{
    
    if (!manager)manager = [[DBManager alloc]init];
    NSArray * records =  [manager selectSplitRecord:[NSNumber numberWithInt:1000]];
    NSNumber * splitsize = nil;
    
    if (!splitindexarray)splitindexarray = [[NSMutableArray alloc]init];
    
    [splitindexarray removeAllObjects];
    
    for (SplitData * splidata in records) {
        splitsize = splidata.splitID;
        if (![splitindexarray containsObject:splitsize]) {
            [splitindexarray addObject:splitsize];
        }
    }
    
    int currentid = self.index +1;
    
    if([splitindexarray count] == 1){
        
        self.leftsidebtn.hidden = YES;
        self.rightsidebtn.hidden = YES;
        
    }else if(currentid == [splitindexarray count]){
        if([splitindexarray count] > 1){
            self.leftsidebtn.hidden = NO;
            self.rightsidebtn.hidden = YES;
        }
    }else if(currentid == 1){
        self.leftsidebtn.hidden = NO;
        self.rightsidebtn.hidden = NO;
    }
        
    
    NSLog(@"1st = %i, 2nd = %i,3rd = %i",self.index,[splitindexarray count],self.totallimit);
}
-(void)genarateDashborddata{
    
    NSArray * records =  [manager selectSplitRecord:[NSNumber numberWithInt:1000]];
    
    float totalvales = 0;
    
    for (SplitData * splidata in records) {
        
        if (splidata.splitID.intValue == (self.index+1)) {
            totalvales = totalvales+splidata.price.floatValue;
        }
        
        self.totalTF.text = [NSString stringWithFormat:@"%.2f",totalvales];
    }
}
- (IBAction)btnprintClicks:(id)sender {
}

- (IBAction)checkoutclicks:(id)sender {
//ksPaybtn:
    if (self.totalTF.text != nil) {
        [self.delegate didClicksPaybtn:[NSString stringWithFormat:@"%i",self.index+1] :self.totalTF.text];
        NSLog(@"totallimit %i",self.index);
    }
}
 
#pragma mark - 
#pragma mark userdefaluts;
-(void)ActiveteButtonpanel{
        
    self.btnCheckout.hidden = NO;
}

@end
