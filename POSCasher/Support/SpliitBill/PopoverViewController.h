//
//  PopoverViewController.h
//  PopoverDemo
//
//  Created by Arthur Knopper on 16-05-13.
//  Copyright (c) 2013 Arthur Knopper. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Popoverdelegate <NSObject>

-(void)didTouchtestfield :(NSString *)mmassage;
-(void)dismisspoponerViewControllr;

@end

@interface PopoverViewController : UITableViewController
@property ( nonatomic, retain ) id <Popoverdelegate> delegate;
@property(nonatomic,retain)NSMutableArray * counter;

@end
