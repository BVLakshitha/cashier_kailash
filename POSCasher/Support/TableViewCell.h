//
//  TableViewCell.h
//  Cashier
//
//  Created by User on 07/12/2014.
//  Copyright (c) 2014 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *checkimg;
@property (weak, nonatomic) IBOutlet UILabel *nameTL;
@property (weak, nonatomic) IBOutlet UILabel *priceTL;
@end
