//
//  WebserviceApi.h
//  PosApp
//
//  Created by Lakshitha on 8/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "CategoryDAO.h"
#import "FoodCategoryDAO.h"
#import "TableReceivedDao.h"
#import "OrderHistoryItemDao.h"
#import "OrderListDao.h"
#import "DBManager.h"


@class CategoryDAO;
@class FoodCategoryDAO;
@class OrderListDao;
@class TableReceivedDao;
@class OrderHistoryItemDao;
@class DBManager;


@protocol webservicedelegate <NSObject>
@optional

-(void)DidReceiveLogininfo :(NSArray *)respond;
-(void)didfatchMainMenu :(BOOL)isFatched;
-(void)DidReceiveHistory :(BOOL)isFatched;
-(void)DidfatchTableStatus :(BOOL)isFatched;
-(void)DidReceiveMenuItems :(NSArray *)respond;
-(void)DidReceiveLocationinfo :(BOOL)isFatched;
-(void)DidReceiveSendinfo :(id)respond;
-(void)DidReceiveSplitinfo :(id)respond;
-(void)DidReceivePaymentinfo :(id)respond;
-(void)DidReceiveRegister :(id)respond;
-(void)DidReceiveAllCustomers :(BOOL)isFatched;
-(void)DidReceiveExtraItems :(id)respond;
-(void)DidReceiveRestuarentProfile :(id)respond;
-(void)DidReceivetaxdetails :(id)respond;
-(void)DidReceiveVoucherDetails :(id)respond;
-(void)DidReceiveSplitCancelDetails :(id)respond;
-(void)DidReceiveGiftVoucherDetails :(id)respond;
-(void)DidReceiveCancelOrderDetails :(id)respond;
-(void)DidReceiveCancelOrderItemDetails :(id)respond;
-(void)DidReceiveAllItems :(id)respond;
-(void)DidReceiveFinalizeData :(id)respond;
-(void)DidReceiveDeliveryUpdateDetails :(id)respond;



@end

@interface WebserviceApi : NSObject{
    
    MBProgressHUD *HUD;
    CategoryDAO *objCategory;
    FoodCategoryDAO * foodsDB;
    TableReceivedDao *objTableReceived;
    OrderListDao *objOrderListDao;
    OrderHistoryItemDao *objOrderListHistoryDao;
    DBManager * dbmanager;
    
}

@property ( nonatomic, retain ) id <webservicedelegate> delegate;

-(void)Loginme :(NSDictionary *)params ;
-(void)GetMainMenus :(NSDictionary *)params;
-(void)GetHistory :(NSDictionary *)params;
-(void)GetTableStatus :(NSDictionary *)params;
-(void)GetMenuItems :(NSDictionary *)params;
-(void)GetLocationInfo;
-(void)SendOrderToServer :(NSDictionary *)params;
-(void)CheckforSplit :(NSDictionary *)params;
-(void)CashPayProcess :(NSDictionary *)params;
-(void)RegisterCustomer :(NSDictionary *)params;
-(void)GetCustomers :(NSDictionary *)params;
-(void)GetExtraItemsbyId :(NSDictionary *)params;
-(void)GetRestuarentProfile;
-(void)ValidateVoucher :(NSDictionary *)params;
-(void)SendPaymentDetails :(NSDictionary *)params;
-(void)SendSplitOrder :(NSDictionary *)params;
-(void)CancelSplitOrder :(NSDictionary *)params;
-(void)GiftVoucherValidate :(NSDictionary *)params;
-(void)getGiftvoucherDetails :(NSDictionary *)params;
-(void)CancelOrder :(NSDictionary *)params;
-(void)CancelOrderItem :(NSDictionary *)params;
-(void)getAllItems :(NSDictionary *)params;
-(void)finalizeTillSession :(NSDictionary *)params;
-(void)UpdateDeliveryDetails :(NSDictionary *)params;


@end
