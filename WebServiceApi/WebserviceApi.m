//
//  WebserviceApi.m
//  PosApp
//
//  Created by Lakshitha on 8/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "WebserviceApi.h"
#import "AFNetworking.h"
#import "CategoryDAO.h"
#import "FoodCategoryDAO.h"
#import "TableReceivedDao.h"
#import "OrderHistoryItemDao.h"
#import "OrderListDao.h"
#import "Constants.h"
#import "AppUserDefaults.h"
#import "Flurry.h"
#import "GAITracker.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "Reachability.h"

@implementation WebserviceApi
@synthesize delegate;
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark -
#pragma mark Login

-(void)Loginme :(NSDictionary *)params{
    NSString *string = [NSString stringWithFormat:@"%@loginap",SERVICEURL];
    
    //    NSLog(@"params")
    
    NSURL *baseURL = [NSURL URLWithString:string];
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:[NSDictionary dictionaryWithDictionary:params] success:^(NSURLSessionDataTask *task, id responseObject) {
        
        
        [self.delegate DidReceiveLogininfo:responseObject];
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Registration Fail"
        //                                                            message:[error localizedDescription]
        //                                                           delegate:nil
        //                                                  cancelButtonTitle:@"Ok"
        //                                                  otherButtonTitles:nil];
        //        [alertView show];
        NSLog(@"error %@",error.description);
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        
        [self DataLoadFail:string];
    }];
    
}
#pragma mark -
#pragma mark Main Menus

-(void)GetMainMenus :(NSDictionary *)params{
    
    //    NSString *string = [NSString stringWithFormat:@"%@GetMainMenua", SERVICEURL];
    NSString *string = [NSString stringWithFormat:@"%@GetMainMenu", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:[NSDictionary dictionaryWithDictionary:params] success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"main menu %@",responseObject);
        
        if (!foodsDB)foodsDB = [[FoodCategoryDAO alloc]init];
        [foodsDB insertCategories:responseObject];
        
        [self.delegate didfatchMainMenu:YES];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"error %@",error.description);
        NSLog(@"URL %@ params %@",string,params);
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
    }];
    
}
#pragma mark -
#pragma mark History

-(void)GetHistory :(NSDictionary *)params{
    
    NSLog(@"Parameters %@",params);
    
    NSString *string = [NSString stringWithFormat:@"%@orderHistory", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:[NSDictionary dictionaryWithDictionary:params] success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"responseObject %@",responseObject);
        [self fatchingHistory:[responseObject objectForKey:@"data"]];
        [AppUserDefaults setHistryDateTime:[responseObject objectForKey:@"lmd"]];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        [self DataLoadFail:string];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}
-(void)fatchingHistory:(NSDictionary *)responseStrig{
    
    //    NSLog(@"responseObject History = %@",responseStrig);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //dateFormatter.dateFormat = @"dd-MM-yyyy";
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableArray *getOrderId=[[NSMutableArray alloc]init];
    getOrderId=[responseStrig valueForKey:@"o_id"];
    
    
    NSMutableArray *getTotleValue=[[NSMutableArray alloc]init];
    getTotleValue=[responseStrig valueForKey:@"total_value"];
    
    NSMutableArray *getCreateDate=[[NSMutableArray alloc]init];
    getCreateDate=[responseStrig valueForKey:@"create_date"];
    
    NSMutableArray *getCreateTime=[[NSMutableArray alloc]init];
    getCreateTime=[responseStrig valueForKey:@"create_time"];
    
    
    NSMutableArray *getMenu=[[NSMutableArray alloc]init];
    getMenu=[responseStrig valueForKey:@"menu"];
    
    NSMutableArray *getstatus=[[NSMutableArray alloc]init];
    getstatus=[responseStrig valueForKey:@"status"];
    
    NSMutableArray *gettableid=[[NSMutableArray alloc]init];
    gettableid=[responseStrig valueForKey:@"table_id"];
    
    NSMutableArray *DeliveryAddress=[[NSMutableArray alloc]init];
    DeliveryAddress=[responseStrig valueForKey:@"delievry_address"];
    
    NSMutableArray *DeliveryName=[[NSMutableArray alloc]init];
    DeliveryName=[responseStrig valueForKey:@"delievry_name"];
    
    NSMutableArray *DeliveryEmail=[[NSMutableArray alloc]init];
    DeliveryEmail =[responseStrig valueForKey:@"delievry_email"];
    
    NSMutableArray *DeliveryPhone=[[NSMutableArray alloc]init];
    DeliveryPhone =[responseStrig valueForKey:@"delievry_phone"];
    
    NSMutableArray *DeliveryCost=[[NSMutableArray alloc]init];
    DeliveryCost =[responseStrig valueForKey:@"delivery_cost"];
    
    NSMutableArray *Deliverytype=[[NSMutableArray alloc]init];
    Deliverytype=[responseStrig valueForKey:@"delievry_type"];
    
    NSMutableArray *PaymentST=[[NSMutableArray alloc]init];
    PaymentST=[responseStrig valueForKey:@"payment_st"];
    
    NSMutableArray *ActiveST=[[NSMutableArray alloc]init];
    ActiveST=[responseStrig valueForKey:@"order_status"];
    
    NSMutableArray *getDeliveryDate=[[NSMutableArray alloc]init];
    getDeliveryDate=[responseStrig valueForKey:@"delivery_datetime"];
    
    NSMutableArray *CustomerArr=[[NSMutableArray alloc]init];
    CustomerArr=[responseStrig valueForKey:@"customer"];
    
    if(!objOrderListDao)objOrderListDao=[[OrderListDao alloc]init];
    if(!objOrderListHistoryDao)objOrderListHistoryDao=[[OrderHistoryItemDao alloc]init];
    //    NSLog(@"print order id%@",getOrderId);
    /*
    NSString *todayDate = [dateFormatter stringFromDate:[NSDate date]];
    for (int i=0; i<[getOrderId count]; i++) {
        
        NSLog(@"today date %@",todayDate);
        NSLog(@"created date %@",[[getCreateDate objectAtIndex:i] substringWithRange:NSMakeRange(0, 10)]);
        
       if ([[[getCreateDate objectAtIndex:i] substringWithRange:NSMakeRange(0, 10)] isEqualToString:[dateFormatter stringFromDate:[NSDate date]]]) {
     */
     for (int i=0; i<[getOrderId count]; i++) {
     
            NSString *orderId=[getOrderId objectAtIndex:i];
            NSString *totalPrice=[getTotleValue objectAtIndex:i];
            NSString *createDate=[getCreateDate objectAtIndex:i];
            NSString *creteTime=[getCreateTime objectAtIndex:i];
            NSString *status=[getstatus objectAtIndex:i];
            NSString *tableid=[gettableid objectAtIndex:i];
            NSString *dtype=[Deliverytype objectAtIndex:i];
            NSString *Pst=[PaymentST objectAtIndex:i];
            NSString *Ast=[ActiveST objectAtIndex:i];
            NSString *deliverydate = [getDeliveryDate objectAtIndex:i];
            NSString *deliveryAddress = [DeliveryAddress objectAtIndex:i];
            NSString *deliveryName = [DeliveryName objectAtIndex:i];
            NSString *deliveryPhone = [DeliveryPhone objectAtIndex:i];
            NSString *deliveryEmail = [DeliveryEmail objectAtIndex:i];
            NSString *deliveryCost = [DeliveryCost objectAtIndex:i];
            
            NSDictionary * customerDic = [CustomerArr objectAtIndex:i];
            
            NSLog(@"Customer %@ %@",customerDic,orderId);
            
            
            NSData * CustomerData = [NSKeyedArchiver archivedDataWithRootObject:customerDic];
            
            [objOrderListDao addOrUpdateCheckOutOrderDetails:orderId :totalPrice :createDate :creteTime :status :tableid :dtype :Pst :Ast :CustomerData :deliverydate :deliveryAddress :deliveryName :deliveryPhone :deliveryCost :deliveryEmail];
            
            
            NSMutableArray *item_id=[[NSMutableArray alloc]init];
            item_id=[getMenu valueForKey:@"item_id"];
            
            NSMutableArray *item_name=[[NSMutableArray alloc]init];
            item_name=[getMenu valueForKey:@"item_name"];
            
            NSMutableArray *item_qyt=[[NSMutableArray alloc]init];
            item_qyt=[getMenu valueForKey:@"item_qyt"];
            
            NSMutableArray *chairids=[[NSMutableArray alloc]init];
            chairids=[getMenu valueForKey:@"chair_id"];
            
            NSMutableArray *detits=[[NSMutableArray alloc]init];
            detits=[getMenu valueForKey:@"details_id"];
            
            NSMutableArray *ItemActiveSt=[[NSMutableArray alloc]init];
            ItemActiveSt=[getMenu valueForKey:@"item_canceled"];
            
            NSArray *myArray=[[NSArray alloc]init];
            myArray=[getMenu objectAtIndex:i];
            
            for (NSDictionary *menuDic in myArray) {
                NSString *item_idStr=[menuDic valueForKey:@"item_id"];
                NSString *item_nameStr=[menuDic valueForKey:@"item_name"];
                NSString *Item_note=[menuDic valueForKey:@"item_note"];
                NSNumber *item_qyt_Str=[menuDic valueForKey:@"item_qyt"];
                NSString *item_price=[menuDic valueForKey:@"item_price"];
                NSString *chairid =[menuDic valueForKey:@"chair_id"];
                NSString *detailid =[menuDic valueForKey:@"details_id"];
                NSString * activeState = [menuDic valueForKey:@"item_canceled"];
                NSString * itemLocation = [menuDic valueForKey:@"item_location"];
                NSString * itemdiscount = [menuDic valueForKey:@"item_discount"];
                NSString * itemdefalutdiscount = [menuDic valueForKey:@"item_default_discount"];
                
                
                
                [objOrderListHistoryDao addOrUpdateCheckOutOrderDetails:orderId :item_idStr :item_nameStr :item_qyt_Str :item_price :chairid :detailid :activeState :Item_note :itemLocation :itemdiscount : itemdefalutdiscount];
                
                NSArray * ExtraArray = [menuDic valueForKey:@"extra_items"];
                
                for (NSDictionary * extra in ExtraArray) {
                    if (!dbmanager)dbmanager = [[DBManager alloc]init];
                    [dbmanager insertExtaFmHistory:extra :menuDic];
                }
            }
        //}
    }
    [self.delegate DidReceiveHistory:YES];
}
#pragma mark -
#pragma mark Table Status

-(void)GetTableStatus :(NSDictionary *)params{
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/getTableStatus"];
    
    NSString *string = [NSString stringWithFormat:@"%@getTableStatus", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:[NSDictionary dictionaryWithDictionary:params] success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Table Status %@",responseObject);
        
        NSArray * dataArray = [responseObject objectForKey:@"Data"];
        
        if(!objTableReceived)objTableReceived=[[TableReceivedDao alloc]init];
        
        for (NSDictionary *tblDic in dataArray) {
            [objTableReceived addOrUpdateTableReceivedDetails:tblDic];
        }
        [self.delegate DidfatchTableStatus:YES];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        [self DataLoadFail:string];
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}
-(void)GetMenuItems :(NSDictionary *)params{
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/GetMenuItemsa"];
    
    //    NSString *string = [NSString stringWithFormat:@"%@GetMenuItemsa", SERVICEURL];
    NSString *string = [NSString stringWithFormat:@"%@GetMenuItems", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:[NSDictionary dictionaryWithDictionary:params] success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveMenuItems:responseObject];
        NSLog(@"Download_Items %@",responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
        
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}
-(void)GetLocationInfo{
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/TableLocation"];
    
    NSString *string = [NSString stringWithFormat:@"%@TableLocation", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:@{@"key" :WEBSERVICEKEY} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Locations %@",responseObject);
        
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        
        if ([responseObject isKindOfClass:[NSArray class]]) {
            
            for (NSDictionary * dic in responseObject) {
                NSString * locationid = [dic objectForKey:@"id"];
                NSString * locationName = [dic objectForKey:@"location"];
                NSString * locationdefault = [dic objectForKey:@"default_location"];
                
                [dbmanager insetorupdateLocations:locationid :locationName :locationdefault];
            }
            [self.delegate DidReceiveLocationinfo:YES];
            
        }else{
            
            [self.delegate DidReceiveLocationinfo:NO];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Fail !"
                                                                message:@"Invalid respont"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
        
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self.delegate DidReceiveLocationinfo:NO];
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        //        [alertView show];
        [self DataLoadFail:string];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:@"empty"                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        NSLog(@"URL %@",string);
    }];
    
}
#pragma mark -
#pragma mark Order Sending Process

-(void)SendOrderToServer :(NSDictionary *)params{
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/putWaiterAppOrderTemp"];
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/AddOrderData"];
    
    NSString *string = [NSString stringWithFormat:@"%@AddOrderData", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveSendinfo:responseObject];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        
        [self.delegate DidReceiveSendinfo:@"fail"];
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        //        [alertView show];
        [self DataLoadFail:string];
        
        NSLog(@"URLparams %@",error.description);
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}
-(void)CashPayProcess :(NSDictionary *)params{
    
    
    //    NSLog(@"Printer Details %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/Payment"];
    
    NSString *string = [NSString stringWithFormat:@"%@Payment", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        //        [self.delegate DidReceivePaymentinfo:responseObject];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        
        [self.delegate DidReceivePaymentinfo:@"fail"];
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}
#pragma mark -
#pragma mark contomerDetailsUpdate

-(void)RegisterCustomer :(NSDictionary *)params{
    
    NSLog(@"Add_customer %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/AddCustomer"];
    
    NSString *string = [NSString stringWithFormat:@"%@AddCustomer", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"regcus =%@",responseObject);
        
        [self.delegate DidReceiveRegister:responseObject];
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"action %@",error.description);
        
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
        //                                                             message:@"Connection Lost.Try again."
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}
-(void)GetCustomers :(NSDictionary *)params{
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/CustomerList"];
    
    NSString *string = [NSString stringWithFormat:@"%@CustomerList", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    
    
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:@{@"key" : WEBSERVICEKEY} success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"contacts %@",responseObject);
        
        if (!dbmanager)dbmanager = [[DBManager alloc]init];
        
        for (NSDictionary * dic in responseObject) {
            [dbmanager insetorupdatCustomer:dic];
        }
        
        [self.delegate DidReceiveAllCustomers:YES];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self.delegate DidReceiveAllCustomers:NO];
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Sorry !"
        //                                                             message:@"Payment System Offline"
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}

#pragma mark -
#pragma mark ExtraItems

-(void)GetExtraItemsbyId :(NSDictionary *)params{
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/GetExtraItem"];
    
    NSString *string = [NSString stringWithFormat:@"%@GetExtraItem", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.delegate DidReceiveExtraItems:responseObject];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Sorry !"
        //                                                             message:@"Payment System Offline"
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}
#pragma mark -
#pragma mark PaymentDetails

-(void)GetRestuarentProfile{
    
    NSDictionary * params = @{@"key": WEBSERVICEKEY};
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/GetRestuarentProfile"];
    
    NSString *string = [NSString stringWithFormat:@"%@GetRestuarentProfile", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"resturentP %@",responseObject);
        
        [self.delegate DidReceiveRestuarentProfile:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        //        UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Sorry !"
        //                                                             message:@"Payment System Offline"
        //                                                            delegate:self
        //                                                   cancelButtonTitle:@"OK"
        //                                                   otherButtonTitles:nil, nil];
        //        [alertError show];
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
        [self.delegate DidReceiveRestuarentProfile:nil];
    }];
    
}

-(void)SendPaymentDetails :(NSDictionary *)params{
    
    //    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/TestPayment"];
    
    NSString *string = [NSString stringWithFormat:@"%@TestPayment", SERVICEURL];
    //    NSString *string = [NSString stringWithFormat:@"%@NewPayment", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceivePaymentinfo:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Error %@",error.description);
        [self DataLoadFail:string];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}



#pragma mark -
#pragma mark SplitPay

-(void)SendSplitOrder :(NSDictionary *)params{
    
    //    NSLog(@"Params %@",params);
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/AddSplitOrder"];
    
    NSString *string = [NSString stringWithFormat:@"%@AddSplitOrder", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveSplitinfo:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        NSLog(@"Error %@",error.description);
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
    }];
    
}

-(void)CancelSplitOrder :(NSDictionary *)params{
    
    NSLog(@"Params %@",params);
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/ResetSplitOrder"];
    
    NSString *string = [NSString stringWithFormat:@"%@ResetSplitOrder", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveSplitCancelDetails:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}
-(void)GiftVoucherValidate :(NSDictionary *)params{
    
    //    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/GetGiftVoucher"];
    
    NSString *string = [NSString stringWithFormat:@"%@GetGiftVoucher", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveVoucherDetails:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}
-(void)getGiftvoucherDetails :(NSDictionary *)params{
    
    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/getGiftVocherAmounts"];
    
    NSString *string = [NSString stringWithFormat:@"%@getGiftVocherAmounts", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveGiftVoucherDetails:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}

-(void)CancelOrder :(NSDictionary *)params{
    
    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/cancelOrder"];
    
    
    NSString *string = [NSString stringWithFormat:@"%@cancelOrder", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveCancelOrderDetails:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}
-(void)CancelOrderItem :(NSDictionary *)params{
    
    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/cancelOrderDetail"];
    
    
    NSString *string = [NSString stringWithFormat:@"%@cancelOrderDetail", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveCancelOrderItemDetails:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:params                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}
#pragma mark -
#pragma mark get All items

-(void)getAllItems :(NSDictionary *)params{
    
    //    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/cancelOrderDetail"];
    
    
    NSString *string = [NSString stringWithFormat:@"%@GetMenuItems", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        [self.delegate DidReceiveAllItems:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:nil                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        //        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}


#pragma mark -
#pragma mark Finalize Till Session

-(void)finalizeTillSession :(NSDictionary *)params{
    
    //    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/cancelOrderDetail"];
    
    
    NSString *string = [NSString stringWithFormat:@"%@finalizeTillSession", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Login %@",responseObject);
        
        [self.delegate DidReceiveFinalizeData:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:nil                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        //        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}

#pragma mark -
#pragma mark Update delivery details

-(void)UpdateDeliveryDetails :(NSDictionary *)params{
    
    //    NSLog(@"Params %@",params);
    
    //    NSURL *baseURL = [NSURL URLWithString:@"http://orderfoodonlinelondon.com/resbook_redberry/mobile/cancelOrderDetail"];
    
    
    NSString *string = [NSString stringWithFormat:@"%@EditOrderDeliveryInfo", SERVICEURL];
    NSURL *baseURL = [NSURL URLWithString:string];
    
    
    
    AFHTTPSessionManager * manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:@"" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSLog(@"Login %@",responseObject);
        
        [self.delegate DidReceiveDeliveryUpdateDetails:responseObject];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
        [self DataLoadFail:string];
        
        NSString * logalert = [NSString stringWithFormat:@"error : from %@ Log :%@ ",string,error.description];
        [Flurry logEvent:logalert];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:string                     // Event category (required)
                                                              action:nil                   // Event action (required)
                                                               label:error.description          // Event label
                                                               value:nil] build]];
        
        //        NSLog(@"URL %@ params %@",string,params);
        
    }];
    
}

#pragma mark -
#pragma mark Json Converter
-(NSString *)jsonconverter :(NSDictionary *)dictionary{
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:0
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"[]";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
-(void)DataLoadFail :(NSString *)servicemessage{
    
    NSString * message = [NSString stringWithFormat:@"Connection Lost.Service msg : %@",servicemessage];
    
    UIAlertView *alertError = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
    [alertError show];
}
#pragma mark -
#pragma mark Sync Time
-(void)UpdateSynctime{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *stringFromDate = [dateFormatter1 stringFromDate:[NSDate date]];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:stringFromDate forKey:@"LMD"];
    [userDefaults synchronize];
}
#pragma mark -
#pragma mark Reachability

-(BOOL)ReachabilityPass{
    
    Reachability * networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus != NotReachable) {
        return true;
    }
    return false;
}


@end
